import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {NativeBaseProvider, extendTheme, StatusBar} from 'native-base';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
// import configureStore from './src/redux/configureStore';
import {store} from './src/stores/store';
import {persistStore} from 'redux-persist';
import firebase from '@react-native-firebase/app';
import database from '@react-native-firebase/database';
import firestore from '@react-native-firebase/firestore';
import JailMonkey from 'jail-monkey';
import {LogBox, View, Image} from 'react-native';
import codePush from 'react-native-code-push';
import LottieView from 'lottie-react-native';
import {ProgressBar} from 'react-native-paper';
import Config from 'react-native-config';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import RNBootSplash from 'react-native-bootsplash';

import TextM from './src/components/shareComponents/TextM';

import {colors} from './src/components/shareStyle';

// Ignore log notification by message:
LogBox.ignoreLogs(['Warning: ...']);
// Ignore all log notifications:
LogBox.ignoreAllLogs();

// Component
import Setup from './src/boot/Setup';

// Setup Redux Configure
// const {store, persistor} = configureStore();
let persistor = persistStore(store);

// Init Firebase
const firebaseConfig = {
  apiKey: Config.FIREBASE_API_KEY,
  authDomain: Config.FIREBASE_AUTH_DOMAIN,
  databaseURL: Config.FIREBASE_DATABASE_URL,
  projectId: Config.FIREBASE_PROJECT_ID,
  storageBucket: Config.FIREBASE_STORAGE_BUCKET,
  messagingSenderId: Config.FIREBASE_MESSAGING_SENDER_ID,
  appId: Config.FIREBASE_APP_ID,
  // measurementId: 'G-8GSGZQ44ST',
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
} else {
  firebase.app();
}

GoogleSignin.configure({
  webClientId: Config.GOOGLE_WEB_CLIENT_ID,
});

const theme = extendTheme({
  colors: {
    // Add new color
    primary: {
      50: '#E3F2F9',
      100: '#C5E4F3',
      200: '#A2D4EC',
      300: '#7AC1E4',
      400: '#47A9DA',
      500: '#0088CC',
      600: colors.DarkLightBlue,
      700: '#0dcaA1',
      800: colors.Blue,
      900: '#003F5E',
    },
    // Redefining only one shade, rest of the color will remain same.
    amber: {
      400: '#d97706',
    },
  },
});

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      timePassed: false,
      appUpdateStatus: '',
      receivedBytes: 0,
      totalBytes: 0,
      codepushStatus: true,
    };
  }

  componentDidMount() {
    RNBootSplash.hide({fade: true});
    try {
      firestore()
        .collection('settingConfig')
        .get()
        .then(querySnapshot => {
          var data = {};
          querySnapshot.forEach(doc => {
            data = doc.data();
            // console.log('data new', data);
            this.setState({codepushStatus: data?.codePushCheck});
          });
          // console.log(allData);
          // setPatientList(allData);
        })
        .catch(error => {
          console.error('Error read ', error);
        });
    } catch (error) {
      console.log(error);
    }
  }

  // Function
  codePushStatusDidChange(status) {
    switch (status) {
      case codePush.SyncStatus.CHECKING_FOR_UPDATE:
        console.log('Checking for updates.');
        this.setState({appUpdateStatus: 'ກວດສອບການອັບເດດ...'});
        break;
      case codePush.SyncStatus.DOWNLOADING_PACKAGE:
        console.log('Downloading package.');
        this.setState({appUpdateStatus: 'ກຳລັງດາວໂຫລດການອັບເດດ...'});
        break;
      case codePush.SyncStatus.INSTALLING_UPDATE:
        console.log('Installing update.');
        this.setState({appUpdateStatus: 'ກຳລັງຕິດຕັ້ງການອັບເດດ...'});
        break;
      case codePush.SyncStatus.UP_TO_DATE:
        console.log('Up-to-date.');
        this.setState({appUpdateStatus: 'ການອັບເດດສົມບູນ'});
        this.setState({
          timePassed: true,
        });
        break;
      case codePush.SyncStatus.UPDATE_INSTALLED:
        console.log('Update installed.');
        this.setState({appUpdateStatus: 'ການຕິດຕັ້ງການອັບເດດສຳເລັດ'});
        break;
    }
  }

  codePushDownloadDidProgress(progress) {
    console.log(
      progress.receivedBytes + ' of ' + progress.totalBytes + ' received.',
    );
    this.setState({
      totalBytes: progress.totalBytes,
      receivedBytes: progress.receivedBytes,
    });
  }

  formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  loadPersent() {
    if (this.state.receivedBytes !== 0 && this.state.totalBytes !== 0) {
      let currentB = this.state.totalBytes / this.state.receivedBytes;
      return currentB;
    } else {
      return 0;
    }
  }

  render() {
    // return (
    //   <Provider store={store}>
    //     <PersistGate loading={null} persistor={persistor}>
    //       <NativeBaseProvider theme={theme}>
    //         <NavigationContainer onReady={() => SplashScreen.hide()}>
    //           <Setup />
    //         </NavigationContainer>
    //       </NativeBaseProvider>
    //     </PersistGate>
    //   </Provider>
    // );
    if (!this.state.timePassed && this.state.codepushStatus) {
      return (
        <NativeBaseProvider theme={theme}>
          <StatusBar backgroundColor={colors.White} barStyle={'dark-content'} />
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#ffffff',
            }}>
            <Image
              source={require('./src/assets/img/morzapp-logo-hor-660x180.png')}
              style={{
                height: 60,
                width: 200,
                marginBottom: 20,
              }}
              resizeMode="contain"
            />
            <LottieView
              source={require('./src/assets/animation/doctor-and-health-symbols.json')}
              style={{width: 380}}
              autoPlay
              loop
            />
            <View
              style={{
                height: 8,
                width: '70%',
                borderRadius: 5,
                marginTop: 20,
              }}>
              <ProgressBar
                progress={this.loadPersent()}
                color={colors.Pink}
                style={{height: 8, borderRadius: 5, marginTop: 10}}
              />
            </View>
            <View
              style={{
                marginTop: 20,
                flexDirection: 'row',
              }}>
              <TextM style={{color: colors.Gray}}>
                {this.state.appUpdateStatus !== '' && 'ສະຖານະ: '}
              </TextM>
              <TextM style={{color: colors.LightBlue}}>
                {this.state.appUpdateStatus !== '' &&
                  this.state.appUpdateStatus}
              </TextM>
            </View>
            <TextM>
              {this.state.totalBytes !== 0
                ? 'ກຳລັງໂຫລດຂໍ້ມູນ' +
                  this.formatBytes(this.state.receivedBytes) +
                  ' ຈາກທັງໝົດ ' +
                  this.formatBytes(this.state.totalBytes)
                : ''}
            </TextM>
          </View>
        </NativeBaseProvider>
      );
    } else if (JailMonkey.isJailBroken()) {
      // Alternative behaviour for jail-broken/rooted devices.
      return <></>;
    } else {
      return (
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <NativeBaseProvider theme={theme}>
              <StatusBar
                backgroundColor={colors.White}
                barStyle={'dark-content'}
              />
              <NavigationContainer>
                <Setup />
              </NavigationContainer>
            </NativeBaseProvider>
          </PersistGate>
        </Provider>
      );
    }
  }
}

let codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.IMMEDIATE,
};

export default codePush(codePushOptions)(App);
