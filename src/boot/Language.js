import React, {useEffect, useState} from 'react';
import {View, Text} from 'react-native';

const Language = () => {
  // useState
  const [stateName, setStateName] = useState('');

  // useEffect
  useEffect(() => {
    setStateName('CustomName');
    console.log('useEffect');
    console.log('stateName', stateName);
  });

  return (
    <View>
      <Text>Language</Text>
    </View>
  );
};

export default Language;
