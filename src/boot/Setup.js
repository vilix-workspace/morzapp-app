import React, {useEffect, useState, useRef} from 'react';
import * as eva from '@eva-design/eva';
import {default as theme} from '../config/morzapp-theme.json';
import {ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import {FeatherIconsPack} from '../config/feather-icons';
import {MaterialIconsPack} from '../config/material-icons';
import {FontAwesome5Pack} from '../config/fontawesome5-icons';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import {LogBox, AppState} from 'react-native';
import {StatusBar} from 'native-base';
import TrackPlayer from 'react-native-track-player';
// import Backendless from 'backendless';
// Firebase
import firebase from '@react-native-firebase/app';
// Redux
import {useSelector, useDispatch} from 'react-redux';

// Color
import {Blue, LightBlue} from '../components/shareStyle/colors';

// Components
import IntroSlide from './IntroSlide';
import NetConnection from './NetConnection';
import StackNavigation from '../components/StackNavigation';

import BackgroundService from 'react-native-background-actions';

TrackPlayer.registerPlaybackService(() =>
  require('../config/TrackPlayerService.js'),
);

LogBox.ignoreLogs(['Remote debugger']);

// Paper Theme
const papertheme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: Blue,
    accent: LightBlue,
  },
};

// Background Service
// const sleep = (time) =>
//   new Promise((resolve) => setTimeout(() => resolve(), time));

const veryIntensiveTask = async taskDataArguments => {
  // Example of an infinite loop task
  console.log('taskDataArguments', taskDataArguments);
  await new Promise(async resolve => {
    // for (let i = 0; BackgroundService.isRunning(); i++) {
    //   console.log(i);
    //   // await BackgroundService.updateNotification({
    //   //   taskDesc: 'New ExampleTask description',
    //   // }); // Only Android, iOS will ignore this call
    //   await sleep(delay);
    // }

    console.log('run once');
    // firebase
    //   .app()
    //   .database(
    //     Config.FIREBASE_REALTIME_DB,
    //   )
    //   .ref(`/doctors`)
    //   .on('value', snapshot => {
    //     console.log('User data RT change: ', snapshot.val());
    //     // setRealtimeDoctorStatus(Object.values(snapshot.val()));
    //   });
  });
};

const options = {
  taskName: 'Example',
  taskTitle: 'ExampleTask title',
  taskDesc: 'ExampleTask description',
  taskIcon: {
    name: 'ic_launcher',
    type: 'mipmap',
  },
  color: '#ff00ff',
  linkingURI: 'yourSchemeHere://chat/jane', // See Deep Linking for more info
  // parameters: {
  //   delay: 1000,
  // },
};

const Setup = () => {
  // Redux
  const userData = useSelector(state => state.userData);
  const globalSettingData = useSelector(state => state.globalSetting);
  const dispatch = useDispatch();

  // useState
  const [isCodePush, setIsCodePush] = useState(true);
  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);

  // Function

  // useEffect
  useEffect(() => {
    TrackPlayer.setupPlayer();
    // App State
    const subscriptionAppState = AppState.addEventListener(
      'change',
      async nextAppState => {
        if (
          appState.current.match(/inactive|background/) &&
          nextAppState === 'active'
        ) {
          // console.log('App has come to the foreground!');
        }

        appState.current = nextAppState;
        setAppStateVisible(appState.current);
        console.log('AppState', appState.current);

        if (appState.current === 'background') {
          // runBG();
        } else if (appState.current === 'active') {
        }

        if (userData?.doctorInfo !== null) {
          if (appState.current === 'background') {
            await BackgroundService.start(veryIntensiveTask, options);
            onSetChangeDoctorStatus('away');
          } else {
            onSetChangeDoctorStatus('online');
            await BackgroundService.stop();
          }
        }
      },
    );

    return () => {
      subscriptionAppState.remove();
    };
  }, []);

  // Function Doctor
  const onSetChangeDoctorStatus = status => {
    firebase
      .app()
      .database(Config.FIREBASE_REALTIME_DB)
      .ref(`/doctors/${userData?.doctorInfo?.morzapp_id}`)
      .once('value')
      .then(snapshot => {
        //success callback
        // console.log('User data: read bor ', snapshot.val());
        if (snapshot.val().activeStatus === 'online') {
          firebase
            .app()
            .database(Config.FIREBASE_REALTIME_DB)
            .ref(`/doctors/${userData?.doctorInfo?.morzapp_id}`)
            .update({
              activeStatus: status,
            })
            .then(data => {
              //success callback
            })
            .catch(error => {
              //error callback
              console.log('error ', error);
            });
        } else if (snapshot.val().activeStatus === 'away') {
          firebase
            .app()
            .database(Config.FIREBASE_REALTIME_DB)
            .ref(`/doctors/${userData?.doctorInfo?.morzapp_id}`)
            .update({
              activeStatus: status,
            })
            .then(data => {
              //success callback
            })
            .catch(error => {
              //error callback
              console.log('error ', error);
            });
        }
      })
      .catch(error => {
        //error callback
        console.log('error ', error);
      });
  };
  // End Function Doctor

  return globalSettingData.checkSetting.checkInternet ? (
    <NetConnection />
  ) : globalSettingData.checkSetting.checkIntroSlide ? (
    <IntroSlide />
  ) : (
    <>
      <IconRegistry
        icons={[MaterialIconsPack, FeatherIconsPack, FontAwesome5Pack]}
      />
      <ApplicationProvider {...eva} theme={{...eva.light, ...theme}}>
        <PaperProvider theme={papertheme}>
          <StatusBar
            barStyle={'dark-content'}
            animated={true}
            backgroundColor={'#ffffff'}
          />
          <StackNavigation />
        </PaperProvider>
      </ApplicationProvider>
    </>
  );
};

export default Setup;
