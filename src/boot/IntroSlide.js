import React, {useEffect, useState} from 'react';
import {Image, View, Text} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
// Redux
import {useDispatch} from 'react-redux';
// Redux action
import {set_first_install} from '../stores/features/globalSetting/globalSettingSlice';
import {NativeBaseProvider, Box, StatusBar} from 'native-base';
import {colors} from '../components/shareStyle';

// Component
import Loading from '../components/shareComponents/Loading';
import TextM from '../components/shareComponents/TextM';

const slides = [
  {
    key: '1',
    title: 'ຍິນດີຕ້ອນຮັບເຂົ້າສູ່ແອັບຂອງພວກເຮົາ',
    text: 'ຄົ້ນຫາ ແລະ ປຶກສາປັນຫາສຸຂະພາບຂອງທ່ານ ພ້ອມທັງຕົວຊ່ວຍອື່ນໆໃຫ້ທ່ານໄດ້ໝັ້ນໃຈ',
    image: require('../assets/img/logo-400x400.png'),
  },
  {
    key: '2',
    title: 'ປຶກສາທ່ານໝໍໄດ້ງ່າຍໆ',
    text: 'ພົບກັບທ່ານໝໍຜູ້ຊ່ຽວຊານທີ່ໃຫ້ຄຳປຶກສາສະເພາະດ້ານໃຫ້ແກ່ທ່ານ',
    image: require('../assets/img/doctor.png'),
  },
  {
    key: '3',
    title: 'ສັ່ງຊື້ສິນຄ້າ ແລະ ອາຫານເສີມໄດ້ທີ່ນີ້',
    text: 'ພົບກັບສິນຄ້າສຸຂະພາບຫຼາກຫຼາຍໃຫ້ທ່ານໄດ້ເລືອກ ແລະ ຊື້ໄດ້ທັນທີ',
    image: require('../assets/img/worldwide.png'),
  },
];

const IntroSlide = () => {
  // Redux
  const dispatch = useDispatch();
  // useState
  const [loading, setLoading] = useState(false);

  const _renderItem = ({item}) => {
    return (
      <NativeBaseProvider>
        <StatusBar backgroundColor={colors.White} barStyle={'dark-content'} />
        <Box style={{flex: 1, backgroundColor: colors.White}}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: '80%',
              }}>
              <TextM
                fontWeight="bold"
                style={{
                  color: colors.DarkBlue,
                  textAlign: 'center',
                  fontSize: 26,
                }}>
                {item?.title}
              </TextM>
              <Image
                style={{marginVertical: 40, height: 140, alignSelf: 'center'}}
                resizeMode={'contain'}
                source={item?.image}
              />
              <TextM style={{color: '#333', textAlign: 'center'}}>
                {item?.text}
              </TextM>
            </View>
          </View>
          {loading && <Loading />}
        </Box>
      </NativeBaseProvider>
    );
  };

  const _onDone = async () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      dispatch(set_first_install());
    }, 200);
  };

  // useEffect
  useEffect(() => {});

  return (
    <AppIntroSlider
      data={slides}
      onDone={_onDone}
      renderItem={_renderItem}
      dotStyle={{backgroundColor: '#F5E1DA'}}
      activeDotStyle={{backgroundColor: '#ed3293'}}
      renderNextButton={() => {
        return (
          <TextM fontWeight="bold" style={{color: '#ed3293', marginRight: 10}}>
            ຖັດໄປ
          </TextM>
        );
      }}
      renderDoneButton={() => {
        return (
          <TextM fontWeight="bold" style={{color: '#ed3293', marginRight: 10}}>
            ເຂົ້າສູ່ແອັບ
          </TextM>
        );
      }}
    />
  );
};

export default IntroSlide;
