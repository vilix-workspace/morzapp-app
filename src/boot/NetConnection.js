import React, {useEffect, useState} from 'react';
import {View, Text, SafeAreaView, TouchableOpacity} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import LottieView from 'lottie-react-native';
import FastImage from 'react-native-fast-image';
// Redux
import {useSelector, useDispatch} from 'react-redux';
// Redux action
import {
  set_has_internet,
  set_no_internet,
} from '../stores/features/globalSetting/globalSettingSlice';
import TextM from '../components/shareComponents/TextM';

const NetConnection = () => {
  // Redux
  const globalSettingData = useSelector(state => state.globalSetting);
  const dispatch = useDispatch();
  // useState
  const [stateName, setStateName] = useState('');

  // useEffect
  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
      // console.log('Connection type', state.type);
      // console.log('Is connected?', state.isConnected);
      if (state.isConnected) {
        dispatch(set_has_internet());
      } else {
        dispatch(set_no_internet());
      }
    });
    return () => {
      // unsubscribe;
      // console.log('unmounting...');
    };
  }, []);

  return (
    <View
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#ffffff',
      }}>
      <SafeAreaView>
        <View style={{alignItems: 'center'}}>
          <FastImage
            style={{
              width: 200,
              height: 100,
            }}
            resizeMode={FastImage.resizeMode.contain}
            source={require('../assets/img/morzapp-logo-hor-660x180.png')}
          />
          <TextM>ຂໍອະໄພ ສັນຍານອິນເຕີເນັດຂອງທ່ານຂາດຫາຍ</TextM>
        </View>
        <LottieView
          source={require('../assets/animation/no-network.json')}
          style={{width: 300}}
          autoPlay
          loop
        />
        <View style={{alignItems: 'center'}}>
          <TextM>ກະລຸນາເຊື່ອມຕໍ່ອິນເຕີເນັດ</TextM>
        </View>
      </SafeAreaView>
    </View>
  );
};

export default NetConnection;
