import React, {useEffect, useState} from 'react';
import {View, Text} from 'react-native';

const CodePush = () => {
  // useState
  const [stateName, setStateName] = useState('');

  // useEffect
  useEffect(() => {
    setStateName('CustomName');
    console.log('useEffect');
    console.log('stateName', stateName);
  });

  return (
    <View>
      <Text>CodePush</Text>
    </View>
  );
};

export default CodePush;
