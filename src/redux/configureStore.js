import {createStore, combineReducers} from 'redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {persistStore, persistReducer} from 'redux-persist';

// Reducer
import userDataReducer from './reducers/userData';
import cartDataReducer from './reducers/cart';
import globalSettingDataReducer from './reducers/globalSetting';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const rootReducer = combineReducers({
  userData: userDataReducer,
  cartData: cartDataReducer,
  globalSettingData: globalSettingDataReducer,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

const configureStore = () => {
  // return createStore(rootReducer);
  let store = createStore(persistedReducer);
  let persistor = persistStore(store);
  return {store, persistor};
};

export default configureStore;
