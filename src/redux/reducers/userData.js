import {
  ADD_USERDATA,
  ADD_DOCTORDATA,
  ADD_HOSPITALDATA,
  SET_SKIP,
  SET_TOKEN,
  LOG_OUT,
  LOG_OUT_DOCTOR,
  LOG_OUT_HOSPITAL,
  RESET_ALL,
} from '../actionTypes';

const initialState = {
  userInfo: null,
  doctorInfo: null,
  hospitalInfo: null,
  isSkip: false,
  userToken: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_USERDATA:
      return {
        ...state,
        userInfo: action.payload,
      };
    case ADD_DOCTORDATA:
      return {
        ...state,
        doctorInfo: action.payload,
      };
    case ADD_HOSPITALDATA:
      return {
        ...state,
        hospitalInfo: action.payload,
      };
    case SET_SKIP:
      return {
        ...state,
        isSkip: true,
      };
    case SET_TOKEN:
      return {
        ...state,
        userToken: action.payload,
      };
    case LOG_OUT:
      return {
        ...state,
        userInfo: null,
        isSkip: true,
      };
    case LOG_OUT_DOCTOR:
      return {
        ...state,
        doctorInfo: null,
      };
    case LOG_OUT_HOSPITAL:
      return {
        ...state,
        hospitalInfo: null,
      };
    case RESET_ALL:
      return {
        userInfo: null,
        doctorInfo: null,
        hospitalInfo: null,
        isSkip: false,
        userToken: null,
      };
    default:
      return state;
  }
};

export default reducer;
