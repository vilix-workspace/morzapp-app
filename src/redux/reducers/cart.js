import {ADD_TO_CART, DELETE_ITEM, CLEAR_CART} from '../actionTypes';

const initialState = {
  cartInfo: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      return {
        ...state,
        cartInfo: [...state.cartInfo, action.payload],
      };
    case DELETE_ITEM:
      // return state.cartInfo
      //   .slice(0, action.index)
      //   .concat(state.cartInfo.slice(action.index + 1));
      return {
        ...state,
        cartInfo: state.cartInfo.filter(
          (item, index) => index !== action.payload,
        ),
      };
    case CLEAR_CART:
      return {
        cartInfo: [],
      };
    default:
      return state;
  }
};

export default reducer;
