import {
  SET_HAS_INTERNET,
  SET_NO_INTERNET,
  SET_FIRST_INSTALL,
  SET_CODEPUSH,
} from '../actionTypes';

const initialState = {
  checkSetting: {
    checkInternet: true,
    checkIntroSlide: true,
    checkCodePush: true,
  },
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_HAS_INTERNET:
      return {
        ...state,
        ...(state.checkSetting.checkInternet = false),
      };
    case SET_NO_INTERNET:
      return {
        ...state,
        ...(state.checkSetting.checkInternet = true),
      };
    case SET_FIRST_INSTALL:
      return {
        ...state,
        ...(state.checkSetting.checkIntroSlide = false),
      };
    case SET_CODEPUSH:
      return {
        ...state,
        ...(state.checkSetting.checkCodePush = false),
      };
    default:
      return state;
  }
};

export default reducer;
