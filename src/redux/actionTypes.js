// UserData
export const ADD_USERDATA = 'ADD_USERDATA';
export const ADD_DOCTORDATA = 'ADD_DOCTORDATA';
export const ADD_HOSPITALDATA = 'ADD_HOSPITALDATA';
export const SET_SKIP = 'SET_SKIP';
export const SET_TOKEN = 'SET_TOKEN';
export const LOG_OUT = 'LOG_OUT';
export const LOG_OUT_DOCTOR = 'LOG_OUT_DOCTOR';
export const LOG_OUT_HOSPITAL = 'LOG_OUT_HOSPITAL';
export const RESET_ALL = 'RESET_ALL';

// Cart
export const ADD_TO_CART = 'ADD_TO_CART';
export const DELETE_ITEM = 'DELETE_ITEM';
export const CLEAR_CART = 'CLEAR_CART';

// Global Setting
export const SET_HAS_INTERNET = 'SET_HAS_INTERNET';
export const SET_NO_INTERNET = 'SET_NO_INTERNET';
export const SET_FIRST_INSTALL = 'SET_FIRST_INSTALL';
export const SET_CODEPUSH = 'SET_FIRST_INSTALL';
