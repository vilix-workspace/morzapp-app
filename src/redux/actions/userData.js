import {
  ADD_USERDATA,
  ADD_DOCTORDATA,
  ADD_HOSPITALDATA,
  SET_SKIP,
  SET_TOKEN,
  LOG_OUT,
  LOG_OUT_DOCTOR,
  LOG_OUT_HOSPITAL,
  RESET_ALL,
} from '../actionTypes';

export const addUserData = (data) => {
  return {
    type: ADD_USERDATA,
    payload: data,
  };
};

export const addDoctorData = (data) => {
  return {
    type: ADD_DOCTORDATA,
    payload: data,
  };
};

export const addHospitalData = (data) => {
  return {
    type: ADD_HOSPITALDATA,
    payload: data,
  };
};

export const setSkip = () => {
  return {
    type: SET_SKIP,
  };
};

export const setToken = (data) => {
  return {
    type: SET_TOKEN,
    payload: data,
  };
};

export const logOut = () => {
  return {
    type: LOG_OUT,
  };
};

export const logOutDoctor = () => {
  return {
    type: LOG_OUT_DOCTOR,
  };
};

export const logOutHospital = () => {
  return {
    type: LOG_OUT_HOSPITAL,
  };
};

export const resetAll = () => {
  return {
    type: RESET_ALL,
  };
};
