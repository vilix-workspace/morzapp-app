import {
  SET_HAS_INTERNET,
  SET_NO_INTERNET,
  SET_FIRST_INSTALL,
  SET_CODEPUSH,
} from '../actionTypes';

export const set_has_internet = (data) => {
  return {
    type: SET_HAS_INTERNET,
  };
};

export const set_no_internet = (data) => {
  return {
    type: SET_NO_INTERNET,
  };
};

export const set_first_install = (data) => {
  return {
    type: SET_FIRST_INSTALL,
  };
};

export const set_codepush = (data) => {
  return {
    type: SET_CODEPUSH,
  };
};
