import {ADD_TO_CART, DELETE_ITEM, CLEAR_CART} from '../actionTypes';

export const addToCart = (data) => {
  return {
    type: ADD_TO_CART,
    payload: data,
  };
};

export const deleteItem = (data) => {
  return {
    type: DELETE_ITEM,
    payload: data,
  };
};

export const clearCart = (data) => {
  return {
    type: CLEAR_CART,
  };
};
