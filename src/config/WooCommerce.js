import WooCommerceAPI from 'react-native-woocommerce-api';
import Config from 'react-native-config';

const WooCommerce = new WooCommerceAPI({
  url: Config.WEB_URL,
  consumerKey: Config.CONSUMER_KEY,
  consumerSecret: Config.CONSUMER_SECRET,
  version: 'wc/v3',
  ssl: true,
  wpAPI: true, // Enable the WP REST API integration
  queryStringAuth: true,
});

export default WooCommerce;
