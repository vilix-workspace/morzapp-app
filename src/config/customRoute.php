// Custom Function REST API

//Home Banner
function get_all_home_banner_list( $data, $post, $context ) {
	return [
        'id'    => $data->data['id'],
        'name'    => $data->data['acf']['banner_name'],
        'img'    => $data->data['acf']['banner_image']['url'],
        'status'    => $data->data['acf']['status'],
	];
}
add_filter( 'rest_prepare_home_banner', 'get_all_home_banner_list', 10, 3 );

//Shop Banner
function get_all_shop_banner_list( $data, $post, $context ) {
	return [
        'id'    => $data->data['id'],
        'name'    => $data->data['acf']['banner_name'],
        'img'    => $data->data['acf']['banner_image']['url'],
        'status'    => $data->data['acf']['status'],
	];
}
add_filter( 'rest_prepare_shop_banner', 'get_all_shop_banner_list', 10, 3 );

//Doctor List
function get_all_doctor_list( $data, $post, $context ) {
	return [
        'id'    => $data->data['id'],
        'morthip_id'    => $data->data['acf']['morthip_id'],
        'fullname'    => $data->data['acf']['doctor_full_name'],
        'picture'    => $data->data['acf']['picture']['url'],
        'institute'    => $data->data['acf']['institute'],
        'specialist'    => $data->data['acf']['doctor_specialist'],
        'description'    => $data->data['acf']['description'],
        'language'    => $data->data['acf']['language'],
        'location'    => $data->data['acf']['location'],
        'active_status'    => $data->data['acf']['active_status'],
	];
}
add_filter( 'rest_prepare_doctorlist', 'get_all_doctor_list', 10, 3 );

//Doctor Specialist Taxonomy
function get_all_doctor_specialist( $data, $post, $context ) {
	return [
        'id'    => $data->data['id'],
        'name'    => $data->data['name'],
        'description'    => $data->data['description'],
        'image'    => $data->data['acf']['image']['url'],
	];
}
add_filter( 'rest_prepare_doctor_specialist', 'get_all_doctor_specialist', 10, 3 );

//Customer Check
function get_all_customer_check( $data, $post, $context ) {
	return [
        'check'    => $data->data['title']['rendered'],
        'valid'    => $data->data['acf']['phone'], 
	];
}
add_filter( 'rest_prepare_customer_check', 'get_all_customer_check', 10, 3);
           
//Public Key
function get_all_public_key( $data, $post, $context ) {
	return [
        'mc'    => $data->data['acf']['client_key'], 
        'mcc'    => $data->data['acf']['secret_key'], 
	];
}
add_filter( 'rest_prepare_public_key', 'get_all_public_key', 10, 3 );

//News & Tips
function get_all_news_tips( $data, $post, $context ) {
	return [
        'id'    => $data->data['id'],
        'title'    => $data->data['acf']['topic'], 
        'image'    => $data->data['acf']['image']['url'], 
        'content'    => $data->data['acf']['content'], 
        'content_render'    => $data->data['acf']['content_render'], 
        'feature_post'    => $data->data['acf']['feature_post'], 
	];
}
add_filter( 'rest_prepare_news_tips', 'get_all_news_tips', 10, 3 );



