import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  userInfo: null,
  doctorInfo: null,
  hospitalInfo: null,
  isSkip: false,
  userToken: null,
};

export const userDataSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setSkip: (state, action) => {
      state.isSkip = true;
    },
    setToken: (state, action) => {
      state.userToken = action.payload;
    },
    addUserData: (state, action) => {
      state.userInfo = action.payload;
    },
    addDoctorData: (state, action) => {
      state.doctorInfo = action.payload;
    },
    addHospitalData: (state, action) => {
      state.hospitalInfo = action.payload;
    },
    logOut: (state, action) => {
      state.userInfo = null;
      state.isSkip = true;
    },
    logOutDoctor: (state, action) => {
      state.doctorInfo = null;
    },
    logOutHospital: (state, action) => {
      state.hospitalInfo = null;
    },
    resetAll: (state, action) => {
      state.userInfo = null;
      state.doctorInfo = null;
      state.hospitalInfo = null;
      state.isSkip = false;
      state.userToken = null;
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  setSkip,
  setToken,
  addUserData,
  addDoctorData,
  addHospitalData,
  logOut,
  logOutDoctor,
  logOutHospital,
  resetAll,
} = userDataSlice.actions;

export default userDataSlice.reducer;
