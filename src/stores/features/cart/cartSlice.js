import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  cartInfo: [],
  shopInfo: null,
};

export const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addToCart: (state, action) => {
      // state.cartInfo = [...state.cartInfo, action.payload];
      const itemIndex = state.cartInfo.findIndex(
        item => item.id === action.payload.id,
      );
      if (itemIndex >= 0) {
        // Item already exists in the cart, update its quantity
        state.cartInfo[itemIndex].quantity += action.payload.quantity;
      } else {
        // Item doesn't exist in the cart, add it
        state.cartInfo = [...state.cartInfo, action.payload];
      }
    },
    increaseQuantity: (state, action) => {
      const itemIndex = state.cartInfo.findIndex(
        item => item.id === action.payload.id,
      );
      state.cartInfo[itemIndex].quantity++;
    },
    decreaseQuantity: (state, action) => {
      const itemIndex = state.cartInfo.findIndex(
        item => item.id === action.payload.id,
      );
      if (state.cartInfo[itemIndex].quantity > 1) {
        state.cartInfo[itemIndex].quantity--;
      } else {
        state.cartInfo.splice(itemIndex, 1);
      }
    },
    deleteItem: (state, action) => {
      state.cartInfo.splice(action.payload, 1);
    },
    clearCart: (state, action) => {
      state.cartInfo = [];
      state.shopInfo = null;
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  addToCart,
  deleteItem,
  clearCart,
  increaseQuantity,
  decreaseQuantity,
} = cartSlice.actions;

export default cartSlice.reducer;
