import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  checkSetting: {
    checkInternet: true,
    checkIntroSlide: true,
    checkCodePush: true,
  },
};

export const globalSettingSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    set_has_internet: (state, action) => {
      state.checkSetting.checkInternet = false;
    },
    set_no_internet: (state, action) => {
      state.checkSetting.checkInternet = true;
    },
    set_first_install: (state, action) => {
      state.checkSetting.checkIntroSlide = false;
    },
    set_codepush: (state, action) => {
      state.checkSetting.checkCodePush = false;
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  set_has_internet,
  set_no_internet,
  set_first_install,
  set_codepush,
} = globalSettingSlice.actions;

export default globalSettingSlice.reducer;
