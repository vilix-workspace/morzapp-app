import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  value: 0,
};

export const countSlice = createSlice({
  name: 'count',
  initialState,
  reducers: {
    plus: state => {
      state.value += 1;
    },
    minus: state => {
      state.value -= 1;
    },
    incrementByAmount: (state, action) => {
      state.value += action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const {plus, minus, incrementByAmount} = countSlice.actions;

export default countSlice.reducer;
