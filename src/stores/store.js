import {configureStore} from '@reduxjs/toolkit';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {combineReducers} from 'redux';
import {persistReducer} from 'redux-persist';
import thunk from 'redux-thunk';

import cartReducer from './features/cart/cartSlice';
import globalSettingReducer from './features/globalSetting/globalSettingSlice';
import userDataReducer from './features/userData/userDataSlice';
import countReducer from './features/count/countSlice';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const reducers = combineReducers({
  count: countReducer,
  globalSetting: globalSettingReducer,
  cart: cartReducer,
  userData: userDataReducer,
});

const persistedReducer = persistReducer(persistConfig, reducers);

export const store = configureStore({
  reducer: persistedReducer,
  devTools: process.env.NODE_ENV !== 'production',
  middleware: [thunk],
});
