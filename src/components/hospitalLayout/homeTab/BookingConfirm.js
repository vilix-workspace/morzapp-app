import React, {useEffect, useState, useCallback} from 'react';
import {StyleSheet, TouchableOpacity, SafeAreaView} from 'react-native';
import {
  Box,
  ScrollView,
  View,
  Text,
  Button,
  HStack,
  VStack,
  Center,
} from 'native-base';
import {Modal, Toggle} from '@ui-kitten/components';
import axios from 'axios';
import moment from 'moment';
import Config from 'react-native-config';
import firebase from '@react-native-firebase/app';
import {useFocusEffect} from '@react-navigation/native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
//Redux package
import {useSelector} from 'react-redux';

// Component
import Loading from '../../shareComponents/Loading';
import TextM from '../../shareComponents/TextM';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';

const BookingConfirm = ({navigation}) => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  // useState
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = useState(false);
  const [realTimeInfo, setRealTimeInfo] = useState(null);
  const [bookingConfirmList, setBookingConfirmList] = useState([]);
  const [selectBooking, setSelectBooking] = useState({});

  const user = userData.hospitalInfo;

  useFocusEffect(
    useCallback(() => {
      // getBookingList();
      // Handler real time status
      const onValueChange = firebase
        .app()
        .database(Config.FIREBASE_REALTIME_DB)
        .ref(`/hospitals/${user?.hospital_clinic_id}`)
        .on('value', snapshot => {
          let data = snapshot.val();
          // console.log('RT change nn: ', data);
          setRealTimeInfo(data);
        });

      return () => {
        firebase
          .database()
          .ref(`/hospitals/${user?.hospital_clinic_id}`)
          .off('value', onValueChange);
      };
    }, []),
  );

  // useEffect
  useEffect(() => {
    console.log('useEffect BookingConfirm', user);

    getBookingConfirmList();
  }, []);

  // Functions
  const numberWithCommas = x => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  const onCheckedChange = isChecked => {
    setLoading(true);
    firebase
      .app()
      .database(Config.FIREBASE_REALTIME_DB)
      .ref(`/hospitals/${user?.hospital_clinic_id}`)
      .update({
        activeStatus:
          realTimeInfo?.activeStatus === 'online' ? 'offline' : 'online',
      })
      .then(data => {
        //success callback
        setLoading(false);
      })
      .catch(error => {
        //error callback
        console.log('error ', error);
      });
  };

  const getBookingConfirmList = () => {
    try {
      axios({
        method: 'get',
        url:
          Config.WP_URL +
          '/wp/v2/mzbooking/?hospital_id=' +
          user?.hospital_clinic_id +
          '&status=confirmed',
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(res => {
          console.log(res);
          setBookingConfirmList(res.data);
          setLoading(false);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <SafeAreaView>
        <Box style={{...defaultStyle.fixedLayout}}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
            <TextM>
              ສະຖານະ:{' '}
              <TextM
                style={{
                  color:
                    realTimeInfo?.activeStatus === 'online'
                      ? colors.Green
                      : colors.Red,
                }}>
                {realTimeInfo?.activeStatus === 'online' ? 'ອອນລາຍ' : 'ປິດ'}
              </TextM>
            </TextM>
            <Toggle
              style={{marginLeft: 10}}
              status={
                realTimeInfo?.activeStatus === 'online' ? 'success' : 'danger'
              }
              checked={realTimeInfo?.activeStatus === 'online' ? true : false}
              onChange={onCheckedChange}></Toggle>
            <FontAwesome5
              name="circle"
              solid
              style={{
                fontSize: 18,
                marginLeft: 10,
                color:
                  realTimeInfo?.activeStatus === 'online'
                    ? colors.Green
                    : colors.Red,
              }}
            />
          </View>
          <View style={{marginTop: 10}}>
            <TextM style={{color: colors.LightBlack, fontSize: 14}}>
              ຍິນດີຕ້ອນຮັບ
            </TextM>
            <TextM style={{color: colors.Pink, fontSize: 22}}>
              {user?.hospitalclinic_name}
            </TextM>
          </View>
        </Box>
      </SafeAreaView>
      <Box style={{...defaultStyle.fixedLayout}}>
        <View style={{marginTop: 10}}>
          <TextM style={{color: colors.Blue, fontSize: 20}}>
            ໂຮງໝໍ ມະໂຫສົດ
          </TextM>
        </View>

        {/* List of Customer */}
        <View
          style={{
            marginTop: 15,
          }}>
          <TextM>
            <FontAwesome5
              name="clipboard-check"
              style={{fontSize: 18, color: colors.DarkBlue}}
            />{' '}
            ລາຍຊື່ຄົນຢືນຢັນລໍຖ້າການພົບໝໍ
          </TextM>
          <VStack>
            <Box thumbnail style={{marginLeft: 0}}>
              <Box>
                <FastImage source={require('../../../assets/img/user.png')} />
              </Box>
              <Box>
                <TextM>Kumar Lopes</TextM>
                <TextM note>ເວລາ: 3:43 pm</TextM>
              </Box>
              <Box>
                <TouchableOpacity
                  onPress={() => {
                    setVisible(true);
                  }}>
                  <TextM style={{fontSize: 14, color: colors.DarkBlue}}>
                    ເບີ່ງຂໍ້ມູນ
                  </TextM>
                </TouchableOpacity>
              </Box>
            </Box>
          </VStack>
        </View>
      </Box>

      {/* Modal */}
      <Modal visible={visible}>
        <View
          style={{
            backgroundColor: '#f6f6f6',
            padding: 15,
            borderRadius: 10,
            width: 300,

            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5,
          }}>
          <FastImage
            style={{alignSelf: 'center', marginBottom: 10}}
            large
            source={require('../../../assets/img/user.png')}
          />
          <TextM>
            <TextM fontWeight="bold" style={{color: colors.DarkBlue}}>
              ຊື່:{' '}
            </TextM>{' '}
            Bounyalith Chanlasnechone
          </TextM>
          <TextM>
            <TextM fontWeight="bold" style={{color: colors.DarkBlue}}>
              ວັນເດືອນປີເກີດ:
            </TextM>{' '}
            October 3, 1992
          </TextM>
          <TextM>
            <TextM fontWeight="bold" style={{color: colors.DarkBlue}}>
              ເພດ:
            </TextM>{' '}
            ຊາຍ
          </TextM>
          <TouchableOpacity
            style={{
              alignSelf: 'flex-end',
            }}
            onPress={() => setVisible(false)}>
            <TextM fontWeight="bold" style={{color: colors.Red}}>
              ປິດ
            </TextM>
          </TouchableOpacity>
        </View>
      </Modal>

      {loading && <Loading />}
    </Box>
  );
};

export default BookingConfirm;

const styles = StyleSheet.create({});
