import React, {useEffect, useState, useCallback} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  FlatList,
  RefreshControl,
  Keyboard,
} from 'react-native';
import {
  Box,
  ScrollView,
  View,
  Text,
  Button,
  HStack,
  VStack,
  Center,
  TextArea,
} from 'native-base';
import {Modal, Toggle} from '@ui-kitten/components';
import axios from 'axios';
import FastImage from 'react-native-fast-image';
import moment from 'moment';
import LottieView from 'lottie-react-native';
import firebase from '@react-native-firebase/app';
import {useFocusEffect} from '@react-navigation/native';
import Config from 'react-native-config';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
//Redux package
import {useSelector} from 'react-redux';

// Component
import Loading from '../../shareComponents/Loading';
import TextM from '../../shareComponents/TextM';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';

const HomeHospital = ({navigation}) => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  // useState
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = useState(false);
  const [visibleCancel, setVisibleCancel] = useState(false);
  const [bookingList, setBookingList] = useState([]);
  const [realTimeInfo, setRealTimeInfo] = useState(null);
  const [selectBooking, setSelectBooking] = useState({});
  const [refreshing, setRefreshing] = useState(false);
  const [cancelReason, setCancelReason] = useState('');

  const user = userData.hospitalInfo;

  useFocusEffect(
    useCallback(() => {
      // getBookingList();
      // Handler real time status
      const onValueChange = firebase
        .app()
        .database(Config.FIREBASE_REALTIME_DB)
        .ref(`/hospitals/${user?.hospital_clinic_id}`)
        .on('value', snapshot => {
          let data = snapshot.val();
          // console.log('RT change nn: ', data);
          setRealTimeInfo(data);
        });

      return () => {
        getBookingList();
        firebase
          .database()
          .ref(`/hospitals/${user?.hospital_clinic_id}`)
          .off('value', onValueChange);
      };
    }, []),
  );

  // useEffect
  useEffect(() => {
    // console.log('useEffect HomeHospital', userData);
    getBookingList();

    // Handler real time status
    const onValueChange = firebase
      .app()
      .database(Config.FIREBASE_REALTIME_DB)
      .ref(`/hospitals/${user?.hospital_clinic_id}`)
      .on('value', snapshot => {
        let data = snapshot.val();
        // console.log('RT change nn: ', data);
        setRealTimeInfo(data);
      });
    // Unmounted
    return () => {
      // console.log('unmounted');
      firebase
        .database()
        .ref(`/hospitals/${user?.hospital_clinic_id}`)
        .off('value', onValueChange);
    };
  }, []);

  // Functions
  const onRefresh = () => {
    setRefreshing(true);
    try {
      getBookingList();
      setRefreshing(false);
    } catch (error) {
      console.log(error);
    }
  };

  const numberWithCommas = x => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  const onCheckedChange = () => {
    setLoading(true);
    firebase
      .app()
      .database(Config.FIREBASE_REALTIME_DB)
      .ref(`/hospitals/${user?.hospital_clinic_id}`)
      .update({
        activeStatus:
          realTimeInfo?.activeStatus === 'online' ? 'offline' : 'online',
      })
      .then(data => {
        //success callback
        setLoading(false);
      })
      .catch(error => {
        //error callback
        console.log('error ', error);
      });
  };

  const getBookingList = () => {
    try {
      axios({
        method: 'get',
        url:
          Config.WP_URL +
          '/wp/v2/mzbooking/?hospital_id=' +
          user?.hospital_clinic_id +
          '&status=pending',
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(res => {
          // console.log(res);
          setBookingList(res.data);
          setLoading(false);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const checkCancelled = status => {
    if (status === 'cancelled') {
      setVisibleCancel(true);
    }
  };

  const onUpdateStatus = status => {
    setVisible(false);
    setLoading(true);
    try {
      axios({
        method: 'post',
        url:
          Config.WP_URL +
          '/wp/v2/mzbooking/' +
          selectBooking?.id +
          '?status=' +
          status,
        data: {
          fields: {
            remark_reason: cancelReason,
          },
        },
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(res => {
          // console.log(res);
          setSelectBooking({});
          if (status === 'confirmed') {
            navigation.navigate('BookingConfirm');
          } else {
            getBookingList();
            setCancelReason('');
          }
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  // Render Item Flatlist
  const renderItem = ({item, index}) => (
    <HStack
      alignItems={'center'}
      justifyContent="space-between"
      space={2}
      style={{
        paddingHorizontal: 12,
        paddingVertical: 5,
        marginBottom: 10,
        borderWidth: 1,
        borderColor: colors.Light,
        marginHorizontal: 20,
        borderRadius: 10,
      }}>
      <Box>
        <TextM style={{fontSize: 24}}>{index + 1}.</TextM>
      </Box>
      <VStack w={'70%'}>
        <TextM fontWeight="bold" style={{color: colors.DarkBlue}}>
          ທ່ານ {item?.patient_name}
        </TextM>

        <HStack alignItems={'center'} space={1}>
          <MaterialCommunityIcons
            name={'clipboard-text-clock'}
            style={{
              fontSize: 20,
              color:
                item?.callType === 'voice' ? colors.DarkLightBlue : colors.Blue,
            }}
          />
          <TextM>
            ວັນນັດພົບ: {moment(item?.booking_time).format('DD MMM YYYY')}
          </TextM>
        </HStack>
        <HStack alignItems={'center'} space={1}>
          <MaterialCommunityIcons
            name={'av-timer'}
            style={{
              fontSize: 20,
              color:
                item?.callType === 'voice' ? colors.DarkLightBlue : colors.Blue,
            }}
          />
          <TextM>ເວລາ: {moment(item?.booking_time).format('h:mm a')}</TextM>
        </HStack>

        <TextM>
          ສະຖານະ:{' '}
          <TextM style={{color: colors.Orange}}>
            {item?.status === 'pending' && 'ລໍຖ້າການຢືນຢັນ'}
          </TextM>
        </TextM>
      </VStack>
      <Box>
        <TouchableOpacity
          onPress={() => {
            setVisible(true);
            setSelectBooking(item);
          }}>
          <TextM
            style={{
              fontSize: 14,
              color: colors.DarkBlue,
              textDecorationLine: 'underline',
            }}>
            ເບີ່ງຂໍ້ມູນ
          </TextM>
        </TouchableOpacity>
      </Box>
    </HStack>
  );

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <SafeAreaView>
        <Box style={{...defaultStyle.fixedLayout}}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
            <TextM>
              ສະຖານະ:{' '}
              <TextM
                style={{
                  color:
                    realTimeInfo?.activeStatus === 'online'
                      ? colors.Green
                      : colors.Red,
                }}>
                {realTimeInfo?.activeStatus === 'online' ? 'ອອນລາຍ' : 'ປິດ'}
              </TextM>
            </TextM>
            <Toggle
              style={{marginLeft: 10}}
              status={
                realTimeInfo?.activeStatus === 'online' ? 'success' : 'danger'
              }
              checked={realTimeInfo?.activeStatus === 'online' ? true : false}
              onChange={onCheckedChange}></Toggle>
            <FontAwesome5
              name="circle"
              solid
              style={{
                fontSize: 18,
                marginLeft: 10,
                color:
                  realTimeInfo?.activeStatus === 'online'
                    ? colors.Green
                    : colors.Red,
              }}
            />
          </View>
          <View style={{marginTop: 10}}>
            <TextM style={{color: colors.LightBlack, fontSize: 14}}>
              ຍິນດີຕ້ອນຮັບ
            </TextM>
            <TextM style={{color: colors.Pink, fontSize: 22}}>
              {user?.hospitalclinic_name}
            </TextM>
          </View>
        </Box>
      </SafeAreaView>

      {/* List of Customer */}
      <HStack
        space={2}
        alignItems={'center'}
        style={{paddingVertical: 5, paddingHorizontal: 20}}>
        <FontAwesome5
          name="stream"
          style={{fontSize: 18, color: colors.DarkBlue}}
        />
        <TextM fontWeight="bold">
          ລາຍຊື່ຄົນຂໍນັດພົບໝໍ (ຄົນເຈັບຂໍນັດພົບໃໝ່)
        </TextM>
      </HStack>
      {bookingList.length > 0 ? (
        <FlatList
          vertical
          data={bookingList}
          renderItem={renderItem}
          keyExtractor={item => item.id}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        />
      ) : (
        <Box style={{alignItems: 'center'}}>
          <Box style={{height: 180, width: 200, marginTop: -80}}>
            <LottieView
              source={require('../../../assets/animation/no-result-found.json')}
              // style={{width: 300}}
              autoPlay
              loop
              resizeMode="cover"
            />
          </Box>
          <TextM>ບໍ່ພົບຂໍ້ມູນຂອງທ່ານ</TextM>
        </Box>
      )}
      {/* Modal */}
      <Modal visible={visible}>
        <View
          style={{
            backgroundColor: '#f6f6f6',
            padding: 15,
            borderRadius: 10,
            width: 300,

            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5,
          }}>
          <TouchableOpacity
            onPress={() => {
              setVisible(false);
              setSelectBooking({});
            }}
            style={{position: 'absolute', top: -10, right: -10}}>
            <MaterialCommunityIcons
              name="close-circle"
              style={{fontSize: 34, color: colors.Red}}
            />
          </TouchableOpacity>
          <Center>
            <TextM fontWeight="bold" style>
              ຂໍ້ມູນຄົນເຈັບ
            </TextM>
          </Center>

          <VStack mt={1} space={1}>
            {/* Patient Name */}
            <HStack alignItems={'center'} space={1}>
              <MaterialCommunityIcons
                name="badge-account"
                style={{fontSize: 18, color: colors.DarkBlue}}
              />
              <TextM>ຊື່:</TextM>
              <TextM style={{color: colors.DarkLightBlue, flex: 1}}>
                ທ່ານ {selectBooking?.patient_name}
              </TextM>
            </HStack>
            {/* Age */}
            <HStack alignItems={'center'} space={1}>
              <MaterialCommunityIcons
                name="cake"
                style={{fontSize: 18, color: colors.DarkBlue}}
              />
              <TextM>ອາຍຸ:</TextM>
              <TextM style={{color: colors.DarkLightBlue, flex: 1}}>
                {selectBooking?.age} ປີ
              </TextM>
            </HStack>
            {/* Gender */}
            <HStack alignItems={'center'} space={1}>
              <MaterialCommunityIcons
                name="gender-male-female"
                style={{fontSize: 18, color: colors.DarkBlue}}
              />
              <TextM>ເພດ:</TextM>
              <TextM style={{color: colors.DarkLightBlue, flex: 1}}>
                {selectBooking?.gender === 'male' ? 'ຊາຍ' : 'ຍິງ'}
              </TextM>
            </HStack>
            {/* Reason */}
            <HStack space={1}>
              <MaterialCommunityIcons
                name="page-next-outline"
                style={{fontSize: 18, color: colors.DarkBlue, marginTop: 4}}
              />
              <VStack>
                <TextM>ສາເຫດທີ່ຢາກພົບໝໍ:</TextM>
                <TextM style={{color: colors.DarkLightBlue, flex: 1}}>
                  {selectBooking?.reason}
                </TextM>
              </VStack>
            </HStack>
            {/* Symptom */}
            <HStack space={1}>
              <MaterialCommunityIcons
                name="text-box-search"
                style={{fontSize: 18, color: colors.DarkBlue}}
              />
              <VStack>
                <TextM>ອາການເບື້ອງຕົ້ນ:</TextM>
                <TextM style={{color: colors.DarkLightBlue, flex: 1}}>
                  {selectBooking?.symptom}
                </TextM>
              </VStack>
            </HStack>
            {/* Booking Date/Time */}
            <TextM note>
              <MaterialCommunityIcons
                name="calendar-range"
                style={{fontSize: 16, color: colors.DarkBlue}}
              />{' '}
              ວັນທີນັດພົບ:
            </TextM>
            <Center>
              <TextM
                fontWeight="bold"
                style={{
                  color: colors.DarkBlue,
                  fontSize: 22,
                }}>
                {moment(selectBooking?.booking_time).format(
                  'ວັນທີ DD MMMM YYYY',
                )}
              </TextM>
              <TextM
                fontWeight="bold"
                style={{
                  color: colors.DarkBlue,
                  fontSize: 22,
                }}>
                {moment(selectBooking?.booking_time).format('ເວລາ: h:mm a')}
              </TextM>
            </Center>
          </VStack>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              marginTop: 20,
            }}>
            <Button
              colorScheme={'error'}
              onPress={() => checkCancelled('cancelled')}>
              <TextM style={{color: colors.White}}>ປະຕິເສດການຂໍພົບ</TextM>
            </Button>
            <Button
              colorScheme={'success'}
              onPress={() => onUpdateStatus('confirmed')}>
              <TextM style={{color: colors.White}}>ຢືນຢັນການຂໍພົບ</TextM>
            </Button>
          </View>
        </View>
      </Modal>

      {/* Cancel Reason */}
      <Modal
        visible={visibleCancel}
        backdropStyle={{backgroundColor: 'rgba(0, 0, 0, 0.6)'}}
        onBackdropPress={() => {
          Keyboard.dismiss();
        }}>
        <Box
          style={{
            backgroundColor: '#f6f6f6',
            padding: 15,
            borderRadius: 10,
            width: 300,
          }}>
          <TouchableOpacity
            onPress={() => {
              setVisibleCancel(false);
            }}
            style={{position: 'absolute', top: -10, right: -10}}>
            <MaterialCommunityIcons
              name="close-circle"
              style={{fontSize: 34, color: colors.Red}}
            />
          </TouchableOpacity>
          <Box mb={2}>
            <TextM>ກະລຸນາບອກເຫດຜົນຂອງທ່ານໃນການປະຕິເສດ</TextM>
          </Box>
          <Box alignItems="center" w="100%" mb={4}>
            <TextArea
              onChangeText={nextValue => setCancelReason(nextValue)}
              h={40}
              placeholder="ກະລຸນາບອກເຫດຜົນຂອງທ່ານ"
              style={{fontSize: 16, fontFamily: 'NotoSansLao-Regular'}}
            />
          </Box>
          <Button
            colorScheme={'error'}
            onPress={() => {
              if (cancelReason === '') {
                alert('ກະລຸນາບອກເຫດຜົນຂອງທ່ານ');
              } else {
                setVisibleCancel(false);
                onUpdateStatus('cancelled');
              }
            }}>
            <TextM style={{color: colors.White}}>ປະຕິເສດການຂໍພົບ</TextM>
          </Button>
        </Box>
      </Modal>

      {loading && <Loading />}
    </Box>
  );
};

export default HomeHospital;

const styles = StyleSheet.create({});
