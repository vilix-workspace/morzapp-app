import React, {useEffect, useState} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Box, ScrollView, View, Text, HStack, VStack} from 'native-base';
import FastImage from 'react-native-fast-image';
import axios from 'axios';
import {Button as ButtonN, Dialog, Portal} from 'react-native-paper';
import {CommonActions} from '@react-navigation/native';
import Config from 'react-native-config';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
// Firebase
import firebase from '@react-native-firebase/app';
//Redux package
import {useSelector, useDispatch} from 'react-redux';
//Redux action
import {
  setToken,
  logOutHospital,
} from '../../../stores/features/userData/userDataSlice';
// Component
import Loading from '../../shareComponents/Loading';
import TextM from '../../shareComponents/TextM';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';

const ProfileHospital = ({navigation}) => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();
  // useState
  const [loading, setLoading] = useState(false);
  const [showAlert, setShowAlert] = useState(false);

  const user = userData.hospitalInfo;

  // useEffect
  useEffect(() => {
    console.log('useEffect ProfileHospital', userData);
  }, []);

  // Functions
  const numberWithCommas = x => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  const getPubKey = async () => {
    try {
      await axios({
        method: 'get',
        url: Config.WP_URL + '/wp/v2/public_key',
      })
        .then(async res => {
          try {
            await axios({
              method: 'post',
              url: Config.WP_URL + '/api/v1/token',
              data: {
                username: res.data[0].mc,
                password: res.data[0].mcc,
              },
            })
              .then(res => {
                let token = res?.data?.jwt_token;
                dispatch(setToken(token));
                setStatusOffline();
                setLoading(false);
                navigation.dispatch(
                  CommonActions.reset({
                    index: 1,
                    routes: [
                      {
                        name: 'HomeStack',
                      },
                    ],
                  }),
                );
              })
              .catch(error => {
                console.log(error);
              });
          } catch (error) {
            console.log(error);
          }
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const onLogOut = async () => {
    setLoading(true);
    dispatch(logOutHospital());
    await getPubKey();
  };

  const setStatusOffline = () => {
    firebase
      .app()
      .database(Config.FIREBASE_REALTIME_DB)
      .ref(`/hospitals/${userData?.hospitalInfo?.hospital_clinic_id}`)
      .update({
        activeStatus: 'offline',
      })
      .then(data => {
        //success callback
      })
      .catch(error => {
        //error callback
        console.log('error ', error);
      });
  };

  const showAlertNow = () => {
    setShowAlert(true);
  };

  const hideAlert = () => {
    setShowAlert(false);
  };

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <ScrollView style={{...defaultStyle.fixedLayout}}>
        <FastImage
          style={{
            width: '100%',
            height: 140,
            marginBottom: 10,
            alignSelf: 'center',
            borderRadius: 5,
          }}
          resizeMode={FastImage.resizeMode.stretch}
          source={{uri: user?.picture}}
        />
        <TextM>
          <FontAwesome5
            name="user-nurse"
            style={{fontSize: 18, color: colors.DarkBlue}}
          />{' '}
          ຜູ້ຮັບຜິດຊອບ
        </TextM>
        <View
          style={{
            marginVertical: 10,
            alignItems: 'center',
          }}>
          <TextM style={{color: colors.Blue, fontSize: 20}}>
            {user?.contact_person}
          </TextM>
          <TextM style={{color: colors.LightBlack, fontSize: 16}}>
            ເບີໂທຜູ້ຮັບຜິດຊອບ:{'  '}
            <TextM style={{color: colors.LightBlack, fontSize: 16}}>
              {user?.contact_phone}
            </TextM>
          </TextM>
        </View>

        {/* Dashboard */}
        {/* <View
          style={{
            marginTop: 10,
          }}>
          <TextM>
            <Icon
              type="FontAwesome5"
              name="chart-bar"
              style={{fontSize: 18, color: colors.DarkBlue}}
            />{' '}
            ສະຖິຕິການໃຊ້ງານ
          </TextM>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              marginTop: 10,
            }}>
            <View style={styles.boxStyle}>
              <TextM style={{color: colors.Gray, fontSize: 14}}>
                ຈຳນວນຄັ້ງ
              </TextM>
              <TextM
                style={{color: colors.Blue, fontSize: 26, paddingVertical: 5}}>
                20
              </TextM>
              <TextM style={{color: colors.Gray, fontSize: 14}}>
                ທີ່ໃຫ້ຄຳປຶກສາ
              </TextM>
            </View>
            <View
              style={[
                styles.boxStyle,
                {
                  borderColor: colors.Orange,
                },
              ]}>
              <TextM style={{color: colors.Gray, fontSize: 14}}>
                ຈຳນວນເງິນສະສົມ
              </TextM>
              <TextM
                style={{
                  color: colors.Orange,
                  fontSize: 26,
                  paddingVertical: 5,
                }}>
                {numberWithCommas(200000000)}
              </TextM>
              <TextM style={{color: colors.Gray, fontSize: 14}}>ກີບ</TextM>
            </View>
          </View>
        </View> */}

        {/* <List>
          <ListItem onPress={() => {}} icon style={{paddingLeft: 0}} noIndent>
            <Left
              style={{
                width: 25,
                paddingRight: 0,
              }}>
              <Icon
                type="FontAwesome5"
                name="clipboard-list"
                style={{
                  fontSize: 18,
                  textAlign: 'center',
                  color: colors.DarkBlue,
                }}
              />
            </Left>
            <Body style={{paddingLeft: 10}}>
              <TextM>ປະຫວັດການນັດຈອງທັງໝົດ</TextM>
            </Body>
          </ListItem>
        </List> */}

        {/* Row Detail */}
        {/* ID */}
        <View style={{marginTop: 10}}>
          <View style={styles.rowDetail}>
            <View
              style={{
                width: 'auto',
                justifyContent: 'center',
              }}>
              <FontAwesome5 name="fingerprint" style={styles.iconDetail} />
            </View>
            <View style={{width: '90%'}}>
              <TextM>
                ID:{'  '}
                <TextM style={styles.textDetail}>
                  {user?.hospital_clinic_id}
                </TextM>
              </TextM>
            </View>
          </View>
          {/* Name */}
          <View style={styles.rowDetail}>
            <View
              style={{
                width: 'auto',
                justifyContent: 'center',
              }}>
              <FontAwesome5 name="hospital" style={styles.iconDetail} />
            </View>
            <View style={{width: '90%'}}>
              <TextM>
                ຊື່:{'  '}
                <TextM style={styles.textDetail}>
                  {user?.hospitalclinic_name}
                </TextM>
              </TextM>
            </View>
          </View>
          {/* Address */}
          <View style={styles.rowDetail}>
            <View
              style={{
                width: 'auto',
                justifyContent: 'center',
              }}>
              <FontAwesome5 name="map-marked" style={styles.iconDetail} />
            </View>
            <View style={{width: '90%'}}>
              <TextM>
                ທີ່ຢູ່:{'  '}
                <TextM style={styles.textDetail}>{user?.address}</TextM>
              </TextM>
            </View>
          </View>
          {/* Hospital Phone */}
          <View style={styles.rowDetail}>
            <View
              style={{
                width: 'auto',
                justifyContent: 'center',
              }}>
              <FontAwesome5 name="phone" style={styles.iconDetail} />
            </View>
            <View style={{width: '90%'}}>
              <TextM>
                ເບີໂທລະສັບ:{'  '}
                <TextM style={styles.textDetail}>{user?.hospital_phone}</TextM>
              </TextM>
            </View>
          </View>
        </View>

        {/* List Item Details */}
        <VStack style={{marginBottom: 20}}>
          <TouchableOpacity
            style={styles.listItem}
            onPress={() => {
              showAlertNow();
            }}
            noBorder>
            <FontAwesome5
              name="sign-out-alt"
              style={[styles.iconDetail, {color: colors.Red}]}
            />
            <Box>
              <TextM style={{color: colors.Gray}}>ອອກຈາກລະບົບ</TextM>
            </Box>
          </TouchableOpacity>
        </VStack>
      </ScrollView>

      {/* Modal Logout */}
      <Portal>
        <Dialog
          visible={showAlert}
          onDismiss={hideAlert}
          style={{borderRadius: 10}}>
          <Dialog.Title>
            <TextM fontWeight="bold" style={{fontSize: 18}}>
              ອອກຈາກລະບົບ ?{' '}
              <MaterialCommunityIcons
                name="emoticon-sad-outline"
                style={{fontSize: 20, color: '#e74c3c'}}
              />
            </TextM>
          </Dialog.Title>
          <Dialog.Content>
            <TextM>ກະລຸນາກົດຢືນຢັນເພື່ອລົງຊື້ອອກຈາກລະບົບ</TextM>
          </Dialog.Content>
          <Dialog.Actions>
            <ButtonN onPress={() => hideAlert()}>
              <TextM>ກັບຄືນ</TextM>
            </ButtonN>
            <ButtonN
              onPress={() => {
                hideAlert();
                onLogOut();
              }}>
              <TextM style={{color: colors.Red}}>ອອກຈາກລະບົບ</TextM>
            </ButtonN>
          </Dialog.Actions>
        </Dialog>
      </Portal>
      {loading && <Loading />}
    </Box>
  );
};

export default ProfileHospital;

const styles = StyleSheet.create({
  textDetail: {
    color: colors.DarkBlue,
  },
  iconDetail: {
    fontSize: 18,
    color: colors.DarkBlue,
    width: 25,
    textAlign: 'center',
  },
  listItem: {
    marginLeft: 0,
  },
  rowDetail: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 8,
    borderBottomWidth: 1,
    borderBottomColor: '#f1f1f1',
  },
  boxStyle: {
    borderColor: colors.Blue,
    borderWidth: 1,
    borderRadius: 10,
    paddingHorizontal: 15,
    paddingVertical: 8,
    alignItems: 'center',
  },
});
