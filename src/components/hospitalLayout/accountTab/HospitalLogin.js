import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import {
  Box,
  ScrollView,
  View,
  Text,
  Icon,
  Button,
  useToast,
  Alert,
  HStack,
  VStack,
} from 'native-base';
import axios from 'axios';
import {Layout, Input} from '@ui-kitten/components';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {ToggleButton} from 'react-native-paper';
import FastImage from 'react-native-fast-image';
// Firebase
import firebase from '@react-native-firebase/app';
// import iid from '@react-native-firebase/iid';
import {CommonActions} from '@react-navigation/native';
import Config from 'react-native-config';
//Redux package
import {useSelector, useDispatch} from 'react-redux';
//Redux action
import {
  setToken,
  addHospitalData,
} from '../../../stores/features/userData/userDataSlice';
// Component
import Loading from '../../shareComponents/Loading';
import TextM from '../../shareComponents/TextM';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';

const HospitalLogin = ({navigation}) => {
  const toast = useToast();
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();

  // useState
  const [loading, setLoading] = useState(false);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [secureTextEntry, setSecureTextEntry] = useState(true);
  const [loginBy, setLoginBy] = useState('otp');
  const [firebaseID, setFirebaseID] = useState(null);

  // useEffect
  useEffect(() => {
    // iid()
    //   .get()
    //   .then(id => {
    //     setFirebaseID(id);
    //   });
  }, []);

  const showToast = (type, title, desc) => {
    toast.show({
      render: () => {
        return (
          <Alert
            maxWidth="95%"
            alignSelf="center"
            flexDirection="row"
            status={type}
            variant={'left-accent'}>
            <VStack space={1} flexShrink={1} w="100%">
              <HStack
                flexShrink={1}
                alignItems="center"
                justifyContent="space-between">
                <HStack space={2} flexShrink={1} alignItems="center">
                  <Alert.Icon />
                  <TextM>{title}</TextM>
                </HStack>
              </HStack>
              {desc !== undefined && (
                <TextM style={{fontSize: 14, paddingLeft: 25}}>{desc}</TextM>
              )}
            </VStack>
          </Alert>
        );
      },
    });
  };

  const toggleSecureEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const renderIcon = () => (
    <TouchableWithoutFeedback onPress={toggleSecureEntry}>
      <MaterialCommunityIcons
        name={secureTextEntry ? 'eye-off' : 'eye'}
        style={{color: '#bdc3c7', fontSize: 20}}
      />
    </TouchableWithoutFeedback>
  );

  // Add User Data
  const onCheckLogin = () => {
    if (username === '' || password === '') {
      showToast('warning', 'ກະລຸນາຕື່ມຂໍ້ມູນໃຫ້ຄົບຖ້ວນ!');
    } else {
      onCheckHospitalUser();
    }
  };

  const onCheckHospitalUser = () => {
    Keyboard.dismiss();
    setLoading(true);
    try {
      axios({
        method: 'get',
        url:
          Config.WP_URL +
          '/wp/v2/users?roles=hospital_clinic&search=' +
          username,
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(res => {
          if (res.data.length > 0) {
            onLoginAndgetUserToken();
          } else {
            showToast('warning', 'ຊື່ຜູ້ໃຊ້ງານ ຫຼື ລະຫັດຜ່ານບໍ່ຖືກຕ້ອງ!');

            setLoading(false);
          }
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const onLoginAndgetUserToken = async () => {
    try {
      await axios({
        method: 'post',
        url: Config.WP_URL + '/api/v1/token',
        data: {
          username: username,
          password: password,
        },
      })
        .then(async res => {
          let token = res?.data?.jwt_token;
          dispatch(setToken(token));
          await saveUserData(token);
        })
        .catch(error => {
          // console.log(error.response);
          showToast('warning', 'ເບີໂທ ຫຼື ລະຫັດຜ່ານບໍ່ຖືກຕ້ອງ!');

          setLoading(false);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const saveUserData = async token => {
    try {
      await axios({
        method: 'get',
        url: Config.WP_URL + '/wp/v2/hospitals/?hospital_clinic_id=' + username,
        headers: {
          Authorization: 'Bearer ' + token,
          'Content-Type': 'application/json',
        },
      })
        .then(async res => {
          let userinfo = res.data[0];
          try {
            await axios({
              method: 'get',
              url: Config.WP_URL + '/wp/v2/users/me',
              headers: {
                Authorization: 'Bearer ' + token,
                'Content-Type': 'application/json',
              },
            })
              .then(async res => {
                let userid = {
                  user_id: res.data.id,
                };
                let combineuserinfo = Object.assign(userinfo, userid);

                dispatch(addHospitalData(combineuserinfo));
                onAddRealTimeStatus(combineuserinfo);
              })

              .catch(error => {
                console.log(error);
              });
          } catch (error) {
            console.log(error);
          }
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  // Add Realtime DB Status
  const onAddRealTimeStatus = async user => {
    console.log('=user der', user);
    await firebase
      .app()
      .database(Config.FIREBASE_REALTIME_DB)
      .ref(`/hospitals/${user?.hospital_clinic_id}`)
      .set({
        // firebaseID: firebaseID,
        morzappID: user?.hospital_clinic_id,
        activeStatus: 'online',
      })
      .then(data => {
        //success callback
        navigation.dispatch(
          CommonActions.reset({
            index: 1,
            routes: [
              {
                name: 'HospitalHomeStack',
              },
            ],
          }),
        );
        showToast('success', 'ການເຂົ້າສູ່ລະບົບສຳເລັດແລ້ວ!');
        setLoading(false);
      })
      .catch(error => {
        //error callback
        console.log('error ', error);
      });
  };

  return (
    <Layout style={styles.container}>
      <Layout style={[styles.layoutCenter, {backgroundColor: '#ffffff'}]}>
        <FastImage
          style={{width: 100, height: 100, marginBottom: 30}}
          source={require('../../../assets/img/hospital.png')}
        />
        {/* <Icon
          type="FontAwesome5"
          name="hospital-alt"
          style={{color: colors.DarkBlue, fontSize: 50, marginBottom: 20}}
        /> */}
        <TextM>ກະລຸນາໃສ່ເບີໂທ 8 ຕົວເລກຂອງທ່ານ</TextM>
        <Layout
          style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 20,
            marginBottom: 5,
          }}>
          <MaterialCommunityIcons
            name="account"
            style={{color: colors.Blue, fontSize: 20, marginRight: 5}}
          />
          <TextM style={{marginRight: 10, width: '25%'}}>ຊື່ຜູ້ໃຊ້ງານ</TextM>
          <Input
            style={{width: '100%', flex: 1}}
            placeholder="ຊື່ຜູ້ໃຊ້ງານ"
            value={username}
            onChangeText={nextValue => setUsername(nextValue.toUpperCase())}
            textStyle={{fontFamily: 'NotoSansLao-Regular'}}
          />
        </Layout>
        {/* Insert Password */}
        <View>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <MaterialCommunityIcons
              name="lock"
              style={{color: colors.Blue, fontSize: 20, marginRight: 5}}
            />
            <TextM style={{marginRight: 10, width: '25%'}}>ລະຫັດຜ່ານ</TextM>
            <Input
              style={{width: '100%', flex: 1}}
              placeholder="**********"
              value={password}
              onChangeText={nextValue => setPassword(nextValue)}
              secureTextEntry={secureTextEntry}
              accessoryRight={renderIcon}
            />
          </View>
          {/* <TouchableOpacity
            style={{alignItems: 'flex-end', marginTop: 10}}>
            <TextM style={{color: '#cccccc'}}>ລືມລະຫັດຜ່ານ ?</TextM>
          </TouchableOpacity> */}
        </View>

        <Button
          style={{
            borderRadius: 5,
            marginTop: 20,
            width: '100%',
          }}
          onPress={() => {
            onCheckLogin();
          }}>
          <TextM style={{color: '#ffffff'}}>ເຂົ້າສູ່ລະບົບ</TextM>
        </Button>
      </Layout>
      {loading && <Loading />}
    </Layout>
  );
};

export default HospitalLogin;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  layoutCenter: {
    alignItems: 'center',
    paddingHorizontal: 30,
    paddingTop: 30,
  },
});
