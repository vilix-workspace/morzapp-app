import React, {useEffect, useState, useRef} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  PermissionsAndroid,
  Platform,
  Dimensions,
  FlatList,
  RefreshControl,
} from 'react-native';
import {Box, View, Text, HStack, VStack, Button, Center} from 'native-base';
import {Modal, Toggle} from '@ui-kitten/components';
// Firebase
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import {
  TwilioVideoLocalView,
  TwilioVideoParticipantView,
  TwilioVideo,
} from 'react-native-twilio-video-webrtc';
import axios from 'axios';
import FastImage from 'react-native-fast-image';
import TrackPlayer, {RepeatMode} from 'react-native-track-player';
import moment from 'moment';
import LottieView from 'lottie-react-native';
import Config from 'react-native-config';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
//Redux package
import {useSelector, useDispatch} from 'react-redux';

// Component
import Loading from '../../shareComponents/Loading';
import TextM from '../../shareComponents/TextM';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';

const windowHeight = Dimensions.get('window').height;

var countDurationSecond;

const HomeDoctor = ({navigation}) => {
  let _session = null;
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();
  // useState
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = useState(false);
  const [checkedStatus, setCheckedStatus] = useState(false);
  const [realTimeCallingInfo, setRealTimeCallingInfo] = useState(null);
  const [callStatus, setCallStatus] = useState('');
  const [connectRoomID, setConnectRoomID] = useState(null);
  const [patientList, setPatientList] = useState([]);
  const [selectPatient, setSelectPatient] = useState({});
  const [callStart, setCallStart] = useState(false);
  // const appState = useRef(AppState.currentState);
  // Call State
  const [isAudioEnabled, setIsAudioEnabled] = useState(true);
  // const [isVideoEnabled, setIsVideoEnabled] = useState(true);
  const [status, setStatus] = useState('disconnected');
  // const [participants, setParticipants] = useState(new Map());
  const [videoTracks, setVideoTracks] = useState(new Map());
  const twilioVideo = useRef(null);
  // Time Count
  const [countDuration, setCountDuration] = useState(0);
  const [refreshing, setRefreshing] = useState(false);

  // useEffect
  useEffect(() => {
    // TrackPlayer.pause();
    // console.log('userData', selectPatient);

    getPatientInfo();
    // Get Firebase Doctor Status
    firebase
      .app()
      .database(Config.FIREBASE_REALTIME_DB)
      .ref(`/doctors/${userData?.doctorInfo?.morzapp_id}`)
      .once('value')
      .then(snapshot => {
        //success callback
        // console.log('User data: read bor ', snapshot.val());
        if (snapshot.val().activeStatus === 'online') {
          setCheckedStatus(true);
        } else {
          setCheckedStatus(false);
        }
      })
      .catch(error => {
        //error callback
        console.log('error ', error);
      });

    // Handler real time status
    const onValueChange = firebase
      .app()
      .database(Config.FIREBASE_REALTIME_DB)
      .ref(`/doctors/${userData?.doctorInfo?.morzapp_id}`)
      .on('value', snapshot => {
        let data = snapshot.val();
        // console.log('RT change nn: ', data);
        if (data?.callStatus === 'calling') {
          startCallingSound();
        }

        getPatientInfo();
        setRealTimeCallingInfo(data);
      });

    // Unmounted
    return () => {
      // console.log('unmounted');
      firebase
        .database()
        .ref(`/doctors/${userData?.doctorInfo?.morzapp_id}`)
        .off('value', onValueChange);
    };
  }, []);

  // Functions
  const onRefresh = () => {
    setRefreshing(true);
    try {
      getPatientInfo();
      setRefreshing(false);
    } catch (error) {
      console.log(error);
    }
  };

  const onCheckedChange = () => {
    setLoading(true);
    try {
      firebase
        .app()
        .database(Config.FIREBASE_REALTIME_DB)
        .ref(`/doctors/${userData?.doctorInfo?.morzapp_id}`)
        .update({
          activeStatus: checkedStatus ? 'offline' : 'online',
        })
        .then(data => {
          //success callback
          setCheckedStatus(checkedStatus ? false : true);
          setLoading(false);
        })
        .catch(error => {
          //error callback
          console.log('error ', error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const getPatientInfo = () => {
    try {
      firestore()
        .collection('callHistory')
        .where('doctorID', '==', userData?.doctorInfo?.morzapp_id)
        .orderBy('startedAt', 'desc')
        .limit(30)
        .get()
        .then(querySnapshot => {
          // console.log('data', documentSnapshot);
          let allData = [];
          querySnapshot.forEach(doc => {
            allData.push(doc.data());
          });
          // console.log(allData);
          setPatientList(allData);
        })
        .catch(error => {
          console.error('Error read ', error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const _cancelCall = () => {
    try {
      firebase
        .app()
        .database(Config.FIREBASE_REALTIME_DB)
        .ref(`/doctors/${userData?.doctorInfo?.morzapp_id}`)
        .update({
          callStatus: 'cancelled',
        })
        .then(data => {
          //success callback
        })
        .catch(error => {
          //error callback
          console.log('error ', error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  // Count Time Duration
  const startDuration = () => {
    let second = 0;
    countDurationSecond = setInterval(() => {
      second += 1;
      setCountDuration(second);
    }, 1000);
  };

  const clearDuration = () => {
    clearInterval(countDurationSecond);
  };

  const renderTimeSecond = value => {
    let second = value % 60;
    return (
      Math.floor(value / 60) +
      ':' +
      (second ? (second < 10 ? '0' + second : second) : '00')
    );
  };

  const updateDuration = () => {
    try {
      firestore()
        .collection('callHistory')
        .doc(realTimeCallingInfo?.firebaseDocId)
        .update({
          durationSecbyDoc: countDuration,
        })
        .then(() => {
          console.log('Document successfully written!');
          setCountDuration(0);
        })
        .catch(error => {
          console.error('Error writing document: ', error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  //
  // Call Service Functions
  //

  const onStartVoiceCall = async () => {
    setCallStatus('voice');

    if (Platform.OS === 'android') {
      await _requestAudioPermission();
      await _requestCameraPermission();
    }

    let roomName = realTimeCallingInfo?.roomID;

    if (realTimeCallingInfo?.activeStatus === 'online') {
      try {
        axios({
          method: 'get',
          url: Config.API_CALL_URL + '?token=' + userData?.userToken,
        })
          .then(res => {
            console.log(res);
            twilioVideo.current.connect({
              roomName: roomName,
              accessToken: res.data.token,
              enableNetworkQualityReporting: true,
              dominantSpeakerEnabled: true,
              enableVideo: false,
            });
            setIsAudioEnabled(true);
            setStatus('connected');
            updateCallStatus('connected');
            updateCallHistoryAction('DoctorAccepted');
            stopCallingSound();

            setTimeout(() => {
              startDuration();
            }, 2000);
          })
          .catch(error => {
            console.log(error);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      console.log('Call Busy');
    }
  };

  const onStartVideoCall = async () => {
    setCallStatus('video');

    if (Platform.OS === 'android') {
      await _requestAudioPermission();
      await _requestCameraPermission();
    }

    let roomName = realTimeCallingInfo?.roomID;

    if (realTimeCallingInfo?.activeStatus === 'online') {
      try {
        axios({
          method: 'get',
          url: Config.API_CALL_URL + '?token=' + userData?.userToken,
        })
          .then(res => {
            console.log(res);
            twilioVideo.current.connect({
              roomName: roomName,
              accessToken: res.data.token,
              enableNetworkQualityReporting: true,
              dominantSpeakerEnabled: true,
              enableVideo: true,
            });
            setIsAudioEnabled(true);
            setStatus('connected');
            updateCallStatus('connected');
            updateCallHistoryAction('DoctorAccepted');
            stopCallingSound();
          })
          .catch(error => {
            console.log(error);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      console.log('Call Busy');
    }
  };

  const updateCallHistoryAction = callActionType => {
    try {
      firestore()
        .collection('callHistory')
        .doc(realTimeCallingInfo?.firebaseDocId)
        .update({
          callAction: callActionType,
        })
        .then(() => {
          console.log('Document successfully written!');
        })
        .catch(error => {
          console.error('Error writing document: ', error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const _onEndButtonPress = () => {
    updateCallStatus('waiting');
    updateCallHistoryAction('DoctorCancelled');
    twilioVideo.current.disconnect();
    setCallStatus('');
    setCallStart(false);
    stopCallingSound();

    if (countDuration > 10) {
      updateDuration();
    }

    clearDuration();
  };

  const _onMuteButtonPress = () => {
    twilioVideo.current
      .setLocalAudioEnabled(!isAudioEnabled)
      .then(isEnabled => setIsAudioEnabled(isEnabled));
  };

  const _onFlipButtonPress = () => {
    twilioVideo.current.flipCamera();
  };

  const _onRoomDidConnect = () => {
    setStatus('connected');
  };

  const _onRoomDidDisconnect = error => {
    console.log('ERROR: ', error);

    clearDuration();
    updateCallStatus('waiting');
    setStatus('disconnected');
    stopCallingSound();
    twilioVideo.current.disconnect();
    setCallStart(false);
  };

  const _onRoomDidFailToConnect = error => {
    console.log('ERROR: ', error);

    clearDuration();
    updateCallStatus('waiting');
    setStatus('disconnected');
    stopCallingSound();
    twilioVideo.current.disconnect();
    setCallStart(false);
  };

  const _onParticipantAddedVideoTrack = ({participant, track}) => {
    console.log('onParticipantAddedVideoTrack: ', participant, track);

    setVideoTracks(
      new Map([
        ...videoTracks,
        [
          track.trackSid,
          {participantSid: participant.sid, videoTrackSid: track.trackSid},
        ],
      ]),
    );

    setCallStart(true);
    startDuration();
  };

  const _onParticipantRemovedVideoTrack = ({participant, track}) => {
    console.log('onParticipantRemovedVideoTrack: ', participant, track);

    const newVideoTracks = new Map(videoTracks);
    newVideoTracks.delete(track.trackSid);

    setVideoTracks(newVideoTracks);
  };

  const _onNetworkLevelChanged = ({participant, isLocalUser, quality}) => {
    console.log(
      'Participant',
      participant,
      'isLocalUser',
      isLocalUser,
      'quality',
      quality,
    );
  };

  const _onDominantSpeakerDidChange = ({roomName, roomSid, participant}) => {
    console.log(
      'onDominantSpeakerDidChange',
      `roomName: ${roomName}`,
      `roomSid: ${roomSid}`,
      'participant:',
      participant,
    );
  };

  const _requestAudioPermission = () => {
    return PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
      {
        title: 'Need permission to access microphone',
        message:
          'To run this demo we need permission to access your microphone',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
  };

  const _requestCameraPermission = () => {
    return PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA, {
      title: 'Need permission to access camera',
      message: 'To run this demo we need permission to access your camera',
      buttonNegative: 'Cancel',
      buttonPositive: 'OK',
    });
  };

  //
  // End Call Service Functions
  //

  const updateCallStatus = status => {
    firebase
      .app()
      .database(Config.FIREBASE_REALTIME_DB)
      .ref(`/doctors/${userData?.doctorInfo?.morzapp_id}`)
      .update({
        callStatus: status,
      })
      .then(data => {
        //success callback
      })
      .catch(error => {
        //error callback
        console.log('error ', error);
      });
  };

  // Start Sound
  const startCallingSound = async () => {
    // Add a track to the queue
    await TrackPlayer.add({
      id: 'callingId',
      url: require('../../../assets/sounds/calling-phone.mp3'),
      title: 'MORZAPP Calling',
      artist: 'MORZAPP',
    });

    await TrackPlayer.setRepeatMode(RepeatMode.Track);
    // Start playing it
    await TrackPlayer.play();
  };

  const stopCallingSound = async () => {
    await TrackPlayer.pause();
    await TrackPlayer.reset();
  };

  // Render Item Flatlist
  const renderItem = ({item}) => (
    <HStack
      alignItems={'center'}
      justifyContent="space-between"
      space={2}
      style={{
        paddingHorizontal: 12,
        paddingVertical: 5,
        marginBottom: 10,
        borderWidth: 1,
        borderColor: colors.Light,
        marginHorizontal: 20,
        borderRadius: 10,
      }}>
      <Box>
        <FastImage
          style={{width: 56, height: 56, borderRadius: 5}}
          source={
            item?.imgUrl !== ''
              ? {uri: item?.imgUrl}
              : require('../../../assets/img/user.png')
          }
        />
      </Box>
      <VStack w={'60%'}>
        <TextM numberOfLines={2} style={{color: colors.DarkBlue}}>
          ທ່ານ {item?.patientName}
        </TextM>
        <HStack alignItems={'center'}>
          <TextM>ຮູບແບບການໂທ: </TextM>
          <MaterialCommunityIcons
            name={item?.callType === 'voice' ? 'phone' : 'video'}
            style={{
              fontSize: 20,
              color:
                item?.callType === 'voice' ? colors.DarkLightBlue : colors.Blue,
            }}
          />
        </HStack>
        <HStack alignItems={'center'}>
          <TextM>ສະຖານະ:</TextM>
          <TextM
            style={{
              color:
                item?.durationSec !== undefined ||
                item?.durationSecbyDoc !== undefined
                  ? colors.Green
                  : colors.Red,
            }}>
            {' '}
            {item?.durationSec !== undefined ||
            item?.durationSecbyDoc !== undefined
              ? 'ສຳເລັດແລ້ວ '
              : 'ບໍ່ຮັບສາຍ '}
          </TextM>
          <MaterialCommunityIcons
            name={
              item?.durationSec !== undefined ||
              item?.durationSecbyDoc !== undefined
                ? 'checkbox-multiple-marked-circle'
                : 'phone-missed'
            }
            style={{
              color:
                item?.durationSec !== undefined ||
                item?.durationSecbyDoc !== undefined
                  ? colors.Green
                  : colors.Red,
              fontSize: 20,
            }}
          />
        </HStack>
        <TextM style={{fontSize: 14}}>
          ເວລາ:{' '}
          {moment(item?.startedAt?.seconds * 1000).format(
            'hh:mm a - DD/MM/YYYY',
          )}
        </TextM>
      </VStack>
      <Box>
        <TouchableOpacity
          onPress={() => {
            setVisible(true);
            setSelectPatient(item);
          }}>
          <TextM
            style={{
              fontSize: 14,
              color: colors.DarkBlue,
            }}>
            ເບິ່ງຂໍ້ມູນ
          </TextM>
        </TouchableOpacity>
      </Box>
    </HStack>
  );

  // Render
  return (
    <Box flex={1} backgroundColor={colors.White}>
      {status === 'disconnected' &&
        realTimeCallingInfo?.callStatus === 'waiting' && (
          <>
            <SafeAreaView>
              <Box style={{...defaultStyle.fixedLayout}}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                  }}>
                  <TextM>
                    ສະຖານະ:{' '}
                    <TextM
                      style={{
                        color: checkedStatus ? colors.Green : colors.Red,
                      }}>
                      {checkedStatus ? 'ອອນລາຍ' : 'ປິດ'}
                    </TextM>
                  </TextM>
                  <Toggle
                    style={{marginLeft: 10}}
                    status={checkedStatus ? 'success' : 'danger'}
                    checked={checkedStatus}
                    onChange={onCheckedChange}></Toggle>
                  <FontAwesome5
                    name="circle"
                    solid
                    style={{
                      fontSize: 18,
                      marginLeft: 10,
                      color: checkedStatus ? colors.Green : colors.Red,
                    }}
                  />
                </View>
                <VStack space={0}>
                  <TextM style={{color: colors.LightBlack, fontSize: 14}}>
                    ຍິນດີຕ້ອນຮັບທ່ານ
                  </TextM>
                  <TextM style={{color: colors.Pink, fontSize: 22}}>
                    {userData?.doctorInfo?.fullname}
                  </TextM>
                </VStack>
              </Box>
            </SafeAreaView>

            {/* List of Customer */}
            <HStack
              space={2}
              alignItems={'center'}
              style={{paddingVertical: 5, paddingHorizontal: 20}}>
              <FontAwesome5
                name="file-medical"
                style={{fontSize: 18, color: colors.DarkBlue}}
              />
              <TextM>ລາຍຊື່ຄົນຂໍຄຳປຶກສາ (ຄົນເຈັບ)</TextM>
            </HStack>
            {patientList.length > 0 ? (
              <FlatList
                vertical
                data={patientList}
                renderItem={renderItem}
                keyExtractor={item => item.id}
                refreshControl={
                  <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                  />
                }
              />
            ) : (
              <Box style={{alignItems: 'center'}}>
                <Box style={{height: 180, width: 200, marginTop: -80}}>
                  <LottieView
                    source={require('../../../assets/animation/no-result-found.json')}
                    // style={{width: 300}}
                    autoPlay
                    loop
                    resizeMode="cover"
                  />
                </Box>
                <TextM>ບໍ່ພົບຂໍ້ມູນຂອງທ່ານ</TextM>
              </Box>
            )}

            {loading && <Loading />}

            {/* Modal Select Patient */}
            <Modal visible={visible}>
              <View
                style={{
                  backgroundColor: colors.LightWhite,
                  padding: 15,
                  borderRadius: 10,
                  width: 320,

                  shadowColor: '#000',
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.25,
                  shadowRadius: 3.84,
                  elevation: 5,
                }}>
                <TouchableOpacity
                  onPress={() => setVisible(false)}
                  style={{position: 'absolute', top: -10, right: -10}}>
                  <MaterialCommunityIcons
                    name="close-circle"
                    style={{fontSize: 34, color: colors.Red}}
                  />
                </TouchableOpacity>
                <FastImage
                  style={{
                    alignSelf: 'center',
                    marginBottom: 10,
                    width: 80,
                    height: 80,
                    borderRadius: 10,
                  }}
                  large
                  source={
                    selectPatient?.imgUrl !== ''
                      ? {uri: selectPatient?.imgUrl}
                      : require('../../../assets/img/user.png')
                  }
                />
                <Center>
                  <TextM style>ລາຍລະອຽດຄົນເຈັບ:</TextM>
                </Center>

                <HStack alignItems={'center'} space={2}>
                  <MaterialCommunityIcons
                    name="badge-account"
                    style={{fontSize: 20, color: colors.DarkBlue}}
                  />
                  <TextM fontWeight="bold" style={{color: colors.DarkBlue}}>
                    ທ່ານ {selectPatient?.patientName}
                  </TextM>
                </HStack>

                {selectPatient?.dob !== '' &&
                  selectPatient?.dob !== undefined && (
                    <HStack alignItems={'center'} space={2}>
                      <MaterialCommunityIcons
                        name="cake"
                        style={{fontSize: 20, color: colors.DarkBlue}}
                      />
                      <TextM fontWeight="bold" style={{color: colors.DarkBlue}}>
                        {moment().diff(selectPatient?.dob, 'years', false)} ປີ
                      </TextM>
                    </HStack>
                  )}

                {selectPatient?.gender !== '' &&
                  selectPatient?.gender !== undefined && (
                    <HStack alignItems={'center'} space={2}>
                      <MaterialCommunityIcons
                        name={
                          selectPatient?.gender === 'male'
                            ? 'gender-male'
                            : 'gender-female'
                        }
                        style={{fontSize: 20, color: colors.DarkBlue}}
                      />
                      <TextM fontWeight="bold" style={{color: colors.DarkBlue}}>
                        {selectPatient?.gender === 'male' ? 'ຊາຍ' : 'ຍິງ'}
                      </TextM>
                    </HStack>
                  )}

                <HStack alignItems={'center'} space={2}>
                  <MaterialCommunityIcons
                    name="clock-outline"
                    style={{fontSize: 20, color: colors.DarkBlue}}
                  />
                  <TextM fontWeight="bold" style={{color: colors.DarkBlue}}>
                    {moment(selectPatient?.startedAt?.seconds * 1000).format(
                      'h:mm a - DD MMM YYYY',
                    )}
                  </TextM>
                </HStack>

                <HStack alignItems={'center'} space={2}>
                  <MaterialCommunityIcons
                    name={
                      selectPatient?.callType === 'voice' ? 'phone' : 'video'
                    }
                    style={{fontSize: 20, color: colors.DarkBlue}}
                  />
                  <TextM fontWeight="bold" style={{color: colors.DarkBlue}}>
                    {selectPatient?.callType === 'voice'
                      ? 'ໂທດ້ວຍສຽງ'
                      : 'ໂທດ້ວຍສຽງ ແລະ ພາບເຄື່ອນໄຫວ'}
                  </TextM>
                </HStack>

                {(selectPatient?.durationSec !== undefined ||
                  selectPatient?.durationSecbyDoc !== undefined) && (
                  <HStack alignItems={'center'} space={2}>
                    <MaterialCommunityIcons
                      name={'clipboard-text-clock'}
                      style={{fontSize: 20, color: colors.DarkBlue}}
                    />
                    <TextM fontWeight="bold" style={{color: colors.DarkBlue}}>
                      ໄລຍະເວລາໃນການໂທ{' '}
                      {selectPatient?.durationSec !== undefined
                        ? renderTimeSecond(selectPatient?.durationSec)
                        : renderTimeSecond(
                            selectPatient?.durationSecbyDoc,
                          )}{' '}
                      (ນາທີ:ວິນາທີ)
                    </TextM>
                  </HStack>
                )}

                <HStack alignItems={'center'} space={2}>
                  <MaterialCommunityIcons
                    name={'phone-missed'}
                    style={{fontSize: 20, color: colors.DarkBlue}}
                  />
                  <TextM fontWeight="bold" style={{color: colors.DarkBlue}}>
                    ສະຖານະ{' '}
                    {selectPatient?.durationSec !== undefined ||
                    selectPatient?.durationSecbyDoc !== undefined
                      ? 'ສຳເລັດແລ້ວ'
                      : 'ບໍ່ຮັບສາຍ'}
                  </TextM>
                </HStack>

                <TextM style={{fontSize: 12}}>
                  (ຂໍ້ມູນຄົນເຈັບແຕ່ລະຄັນອາດມີຄວາມແຕກຕ່າງກັນຂຶ້ນກັບຂໍ້ມູນທີ່ຄົນເຈັບໃຫ້ມາ)
                </TextM>
                <TouchableOpacity
                  style={{
                    alignSelf: 'flex-end',
                  }}
                  onPress={() => setVisible(false)}>
                  <TextM fontWeight="bold" style={{color: colors.Red}}>
                    ປິດ
                  </TextM>
                </TouchableOpacity>
              </View>
            </Modal>
          </>
        )}

      {realTimeCallingInfo?.callStatus === 'calling' && (
        <>
          <View
            style={{
              backgroundColor: colors.White,
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TextM fontWeight="bold" style={{fontSize: 22}}>
              ຊື່ຄົນເຈັບ:
            </TextM>
            <TextM fontWeight="bold" style={{fontSize: 22}}>
              ທ່ານ {realTimeCallingInfo?.patientInfo?.name}
            </TextM>
            <FastImage
              style={{
                width: 120,
                height: 120,
                borderRadius: 100,
                marginVertical: 20,
              }}
              resizeMode={FastImage.resizeMode.cover}
              source={
                realTimeCallingInfo?.patientInfo?.imgUrl !== ''
                  ? {uri: realTimeCallingInfo?.patientInfo?.imgUrl}
                  : require('../../../assets/img/user.png')
              }
            />
            {/* <TextM note>ຂໍ້ມູນເບື້ອງຕົ້ນ</TextM>
            <TextM>
              ເພດ:{' '}
              {realTimeCallingInfo?.patientInfo?.gender === 'male'
                ? 'ຊາຍ'
                : 'ຍິງ'}
            </TextM>
            <TextM>
              ອາຍຸ:{' '}
              {moment().diff(
                realTimeCallingInfo?.patientInfo?.dob,
                'years',
                false,
              )}{' '}
              ປີ
            </TextM> */}
            <TextM style={{marginTop: 20}}>ກຳລັງເຊື່ອມຕໍ່...</TextM>
            <TextM style={{marginTop: 20}}>
              ໃນຮູບແບບ:{' '}
              <TextM style={{fontSize: 24, color: colors.DarkBlue}}>
                {realTimeCallingInfo?.callType === 'voice' ? 'ສຽງ' : 'ວິດີໂອ'}
              </TextM>
            </TextM>
          </View>
          <View style={styles.optionsContainer}>
            <TouchableOpacity
              style={{alignItems: 'center'}}
              onPress={() =>
                realTimeCallingInfo?.callType === 'voice'
                  ? onStartVoiceCall()
                  : onStartVideoCall()
              }>
              <View
                style={{
                  backgroundColor: colors.Green,
                  marginBottom: 5,
                  borderRadius: 100,
                }}>
                <MaterialCommunityIcons
                  name={
                    realTimeCallingInfo?.callType === 'voice'
                      ? 'phone'
                      : 'video'
                  }
                  style={{
                    color: colors.White,
                    fontSize: 28,
                    padding: 15,
                  }}
                />
              </View>
              <TextM style={{fontSize: 14}}>ຮັບສາຍ</TextM>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                _onEndButtonPress();
                _cancelCall();
              }}
              style={{alignItems: 'center'}}>
              <View
                style={{
                  backgroundColor: colors.Red,
                  marginBottom: 5,
                  borderRadius: 100,
                }}>
                <MaterialCommunityIcons
                  name="phone-hangup"
                  style={{
                    color: colors.White,
                    fontSize: 28,
                    padding: 15,
                  }}
                />
              </View>
              <TextM style={{fontSize: 14}}>ວາງສາຍ</TextM>
            </TouchableOpacity>
          </View>
        </>
      )}

      {/* */}
      {/* Call Connect */}
      {/* */}
      {(status === 'connected' || status === 'connecting') && (
        <View style={styles.callContainer}>
          {callStart
            ? callStatus === 'video' &&
              status === 'connected' &&
              realTimeCallingInfo?.callStatus === 'connected' && (
                <View style={styles.remoteGrid}>
                  {Array.from(videoTracks, ([trackSid, trackIdentifier]) => {
                    return (
                      <TwilioVideoParticipantView
                        style={styles.remoteVideo}
                        key={trackSid}
                        trackIdentifier={trackIdentifier}
                      />
                    );
                  })}
                </View>
              )
            : null}

          {callStart
            ? callStatus === 'video' && (
                <TwilioVideoLocalView
                  applyZOrder={true}
                  enabled={true}
                  style={styles.localVideo}
                />
              )
            : null}

          {callStatus === 'voice' && (
            <View
              style={{
                backgroundColor: colors.White,
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <TextM fontWeight="bold" style={{fontSize: 22}}>
                {realTimeCallingInfo?.patientInfo?.name}
              </TextM>
              <FastImage
                style={{
                  width: 120,
                  height: 120,
                  borderRadius: 100,
                  marginVertical: 20,
                }}
                resizeMode={FastImage.resizeMode.cover}
                source={{uri: realTimeCallingInfo?.patientInfo?.imgUrl}}
              />
              <TextM>{renderTimeSecond(countDuration)}</TextM>
            </View>
          )}

          {callStatus === '' && (
            <View
              style={{
                backgroundColor: colors.White,
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <TextM fontWeight="bold" style={{fontSize: 22}}>
                {realTimeCallingInfo?.patientInfo?.name}
              </TextM>
              <FastImage
                style={{
                  width: 120,
                  height: 120,
                  marginVertical: 20,
                }}
                resizeMode={FastImage.resizeMode.cover}
                source={require('../../../assets/img/logo-400x400.png')}
              />
              <TextM>ກຳລັງວາງສາຍ</TextM>
            </View>
          )}

          {callStatus === 'video' &&
          realTimeCallingInfo?.callStatus === 'connected' &&
          !callStart ? (
            <View
              style={{
                backgroundColor: colors.White,
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <TextM fontWeight="bold" style={{fontSize: 22}}>
                {realTimeCallingInfo?.patientInfo?.name}
              </TextM>
              <FastImage
                style={{
                  width: 120,
                  height: 120,
                  borderRadius: 100,
                  marginVertical: 20,
                }}
                resizeMode={FastImage.resizeMode.contain}
                source={{uri: realTimeCallingInfo?.patientInfo?.imgUrl}}
              />

              <TextM>ກະລຸນາລໍຖ້າ...</TextM>
            </View>
          ) : null}

          {callStart
            ? realTimeCallingInfo?.callStatus === 'connected' &&
              callStatus === 'video' && (
                <View
                  style={{
                    backgroundColor: 'rgba(255, 255, 255, 0.8)',
                    alignSelf: 'center',
                    paddingHorizontal: 10,
                    paddingVertical: 0,
                    borderRadius: 8,
                    marginBottom: 5,
                  }}>
                  <TextM>{renderTimeSecond(countDuration)}</TextM>
                </View>
              )
            : null}

          <View style={styles.optionsContainer}>
            {realTimeCallingInfo?.callStatus === 'connected' && (
              <TouchableOpacity
                onPress={_onMuteButtonPress}
                style={{alignItems: 'center'}}>
                <View
                  style={{
                    backgroundColor: isAudioEnabled
                      ? colors.Green
                      : colors.LightBlack,
                    padding: 15,
                    borderRadius: 100,
                    marginBottom: 5,
                  }}>
                  <MaterialCommunityIcons
                    name={isAudioEnabled ? 'microphone' : 'microphone-off'}
                    style={{
                      color: colors.White,
                      fontSize: 20,
                    }}
                  />
                </View>
                <TextM style={{fontSize: 14}}>
                  {isAudioEnabled ? 'ເປີດໄມ' : 'ປິດໄມ'}
                </TextM>
              </TouchableOpacity>
            )}

            {callStatus === 'video' &&
              realTimeCallingInfo?.callStatus === 'connected' && (
                <TouchableOpacity
                  style={{alignItems: 'center'}}
                  onPress={_onFlipButtonPress}>
                  <View
                    style={{
                      backgroundColor: colors.Blue,
                      padding: 15,
                      borderRadius: 100,
                      marginBottom: 5,
                    }}>
                    <MaterialCommunityIcons
                      name="repeat"
                      style={{
                        color: colors.White,
                        fontSize: 20,
                      }}
                    />
                  </View>
                  <TextM style={{fontSize: 14}}>ປ່ຽນກ້ອງ</TextM>
                </TouchableOpacity>
              )}

            {realTimeCallingInfo?.callStatus === 'connected' ? (
              <TouchableOpacity
                onPress={() => {
                  _onEndButtonPress();
                  _cancelCall();
                }}
                style={{alignItems: 'center'}}>
                <View
                  style={{
                    backgroundColor: colors.Red,
                    padding: 15,
                    borderRadius: 100,
                    marginBottom: 5,
                  }}>
                  <MaterialCommunityIcons
                    name="phone-hangup"
                    style={{
                      color: colors.White,
                      fontSize: 20,
                    }}
                  />
                </View>
                <TextM style={{fontSize: 14}}>ວາງສາຍ</TextM>
              </TouchableOpacity>
            ) : null}
          </View>
        </View>
      )}

      <TwilioVideo
        ref={twilioVideo}
        onRoomDidConnect={_onRoomDidConnect}
        onRoomDidDisconnect={_onRoomDidDisconnect}
        onRoomDidFailToConnect={_onRoomDidFailToConnect}
        onParticipantAddedVideoTrack={_onParticipantAddedVideoTrack}
        onParticipantRemovedVideoTrack={_onParticipantRemovedVideoTrack}
        onNetworkQualityLevelsChanged={_onNetworkLevelChanged}
        onDominantSpeakerDidChange={_onDominantSpeakerDidChange}
      />
    </Box>
  );
};

export default HomeDoctor;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  callContainer: {
    flex: 1,
    position: 'absolute',
    bottom: 0,
    top: 0,
    left: 0,
    right: 0,
  },
  welcome: {
    fontSize: 30,
    textAlign: 'center',
    paddingTop: 40,
  },
  input: {
    height: 50,
    borderWidth: 1,
    marginRight: 70,
    marginLeft: 70,
    marginTop: 20,
    marginBottom: 20,
    textAlign: 'center',
    backgroundColor: 'white',
  },
  button: {
    marginTop: 100,
  },
  localVideo: {
    flex: 1,
    width: 100,
    height: 160,
    position: 'absolute',
    right: 10,
    top: 10,
  },
  remoteGrid: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  remoteVideo: {
    flex: 1,
    height: windowHeight,
  },
  optionsContainer: {
    paddingVertical: 10,
    backgroundColor: 'rgba(255, 255, 255, 1)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginBottom: 10,
    marginHorizontal: 10,
    borderRadius: 20,
  },
  optionButton: {
    width: 60,
    height: 60,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 100 / 2,
    backgroundColor: 'grey',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
