import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  TouchableOpacity,
  ActivityIndicator,
  Keyboard,
  Dimensions,
} from 'react-native';
import axios from 'axios';
import {Layout, Input} from '@ui-kitten/components';
import {Button, useToast, Alert, HStack, VStack} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image';
import {CommonActions} from '@react-navigation/native';
// Firebase
import firebase from '@react-native-firebase/app';
// import iid from '@react-native-firebase/iid';
// FBSDK
import {LoginManager, AccessToken} from 'react-native-fbsdk-next';
import Config from 'react-native-config';
//Redux package
import {useSelector, useDispatch} from 'react-redux';
//Redux action
import {
  setToken,
  addDoctorData,
} from '../../../stores/features/userData/userDataSlice';
// Component
import Loading from '../../shareComponents/Loading';
import TextM from '../../shareComponents/TextM';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const DoctorLogin = ({navigation}) => {
  const toast = useToast();
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();

  // useState
  const [loading, setLoading] = useState(false);
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const [secureTextEntry, setSecureTextEntry] = useState(true);

  // useEffect
  useEffect(() => {
    console.log('useEffect DoctorLogin', userData);
  }, []);

  const showToast = (type, title, desc) => {
    toast.show({
      render: () => {
        return (
          <Alert
            maxWidth="95%"
            alignSelf="center"
            flexDirection="row"
            status={type}
            variant={'left-accent'}>
            <VStack space={1} flexShrink={1} w="100%">
              <HStack
                flexShrink={1}
                alignItems="center"
                justifyContent="space-between">
                <HStack space={2} flexShrink={1} alignItems="center">
                  <Alert.Icon />
                  <TextM>{title}</TextM>
                </HStack>
              </HStack>
              {desc !== undefined && (
                <TextM style={{fontSize: 14, paddingLeft: 25}}>{desc}</TextM>
              )}
            </VStack>
          </Alert>
        );
      },
    });
  };

  const toggleSecureEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const renderIcon = () => (
    <TouchableWithoutFeedback onPress={toggleSecureEntry}>
      <MaterialCommunityIcons
        name={secureTextEntry ? 'eye-off' : 'eye'}
        style={{color: '#bdc3c7', fontSize: 20}}
      />
    </TouchableWithoutFeedback>
  );

  // Add User Data
  const onCheckLogin = () => {
    if (phone.length !== 8) {
      showToast('warning', 'ເບີບໍ່ຄົບ 8 ຕົວເລກ!');
    } else if (password === '') {
      showToast('warning', 'ກະລຸນາໃສ່ລະຫັດຜ່ານ!');
    } else {
      onCheckDoctorUser();
    }
  };

  const onCheckDoctorUser = () => {
    Keyboard.dismiss();
    setLoading(true);
    try {
      axios({
        method: 'get',
        url: Config.WP_URL + '/wp/v2/users?roles=doctor&search=' + phone,
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(res => {
          if (res.data.length > 0) {
            onLoginAndgetUserToken();
          } else {
            showToast('warning', 'ເບີໂທ ຫຼື ລະຫັດຜ່ານບໍ່ຖືກຕ້ອງ!');
            setLoading(false);
          }
        })
        .catch(error => {
          console.log(error);
          setLoading(true);
          showToast('error', 'ກະລຸນາກວດສອບຂໍ້ມູນອີກຄັ້ງ!');
        });
    } catch (error) {
      setLoading(false);
      console.log(error);
    }
  };

  const onLoginAndgetUserToken = async () => {
    try {
      await axios({
        method: 'post',
        url: Config.WP_URL + '/api/v1/token',
        data: {
          username: phone,
          password: password,
        },
      })
        .then(async res => {
          let token = res?.data?.jwt_token;
          dispatch(setToken(token));
          await saveUserData(token);
        })
        .catch(error => {
          // console.log(error.response);
          showToast('warning', 'ເບີໂທ ຫຼື ລະຫັດຜ່ານບໍ່ຖືກຕ້ອງ!');

          setLoading(false);
        });
    } catch (error) {
      setLoading(false);
      console.log(error);
    }
  };

  const saveUserData = async token => {
    try {
      await axios({
        method: 'get',
        url: Config.WP_URL + '/wp/v2/doctorlist/?mobile_phone=' + phone,
        headers: {
          Authorization: 'Bearer ' + token,
          'Content-Type': 'application/json',
        },
      })
        .then(async res => {
          let userinfo = res.data[0];
          try {
            await axios({
              method: 'get',
              url: Config.WP_URL + '/wp/v2/users/me',
              headers: {
                Authorization: 'Bearer ' + token,
                'Content-Type': 'application/json',
              },
            })
              .then(async res => {
                let userid = {
                  user_id: res.data.id,
                };
                let combineuserinfo = Object.assign(userinfo, userid);

                dispatch(addDoctorData(combineuserinfo));
                onAddRealTimeStatus(combineuserinfo);
              })

              .catch(error => {
                console.log(error);
                setLoading(false);
              });
          } catch (error) {
            setLoading(false);
            console.log(error);
          }
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  // Add Realtime DB Status
  const onAddRealTimeStatus = async user => {
    try {
      await firebase
        .app()
        .database(Config.FIREBASE_REALTIME_DB)
        .ref(`/doctors/${user.morzapp_id}`)
        .update({
          // firebaseID: firebaseID,
          doctorName: user?.fullname,
          callStatus: 'waiting',
          activeStatus: 'online',
          morzappID: user.morzapp_id,
        })
        .then(data => {
          //success callback
          navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes: [
                {
                  name: 'DoctorHomeStack',
                },
              ],
            }),
          );
          setLoading(false);
          showToast('success', 'ການເຂົ້າສູ່ລະບົບສຳເລັດແລ້ວ!');
        })
        .catch(error => {
          //error callback
          console.log('error ', error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Layout style={styles.container}>
      <Layout style={[styles.layoutCenter, {backgroundColor: '#ffffff'}]}>
        <FastImage
          style={{width: 100, height: 100, marginBottom: 30}}
          source={require('../../../assets/img/doctor.png')}
        />
        {/* <FontAwesome5
          name="user-md"
          style={{color: colors.Blue, fontSize: 50, marginBottom: 20}}
        /> */}
        <TextM>ກະລຸນາໃສ່ເບີໂທ 8 ຕົວເລກຂອງທ່ານ</TextM>
        <Layout
          style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 20,
            marginBottom: 5,
          }}>
          <MaterialCommunityIcons
            name="phone"
            style={{color: colors.Blue, fontSize: 20, marginRight: 5}}
          />
          <TextM style={{marginRight: 10, width: '25%'}}>+856 20</TextM>
          <Input
            style={{width: '100%', flex: 1}}
            placeholder="ເບີໂທ 8 ຕົວເລກ"
            value={phone}
            onChangeText={nextValue =>
              setPhone(nextValue.replace(/[^0-9]/g, ''))
            }
            maxLength={8}
            keyboardType={'numeric'}
            textStyle={{fontFamily: 'NotoSansLao-Regular', fontSize: 18}}
          />
        </Layout>
        {/* Insert Password */}
        <View>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <MaterialCommunityIcons
              name="lock"
              style={{color: colors.Blue, fontSize: 20, marginRight: 5}}
            />
            <TextM style={{marginRight: 10, width: '25%'}}>ລະຫັດຜ່ານ</TextM>
            <Input
              style={{width: '100%', flex: 1}}
              placeholder="**********"
              value={password}
              onChangeText={nextValue => setPassword(nextValue)}
              secureTextEntry={secureTextEntry}
              accessoryRight={renderIcon}
              textStyle={{fontSize: 18}}
            />
          </View>
          {/* <TouchableOpacity
            style={{alignItems: 'flex-end', marginTop: 10}}>
            <TextM style={{color: '#cccccc'}}>ລືມລະຫັດຜ່ານ ?</TextM>
          </TouchableOpacity> */}
        </View>

        <Button
          style={{marginTop: 20, borderRadius: 5, width: '100%'}}
          onPress={() => {
            onCheckLogin();
          }}>
          <TextM style={{color: '#ffffff'}}>ເຂົ້າສູ່ລະບົບ</TextM>
        </Button>
      </Layout>
      {loading && <Loading />}
    </Layout>
  );
};

export default DoctorLogin;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    height: deviceHeight - 120,
  },
  layoutCenter: {
    alignItems: 'center',
    paddingHorizontal: 30,
    paddingTop: 30,
  },
});
