import React, {useEffect, useState} from 'react';
import {StyleSheet, TouchableOpacity, SafeAreaView} from 'react-native';
import {Box, ScrollView, View, HStack} from 'native-base';
import axios from 'axios';
import FastImage from 'react-native-fast-image';
import {Button, Dialog, Portal} from 'react-native-paper';
import {CommonActions} from '@react-navigation/native';
import Config from 'react-native-config';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// Firebase
import firebase from '@react-native-firebase/app';
//Redux package
import {useSelector, useDispatch} from 'react-redux';
//Redux action
import {
  setToken,
  logOutDoctor,
} from '../../../stores/features/userData/userDataSlice';
// Component
import Loading from '../../shareComponents/Loading';
import TextM from '../../shareComponents/TextM';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';

const ProfileDoctor = ({navigation}) => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();
  // useState
  const [loading, setLoading] = useState(false);
  const [showAlert, setShowAlert] = useState(false);

  // useEffect
  useEffect(() => {
    console.log('useEffect ProfileDoctor', userData);
  }, []);

  // Functions
  const numberWithCommas = x => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  const getPubKey = async () => {
    try {
      await axios({
        method: 'get',
        url: Config.WP_URL + '/wp/v2/public_key',
      })
        .then(async res => {
          try {
            await axios({
              method: 'post',
              url: Config.WP_URL + '/api/v1/token',
              data: {
                username: res.data[0].mc,
                password: res.data[0].mcc,
              },
            })
              .then(res => {
                let token = res?.data?.jwt_token;
                dispatch(setToken(token));
                setStatusOffline();
                setLoading(false);
                navigation.dispatch(
                  CommonActions.reset({
                    index: 1,
                    routes: [
                      {
                        name: 'HomeStack',
                      },
                    ],
                  }),
                );
              })
              .catch(error => {
                console.log(error);
              });
          } catch (error) {
            console.log(error);
          }
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const onLogOut = async () => {
    setLoading(true);
    dispatch(logOutDoctor());
    await getPubKey();
  };

  const setStatusOffline = () => {
    firebase
      .app()
      .database(Config.FIREBASE_REALTIME_DB)
      .ref(`/doctors/${userData?.doctorInfo?.morzapp_id}`)
      .update({
        activeStatus: 'offline',
      })
      .then(data => {
        //success callback
      })
      .catch(error => {
        //error callback
        console.log('error ', error);
      });
  };

  const showAlertNow = () => {
    setShowAlert(true);
  };

  const hideAlert = () => {
    setShowAlert(false);
  };

  const getPatientInfo = () => {
    try {
      firestore()
        .collection('callHistory')
        .where('roomID', '==', userData?.doctorInfo?.morzapp_id)
        .orderBy('startedAt', 'desc')
        .limit(30)
        .get()
        .then(querySnapshot => {
          // console.log('data', documentSnapshot);
          let allData = [];
          querySnapshot.forEach(doc => {
            allData.push(doc.data());
          });
          console.log(allData);
          setPatientList(allData);
        })
        .catch(error => {
          console.error('Error read ', error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  // Render
  return (
    <Box flex={1} backgroundColor={colors.White}>
      <SafeAreaView>
        <ScrollView style={{...defaultStyle.fixedLayout}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginVertical: 10,
            }}>
            <View
              style={{
                width: '30%',
                alignItems: 'center',
              }}>
              <FastImage
                style={{width: 80, height: 80, borderRadius: 10}}
                source={
                  userData?.doctorInfo?.picture !== ''
                    ? {uri: userData?.doctorInfo?.picture}
                    : require('../../../assets/img/user.png')
                }
                resizeMode={FastImage.resizeMode.contain}
              />
            </View>
            <View style={{width: '68%'}}>
              <TextM style={{color: colors.DarkLightBlue, fontSize: 20}}>
                {userData?.doctorInfo?.fullname}
              </TextM>
              {/* <TextM style={{color: colors.LightBlack, fontSize: 14}}>
              ອີເມວ:{'  '}
              <TextM style={{color: colors.LightBlack, fontSize: 14}}>
                email@email.com
              </TextM>
            </TextM> */}
              <TextM style={{color: colors.LightBlack, fontSize: 14}}>
                ເບີໂທ:{'  '}
                <TextM style={{color: colors.LightBlack, fontSize: 14}}>
                  020 {userData?.doctorInfo?.mobile_phone}
                </TextM>
              </TextM>
            </View>
          </View>

          {/* Dashboard */}
          {/* <View
          style={{
            marginTop: 10,
          }}>
          <TextM>
            <Icon
              type="FontAwesome5"
              name="chart-bar"
              style={{fontSize: 18, color: colors.DarkBlue}}
            />{' '}
            ສະຖິຕິການໃຊ້ງານ
          </TextM>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              marginTop: 10,
            }}>
            <View style={[styles.boxStyle, {width: '50%'}]}>
              <TextM style={{color: colors.Gray, fontSize: 14}}>
                ຈຳນວນຄັ້ງ
              </TextM>
              <TextM
                style={{color: colors.Blue, fontSize: 26, paddingVertical: 5}}>
                0
              </TextM>
              <TextM style={{color: colors.Gray, fontSize: 14}}>
                ທີ່ໃຫ້ຄຳປຶກສາ
              </TextM>
            </View>
            <View
              style={[
                styles.boxStyle,
                {
                  borderColor: colors.Orange,
                },
              ]}>
              <TextM style={{color: colors.Gray, fontSize: 14}}>
                ຈຳນວນເງິນສະສົມ
              </TextM>
              <TextM
                style={{
                  color: colors.Orange,
                  fontSize: 26,
                  paddingVertical: 5,
                }}>
                {numberWithCommas(0)}
              </TextM>
              <TextM style={{color: colors.Gray, fontSize: 14}}>ກີບ</TextM>
            </View>
          </View>
        </View> */}

          {/* Row Detail */}
          {/* ID */}
          <View style={{marginTop: 10}}>
            <View style={styles.rowDetail}>
              <View
                style={{
                  width: 'auto',
                  justifyContent: 'center',
                }}>
                <FontAwesome5 name="id-badge" style={styles.iconDetail} />
              </View>
              <View style={{width: '90%'}}>
                <TextM>
                  ID:{'  '}
                  <TextM style={styles.textDetail}>
                    {userData?.doctorInfo?.morzapp_id}
                  </TextM>
                </TextM>
              </View>
            </View>
            {/* Specialist */}
            <View style={styles.rowDetail}>
              <View
                style={{
                  width: 'auto',
                  justifyContent: 'center',
                }}>
                <FontAwesome5 name="user-md" style={styles.iconDetail} />
              </View>
              <View style={{width: '90%'}}>
                <TextM>
                  ຊ່ຽວຊານດ້ານ:{'  '}
                  <TextM style={styles.textDetail}>ປິ່ນປົວທົ່ວໄປ</TextM>
                </TextM>
              </View>
            </View>
            {/* Graduated */}
            <View style={styles.rowDetail}>
              <View
                style={{
                  width: 'auto',
                  justifyContent: 'center',
                }}>
                <FontAwesome5 name="graduation-cap" style={styles.iconDetail} />
              </View>
              <View style={{width: '90%'}}>
                <TextM>
                  ຈົບການສຶກສາທີ່:{'  '}
                  <TextM style={styles.textDetail}>
                    {userData?.doctorInfo?.institute}
                  </TextM>
                </TextM>
              </View>
            </View>
            {/* Work Place */}
            <View style={styles.rowDetail}>
              <View
                style={{
                  width: 'auto',
                  justifyContent: 'center',
                }}>
                <FontAwesome5 name="briefcase" style={styles.iconDetail} />
              </View>
              <View style={{width: '90%'}}>
                <TextM>
                  ບ່ອນປະຈຳການ:{'  '}
                  <TextM style={styles.textDetail}>
                    {userData?.doctorInfo?.work_location}
                  </TextM>
                </TextM>
              </View>
            </View>
          </View>

          {/* List Item Details */}
          <TouchableOpacity
            style={{paddingVertical: 10, alignSelf: 'flex-end'}}
            onPress={() => {
              showAlertNow();
            }}>
            <HStack alignItems={'center'}>
              <FontAwesome5
                name="sign-out-alt"
                style={[styles.iconDetail, {color: colors.Red}]}
              />
              <Box>
                <TextM style={{color: colors.Gray}}>ອອກຈາກລະບົບ</TextM>
              </Box>
            </HStack>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
      {/* Modal Box */}
      <Portal>
        <Dialog
          visible={showAlert}
          onDismiss={hideAlert}
          style={{borderRadius: 10}}>
          <Dialog.Title>
            <TextM fontWeight="bold" style={{fontSize: 18}}>
              ອອກຈາກລະບົບ ?{' '}
              <MaterialCommunityIcons
                name="emoticon-sad-outline"
                style={{fontSize: 20, color: '#e74c3c'}}
              />
            </TextM>
          </Dialog.Title>
          <Dialog.Content>
            <TextM>ກະລຸນາກົດຢືນຢັນເພື່ອລົງຊື້ອອກຈາກລະບົບ</TextM>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={() => hideAlert()}>
              <TextM>ກັບຄືນ</TextM>
            </Button>
            <Button
              onPress={() => {
                hideAlert();
                onLogOut();
              }}>
              <TextM style={{color: colors.Red}}>ອອກຈາກລະບົບ</TextM>
            </Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
      {loading && <Loading />}
    </Box>
  );
};

export default ProfileDoctor;

const styles = StyleSheet.create({
  textDetail: {
    color: colors.DarkBlue,
  },
  iconDetail: {
    fontSize: 18,
    color: colors.DarkBlue,
    width: 25,
    textAlign: 'center',
  },
  rowDetail: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 8,
    borderBottomWidth: 1,
    borderBottomColor: '#f1f1f1',
  },
  boxStyle: {
    borderColor: colors.Blue,
    borderWidth: 1,
    borderRadius: 10,
    paddingHorizontal: 15,
    paddingVertical: 8,
    alignItems: 'center',
  },
});
