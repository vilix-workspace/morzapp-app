import React, {useEffect, useState} from 'react';
import {View, Text, Button, TouchableOpacity, StyleSheet} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
// import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
//Redux package
import {useSelector, useDispatch} from 'react-redux';

// Components
// Tab
import Home from './layouts/homeTab/Home';
import HealthAid from './layouts/healthAidTab/HealthAid';
import Pharmacy from './layouts/pharmacyTab/Pharmacy';
import News from './layouts/newsTab/News';
import Profile from './layouts/accountTab/Profile';

// Doctor Service
import DoctorService from './layouts/homeTab/DoctorService';
import DoctorDetail from './layouts/homeTab/DoctorDetail';
import Calling from './layouts/homeTab/Calling';

import AppointmentService from './layouts/homeTab/AppointmentService';
import HPTClinicDetail from './layouts/homeTab/HPTClinicDetail';
import BookingScreen from './layouts/homeTab/BookingScreen';

// Pharmacy
import ProductDetail from './layouts/pharmacyTab/ProductDetail';
import ProductByCat from './layouts/pharmacyTab/ProductByCat';
import MyCart from './layouts/pharmacyTab/MyCart';
import Checkout from './layouts/pharmacyTab/Checkout';

import HomeServices from './layouts/homeTab/HomeServices';
import HealthInsurance from './layouts/homeTab/HealthInsurance';

// News
import SingleNews from './layouts/newsTab/SingleNews';

// Account Layout
import Login from './layouts/accountTab/Login';
import OTPVerification from './layouts/accountTab/OTPVerification';
import OTPVerificationRegis from './layouts/accountTab/OTPVerificationRegis';
import Registration from './layouts/accountTab/Registration';
import CompletedRegis from './layouts/accountTab/CompletedRegis';
import OrderHistory from './layouts/accountTab/OrderHistory';
import ContactCenter from './layouts/accountTab/ContactCenter';
import FAQ from './layouts/accountTab/FAQ';
import ReportUs from './layouts/accountTab/ReportUs';
import PartnerLogin from './layouts/accountTab/PartnerLogin';
import TopupMoney from './layouts/accountTab/TopupMoney';
import TopupHistory from './layouts/accountTab/TopupHistory';
import WalletPayment from './layouts/accountTab/WalletPayment';
import CallHistory from './layouts/accountTab/CallHistory';
import AppointmentHistory from './layouts/accountTab/AppointmentHistory';

// Doctor Account Layout
import DoctorLogin from './doctorLayout/profileTab/DoctorLogin';

// Share Layout
import NotificationList from './shareLayout/NotificationList';
import LiveChat from './shareComponents/LiveChat';
// How to
import HowtoUse from './shareLayout/HowtoUse';
// Policy
import Policy from './shareLayout/Policy';
import CurrentTransaction from './shareLayout/CurrentTransaction';

// Doctor Home Stack
import HomeDoctor from './doctorLayout/homeTab/HomeDoctor';
import ProfileDoctor from './doctorLayout/profileTab/ProfileDoctor';

// Hospital Home Stack
import HomeHospital from './hospitalLayout/homeTab/HomeHospital';
import BookingConfirm from './hospitalLayout/homeTab/BookingConfirm';
import ProfileHospital from './hospitalLayout/accountTab/ProfileHospital';

// Share Style
import {colors} from './shareStyle';
import TextM from './shareComponents/TextM';

const Stack = createStackNavigator();
// const Tab = createBottomTabNavigator();
const Tab = createMaterialBottomTabNavigator();

const sizeTabIcon = 24;

const titleTab = t => {
  return <TextM style={{fontSize: 12, color: colors.Blue}}>{t}</TextM>;
};

const HomeStack = () => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();

  return (
    <Tab.Navigator
      activeColor={colors.Blue}
      inactiveColor={colors.Light}
      barStyle={{
        backgroundColor: colors.White,
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: titleTab('ໜ້າຫຼັກ'),
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name={'home-outline'}
              color={color}
              size={sizeTabIcon}
            />
          ),
          headerShown: false,
        }}
      />
      {/* <Tab.Screen
        name="HealthAid"
        component={HealthAid}
        options={{
          tabBarLabel: 'ຕົວເສີມ',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name={'heart-pulse'}
              color={color}
              size={sizeTabIcon}
            />
          ),
        }}
      /> */}
      <Tab.Screen
        name="Pharmacy"
        component={Pharmacy}
        options={{
          tabBarLabel: titleTab('ຮ້ານຄ້າ'),
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name={'store-outline'}
              color={color}
              size={sizeTabIcon}
            />
          ),
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="News"
        component={News}
        options={{
          tabBarLabel: titleTab('ຂໍ້ມູນຂ່າວສານ'),
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name={'newspaper-variant-multiple-outline'}
              color={color}
              size={sizeTabIcon}
            />
          ),
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="Profile"
        component={userData.userInfo !== null ? Profile : Login}
        options={{
          tabBarLabel: titleTab('ບັນຊີທ່ານ'),
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name={'account-outline'}
              color={color}
              size={sizeTabIcon}
            />
          ),
          headerShown: false,
        }}
      />
    </Tab.Navigator>
  );
};

// Doctor Stack
const DoctorHomeStack = () => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();

  return (
    <Tab.Navigator
      activeColor={colors.Blue}
      inactiveColor={colors.Light}
      barStyle={{
        backgroundColor: colors.White,
      }}>
      <Tab.Screen
        name="HomeDoctor"
        component={HomeDoctor}
        options={{
          tabBarLabel: titleTab('ແດສບອດລາຍລະອຽດ'),
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name={'home-outline'}
              color={color}
              size={sizeTabIcon}
            />
          ),
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="ProfileDoctor"
        component={ProfileDoctor}
        options={{
          tabBarLabel: titleTab('ຈັດການບັນຊີຂອງທ່ານ'),
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name={'account-outline'}
              color={color}
              size={sizeTabIcon}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

// Hospital Stack
const HospitalHomeStack = () => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();

  return (
    <Tab.Navigator
      activeColor={colors.Blue}
      inactiveColor={colors.Light}
      barStyle={{
        backgroundColor: colors.White,
      }}>
      <Tab.Screen
        name="HomeHospital"
        component={HomeHospital}
        options={{
          tabBarLabel: titleTab('ແດສບອດຂໍນັດພົບ'),
          tabBarIcon: ({color}) => (
            <FontAwesome5
              name={'clipboard-list'}
              style={{color: color, fontSize: 20}}
            />
          ),
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="BookingConfirm"
        component={BookingConfirm}
        options={{
          tabBarLabel: titleTab('ຢືນຢັນການຂໍນັດພົບ'),
          tabBarIcon: ({color}) => (
            <FontAwesome5
              name={'calendar-check'}
              style={{color: color, fontSize: 20}}
            />
          ),
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="ProfileHospital"
        component={ProfileHospital}
        options={{
          tabBarLabel: titleTab('ຈັດການບັນຊີ'),
          tabBarIcon: ({color}) => (
            <FontAwesome5
              name={'hospital-user'}
              style={{color: color, fontSize: 20}}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

// Stack Screen
const StackNavigation = () => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;

  // useState
  const [checkLogin, setcheckLogin] = useState(true);

  // useEffect
  useEffect(() => {});

  const goBackHeader = navigation => {
    return (
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={{
          marginLeft: 15,
        }}>
        <MaterialCommunityIcons
          name={'arrow-left-circle'}
          color={colors.DarkLightBlue}
          size={36}
        />
      </TouchableOpacity>
    );
  };

  const goBackHeaderNoTitle = navigation => {
    return (
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={{
          marginLeft: 15,
          // marginTop: 10,
          backgroundColor: '#ffffff',
          borderRadius: 20,
        }}>
        <MaterialCommunityIcons
          name={'arrow-left-circle'}
          color={colors.DarkLightBlue}
          size={36}
        />
      </TouchableOpacity>
    );
  };

  return (
    <Stack.Navigator
      initialRouteName={
        userData?.doctorInfo !== null
          ? 'DoctorHomeStack'
          : userData?.hospitalInfo !== null
          ? 'HospitalHomeStack'
          : userData.userInfo !== null
          ? 'HomeStack'
          : 'Login'
        // 'Calling'
      }>
      <Stack.Screen
        name="HomeStack"
        component={userData.isSkip ? HomeStack : Login}
        options={{headerShown: false}}
      />

      {/* Doctor Home Stack */}
      <Stack.Screen
        name="DoctorHomeStack"
        component={DoctorHomeStack}
        options={{headerShown: false}}
      />

      {/* Hospital Home Stack */}
      <Stack.Screen
        name="HospitalHomeStack"
        component={HospitalHomeStack}
        options={{headerShown: false}}
      />

      {/* Share Layout */}
      <Stack.Screen
        name="NotificationList"
        component={NotificationList}
        options={({navigation}) => ({
          title: 'ການແຈ້ງເຕືອນ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />
      <Stack.Screen
        name="CurrentTransaction"
        component={CurrentTransaction}
        options={({navigation}) => ({
          title: 'ລາຍການທີ່ກຳລັງຢູ່ໃນຂັ້ນຕອນ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />
      <Stack.Screen
        name="LiveChat"
        component={LiveChat}
        options={({navigation}) => ({
          title: 'ສົນທະນາກັບທີມງານເຮົາ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />

      {/* Doctor Service Link*/}
      <Stack.Screen
        name="DoctorService"
        component={DoctorService}
        options={({navigation}) => ({
          title: 'ປຶກສາທ່ານໝໍ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />
      <Stack.Screen
        name="DoctorDetail"
        component={DoctorDetail}
        options={({navigation}) => ({
          title: null,
          headerTransparent: true,
          headerLeft: () => goBackHeaderNoTitle(navigation),
        })}
      />
      <Stack.Screen
        name="Calling"
        component={Calling}
        options={({navigation}) => ({
          headerShown: false,
        })}
      />
      <Stack.Screen
        name="AppointmentService"
        component={AppointmentService}
        options={({navigation}) => ({
          title: 'ນັດພົບທ່ານໝໍ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />
      <Stack.Screen
        name="HPTClinicDetail"
        component={HPTClinicDetail}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="BookingScreen"
        component={BookingScreen}
        options={({navigation}) => ({
          title: 'ນັດຄິວພົບທ່ານໝໍ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />

      <Stack.Screen name="HomeServices" component={HomeServices} />
      <Stack.Screen name="HealthInsurance" component={HealthInsurance} />

      {/* Pharmacy Link*/}
      <Stack.Screen
        name="ProductDetail"
        component={ProductDetail}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="ProductByCat"
        component={ProductByCat}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="MyCart"
        component={MyCart}
        options={({navigation}) => ({
          title: 'ກະຕ່າສິນຄ້າ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />
      <Stack.Screen
        name="Checkout"
        component={Checkout}
        options={({navigation}) => ({
          title: 'ສະຫລຸບການສັ່ງຊື້',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />

      {/* How to */}
      <Stack.Screen
        name="HowtoUse"
        component={HowtoUse}
        options={({navigation}) => ({
          title: 'ວິທີການໃຊ້ງານ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />
      {/* Policy */}
      <Stack.Screen
        name="Policy"
        component={Policy}
        options={({navigation}) => ({
          title: 'ເງື່ອນໄຂ ແລະ ການໃຫ້ບໍລິການ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />

      {/* News */}
      <Stack.Screen
        name="SingleNews"
        component={SingleNews}
        options={({navigation}) => ({
          title: 'ຂ່າວສານ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />

      {/* Account Link */}
      <Stack.Screen
        name="OTPVerification"
        component={OTPVerification}
        options={({navigation}) => ({
          title: null,
          headerTransparent: true,
          headerLeft: () => goBackHeaderNoTitle(navigation),
        })}
      />

      <Stack.Screen
        name="Registration"
        component={Registration}
        options={({navigation}) => ({
          title: 'ລົງທະບຽນໃໝ່',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />
      <Stack.Screen
        name="OTPVerificationRegis"
        component={OTPVerificationRegis}
        options={({navigation}) => ({
          title: null,
          headerTransparent: true,
          headerLeft: () => goBackHeaderNoTitle(navigation),
        })}
      />
      <Stack.Screen
        name="CompletedRegis"
        component={CompletedRegis}
        options={({navigation}) => ({
          title: null,
          headerShown: false,
        })}
      />

      <Stack.Screen
        name="OrderHistory"
        component={OrderHistory}
        options={({navigation}) => ({
          title: 'ປະຫວັດການສັ່ງຊື້ສິນຄ້າ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />
      <Stack.Screen
        name="ContactCenter"
        component={ContactCenter}
        options={({navigation}) => ({
          title: 'ຕິດຕໍ່ສູນບໍລິການລູກຄ້າ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />
      <Stack.Screen
        name="FAQ"
        component={FAQ}
        options={({navigation}) => ({
          title: 'ຖາມ-ຕອບ FAQ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />
      <Stack.Screen
        name="ReportUs"
        component={ReportUs}
        options={({navigation}) => ({
          title: 'ລາຍງານບັນຫາໃຫ້ພວກເຮົາຊາບ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />

      {/* Partner Login */}
      <Stack.Screen
        name="PartnerLogin"
        component={PartnerLogin}
        options={({navigation}) => ({
          title: 'ສຳລັບທີ່ປຶກສາຜູ້ຊ່ຽວຊານ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />

      {/* Topup Money */}
      <Stack.Screen
        name="TopupMoney"
        component={TopupMoney}
        options={({navigation}) => ({
          title: 'ເຕີມເງິນເຂົ້າບັນຊີ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />
      <Stack.Screen
        name="TopupHistory"
        component={TopupHistory}
        options={({navigation}) => ({
          title: 'ປະຫວັດການເຕີມເງິນ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />
      <Stack.Screen
        name="WalletPayment"
        component={WalletPayment}
        options={({navigation}) => ({
          title: 'ປະຫວັດການໃຊ້ຈ່າຍ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />
      <Stack.Screen
        name="CallHistory"
        component={CallHistory}
        options={({navigation}) => ({
          title: 'ປະຫວັດໂທປຶກສາໝໍ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />
      <Stack.Screen
        name="AppointmentHistory"
        component={AppointmentHistory}
        options={({navigation}) => ({
          title: 'ປະຫວັດການນັດພົບໝໍ',
          headerTitleStyle: styles.headerTitle,
          headerLeft: () => goBackHeader(navigation),
        })}
      />
    </Stack.Navigator>
  );
};

export default StackNavigation;

const styles = StyleSheet.create({
  headerTitle: {
    fontFamily: 'NotoSansLao-Regular',
    fontSize: 18,
  },
});
