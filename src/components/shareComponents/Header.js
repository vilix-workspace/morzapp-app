import React, {useEffect, useState} from 'react';
import {View} from 'native-base';
import {Image, SafeAreaView} from 'react-native';
import {TopNavigation} from '@ui-kitten/components';
import {TouchableOpacity} from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import {colors} from '../shareStyle';

const Header = ({navigation}) => {
  const renderTitle = () => (
    <View>
      <FastImage
        style={{width: 120, height: 40}}
        resizeMode="contain"
        source={require('../../assets/img/morzapp-logo-hor-660x180.png')}
      />
    </View>
  );

  const renderRightNotification = () => (
    <View style={{flexDirection: 'row'}}>
      <TouchableOpacity
        style={{marginRight: 10}}
        onPress={() => navigation.navigate('CurrentTransaction')}>
        <Image
          style={{width: 40, height: 40}}
          source={require('../../assets/img/process.gif')}
        />
      </TouchableOpacity>
      {/* <TouchableOpacity onPress={() => navigation.navigate('NotificationList')}>
        <MaterialCommunityIcons
          name={'bell-ring'}
          style={{color: colors.Orange, fontSize: 28}}
        />
      </TouchableOpacity> */}
    </View>
  );

  return (
    <SafeAreaView>
      <TopNavigation
        style={{
          paddingHorizontal: 15,
          borderBottomColor: '#ebebeb',
          borderBottomWidth: 1,
        }}
        title={renderTitle}
        accessoryRight={renderRightNotification}
      />
    </SafeAreaView>
  );
};

export default Header;
