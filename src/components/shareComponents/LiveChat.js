import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  Linking,
  TouchableOpacity,
} from 'react-native';
import {Box, ScrollView, View, Text} from 'native-base';
import {WebView} from 'react-native-webview';

const LiveChat = ({navigation}) => {
  // useState
  const [state, setState] = useState(null);

  // useEffect
  useEffect(() => {
    // console.log('useEffect LiveChat');
  }, []);

  return (
    // <Box style={{position: 'absolute', bottom: 0, right: 0}}>
    //   <TouchableOpacity
    //     onPress={() => {
    //       let msg = 'Hello MORZAPP';
    //       // navigation.navigate('LiveChat');
    //       Linking.openURL(
    //         'whatsapp://send?text=' + msg + '&phone=+8562076423996',
    //       )
    //         .then((data) => {
    //           console.log('WhatsApp Opened', data);
    //         })
    //         .catch((err) => {
    //           // alert('Make sure Whatsapp installed on your device');
    //           console.log(err.response);
    //         });
    //     }}
    //     style={{
    //       margin: 15,
    //       backgroundColor: '#40bffc',
    //       padding: 8,
    //       borderRadius: 20,
    //     }}>
    //     <Icon
    //       type="MaterialCommunityIcons"
    //       name={'chat'}
    //       style={{color: '#ffffff', fontSize: 24}}
    //     />
    //   </TouchableOpacity>
    // </Box>
    <WebView
      source={{
        uri: 'https://tawk.to/chat/613ae898d326717cb680b214/1ff71p5b0',
      }}
      style={{marginBottom: 30}}
    />
  );
};

export default LiveChat;

const styles = StyleSheet.create({});
