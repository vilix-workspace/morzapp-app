import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import {View, Text} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// Color
import {colors} from '../shareStyle';
import TextM from './TextM';

const TopicH = ({title, icon}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
      }}>
      <MaterialCommunityIcons
        name={icon}
        style={{color: colors.DarkLightBlue, fontSize: 22, marginRight: 5}}
      />
      <TextM
        fontWeight="bold"
        style={{
          fontSize: 18,
          color: colors.DarkLightBlue,
        }}>
        {title}
      </TextM>
    </View>
  );
};

export default TopicH;

const styles = StyleSheet.create({});
