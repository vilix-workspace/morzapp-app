import React from 'react';
import {ActivityIndicator} from 'react-native';
import {View} from 'native-base';
import TextM from './TextM';

const Loading = () => {
  return (
    <View
      style={{
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(255, 255, 255, 0.9)',
      }}>
      <ActivityIndicator size="large" color="#ed3293" />
      <TextM style={{marginTop: 15}}>ກຳລັງໂຫລດ...</TextM>
    </View>
  );
};

export default Loading;
