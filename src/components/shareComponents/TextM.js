import React from 'react';
import {Text} from 'react-native';

const TextM = props => {
  return (
    <Text
      {...props}
      allowFontScaling={false}
      style={[
        {
          fontFamily:
            props?.fontWeight === 'bold'
              ? 'NotoSansLao-SemiBold'
              : 'NotoSansLao-Regular',
          fontSize: 16,
          color: '#333333',
        },
        props.style,
      ]}>
      {props.children}
    </Text>
  );
};

export default TextM;
