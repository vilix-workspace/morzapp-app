import React, {useEffect, useState} from 'react';
import {StyleSheet, TouchableOpacity, Dimensions, Keyboard} from 'react-native';
import {
  Box,
  ScrollView,
  View,
  Text,
  Button,
  useToast,
  Alert,
  HStack,
  VStack,
} from 'native-base';
import {Layout, Input, Calendar} from '@ui-kitten/components';
import {Button as ButtonN, Dialog, Portal} from 'react-native-paper';
import DatePicker from 'react-native-date-picker';
import axios from 'axios';
import {CommonActions} from '@react-navigation/native';
import Config from 'react-native-config';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import moment from 'moment';
//Redux package
import {useSelector, useDispatch} from 'react-redux';
// Component
import Loading from '../../shareComponents/Loading';
import TextM from '../../shareComponents/TextM';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';

const width = Dimensions.get('window').width;

var gender_props = [
  {label: 'ຊາຍ', value: 'male'},
  {label: 'ຍິງ', value: 'female'},
];

const BookingScreen = ({navigation, route}) => {
  const toast = useToast();
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();
  // Params
  const {hospital_info} = route.params;
  // useState
  const [patientName, setPatientName] = useState('');
  const [age, setAge] = useState('');
  const [activeGenderIndex, setActiveGenderIndex] = useState(0);
  const [activeGenderValue, setActiveGenderValue] = useState('male');
  const [reason, setReason] = useState('');
  const [phone, setPhone] = useState('');
  const [symptom, setSymptom] = useState('');
  const [bookingDate, setBookingDate] = useState(new Date());
  const [bookingTime, setBookingTime] = useState(false);
  const [showAlert, setShowAlert] = useState(false);
  const [showCannotPickDateAlert, setShowCannotPickDateAlert] = useState(false);
  const [openTime, setOpenTime] = useState(false);
  const [loading, setLoading] = useState(false);

  // Functions
  const showToast = (type, title, desc) => {
    toast.show({
      render: () => {
        return (
          <Alert
            maxWidth="95%"
            alignSelf="center"
            flexDirection="row"
            status={type}
            variant={'left-accent'}>
            <VStack space={1} flexShrink={1} w="100%">
              <HStack
                flexShrink={1}
                alignItems="center"
                justifyContent="space-between">
                <HStack space={2} flexShrink={1} alignItems="center">
                  <Alert.Icon />
                  <TextM>{title}</TextM>
                </HStack>
              </HStack>
              {desc !== undefined && (
                <TextM style={{fontSize: 14, paddingLeft: 25}}>{desc}</TextM>
              )}
            </VStack>
          </Alert>
        );
      },
    });
  };

  const onCheckBooking = () => {
    if (
      patientName === '' ||
      age === '' ||
      activeGenderValue === '' ||
      phone === '' ||
      reason === '' ||
      symptom === '' ||
      bookingDate === false ||
      bookingTime === false
    ) {
      showToast('warning', 'ຂໍອະໄພ ຂໍ້ມູນບໍ່ຄົບຖ້ວນ!');
    } else {
      Keyboard.dismiss();
      showAlertNow();
    }
  };

  const onBooking = () => {
    hideAlert();
    setLoading(true);
    try {
      axios({
        method: 'post',
        url: Config.WP_URL + '/wp/v2/mzbooking',
        data: {
          fields: {
            patient_name: patientName,
            age: age,
            gender: activeGenderValue,
            phone: phone,
            reason: reason,
            symptom: symptom,
            booking_time: bookingDate,
            hospital_id: hospital_info?.hospital_clinic_id,
            hospital_name: hospital_info?.hospitalclinic_name,
            contact_name: userData?.userInfo?.name,
            contact_phone: userData?.userInfo?.phone,
            user_id: userData?.userInfo?.user_id,
            booking_status: 'pending',
          },
          status: 'publish',
        },
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(res => {
          console.log(res);
          setLoading(false);
          navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes: [
                {
                  name: 'HomeStack',
                },
                {
                  name: 'CurrentTransaction',
                },
              ],
            }),
          );
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  // Alert
  const showAlertNow = () => {
    setShowAlert(true);
  };

  const hideAlert = () => {
    setShowAlert(false);
  };

  const datesAreOnSameDay = (first, second) =>
    first.getFullYear() === second.getFullYear() &&
    first.getMonth() === second.getMonth() &&
    first.getDate() === second.getDate();

  const onCheckDate = date => {
    const today = new Date();
    if (dateInPast(date, today)) {
      if (datesAreOnSameDay(date, today)) {
        setBookingDate(date);
        setBookingTime(false);
      } else {
        setBookingDate(today);
        setShowCannotPickDateAlert(true);
        setBookingTime(false);
      }
    } else {
      // Future Date
      setBookingDate(date);
      setBookingTime(false);
    }

    // console.log(dateInPast(date, today));
  };

  var dateInPast = function (firstDate, secondDate) {
    if (firstDate.setHours(0, 0, 0, 0) <= secondDate.setHours(0, 0, 0, 0)) {
      return true;
    }

    return false;
  };

  // useEffect
  useEffect(() => {
    console.log('useEffect BookingScreen', userData);
  }, []);

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <ScrollView style={{...defaultStyle.fixedLayout}}>
        <TextM>ກະລຸນາເພີ່ມຂໍ້ມູນໃຫ້ຄົບຖ້ວນ</TextM>

        {/* Patient Name */}
        <Layout style={{marginTop: 5}}>
          <Input
            value={patientName}
            label={
              <HStack alignItems={'center'} space={1}>
                <MaterialCommunityIcons
                  name="account-outline"
                  style={{color: colors.DarkBlue, fontSize: 16}}
                />
                <TextM style={{color: colors.DarkBlue, paddingTop: 4}}>
                  ຊື່ຄົນເຈັບ<Text style={{color: colors.Red}}>*</Text>
                </TextM>
              </HStack>
            }
            placeholder="ຊື່ ແລະ ນາມສະກຸນ"
            onChangeText={nextValue => setPatientName(nextValue)}
            textStyle={{
              minHeight: 30,
              fontFamily: 'NotoSansLao-Regular',
            }}
          />
        </Layout>

        {/* Age */}
        <Layout style={{marginTop: 10}}>
          <Input
            value={age}
            label={
              <HStack alignItems={'center'} space={1}>
                <MaterialCommunityIcons
                  name="cake"
                  style={{color: colors.DarkBlue, fontSize: 16}}
                />
                <TextM style={{color: colors.DarkBlue}}>
                  ອາຍຸ<Text style={{color: colors.Red}}>*</Text>
                </TextM>
              </HStack>
            }
            placeholder="23"
            onChangeText={nextValue => setAge(nextValue)}
            textStyle={{
              minHeight: 30,
              fontFamily: 'NotoSansLao-Regular',
            }}
            keyboardType={'numeric'}
            maxLength={2}
          />
        </Layout>

        {/* Gender */}
        <Layout style={{marginTop: 10}}>
          <TextM style={{marginBottom: 5, color: colors.DarkBlue}}>
            <MaterialCommunityIcons
              name="gender-male-female"
              style={{color: colors.DarkBlue, fontSize: 20}}
            />{' '}
            ເພດ<Text style={{color: colors.Red}}>*</Text>
          </TextM>
          <View style={styles.columncontainer}>
            {gender_props.map((data, i) => {
              let index = i;
              return (
                <TouchableOpacity
                  key={i}
                  onPress={() => {
                    setActiveGenderIndex(index);
                    setActiveGenderValue(data.value);
                  }}>
                  <View
                    style={[
                      styles.two_columnlayout,
                      {
                        backgroundColor:
                          activeGenderIndex == i ? colors.Blue : 'transparent',
                        flexDirection: 'row',
                        justifyContent: 'center',
                      },
                    ]}>
                    <MaterialCommunityIcons
                      name={
                        data.value === 'male' ? 'gender-male' : 'gender-female'
                      }
                      style={{
                        color:
                          activeGenderIndex == i ? colors.White : colors.Black,
                      }}
                    />
                    <TextM
                      style={{
                        color:
                          activeGenderIndex == i ? colors.White : colors.Black,
                        paddingLeft: 10,
                      }}>
                      {data?.label}
                    </TextM>
                  </View>
                </TouchableOpacity>
              );
            })}
          </View>
        </Layout>

        {/* Phone */}
        <Layout>
          <Input
            value={phone}
            label={
              <HStack alignItems={'center'} space={1}>
                <MaterialCommunityIcons
                  name="phone"
                  style={{color: colors.DarkBlue, fontSize: 16}}
                />
                <TextM
                  style={{
                    color: colors.DarkBlue,
                    paddingTop: 4,
                  }}>
                  ເບີໂທຕິດຕໍ່<Text style={{color: colors.Red}}>*</Text>
                </TextM>
              </HStack>
            }
            placeholder="020 1234 5678"
            onChangeText={nextValue => setPhone(nextValue)}
            textStyle={{
              minHeight: 30,
              fontFamily: 'NotoSansLao-Regular',
            }}
            keyboardType={'numeric'}
          />
        </Layout>

        {/* Reason */}
        <Layout style={{marginTop: 5}}>
          <Input
            value={reason}
            label={
              <HStack alignItems={'center'} space={1}>
                <MaterialCommunityIcons
                  name="file-edit-outline"
                  style={{color: colors.DarkBlue, fontSize: 16}}
                />
                <TextM style={{color: colors.DarkBlue, paddingTop: 5}}>
                  ສາເຫດທີ່ຢາກພົບໝໍ<Text style={{color: colors.Red}}>*</Text>
                </TextM>
              </HStack>
            }
            placeholder="ສາເຫດ..."
            onChangeText={nextValue => setReason(nextValue)}
            textStyle={{
              minHeight: 60,
              fontFamily: 'NotoSansLao-Regular',
              textAlignVertical: 'top',
            }}
            multiline={true}
          />
        </Layout>

        {/* Symptom */}
        <Layout style={{marginTop: 5}}>
          <Input
            value={symptom}
            label={
              <HStack alignItems={'center'} space={1}>
                <MaterialCommunityIcons
                  name="clipboard-list-outline"
                  style={{color: colors.DarkBlue, fontSize: 16}}
                />
                <TextM style={{color: colors.DarkBlue, paddingTop: 5}}>
                  ອາການເບື້ອງຕົ້ນ<Text style={{color: colors.Red}}>*</Text>
                </TextM>
              </HStack>
            }
            placeholder="ອາການ..."
            onChangeText={nextValue => setSymptom(nextValue)}
            textStyle={{
              minHeight: 60,
              fontFamily: 'NotoSansLao-Regular',
              textAlignVertical: 'top',
            }}
            multiline={true}
          />
        </Layout>

        {/* Booking Date & Time */}
        <Layout style={{marginTop: 10, marginBottom: 20}}>
          <TextM style={{marginBottom: 5, color: colors.DarkBlue}}>
            <MaterialCommunityIcons
              name="calendar-clock"
              style={{color: colors.DarkBlue, fontSize: 16}}
            />{' '}
            ວັນ ແລະ ເວລາສຳລັບການຈອງຄິວ<Text style={{color: colors.Red}}>*</Text>
          </TextM>
          <TextM style={{color: colors.Green}}>
            ກະລຸນາເລືອກວັນທີ<Text style={{color: colors.Red}}>*</Text>
          </TextM>
          <View
            style={{
              alignItems: 'center',
            }}>
            <Calendar
              date={bookingDate}
              // onSelect={nextDate => setBookingDate(nextDate)}
              onSelect={nextDate => onCheckDate(nextDate)}
              style={{width: '100%', marginTop: 10}}
            />
          </View>

          <TextM style={{marginTop: 10, color: colors.Green}}>
            ກະລຸນາເລືອກເວລາ<Text style={{color: colors.Red}}>*</Text>
          </TextM>
          <TouchableOpacity
            style={{
              alignItems: 'center',
              backgroundColor: bookingTime ? colors.Green : '#ebebeb',
              paddingVertical: 8,
              marginTop: 4,
              borderRadius: 5,
              flexDirection: 'row',
              justifyContent: 'center',
            }}
            onPress={() => setOpenTime(true)}>
            <TextM>
              {bookingTime
                ? moment(bookingDate).format('ເວລາ hh:mm a')
                : 'ເລືອກເວລານັດພົບໝໍ'}
            </TextM>
            {bookingTime && (
              <MaterialCommunityIcons
                name="clock-check"
                style={{color: colors.White, fontSize: 24, marginLeft: 10}}
              />
            )}
          </TouchableOpacity>
          <View
            style={{
              alignItems: 'center',
            }}>
            <DatePicker
              mode="time"
              open={openTime}
              date={bookingDate}
              modal
              // onDateChange={setBookingDate}
              locale="en"
              is24hourSource="locale"
              textColor="#ffffff"
              onConfirm={date => {
                setOpenTime(false);
                setBookingDate(date);
                setBookingTime(true);
              }}
              onCancel={() => {
                setOpenTime(false);
              }}
            />
          </View>
          <TextM note style={{marginTop: 10, color: colors.Red}}>
            * ໝາຍເຫດ:
            ໃນກໍລະນິທ່ານປຽນວັນທີນັດພົບໃໝ່ກະລຸນາລະບຸເວລານັດພົບໃໝ່ອີກຄັ້ງ
          </TextM>
        </Layout>
        <View
          style={{
            paddingVertical: 5,
            paddingHorizontal: 20,
            alignItems: 'center',
          }}>
          <View style={{justifyContent: 'center', width: '100%'}}>
            <Button
              full
              onPress={() => {
                onCheckBooking();
              }}
              style={{
                backgroundColor: colors.DarkLightBlue,
                borderRadius: 5,
                marginBottom: 15,
              }}>
              <TextM style={{color: colors.White}}>ນັດຈອງເລີຍ</TextM>
            </Button>
          </View>
        </View>
      </ScrollView>

      {/* Cannot Select Past Date */}
      <Portal>
        <Dialog
          visible={showCannotPickDateAlert}
          onDismiss={() => setShowCannotPickDateAlert(false)}
          style={{borderRadius: 10}}>
          <Dialog.Title>
            <MaterialCommunityIcons
              name="alert-circle"
              style={{color: colors.Orange, fontSize: 20, marginLeft: 10}}
            />
            <TextM fontWeight="bold" style={{fontSize: 18}}>
              {' '}
              ແຈ້ງເຕືອນ
            </TextM>
          </Dialog.Title>
          <Dialog.Content>
            <TextM>ທ່ານບໍ່ສາມາດເລືອກວັນທີທີ່ຜ່ານມາແລ້ວໄດ້ ກະລຸນາເລືອກໃໝ່</TextM>
          </Dialog.Content>
          <Dialog.Actions>
            <ButtonN onPress={() => setShowCannotPickDateAlert(false)}>
              <TextM>ຕົງລົງ</TextM>
            </ButtonN>
          </Dialog.Actions>
        </Dialog>
      </Portal>

      {/* Confirm Booking */}
      <Portal>
        <Dialog
          visible={showAlert}
          onDismiss={hideAlert}
          style={{borderRadius: 10}}>
          <Dialog.Title>
            <TextM fontWeight="bold" style={{fontSize: 18}}>
              ຢືນຢັນການຈອງຄິວເຂົ້າພົບໝໍ ?
            </TextM>
          </Dialog.Title>
          <Dialog.Content>
            <TextM>ກະລຸນາກົດຢືນຢັນການຈອງຄິວເຂົ້າພົບໝໍ</TextM>
          </Dialog.Content>
          <Dialog.Actions>
            <ButtonN onPress={() => hideAlert()}>
              <TextM style={{color: colors.Gray}}>ກັບຄືນ</TextM>
            </ButtonN>
            <ButtonN
              onPress={() => {
                onBooking();
              }}>
              <TextM style={{color: colors.Green}}>ຢືນຢັນການນັດພົບ</TextM>
            </ButtonN>
          </Dialog.Actions>
        </Dialog>
      </Portal>

      {/* Loading  */}
      {loading && <Loading />}
    </Box>
  );
};

export default BookingScreen;

const styles = StyleSheet.create({
  columncontainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  two_columnlayout: {
    padding: 10,
    borderColor: '#ececec',
    borderWidth: 1,
    borderRadius: 5,
    width: width / 2.6,
    marginBottom: 10,
    alignItems: 'center',
    paddingVertical: 15,
  },
});
