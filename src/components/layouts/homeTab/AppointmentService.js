import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
  RefreshControl,
} from 'react-native';
import {Box, ScrollView, View, VStack, HStack} from 'native-base';
import FastImage from 'react-native-fast-image';
import axios from 'axios';
import LottieView from 'lottie-react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import Config from 'react-native-config';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import firebase from '@react-native-firebase/app';
// Redux
import {useSelector, useDispatch} from 'react-redux';

// Custom Style
import {defaultStyle, colors} from '../../shareStyle';
import TextM from '../../shareComponents/TextM';
import TopicH from '../../shareComponents/TopicH';

const width = Dimensions.get('window').width;

const AppointmentService = ({navigation}) => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();
  // useState
  const [featureHospitals, setFeatureHospitals] = useState([]);
  const [allHospitals, setAllHospitals] = useState([]);
  const [realtimeHospitalsStatus, setRealtimeHospitalsStatus] = useState(null);
  const [refreshing, setRefreshing] = useState(false);

  const imageWidth = 600; // Replace with the actual width of the image
  const imageHeight = 300; // Replace with the actual height of the image
  const aspectRatio = imageWidth / imageHeight;

  // Functions
  const onRefresh = () => {
    setRefreshing(true);
    try {
      getHospitalList();
      getFeatureHospitalList();
      setRefreshing(false);
    } catch (error) {
      console.log(error);
    }
  };

  const getHospitalList = async () => {
    try {
      await axios({
        method: 'get',
        url: Config.WP_URL + '/wp/v2/hospitals',
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
        },
      })
        .then(res => {
          // console.log(res.data);
          setAllHospitals(res.data);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const getFeatureHospitalList = async () => {
    try {
      await axios({
        method: 'get',
        url: Config.WP_URL + '/wp/v2/hospitals?hospital_tag=53',
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
        },
      })
        .then(res => {
          // console.log(res.data);
          setFeatureHospitals(res.data);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const getHospitalStatus = mzid => {
    if (realtimeHospitalsStatus !== null) {
      let status = realtimeHospitalsStatus.find(
        item => item.morzappID === mzid,
      );
      if (typeof status != 'undefined') {
        if (status.activeStatus === 'online') {
          return 'ສາມາດນັດຈອງໄດ້';
        } else if (status.activeStatus === 'offline') {
          return 'ຍັງບໍ່ສາມາດນັດຈອງໄດ້';
        }
      } else {
        return 'ຍັງບໍ່ສາມາດນັດຈອງໄດ້';
      }
    } else {
      return '';
    }
  };

  // Render Cat
  const renderHPTItem = ({item}) => (
    <TouchableOpacity
      onPress={() =>
        navigation.navigate('HPTClinicDetail', {hospital_info: item})
      }>
      <Box
        style={{
          marginRight: 10,
          padding: 8,
          borderRadius: 10,
          backgroundColor: colors.LightWhite,
          width: 200,
        }}>
        <FastImage
          source={{
            uri: item?.picture,
          }}
          style={{width: '100%', aspectRatio, borderRadius: 10}}
          resizeMode={FastImage.resizeMode.cover}
        />
        <TextM
          fontWeight="bold"
          style={{marginTop: 5, color: colors.DarkBlue}}
          numberOfLines={1}>
          {item?.hospitalclinic_name}
        </TextM>
        <HStack space={1} alignItems="center">
          <MaterialCommunityIcons
            name="map-marker"
            style={{
              color: colors.Pink,
              fontSize: 16,
            }}
          />
          <TextM numberOfLines={1} style={{fontSize: 14}}>
            {item?.address}{' '}
          </TextM>
        </HStack>
      </Box>
    </TouchableOpacity>
  );

  // useEffect
  useEffect(() => {
    getHospitalList();
    getFeatureHospitalList();

    // Firebase Real-time Database
    const onValueChange = firebase
      .app()
      .database(Config.FIREBASE_REALTIME_DB)
      .ref(`/hospitals`)
      .on('value', snapshot => {
        // console.log('hospital data RT change: ', snapshot.val());
        setRealtimeHospitalsStatus(Object.values(snapshot.val()));
      });

    // Stop listening for updates when no longer required
    return () =>
      firebase.database().ref(`/doctors`).off('value', onValueChange);
  }, []);

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <ScrollView
        style={{...defaultStyle.fixedLayout}}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        {featureHospitals.length > 0 ? (
          <>
            <TopicH
              title="ໂຮງຫມໍ ແລະ ຄຣີນິກທີ່ໄດ້ເຂົ້າຮ່ວມ"
              icon="hospital-box"
            />
            <FlatList
              style={{marginTop: 10}}
              horizontal
              data={featureHospitals}
              renderItem={renderHPTItem}
              keyExtractor={item => item.id}
            />
          </>
        ) : (
          <SkeletonPlaceholder>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <View
                style={{
                  height: 25,
                  width: 240,
                  borderRadius: 10,
                  marginBottom: 10,
                }}
              />
            </View>
            <View style={{flexDirection: 'row'}}>
              <View
                style={{
                  width: 220,
                  height: 160,
                  borderRadius: 10,
                  marginRight: 10,
                }}
              />
              <View
                style={{
                  width: 220,
                  height: 160,
                  borderRadius: 10,
                }}
              />
            </View>
          </SkeletonPlaceholder>
        )}

        <VStack style={{marginTop: 10}}>
          <TopicH
            title="ລາຍຊື່ໂຮງຫມໍ ແລະ ຄຣີນິກທັງໝົດ"
            icon="hospital-building"
          />
          {allHospitals.length > 0 ? (
            allHospitals.map((item, i) => {
              return (
                <TouchableOpacity
                  key={i}
                  style={{marginBottom: 10, marginTop: 5}}
                  onPress={() =>
                    navigation.navigate('HPTClinicDetail', {
                      hospital_info: item,
                    })
                  }>
                  <HStack space={3}>
                    <Box
                      style={{
                        justifyContent: 'center',
                      }}>
                      <FastImage
                        source={{
                          uri: item?.picture,
                        }}
                        resizeMode={FastImage.resizeMode.cover}
                        style={{width: 100, height: 80, borderRadius: 5}}
                      />
                    </Box>
                    <VStack
                      style={{
                        width: '68%',
                      }}>
                      <TextM
                        fontWeight="bold"
                        numberOfLines={1}
                        style={{color: colors.DarkLightBlue}}>
                        {item?.hospitalclinic_name}
                      </TextM>
                      <HStack space={1} alignItems={'center'}>
                        <MaterialCommunityIcons
                          name="map-marker"
                          style={{
                            color: colors.Pink,
                            fontSize: 14,
                          }}
                        />
                        <TextM
                          style={{fontSize: 14, color: colors.LightBlack}}
                          numberOfLines={1}>
                          {item?.address}
                        </TextM>
                      </HStack>

                      <HStack space={1} alignItems={'center'}>
                        <MaterialCommunityIcons
                          name="clock-outline"
                          style={{
                            color: colors.Pink,
                            fontSize: 14,
                          }}
                        />
                        <TextM
                          style={{fontSize: 14, color: colors.LightBlack}}
                          numberOfLines={1}>
                          {item?.open_time + ' - ' + item?.close_time}
                        </TextM>
                      </HStack>

                      <HStack space={1} alignItems={'center'}>
                        <MaterialCommunityIcons
                          name={
                            getHospitalStatus(item?.hospital_clinic_id) ===
                            'ສາມາດນັດຈອງໄດ້'
                              ? 'calendar'
                              : 'calendar-lock'
                          }
                          style={{
                            color:
                              getHospitalStatus(item?.hospital_clinic_id) ===
                              'ສາມາດນັດຈອງໄດ້'
                                ? colors.Green
                                : colors.Red,
                            fontSize: 14,
                          }}
                        />
                        <TextM
                          style={{
                            fontSize: 13,
                            color:
                              getHospitalStatus(item?.hospital_clinic_id) ===
                              'ສາມາດນັດຈອງໄດ້'
                                ? colors.Green
                                : colors.Red,
                          }}
                          numberOfLines={1}>
                          {getHospitalStatus(item?.hospital_clinic_id)}
                        </TextM>
                      </HStack>
                    </VStack>
                  </HStack>
                </TouchableOpacity>
              );
            })
          ) : (
            <SkeletonPlaceholder>
              {['1', '2', '3', '4', '5'].map((l, i) => {
                return (
                  <View key={i} style={{flexDirection: 'row', marginTop: 10}}>
                    <View
                      style={{
                        width: 100,
                        height: 60,
                        borderRadius: 10,
                        marginRight: 10,
                      }}
                    />
                    <View>
                      <View
                        style={{
                          width: 150,
                          height: 18,
                          borderRadius: 8,
                          marginBottom: 5,
                        }}
                      />
                      <View
                        style={{
                          width: 200,
                          height: 15,
                          borderRadius: 5,
                          marginBottom: 5,
                        }}
                      />
                      <View
                        style={{
                          width: 200,
                          height: 15,
                          borderRadius: 5,
                          marginBottom: 5,
                        }}
                      />
                    </View>
                  </View>
                );
              })}
            </SkeletonPlaceholder>
          )}
        </VStack>
      </ScrollView>
    </Box>
  );
};

export default AppointmentService;

const styles = StyleSheet.create({
  container: {},
});
