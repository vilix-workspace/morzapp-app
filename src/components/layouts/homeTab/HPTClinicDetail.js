import React, {useEffect, useState} from 'react';
import {StyleSheet, useWindowDimensions, SafeAreaView} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {
  Box,
  ScrollView,
  View,
  HStack,
  VStack,
  Alert,
  useToast,
} from 'native-base';
import FastImage from 'react-native-fast-image';
import {Button as ButtonN, Dialog, Portal} from 'react-native-paper';
import RenderHtml, {
  HTMLElementModel,
  HTMLContentModel,
  defaultSystemFonts,
} from 'react-native-render-html';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Config from 'react-native-config';
import firebase from '@react-native-firebase/app';
// Redux
import {useSelector, useDispatch} from 'react-redux';

// Custom Style
import {defaultStyle, colors} from '../../shareStyle';
import TextM from '../../shareComponents/TextM';
import TopicH from '../../shareComponents/TopicH';

const HPTClinicDetail = ({navigation, route}) => {
  const toast = useToast();
  const {width} = useWindowDimensions();
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();
  // Params
  const {hospital_info} = route.params;
  // useState
  const [showAlert, setShowAlert] = useState(false);
  const [realtimeHospitalsInfo, setRealtimeHospitalsInfo] = useState(null);

  const imageWidth = 600; // Replace with the actual width of the image
  const imageHeight = 300; // Replace with the actual height of the image
  const aspectRatio = imageWidth / imageHeight;

  const customHTMLElementModels = {
    p: HTMLElementModel.fromCustomModel({
      tagName: 'p',
      mixedUAStyles: {
        // backgroundColor: 'blue',
      },
      contentModel: HTMLContentModel.block,
    }),
    body: HTMLElementModel.fromCustomModel({
      tagName: 'body',
      mixedUAStyles: {
        // backgroundColor: 'blue',
        fontFamily: 'NotoSansLao-Regular',
        marginTop: 10,
        paddingBottom: 10,
      },
      contentModel: HTMLContentModel.block,
    }),
  };

  const systemFonts = [...defaultSystemFonts, 'NotoSansLao-Regular'];

  // Alert
  const showAlertNow = () => {
    setShowAlert(true);
  };

  const hideAlert = () => {
    setShowAlert(false);
  };

  // Check User Login Alert
  const checkUserLogin = () => {
    if (getHospitalStatus() !== 'ສາມາດນັດຈອງໄດ້') {
      showToast('warning', 'ທ່ານຍັງບໍ່ສາມາດນັດພົບໝໍໄດ້!');
    } else if (userData?.userInfo === null) {
      showAlertNow();
    } else {
      // onAddToCart();
      navigation.navigate('BookingScreen', {hospital_info: hospital_info});
    }
  };

  const getHospitalStatus = () => {
    if (realtimeHospitalsInfo !== null) {
      if (typeof realtimeHospitalsInfo != 'undefined') {
        if (realtimeHospitalsInfo.activeStatus === 'online') {
          return 'ສາມາດນັດຈອງໄດ້';
        } else if (realtimeHospitalsInfo.activeStatus === 'offline') {
          return 'ຍັງບໍ່ສາມາດນັດຈອງໄດ້';
        }
      } else {
        return 'ຍັງບໍ່ສາມາດນັດຈອງໄດ້';
      }
    } else {
      return 'ຍັງບໍ່ສາມາດນັດຈອງໄດ້';
    }
  };

  const showToast = (type, title, desc) => {
    toast.show({
      render: () => {
        return (
          <Alert
            maxWidth="95%"
            alignSelf="center"
            flexDirection="row"
            status={type}
            variant={'left-accent'}>
            <VStack space={1} flexShrink={1} w="100%">
              <HStack
                flexShrink={1}
                alignItems="center"
                justifyContent="space-between">
                <HStack space={2} flexShrink={1} alignItems="center">
                  <Alert.Icon />
                  <TextM>{title}</TextM>
                </HStack>
              </HStack>
              {desc !== undefined && (
                <TextM style={{fontSize: 14, paddingLeft: 25}}>{desc}</TextM>
              )}
            </VStack>
          </Alert>
        );
      },
    });
  };

  // useEffect
  useEffect(() => {
    // console.log('hospital_info', hospital_info);
    // Firebase Real-time Database
    const onValueChange = firebase
      .app()
      .database(Config.FIREBASE_REALTIME_DB)
      .ref(`/hospitals/${hospital_info?.hospital_clinic_id}`)
      .on('value', snapshot => {
        let data = snapshot.val();
        setRealtimeHospitalsInfo(data);
      });

    // console.log('hh data RT change: ', realtimeHospitalsInfo);

    // Unmounted
    return () => {
      firebase
        .database()
        .ref(`/hospitals/${hospital_info?.hospital_clinic_id}`)
        .off('value', onValueChange);
    };
  }, []);

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <SafeAreaView>
        <HStack space={2} px={4} pt={1} alignItems="center">
          <Box>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <MaterialCommunityIcons
                name="arrow-left-circle"
                style={{color: colors.DarkLightBlue, fontSize: 38}}
              />
            </TouchableOpacity>
          </Box>
          <Box>
            <TextM
              fontWeight="bold"
              numberOfLines={1}
              style={{
                color: colors.DarkLightBlue,
                fontSize: 18,
              }}>
              {hospital_info?.hospitalclinic_name}
            </TextM>
          </Box>
        </HStack>
      </SafeAreaView>
      <ScrollView style={{...defaultStyle.fixedLayout}}>
        <Box alignItems={'center'}>
          <FastImage
            source={{
              uri: hospital_info?.picture,
            }}
            style={{width: '100%', aspectRatio, borderRadius: 10}}
            resizeMode={FastImage.resizeMode.contain}
          />
        </Box>

        <Box
          style={{
            borderColor:
              getHospitalStatus() === 'ສາມາດນັດຈອງໄດ້'
                ? colors.Green
                : colors.Red,
            borderWidth: 1,
            marginTop: 10,
            alignItems: 'center',
            borderRadius: 5,
            borderStyle: 'dashed',
            paddingVertical: 8,
          }}>
          <TextM
            style={{
              color:
                getHospitalStatus() === 'ສາມາດນັດຈອງໄດ້'
                  ? colors.Green
                  : colors.Red,
            }}>
            {getHospitalStatus()}
          </TextM>
        </Box>

        <View style={{marginTop: 10}}>
          <TopicH title="ຂໍ້ມູນຕິດຕໍ່" icon="hospital-box-outline" />
        </View>

        {/* List Infomation */}
        <VStack space={1} mt={2}>
          <HStack space={1}>
            <MaterialCommunityIcons
              name="office-building"
              style={styles.iconStyle}
            />
            <TextM>{hospital_info?.hospitalclinic_name}</TextM>
          </HStack>
          {/* Location */}
          <HStack space={1}>
            <MaterialCommunityIcons
              name="map-marker"
              style={styles.iconStyle}
            />
            <TextM>{hospital_info?.address}</TextM>
          </HStack>
          {/* Time */}
          <HStack space={1}>
            <MaterialCommunityIcons name="clock" style={styles.iconStyle} />
            <TextM>
              {hospital_info?.open_time + ' - ' + hospital_info?.close_time}
            </TextM>
          </HStack>
          {/* Phone */}
          <HStack space={1}>
            <MaterialCommunityIcons name="phone" style={styles.iconStyle} />
            <TextM>{hospital_info?.contact_phone}</TextM>
          </HStack>
          {/* Contact */}
          <HStack space={1}>
            <MaterialCommunityIcons
              name="badge-account"
              style={styles.iconStyle}
            />
            <TextM>ຜູ້ຮັບຜິດຊອບ: {hospital_info?.contact_person}</TextM>
          </HStack>
        </VStack>

        <View style={{marginTop: 10}}>
          <TopicH title="ລາຍລະອຽດເພີ່ມເຕີມ" icon="information-outline" />
        </View>
        <RenderHtml
          contentWidth={width}
          source={{
            html: hospital_info?.introduction,
          }}
          tagsStyles={{
            p: {
              color: colors.LightBlack,
              fontSize: 16,
              lineHeight: 28,
              marginTop: 0,
              marginBottom: 6,
            },
            li: {
              color: colors.LightBlack,
              fontSize: 16,
            },
            ol: {
              lineHeight: 30,
              marginTop: 0,
              marginBottom: 0,
              color: colors.LightBlack,
            },
            ul: {
              lineHeight: 30,
              marginTop: 0,
              marginBottom: 0,
              color: colors.LightBlack,
            },
          }}
          systemFonts={systemFonts}
          customHTMLElementModels={customHTMLElementModels}
        />
      </ScrollView>

      <SafeAreaView>
        <Box
          style={{
            alignItems: 'center',
            width: '100%',
            paddingVertical: 10,
          }}>
          <TouchableOpacity
            disabled={getHospitalStatus() === 'ສາມາດນັດຈອງໄດ້' ? false : true}
            onPress={() => checkUserLogin()}
            style={{
              backgroundColor:
                getHospitalStatus() === 'ສາມາດນັດຈອງໄດ້'
                  ? colors.DarkLightBlue
                  : colors.Light,
              paddingVertical: 6,
              paddingHorizontal: 50,
              borderRadius: 5,
            }}>
            <TextM fontWeight="bold" style={{color: '#ffffff'}}>
              ຂໍຈອງຄິວເຂົ້າພົບໝໍ
            </TextM>
          </TouchableOpacity>
        </Box>
      </SafeAreaView>

      {/* Check Login User*/}
      <Portal>
        <Dialog
          visible={showAlert}
          onDismiss={hideAlert}
          style={{borderRadius: 10}}>
          <Dialog.Title>
            <TextM fontWeight="bold" style={{fontSize: 18}}>
              ຂໍອະໄພທ່ານຍັງບໍ່ທັນໄດ້ເຂົ້າສູ່ລະບົບ
            </TextM>
          </Dialog.Title>
          <Dialog.Content>
            <TextM>ກະລຸນາເຂົ້າສູ່ລະບົບກ່ອນຂໍນັດພົບໝໍ</TextM>
          </Dialog.Content>
          <Dialog.Actions>
            <ButtonN onPress={() => hideAlert()}>
              <TextM>ກັບຄືນ</TextM>
            </ButtonN>
            <ButtonN
              onPress={() => {
                hideAlert();
                navigation.navigate('Profile');
              }}>
              <TextM style={{color: colors.Red}}>ເຂົ້າສູ່ລະບົບ</TextM>
            </ButtonN>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </Box>
  );
};

export default HPTClinicDetail;

const styles = StyleSheet.create({
  iconStyle: {
    fontSize: 22,
    color: colors.Pink,
  },
});
