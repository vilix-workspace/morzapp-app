import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import {
  Box,
  ScrollView,
  Text,
  View,
  Button,
  Icon,
  HStack,
  VStack,
} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image';
import axios from 'axios';
import LottieView from 'lottie-react-native';
import {Rating} from 'react-native-ratings';
import firebase from '@react-native-firebase/app';
import Config from 'react-native-config';
// Redux
import {useSelector, useDispatch} from 'react-redux';

// Custom Style
import {defaultStyle, colors} from '../../shareStyle';
import TextM from '../../shareComponents/TextM';
import TopicH from '../../shareComponents/TopicH';

const CatData = [
  {
    id: '1',
    cat_name: 'Dental',
    icon: require('../../../assets/img/dental-icon.png'),
    numberofdoctor: 18,
    color: '#C4A01E',
  },
  {
    id: '2',
    cat_name: 'Heart',
    icon: require('../../../assets/img/heart-icon.png'),
    numberofdoctor: 13,
    color: '#08ACB8',
  },
  {
    id: '3',
    cat_name: 'Brain',
    icon: require('../../../assets/img/brain-icon.png'),
    numberofdoctor: 18,
    color: '#0271DE',
  },
  {
    id: '4',
    cat_name: 'Bone',
    icon: require('../../../assets/img/bone-icon.png'),
    numberofdoctor: 13,
    color: '#23AA32',
  },
];

const DoctorServices = ({navigation}) => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();
  // useState
  const [allDoctor, setAllDoctor] = useState([]);
  const [realtimeDoctorStatus, setRealtimeDoctorStatus] = useState(null);
  const [refreshing, setRefreshing] = useState(false);

  // Functions
  const onRefresh = () => {
    setRefreshing(true);
    try {
      getDoctorList();
      setRefreshing(false);
    } catch (error) {
      console.log(error);
    }
  };

  const getDoctorList = async () => {
    try {
      await axios({
        method: 'get',
        url: Config.WP_URL + '/wp/v2/doctorlist?orderby=rand',
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
        },
      })
        .then(res => {
          // console.log(res.data);
          setAllDoctor(res.data);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  // Render Cat
  // const renderCatItem = ({item}) => (
  //   <TouchableOpacity>
  //     <View
  //       style={{
  //         marginRight: 10,
  //         padding: 10,
  //         marginTop: 10,
  //         backgroundColor: item.color,
  //         borderRadius: 10,
  //         alignItems: 'center',
  //       }}>
  //       <FastImage
  //         style={{width: 30, height: 30}}
  //         resizeMode="contain"
  //         source={item.icon}
  //       />
  //       <Text style={{marginVertical: 5, color: '#ffffff'}}>
  //         {item.cat_name}
  //       </Text>
  //     </View>
  //   </TouchableOpacity>
  // );

  const getDoctorStatus = mzid => {
    if (realtimeDoctorStatus !== null) {
      let status = realtimeDoctorStatus.find(item => item.morzappID === mzid);
      if (typeof status != 'undefined') {
        return status.activeStatus.toUpperCase();
      } else {
        return 'OFFLINE';
      }
    } else {
      return '';
    }
  };

  // useEffect
  useEffect(() => {
    getDoctorList();

    // Firebase Real-time Database
    const onValueChange = firebase
      .app()
      .database(Config.FIREBASE_REALTIME_DB)
      .ref(`/doctors`)
      .on('value', snapshot => {
        // console.log('User data RT change: ', snapshot.val());
        setRealtimeDoctorStatus(Object.values(snapshot.val()));
      });

    // Stop listening for updates when no longer required
    return () =>
      firebase.database().ref(`/doctors`).off('value', onValueChange);
  }, []);

  return (
    <Box flex={1} backgroundColor={colors.White}>
      {/* Fix Selection Category */}
      {/* <View style={{...defaultStyle.fixedLayout}}>
        <SafeAreaView>
          <FlatList
            horizontal
            data={CatData}
            renderItem={renderCatItem}
            keyExtractor={(item) => item.id}
          />
        </SafeAreaView>
      </View> */}

      {/* Content */}
      <ScrollView
        style={{...defaultStyle.fixedLayout, backgroundColor: '#f6f6f6'}}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        {allDoctor?.length > 0 ? (
          <View>
            <TopicH
              title="ລາຍຊື່ທ່ານໝໍຜູ້ຊ່ຽວຊານທັງໝົດ"
              icon="hexagon-multiple"
            />
            <VStack style={{marginTop: 10, marginBottom: 20}}>
              {allDoctor.map(item => {
                return (
                  <Box
                    style={{
                      backgroundColor: colors.White,
                      marginBottom: 8,
                      padding: 15,
                      borderRadius: 10,
                      shadowRadius: 10,
                      shadowColor: colors.Gray,
                      shadowOffset: {
                        width: 0,
                        height: 1,
                      },
                      shadowOpacity: 0.2,
                      shadowRadius: 1.41,
                      elevation: 1,
                    }}
                    key={item?.id}>
                    <TouchableOpacity
                      style={{flexDirection: 'row'}}
                      onPress={() =>
                        navigation.navigate('DoctorDetail', {doctor_info: item})
                      }>
                      <HStack space={3} justifyContent={'space-between'}>
                        <VStack w={'72%'}>
                          <TextM
                            numberOfLines={1}
                            fontWeight="bold"
                            style={{color: colors.DarkBlue}}>
                            {item?.fullname}
                          </TextM>

                          {/* Consultancy */}
                          <TextM style={{color: colors.Black, fontSize: 14}}>
                            ໃຫ້ຄຳປຶກສາດ້ານ:
                          </TextM>
                          {item?.consultancy.length > 0 && (
                            <View
                              style={{
                                flexDirection: 'column',
                                alignItems: 'flex-start',
                              }}>
                              {item?.consultancy.map((item, i) => {
                                return (
                                  <HStack
                                    space={1}
                                    maxW={'100%'}
                                    alignItems={'center'}
                                    key={i}>
                                    <FontAwesome5
                                      name="stethoscope"
                                      style={{
                                        color: colors.Blue,
                                        fontSize: 12,
                                      }}
                                    />
                                    <TextM
                                      numberOfLines={1}
                                      style={{
                                        fontSize: 12,
                                        color: colors.Blue,
                                      }}>
                                      {item?.name}
                                    </TextM>
                                  </HStack>
                                );
                              })}
                            </View>
                          )}

                          {/* Specialist */}
                          <TextM style={{color: colors.Black, fontSize: 14}}>
                            ສາຂາຮຽນຈົບ:
                          </TextM>
                          {item?.specialist.length > 0 && (
                            <View
                              style={{
                                flexDirection: 'column',
                                alignItems: 'flex-start',
                              }}>
                              {item?.specialist.map((item, i) => {
                                return (
                                  <HStack
                                    space={1}
                                    maxW={'100%'}
                                    alignItems={'center'}
                                    key={i}>
                                    <MaterialCommunityIcons
                                      name="certificate"
                                      style={{
                                        color: colors.LightPink,
                                        fontSize: 12,
                                      }}
                                    />
                                    <TextM
                                      numberOfLines={1}
                                      style={{
                                        fontSize: 12,
                                        color: colors.LightPink,
                                      }}>
                                      {item?.name}
                                    </TextM>
                                  </HStack>
                                );
                              })}
                            </View>
                          )}

                          {/* <View
                          style={{
                            alignSelf: 'flex-start',
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginTop: 5,
                          }}>
                          <Rating imageSize={14} startingValue={1} readonly={true} />
                        </View> */}
                        </VStack>
                        <VStack w={'25%'}>
                          <FastImage
                            style={{
                              width: 65,
                              height: 65,
                              alignSelf: 'center',
                              borderRadius: 20,
                            }}
                            source={{
                              uri: item?.picture,
                            }}
                          />
                          {/* Status */}
                          <HStack mt={2} alignItems={'center'}>
                            <MaterialCommunityIcons
                              name={'checkbox-blank-circle'}
                              style={{
                                color:
                                  getDoctorStatus(item?.morzapp_id) === 'ONLINE'
                                    ? colors.Green
                                    : getDoctorStatus(item?.morzapp_id) ===
                                      'AWAY'
                                    ? colors.Orange
                                    : colors.Red,
                                fontSize: 12,
                                marginRight: 2,
                              }}
                            />
                            <TextM
                              style={{
                                color:
                                  getDoctorStatus(item?.morzapp_id) === 'ONLINE'
                                    ? colors.Green
                                    : getDoctorStatus(item?.morzapp_id) ===
                                      'AWAY'
                                    ? colors.Orange
                                    : colors.Red,
                                fontSize: 12,
                              }}>
                              {getDoctorStatus(item?.morzapp_id)}
                            </TextM>
                          </HStack>
                        </VStack>
                      </HStack>
                    </TouchableOpacity>
                  </Box>
                );
              })}
            </VStack>
          </View>
        ) : (
          <LottieView
            source={require('../../../assets/animation/search-doctor.json')}
            style={{width: 300, marginTop: 20}}
            autoPlay
            loop
          />
        )}
      </ScrollView>
    </Box>
  );
};

export default DoctorServices;

const styles = StyleSheet.create({});
