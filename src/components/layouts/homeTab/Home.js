import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  Dimensions,
  TouchableOpacity,
  Linking,
  Platform,
} from 'react-native';
// UI
import {
  Box,
  ScrollView,
  View,
  VStack,
  HStack,
  AlertDialog,
  Button,
} from 'native-base';
// Package
import FastImage from 'react-native-fast-image';
import Swiper from 'react-native-swiper';
// import {Rating, AirbnbRating} from 'react-native-ratings';
import axios from 'axios';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import Config from 'react-native-config';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
// Redux
import {useSelector, useDispatch} from 'react-redux';

import {plus, minus} from '../../../stores/features/count/countSlice';
// Component
import Header from '../../shareComponents/Header';
import TopicH from '../../shareComponents/TopicH';
import LiveChat from '../../shareComponents/LiveChat';
import TextM from '../../shareComponents/TextM';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';

import TrackPlayer from 'react-native-track-player';

const width = Dimensions.get('window').width;

const Home = ({navigation}) => {
  // Redux
  const reduxStore = useSelector(state => state);
  const count = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();
  // useState
  const [showPaginate, setShowPaginate] = useState(true);
  const [rateValue, setRateValue] = useState(0);
  const [banner, setBanner] = useState([]);
  const [doctorList, setDoctorList] = useState([]);
  const [realtimeDoctorStatus, setRealtimeDoctorStatus] = useState(null);
  const [forceUpdate, setForceUpdate] = useState(false);
  const [iosLink, setIosLink] = useState('');
  const [androidLink, setAndroidLink] = useState('');

  const imageWidth = 640; // Replace with the actual width of the image
  const imageHeight = 240; // Replace with the actual height of the image
  const aspectRatio = imageWidth / imageHeight;

  // useEffect
  useEffect(() => {
    // setRateValue(4.2);
    checkForceUpdate();
    getHomeBanner();
    getDoctorPopularList();

    // Firebase Real-time Database
    const onValueChange = firebase
      .app()
      .database(Config.FIREBASE_REALTIME_DB)
      .ref(`/doctors`)
      .on('value', snapshot => {
        // console.log('User data RT change: ', snapshot.val());
        setRealtimeDoctorStatus(Object.values(snapshot.val()));
      });

    // Stop listening for updates when no longer required
    return () =>
      firebase.database().ref(`/doctors`).off('value', onValueChange);
  }, []);

  // Function
  const checkForceUpdate = () => {
    try {
      firestore()
        .collection('settingConfig')
        .get()
        .then(querySnapshot => {
          var data = {};
          querySnapshot.forEach(doc => {
            data = doc.data();
            // console.log('data new', data);
            setForceUpdate(data?.forceUpdateApp);
            setIosLink(data?.iosLink);
            setAndroidLink(data?.androidLink);
          });
          // console.log(allData);
          // setPatientList(allData);
        })
        .catch(error => {
          console.error('Error read ', error);
        });
    } catch (error) {
      console.log(error);
    }
  };
  const getHomeBanner = async () => {
    try {
      await axios({
        method: 'get',
        url: Config.WP_URL + '/wp/v2/home_banner',
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
        },
      })
        .then(res => {
          setBanner(res.data);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const getDoctorPopularList = async () => {
    try {
      await axios({
        method: 'get',
        url: Config.WP_URL + '/wp/v2/doctorlist?doctor_tag=5',
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
        },
      })
        .then(res => {
          // console.log(res.data);
          setDoctorList(res.data);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const getDoctorStatus = mzid => {
    if (realtimeDoctorStatus !== null) {
      let status = realtimeDoctorStatus.find(item => item.morzappID === mzid);
      if (typeof status != 'undefined') {
        return status.activeStatus.toUpperCase();
      } else {
        return 'OFFLINE';
      }
    } else {
      return '';
    }
  };

  const handleUpdateApp = () => {
    if (Platform.OS === 'ios') {
      Linking.openURL(iosLink);
    } else {
      Linking.openURL(androidLink);
    }
  };

  const ratingCompleted = rating => {
    console.log('Rating is: ' + rating);
  };

  const start = async () => {
    // console.log('sound');
    // Set up the player

    // Add a track to the queue
    await TrackPlayer.add({
      id: 'trackId',
      url: require('../../../assets/sounds/telephone-ring.mp3'),
      title: 'Track Title',
      artist: 'Track Artist',
      // artwork: require('track.png'),
    });

    // Start playing it
    await TrackPlayer.play();
  };

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <Header navigation={navigation} />
      <ScrollView style={{...defaultStyle.fixedLayout}}>
        <TopicH title="ສູນລວມການບໍລິການສຸຂະພາບ" icon="hexagon-multiple" />

        <HStack justifyContent={'space-between'} my={2} w={'100%'}>
          {/* Doctor */}

          <Box
            w={'48%'}
            style={[
              styles.columnlayout,
              {
                backgroundColor: '#fff',
                marginLeft: Platform.OS === 'android' ? 2 : null,
              },
            ]}>
            <TouchableOpacity
              onPress={() => navigation.navigate('DoctorService')}>
              <HStack
                alignItems={'center'}
                justifyContent={'space-between'}
                mb={2}>
                <FastImage
                  style={styles.iconService}
                  resizeMode="contain"
                  source={require('../../../assets/img/doctor-1.png')}
                />
                <Box
                  style={{
                    backgroundColor: '#62CDFF',
                    paddingLeft: 10,
                    paddingRight: 5,
                    paddingVertical: 3,
                    borderTopLeftRadius: 15,
                    borderBottomLeftRadius: 15,
                  }}>
                  <TextM style={[styles.textService, {color: '#fff'}]}>
                    ປຶກສາທ່ານໝໍ
                  </TextM>
                </Box>
              </HStack>
              <TextM style={styles.textServiceDetail}>
                ພົບກັບທ່ານໝໍທີ່ຊ່ຽວຊານສະເພາະດ້ານ
              </TextM>
            </TouchableOpacity>
          </Box>

          {/* Appointment */}
          <Box
            w={'48%'}
            style={[
              styles.columnlayout,
              {
                backgroundColor: '#fff',
                marginRight: Platform.OS === 'android' ? 2 : null,
              },
            ]}>
            <TouchableOpacity
              onPress={() => navigation.navigate('AppointmentService')}>
              <HStack
                alignItems={'center'}
                justifyContent={'space-between'}
                mb={2}>
                <FastImage
                  style={styles.iconService}
                  resizeMode="contain"
                  source={require('../../../assets/img/clinic-1.png')}
                />
                <Box
                  style={{
                    backgroundColor: '#FF85B3',
                    paddingLeft: 10,
                    paddingRight: 5,
                    paddingVertical: 3,
                    borderTopLeftRadius: 15,
                    borderBottomLeftRadius: 15,
                  }}>
                  <TextM style={[styles.textService, {color: '#fff'}]}>
                    ນັດພົບທ່ານໝໍ
                  </TextM>
                </Box>
              </HStack>
              <TextM style={[styles.textServiceDetail]}>
                ນັດເຂົ້າພົບທ່ານໝໍໄດ້ຢ່າງສະດວກສະບາຍ
              </TextM>
            </TouchableOpacity>
          </Box>
        </HStack>

        {/* Home Ads Slider */}
        {banner?.length > 0 ? (
          <View
            style={{
              width: '100%',
              aspectRatio,
              marginBottom: 10,
              marginTop: 2,
            }}>
            <Swiper
              autoplay={true}
              autoplayTimeout={3.5}
              loop={true}
              showsPagination={false}>
              {banner?.map((data, index) => {
                return (
                  <View key={data?.id} style={styles.slide}>
                    <FastImage
                      style={{
                        width: '100%',
                        aspectRatio,
                        borderRadius: 10,
                      }}
                      resizeMode={FastImage.resizeMode.contain}
                      source={{uri: data?.img}}
                    />
                  </View>
                );
              })}
            </Swiper>
          </View>
        ) : (
          <SkeletonPlaceholder>
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginBottom: 10,
                width: '100%',
                aspectRatio,
                marginTop: 2,
              }}>
              <View
                style={{
                  width: '100%',
                  aspectRatio,
                  borderRadius: 10,
                }}
              />
            </View>
          </SkeletonPlaceholder>
        )}

        {/* How to */}
        <HStack space={3} justifyContent="center">
          <TouchableOpacity
            style={{
              backgroundColor: '#f1f2f6',
              alignItems: 'center',
              paddingVertical: 10,
              borderRadius: 5,
              borderTopWidth: 1,
              borderTopColor: '#00a8e8',
              borderTopWidth: 3,
              flex: 1,
            }}
            onPress={() => navigation.navigate('HowtoUse')}>
            <HStack alignItems={'center'} space={2}>
              <FastImage
                style={{width: 26, height: 26}}
                resizeMode="contain"
                source={require('../../../assets/img/user-guide.png')}
              />
              <TextM
                fontWeight="bold"
                style={{
                  textAlign: 'center',
                  color: '#00a8e8',
                }}>
                ວິທີໃຊ້ງານ
              </TextM>
            </HStack>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              backgroundColor: '#f1f2f6',
              alignItems: 'center',
              paddingVertical: 10,
              borderRadius: 5,
              borderTopWidth: 1,
              borderTopColor: colors.Orange,
              borderTopWidth: 3,
              flex: 1,
            }}
            onPress={() => navigation.navigate('Policy')}>
            <HStack alignItems={'center'} space={2}>
              <TextM
                fontWeight="bold"
                style={{
                  textAlign: 'center',
                  color: colors.Orange,
                }}>
                ເງື່ອນໄຂຂໍ້ກຳນົດ
              </TextM>
              <FastImage
                style={{width: 26, height: 26}}
                resizeMode="contain"
                source={require('../../../assets/img/compliant.png')}
              />
            </HStack>
          </TouchableOpacity>
        </HStack>

        {/* For Rating Review */}
        {/* <AirbnbRating
          defaultRating={1}
          size={20}
          reviewSize={14}
          onFinishRating={ratingCompleted}
        /> */}

        {/* Doctor nearby you */}
        <View style={{marginTop: 10}}>
          <TopicH title="ທ່ານໝໍຊ່ຽວຊານ" icon="doctor" />
        </View>
        {doctorList?.length > 0 ? (
          <VStack
            style={{marginTop: 10, marginBottom: 20, paddingHorizontal: 5}}>
            {doctorList.map(item => {
              return (
                <Box
                  style={{
                    backgroundColor: colors.White,
                    marginBottom: 8,
                    padding: 12,
                    borderRadius: 10,
                    shadowColor: '#666',
                    shadowOffset: {
                      width: 0,
                      height: 1,
                    },
                    shadowOpacity: 0.2,
                    shadowRadius: 1.41,

                    elevation: 2,
                  }}
                  key={item?.id}>
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate('DoctorDetail', {doctor_info: item})
                    }>
                    <HStack space={3} justifyContent={'space-between'}>
                      <VStack w={'72%'}>
                        <TextM
                          numberOfLines={1}
                          fontWeight="bold"
                          style={{color: colors.DarkBlue}}>
                          {item?.fullname}
                        </TextM>

                        {/* Consultancy */}
                        <TextM style={{color: colors.Black, fontSize: 14}}>
                          ໃຫ້ຄຳປຶກສາດ້ານ:
                        </TextM>
                        {item?.consultancy?.length > 0 && (
                          <View
                            style={{
                              flexDirection: 'column',
                              alignItems: 'flex-start',
                            }}>
                            {item?.consultancy?.map((item, i) => {
                              return (
                                <HStack alignItems={'center'} key={i} space={1}>
                                  <FontAwesome5
                                    name="stethoscope"
                                    style={{
                                      color: colors.Blue,
                                      fontSize: 12,
                                    }}
                                  />
                                  <TextM
                                    numberOfLines={1}
                                    style={{
                                      fontSize: 12,
                                      color: colors.Blue,
                                    }}>
                                    {item?.name}
                                  </TextM>
                                </HStack>
                              );
                            })}
                          </View>
                        )}

                        {/* Specialist */}
                        <TextM style={{color: colors.Black, fontSize: 14}}>
                          ສາຂາຮຽນຈົບ:
                        </TextM>
                        {item?.specialist?.length > 0 && (
                          <View
                            style={{
                              flexDirection: 'column',
                              alignItems: 'flex-start',
                            }}>
                            {item?.specialist?.map((item, i) => {
                              return (
                                <HStack
                                  maxW={'100%'}
                                  alignItems={'center'}
                                  key={i}
                                  space={1}>
                                  <MaterialCommunityIcons
                                    name="certificate"
                                    style={{
                                      color: colors.LightPink,
                                      fontSize: 12,
                                    }}
                                  />
                                  <TextM
                                    numberOfLines={1}
                                    style={{
                                      fontSize: 12,
                                      color: colors.LightPink,
                                    }}>
                                    {item?.name}
                                  </TextM>
                                </HStack>
                              );
                            })}
                          </View>
                        )}
                      </VStack>

                      <VStack w={'25%'}>
                        <FastImage
                          style={{
                            width: 65,
                            height: 65,
                            alignSelf: 'center',
                            borderRadius: 20,
                          }}
                          source={{
                            uri: item?.picture,
                          }}
                        />
                        {/* Status */}
                        <HStack mt={2} alignItems={'center'}>
                          <MaterialCommunityIcons
                            name={'checkbox-blank-circle'}
                            style={{
                              color:
                                getDoctorStatus(item?.morzapp_id) === 'ONLINE'
                                  ? colors.Green
                                  : getDoctorStatus(item?.morzapp_id) === 'AWAY'
                                  ? colors.Orange
                                  : colors.Red,
                              fontSize: 12,
                              marginRight: 2,
                            }}
                          />
                          <TextM
                            style={{
                              color:
                                getDoctorStatus(item?.morzapp_id) === 'ONLINE'
                                  ? colors.Green
                                  : getDoctorStatus(item?.morzapp_id) === 'AWAY'
                                  ? colors.Orange
                                  : colors.Red,
                              fontSize: 12,
                            }}>
                            {getDoctorStatus(item?.morzapp_id)}
                          </TextM>
                        </HStack>
                      </VStack>
                    </HStack>
                  </TouchableOpacity>
                </Box>
              );
            })}
          </VStack>
        ) : (
          <SkeletonPlaceholder>
            <View
              style={{
                marginTop: 10,
                marginBottom: 0,
              }}>
              <View
                style={{
                  height: 160,
                  borderRadius: 10,
                  marginBottom: 10,
                }}
              />
              <View
                style={{
                  height: 160,
                  borderRadius: 10,
                }}
              />
            </View>
          </SkeletonPlaceholder>
        )}
      </ScrollView>
      <View style={{position: 'absolute', bottom: 0, right: 0}}>
        <TouchableOpacity
          onPress={() => {
            // navigation.navigate('LiveChat');
            Linking.openURL('whatsapp://send?text=hello&phone=8562095948769');
          }}
          style={{
            margin: 15,
            backgroundColor: colors.Blue,
            padding: 8,
            borderRadius: 20,
          }}>
          <MaterialCommunityIcons
            name={'chat'}
            style={{color: '#ffffff', fontSize: 24}}
          />
        </TouchableOpacity>
      </View>

      {/* Force Update Dialog */}
      <AlertDialog isOpen={forceUpdate}>
        <AlertDialog.Content>
          {/* <AlertDialog.CloseButton /> */}
          <AlertDialog.Header>
            <TextM style={{color: colors.Green}} fontWeight="bold">
              ກວດພົບເວີຊັ້ນໃໝ່
            </TextM>
          </AlertDialog.Header>
          <AlertDialog.Body>
            <TextM>
              ກະລຸນາອັບເດດແອັບຂອງຂອງທ່ານເປັນເວີຊັ້ນລ່າສຸດ
              ເພື່ອທີ່ທ່ານຈະສາມາດໃຊ້ງານແອັບໄດ້ເຕັມປະສິດທິພາບ
            </TextM>
          </AlertDialog.Body>
          <AlertDialog.Footer>
            <Button.Group space={2}>
              <Button
                variant="unstyled"
                colorScheme="coolGray"
                onPress={() => setForceUpdate(false)}>
                <TextM style={{color: colors.Gray}}>ອັບເດດພາຍຫຼັງ</TextM>
              </Button>
              <Button
                colorScheme="success"
                onPress={() => {
                  handleUpdateApp();
                }}>
                <TextM style={{color: colors.White}}>ອັບເດດທັນທີ</TextM>
              </Button>
            </Button.Group>
          </AlertDialog.Footer>
        </AlertDialog.Content>
      </AlertDialog>
    </Box>

    //   <SafeAreaView>
    //     <Header navigation={navigation} />
    //   </SafeAreaView>
    //   <Content style={{...defaultStyle.fixedLayout}}>

    //     {/* Testing */}
    //     {/* <TouchableOpacity
    //       style={{margin: 20, padding: 10}}
    //       onPress={() => start()}>
    //       <TextM>Test me</TextM>
    //     </TouchableOpacity> */}

    //   </Content>
    //   {/* <LiveChat /> */}
  );
};

export default Home;

const styles = StyleSheet.create({
  columncontainer: {
    flexDirection: 'row',
    marginTop: 10,
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  columnlayout: {
    borderRadius: 10,
    paddingVertical: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },

  slide: {
    alignItems: 'center',
  },
  iconService: {
    width: 45,
    height: 45,
    marginLeft: 8,
  },
  textService: {
    color: colors.Black,
  },
  textServiceDetail: {
    color: colors.LightBlack,
    fontSize: 14,
    paddingHorizontal: 10,
  },
});
