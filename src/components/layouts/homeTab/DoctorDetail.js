import React, {useEffect, useState, useRef} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  useWindowDimensions,
  PermissionsAndroid,
  SafeAreaView,
  Platform,
  Dimensions,
} from 'react-native';
import {
  Box,
  ScrollView,
  View,
  Text,
  Icon,
  useToast,
  Alert,
  HStack,
  VStack,
} from 'native-base';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Button as ButtonN, Dialog, Portal} from 'react-native-paper';
import {Rating} from 'react-native-ratings';
import RenderHtml, {
  HTMLElementModel,
  HTMLContentModel,
  defaultSystemFonts,
} from 'react-native-render-html';
import FastImage from 'react-native-fast-image';
// Firebase
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import {
  TwilioVideoLocalView,
  TwilioVideoParticipantView,
  TwilioVideo,
} from 'react-native-twilio-video-webrtc';
import axios from 'axios';
import TrackPlayer, {RepeatMode} from 'react-native-track-player';
import Config from 'react-native-config';
// Redux
import {useSelector, useDispatch} from 'react-redux';

// Custom Style
import {defaultStyle, colors} from '../../shareStyle';
import TextM from '../../shareComponents/TextM';

const windowHeight = Dimensions.get('window').height;

var countDurationSecond;

const DoctorDetail = ({route, navigation}) => {
  const toast = useToast();
  const {width} = useWindowDimensions();
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();
  // Params
  const {doctor_info} = route.params;
  // useState
  const [showAlert, setShowAlert] = useState(false);
  const [showCallAlert, setShowCallAlert] = useState(false);
  const [callStatus, setCallStatus] = useState('');
  const [callType, setCallType] = useState('');
  const [firebaseDocId, setFirebaseDocId] = useState('');
  // Call State
  const [isAudioEnabled, setIsAudioEnabled] = useState(true);
  const [isVideoEnabled, setIsVideoEnabled] = useState(true);
  const [status, setStatus] = useState('disconnected');
  const [participants, setParticipants] = useState(new Map());
  const [videoTracks, setVideoTracks] = useState(new Map());
  const [realtimeDoctorInfo, setRealtimeDoctorInfo] = useState(null);
  const [callStart, setCallStart] = useState(false);
  const twilioVideo = useRef(null);
  // Time Count
  const [countDuration, setCountDuration] = useState(0);

  // useEffect
  useEffect(() => {
    // console.log('userData', userData);
    // console.log('doctor_info', doctor_info);

    // Firebase Real-time Database
    const onValueChange = firebase
      .app()
      .database(Config.FIREBASE_REALTIME_DB)
      .ref(`/doctors/${doctor_info?.morzapp_id}`)
      .on('value', snapshot => {
        let data = snapshot.val();
        // console.log('User data RT change nn: ', snapshot.val());
        setRealtimeDoctorInfo(data);
        if (data.callStatus === 'cancelled' || data.callStatus === 'waiting') {
          stopCallingSound();
          updateCallInfo('waiting', '', '');
          twilioVideo.current.disconnect();
          setCallStatus('');
          clearDuration();
        } else if (data.callStatus === 'connected') {
          stopCallingSound();
        }
      });

    // Unmounted
    return () => {
      firebase
        .database()
        .ref(`/doctors/${doctor_info?.morzapp_id}`)
        .off('value', onValueChange);

      TrackPlayer.reset();
    };
  }, []);

  // Alert
  const showAlertNow = () => {
    setShowAlert(true);
  };

  const hideAlert = () => {
    setShowAlert(false);
  };

  const showToast = (type, title, desc) => {
    toast.show({
      render: () => {
        return (
          <Alert
            maxWidth="95%"
            alignSelf="center"
            flexDirection="row"
            status={type}
            variant={'left-accent'}>
            <VStack space={1} flexShrink={1} w="100%">
              <HStack
                flexShrink={1}
                alignItems="center"
                justifyContent="space-between">
                <HStack space={2} flexShrink={1} alignItems="center">
                  <Alert.Icon />
                  <TextM>{title}</TextM>
                </HStack>
              </HStack>
              {desc !== undefined && (
                <TextM style={{fontSize: 14, paddingLeft: 25}}>{desc}</TextM>
              )}
            </VStack>
          </Alert>
        );
      },
    });
  };

  // Check User Login Alert
  const checkUserLogin = type => {
    if (
      renderDoctorStatus(doctor_info?.morzapp_id) === 'OFFLINE' ||
      realtimeDoctorInfo?.callStatus === 'calling' ||
      realtimeDoctorInfo?.callStatus === 'incall'
    ) {
      showToast('warning', 'ຂໍອະໄພ ທ່ານບໍ່ສາມາດຕິດຕໍ່ໄດ້ໃນຂະນະນີ້!');
    } else if (userData?.userInfo === null) {
      showAlertNow();
    } else {
      setShowCallAlert(true);
      setCallType(type);
    }
  };

  // Render Component Function
  const langSupport = () => {
    let object = doctor_info?.language;
    let lang = '';
    for (const iterator of object) {
      if (object.length - 1 !== object.indexOf(iterator)) {
        lang += iterator + ', ';
      } else {
        lang += iterator;
      }
    }
    return lang;
  };

  const customHTMLElementModels = {
    p: HTMLElementModel.fromCustomModel({
      tagName: 'p',
      mixedUAStyles: {
        // backgroundColor: 'blue',
      },
      contentModel: HTMLContentModel.block,
    }),
    body: HTMLElementModel.fromCustomModel({
      tagName: 'body',
      mixedUAStyles: {
        // backgroundColor: 'blue',
        fontFamily: 'NotoSansLao-Regular',
      },
      contentModel: HTMLContentModel.block,
    }),
  };

  const systemFonts = [...defaultSystemFonts, 'NotoSansLao-Regular'];

  // Provinces Location Name
  const renderLocationName = location => {
    switch (location) {
      case 'vientiane_capital':
        return 'ນະຄອນຫຼວງວຽງຈັນ';
        break;

      default:
        break;
    }
  };

  // Number Commas
  const numberWithCommas = x => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  const renderDoctorStatus = () => {
    if (realtimeDoctorInfo !== null) {
      if (typeof realtimeDoctorInfo != 'undefined') {
        return realtimeDoctorInfo?.activeStatus.toUpperCase();
      } else {
        return 'OFFLINE';
      }
    } else {
      return '';
    }
  };

  const checkCallPrice = callfee => {
    if (callfee === '' || callfee === '0') {
      return 'ບໍ່ເສຍຄ່າໃຊ້ຈ່າຍ';
    } else {
      let price = numberWithCommas(callfee);
      return price + ' ກີບ';
    }
  };

  const makeid = length => {
    var result = '';
    var characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  };

  //
  // Call Service Functions
  //

  const onStartVoiceCall = async () => {
    setCallStatus('voice');

    if (Platform.OS === 'android') {
      await _requestAudioPermission();
      await _requestCameraPermission();
    }

    let connectRoomID = doctor_info?.morzapp_id.concat(makeid(18));

    if (realtimeDoctorInfo?.activeStatus == 'online') {
      try {
        await axios({
          method: 'get',
          url: Config.API_CALL_URL + '?token=' + userData?.userToken,
        })
          .then(res => {
            // console.log('inroom', connectRoomID);
            // console.log(res.data.token);
            twilioVideo.current.connect({
              roomName: connectRoomID,
              accessToken: res.data.token,
              enableNetworkQualityReporting: true,
              dominantSpeakerEnabled: true,
              enableVideo: false,
            });
            setIsAudioEnabled(true);
            startCallingSound();
            setStatus('connecting');
            recordCallHistory('voice', 'calling', connectRoomID);
          })
          .catch(error => {
            console.log(error);
            console.log(error.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      showToast('warning', 'ຂໍອະໄພ ທ່ານບໍ່ສາມາດຕິດຕໍ່ໄດ້ໃນຂະນະນີ້!');
    }
  };

  const onStartVideoCall = async () => {
    setCallStatus('video');

    if (Platform.OS === 'android') {
      await _requestAudioPermission();
      await _requestCameraPermission();
    }

    let connectRoomID = doctor_info?.morzapp_id.concat(makeid(18));

    if (realtimeDoctorInfo?.activeStatus === 'online') {
      try {
        await axios({
          method: 'get',
          url: Config.API_CALL_URL + '?token=' + userData?.userToken,
        })
          .then(res => {
            console.log(res);
            twilioVideo.current.connect({
              roomName: connectRoomID,
              accessToken: res.data.token,
              enableNetworkQualityReporting: true,
              dominantSpeakerEnabled: true,
              enableVideo: true,
            });
            setIsAudioEnabled(true);
            setStatus('connecting');
            startCallingSound();
            recordCallHistory('video', 'calling', connectRoomID);
          })
          .catch(error => {
            console.log(error);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      showToast('warning', 'ຂໍອະໄພ ທ່ານບໍ່ສາມາດຕິດຕໍ່ໄດ້ໃນຂະນະນີ້!');
    }
  };

  const updatePatientCancelCall = () => {
    try {
      firestore()
        .collection('callHistory')
        .doc(firebaseDocId)
        .update({
          callAction: 'PatientCancelled',
        })
        .then(() => {
          console.log('Document successfully written!');
        })
        .catch(error => {
          console.error('Error writing document: ', error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const _onEndButtonPress = () => {
    stopCallingSound();
    updateCallInfo('waiting', '', '');
    updatePatientCancelCall();
    twilioVideo.current.disconnect();
    setCallStatus('');
    setCallStart(false);

    // Add Call Duration when call more than 10s
    if (countDuration > 10) {
      updateDuration();
    }

    clearDuration();
  };

  const _onMuteButtonPress = () => {
    twilioVideo.current
      .setLocalAudioEnabled(!isAudioEnabled)
      .then(isEnabled => setIsAudioEnabled(isEnabled));
  };

  const _onFlipButtonPress = () => {
    twilioVideo.current.flipCamera();
  };

  const _onRoomDidConnect = () => {
    setStatus('connected');
  };

  const _onRoomDidDisconnect = error => {
    console.log('ERROR: ', error);

    // End Call
    stopCallingSound();
    updateCallInfo('waiting', '', '');
    twilioVideo.current.disconnect();
    setCallStatus('');
    clearDuration();

    setStatus('disconnected');
    setCallStart(false);
  };

  const _onRoomDidFailToConnect = error => {
    console.log('ERROR: ', error);

    // End Call
    stopCallingSound();
    updateCallInfo('waiting', '', '');
    twilioVideo.current.disconnect();
    setCallStatus('');
    clearDuration();

    setStatus('disconnected');
    setCallStart(false);
  };

  const _onParticipantAddedVideoTrack = ({participant, track}) => {
    console.log('onParticipantAddedVideoTrack: ', participant, track);

    setVideoTracks(
      new Map([
        ...videoTracks,
        [
          track.trackSid,
          {participantSid: participant.sid, videoTrackSid: track.trackSid},
        ],
      ]),
    );

    setCallStart(true);
    startDuration();
  };

  const _onParticipantRemovedVideoTrack = ({participant, track}) => {
    console.log('onParticipantRemovedVideoTrack: ', participant, track);

    const newVideoTracks = new Map(videoTracks);
    newVideoTracks.delete(track.trackSid);

    setVideoTracks(newVideoTracks);
  };

  const _onNetworkLevelChanged = ({participant, isLocalUser, quality}) => {
    console.log(
      'Participant',
      participant,
      'isLocalUser',
      isLocalUser,
      'quality',
      quality,
    );
  };

  const _onDominantSpeakerDidChange = ({roomName, roomSid, participant}) => {
    console.log(
      'onDominantSpeakerDidChange',
      `roomName: ${roomName}`,
      `roomSid: ${roomSid}`,
      'participant:',
      participant,
    );
  };

  const _requestAudioPermission = () => {
    return PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
      {
        title: 'Need permission to access microphone',
        message:
          'To run this demo we need permission to access your microphone',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
  };

  const _requestCameraPermission = () => {
    return PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA, {
      title: 'Need permission to access camera',
      message: 'To run this demo we need permission to access your camera',
      buttonNegative: 'Cancel',
      buttonPositive: 'OK',
    });
  };

  //
  // End Call Service Functions
  //

  const updateCallInfo = (status, type, roomID) => {
    console.log('firebaseDocId update callinfo', firebaseDocId);
    console.log('step 2');

    let patientData = {
      name: userData?.userInfo?.name,
      gender: userData?.userInfo?.gender,
      imgUrl: userData?.userInfo?.image_url,
      dob: userData?.userInfo?.dob,
    };

    firebase
      .app()
      .database(Config.FIREBASE_REALTIME_DB)
      .ref(`/doctors/${doctor_info?.morzapp_id}`)
      .update({
        firebaseDocId: status === 'calling' ? firebaseDocId : '',
        callStatus: status,
        callType: type,
        patientInfo: status === 'calling' ? patientData : {},
        roomID: roomID,
      })
      .then(data => {
        //success callback
      })
      .catch(error => {
        //error callback
        console.log('error ', error);
      });
  };

  const updateCallWithFirebase = (status, type, roomID, firebaseID) => {
    let patientData = {
      name: userData?.userInfo?.name,
      gender: userData?.userInfo?.gender,
      imgUrl: userData?.userInfo?.image_url,
      dob: userData?.userInfo?.dob,
    };

    firebase
      .app()
      .database(Config.FIREBASE_REALTIME_DB)
      .ref(`/doctors/${doctor_info?.morzapp_id}`)
      .update({
        firebaseDocId: firebaseID,
        callStatus: status,
        callType: type,
        patientInfo: status === 'calling' ? patientData : {},
        roomID: roomID,
      })
      .then(data => {
        //success callback
      })
      .catch(error => {
        //error callback
        console.log('error ', error);
      });
  };

  // Start Sound
  const startCallingSound = async () => {
    // Add a track to the queue
    await TrackPlayer.add({
      id: 'callId',
      url: require('../../../assets/sounds/calling.mp3'),
      title: 'MORZAPP Call',
      artist: 'MORZAPP',
      artwork: require('../../../assets/img/heart-icon.png'),
    });

    await TrackPlayer.setRepeatMode(RepeatMode.Track);
    // Start playing it
    await TrackPlayer.play();
  };

  const stopCallingSound = async () => {
    await TrackPlayer.pause();
    await TrackPlayer.reset();
  };

  const recordCallHistory = (calltype, callStatus, connectRoomID) => {
    let data = {
      roomID: connectRoomID,
      doctorID: doctor_info?.morzapp_id,
      doctorName: realtimeDoctorInfo?.doctorName,
      callAction: 'PatientCalling',
      user_id: userData?.userInfo?.user_id,
      phone:
        userData?.userInfo?.phone !== undefined
          ? userData?.userInfo?.phone
          : '',
      patientName: userData?.userInfo?.name,
      gender:
        userData?.userInfo?.gender !== undefined
          ? userData?.userInfo?.gender
          : '',
      imgUrl: userData?.userInfo?.image_url,
      dob: userData?.userInfo?.dob !== undefined ? userData?.userInfo?.dob : '',
      callType: calltype,
      startedAt: firebase.firestore.FieldValue.serverTimestamp(),
    };

    try {
      firestore()
        .collection('callHistory')
        .add(data)
        .then(data => {
          console.log('Document successfully written!');
          setFirebaseDocId(data.id);
          updateCallWithFirebase(callStatus, calltype, connectRoomID, data.id);
        })
        .catch(error => {
          console.error('Error writing document: ', error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  // Count Time Duration
  const startDuration = () => {
    let second = 0;
    countDurationSecond = setInterval(() => {
      second += 1;
      setCountDuration(second);
    }, 1000);
  };

  const clearDuration = () => {
    clearInterval(countDurationSecond);
  };

  const renderTimeSecond = value => {
    let second = value % 60;
    return (
      Math.floor(value / 60) +
      ':' +
      (second ? (second < 10 ? '0' + second : second) : '00')
    );
  };

  const updateDuration = () => {
    try {
      firestore()
        .collection('callHistory')
        .doc(firebaseDocId)
        .update({
          durationSec: countDuration,
        })
        .then(() => {
          console.log('Document successfully written!');
          setCountDuration(0);
        })
        .catch(error => {
          console.error('Error writing document: ', error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Box flex={1} backgroundColor={colors.White}>
      {status === 'disconnected' && (
        <>
          <ScrollView style={{...defaultStyle.fixedLayout}}>
            <SafeAreaView>
              <HStack space={1} style={{paddingTop: 20, paddingBottom: 10}}>
                <VStack
                  w={'35%'}
                  style={{
                    alignItems: 'center',
                  }}>
                  <FastImage
                    style={{
                      width: 100,
                      height: 100,
                      borderRadius: 20,
                      alignSelf: 'center',
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                    source={{uri: doctor_info?.picture}}
                  />
                </VStack>

                {/* Col 2 */}
                <VStack w={'62%'}>
                  <TextM
                    fontWeight="bold"
                    style={{color: colors.DarkBlue, fontSize: 18}}>
                    {doctor_info?.fullname}
                  </TextM>
                  {/* Status */}
                  {/* <TextM style={{color: colors.Gray}}>ສະຖານະ:</TextM> */}
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <MaterialCommunityIcons
                      name={'checkbox-blank-circle'}
                      style={{
                        color:
                          renderDoctorStatus() === 'ONLINE'
                            ? colors.Green
                            : renderDoctorStatus() === 'AWAY'
                            ? colors.Orange
                            : colors.Red,
                        fontSize: 14,
                        marginRight: 2,
                      }}
                    />
                    <TextM
                      style={{
                        color:
                          renderDoctorStatus() === 'ONLINE'
                            ? colors.Green
                            : renderDoctorStatus() === 'AWAY'
                            ? colors.Orange
                            : colors.Red,
                        fontSize: 14,
                      }}>
                      {renderDoctorStatus()}
                    </TextM>
                  </View>
                  {/* School */}
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <MaterialCommunityIcons
                      name="school"
                      style={{
                        color: colors.Blue,
                        marginRight: 5,
                        fontSize: 18,
                      }}
                    />
                    <TextM style={{fontSize: 14}}>
                      {doctor_info?.institute}
                    </TextM>
                  </View>
                  {/* Address */}
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <MaterialCommunityIcons
                      name="map-marker"
                      style={{
                        color: colors.Blue,
                        marginRight: 5,
                        fontSize: 18,
                      }}
                    />
                    <TextM style={{fontSize: 14}}>
                      {renderLocationName(doctor_info?.location)}
                    </TextM>
                  </View>
                </VStack>
              </HStack>
            </SafeAreaView>

            {/* Content Body */}
            <View>
              {/* Consultancy */}
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginBottom: 5,
                }}>
                <FontAwesome5
                  name="stethoscope"
                  style={{color: colors.DarkBlue, marginRight: 5, fontSize: 18}}
                />
                <TextM style={{color: colors.DarkBlue, fontSize: 18}}>
                  ໃຫ້ຄຳປຶກສາດ້ານ:
                </TextM>
              </View>
              {doctor_info?.consultancy.length > 0 && (
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'flex-start',
                    flexWrap: 'wrap',
                  }}>
                  {doctor_info?.consultancy.map((item, i) => {
                    return (
                      <View
                        key={i}
                        style={{
                          // backgroundColor: colors.LightPink,
                          marginBottom: 4,
                          paddingHorizontal: 8,
                          borderRadius: 8,
                          borderColor: colors.Blue,
                          borderWidth: 1,
                          marginRight: 5,
                        }}>
                        <TextM style={{fontSize: 14, color: colors.Blue}}>
                          {item?.name}
                        </TextM>
                      </View>
                    );
                  })}
                </View>
              )}

              {/* Specialist */}
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginBottom: 5,
                  marginTop: 10,
                }}>
                <MaterialCommunityIcons
                  name="certificate"
                  style={{color: colors.DarkBlue, marginRight: 5, fontSize: 18}}
                />
                <TextM style={{color: colors.DarkBlue, fontSize: 18}}>
                  ສາຂາຮຽນຈົບ:
                </TextM>
              </View>
              {doctor_info?.specialist.length > 0 && (
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'flex-start',
                    marginBottom: 5,
                    flexWrap: 'wrap',
                  }}>
                  {doctor_info?.specialist.map((item, i) => {
                    return (
                      <View
                        key={i}
                        style={{
                          // backgroundColor: colors.LightPink,
                          paddingHorizontal: 8,
                          borderRadius: 8,
                          marginBottom: 4,
                          marginRight: 5,
                          borderColor: colors.LightPink,
                          borderWidth: 1,
                        }}>
                        <TextM style={{fontSize: 14, color: colors.LightPink}}>
                          {item?.name}
                        </TextM>
                      </View>
                    );
                  })}
                </View>
              )}

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginBottom: 5,
                  marginTop: 5,
                }}>
                <MaterialCommunityIcons
                  name="card-account-details"
                  style={{color: colors.DarkBlue, marginRight: 5, fontSize: 18}}
                />
                <TextM style={{color: colors.DarkBlue, fontSize: 18}}>
                  ປະຫວັດຫຍໍ້ຂອງໝໍ
                </TextM>
              </View>
              <RenderHtml
                contentWidth={width}
                source={{
                  html: doctor_info?.description,
                }}
                tagsStyles={defaultStyle.htmlStyle}
                systemFonts={systemFonts}
                customHTMLElementModels={customHTMLElementModels}
              />
            </View>
            <VStack style={{marginVertical: 10}}>
              <Box
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginBottom: 5,
                }}>
                <MaterialCommunityIcons
                  name="translate"
                  style={{color: colors.DarkBlue, marginRight: 5, fontSize: 18}}
                />
                <TextM fontWeight="bold" style={{color: colors.DarkBlue}}>
                  ພາສາທີ່ເວົ້າໄດ້:
                </TextM>
              </Box>
              <Box>
                <TextM note>{langSupport()}</TextM>
              </Box>
            </VStack>
          </ScrollView>

          {/* Call Button Action */}
          <SafeAreaView>
            <View style={{paddingVertical: 20}}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                }}>
                <View
                  style={{
                    alignItems: 'center',
                    alignSelf: 'center',
                  }}>
                  <TouchableOpacity
                    style={{
                      backgroundColor: colors.White,
                      borderColor: colors.Blue,
                      borderWidth: 1,
                      padding: 10,
                      borderRadius: 10,
                      alignItems: 'center',
                    }}
                    onPress={() => {
                      checkUserLogin('voice');
                    }}>
                    {renderDoctorStatus() !== 'ONLINE' ? (
                      <Box
                        style={{
                          position: 'absolute',
                          top: -10,
                          right: -10,
                          backgroundColor: colors.White,
                          borderRadius: 15,
                        }}>
                        <MaterialCommunityIcons
                          name="alert-circle"
                          style={{
                            color: colors.Red,
                            fontSize: 24,
                          }}
                        />
                      </Box>
                    ) : null}
                    <MaterialCommunityIcons
                      name="phone-outline"
                      style={{color: colors.Blue, fontSize: 24}}
                    />
                    <TextM
                      fontWeight="bold"
                      style={{color: colors.Blue, fontSize: 12}}>
                      {checkCallPrice(doctor_info?.voice_call_fee)}
                    </TextM>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    alignItems: 'center',
                    alignSelf: 'center',
                  }}>
                  <TouchableOpacity
                    style={{
                      backgroundColor: colors.White,
                      borderColor: colors.Orange,
                      borderWidth: 1,
                      padding: 10,
                      borderRadius: 10,
                      alignItems: 'center',
                    }}
                    onPress={() => {
                      checkUserLogin('video');
                    }}>
                    {renderDoctorStatus() !== 'ONLINE' ? (
                      <Box
                        style={{
                          position: 'absolute',
                          top: -10,
                          right: -10,
                          backgroundColor: colors.White,
                          borderRadius: 15,
                        }}>
                        <MaterialCommunityIcons
                          name="alert-circle"
                          style={{
                            color: colors.Red,
                            fontSize: 24,
                          }}
                        />
                      </Box>
                    ) : null}

                    <MaterialCommunityIcons
                      name="video-outline"
                      style={{color: colors.Orange, fontSize: 24}}
                    />
                    <TextM
                      fontWeight="bold"
                      style={{color: colors.Orange, fontSize: 12}}>
                      {checkCallPrice(doctor_info?.video_call_fee)}
                    </TextM>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </SafeAreaView>
        </>
      )}

      {/* */}
      {/* */}
      {/* */}
      {/* */}
      {/* */}
      {/* Call Connect */}
      {/* */}
      {/* */}
      {/* */}
      {/* */}
      {/* */}
      {(status === 'connected' || status === 'connecting') && (
        <View style={styles.callContainer}>
          {callStart
            ? callStatus === 'video' &&
              status === 'connected' &&
              realtimeDoctorInfo?.callStatus === 'connected' && (
                <View style={styles.remoteGrid}>
                  {Array.from(videoTracks, ([trackSid, trackIdentifier]) => {
                    return (
                      <TwilioVideoParticipantView
                        style={styles.remoteVideo}
                        key={trackSid}
                        trackIdentifier={trackIdentifier}
                      />
                    );
                  })}
                </View>
              )
            : null}

          {callStart
            ? callStatus === 'video' && (
                <TwilioVideoLocalView
                  applyZOrder={true}
                  enabled={true}
                  style={styles.localVideo}
                />
              )
            : null}

          {/* Start Connecting */}
          {callStart
            ? callStatus === 'voice' && (
                <View
                  style={{
                    backgroundColor: colors.White,
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TextM fontWeight="bold" style={{fontSize: 22}}>
                    {doctor_info?.fullname}
                  </TextM>
                  <FastImage
                    style={{
                      width: 120,
                      height: 120,
                      borderRadius: 100,
                      marginVertical: 20,
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                    source={{uri: doctor_info?.picture}}
                  />
                  <TextM>{renderTimeSecond(countDuration)}</TextM>
                </View>
              )
            : null}

          {callStatus === '' && (
            <View
              style={{
                backgroundColor: colors.White,
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <FastImage
                style={{
                  width: 120,
                  height: 120,
                  marginVertical: 20,
                }}
                resizeMode={FastImage.resizeMode.cover}
                source={require('../../../assets/img/logo-400x400.png')}
              />
              <TextM>ກຳລັງວາງສາຍ</TextM>
            </View>
          )}

          {callStatus !== '' &&
          realtimeDoctorInfo?.callStatus !== 'connected' ? (
            <View
              style={{
                backgroundColor: colors.White,
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <TextM fontWeight="bold" style={{fontSize: 22}}>
                {doctor_info?.fullname}
              </TextM>
              <FastImage
                style={{
                  width: 120,
                  height: 120,
                  borderRadius: 100,
                  marginVertical: 20,
                }}
                resizeMode={FastImage.resizeMode.contain}
                source={{uri: doctor_info?.picture}}
              />

              <TextM>ກຳລັງເຊື່ອມຕໍ່...</TextM>
            </View>
          ) : null}

          {realtimeDoctorInfo?.callStatus === 'connected' && !callStart ? (
            <View
              style={{
                backgroundColor: colors.White,
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <TextM fontWeight="bold" style={{fontSize: 22}}>
                {doctor_info?.fullname}
              </TextM>
              <FastImage
                style={{
                  width: 120,
                  height: 120,
                  borderRadius: 100,
                  marginVertical: 20,
                }}
                resizeMode={FastImage.resizeMode.contain}
                source={{uri: doctor_info?.picture}}
              />

              <TextM>ກະລຸນາລໍຖ້າ...</TextM>
            </View>
          ) : null}

          {/* Count Time Duration */}
          {callStart
            ? realtimeDoctorInfo?.callStatus === 'connected' &&
              callStatus === 'video' && (
                <View
                  style={{
                    backgroundColor: 'rgba(255, 255, 255, 0.8)',
                    alignSelf: 'center',
                    paddingHorizontal: 10,
                    paddingVertical: 0,
                    borderRadius: 8,
                    marginBottom: 5,
                  }}>
                  <TextM>{renderTimeSecond(countDuration)}</TextM>
                </View>
              )
            : null}

          {/* Action on Call Button */}
          <View style={styles.optionsContainer}>
            {callStart
              ? realtimeDoctorInfo?.callStatus === 'connected' && (
                  <TouchableOpacity
                    onPress={_onMuteButtonPress}
                    style={{alignItems: 'center'}}>
                    <View
                      style={{
                        backgroundColor: isAudioEnabled
                          ? colors.Green
                          : colors.LightBlack,
                        padding: 15,
                        borderRadius: 100,
                        marginBottom: 5,
                      }}>
                      <MaterialCommunityIcons
                        name={isAudioEnabled ? 'microphone' : 'microphone-off'}
                        style={{
                          color: colors.White,
                          fontSize: 20,
                        }}
                      />
                    </View>
                    <TextM style={{fontSize: 14}}>
                      {isAudioEnabled ? 'ເປີດໄມ' : 'ປິດໄມ'}
                    </TextM>
                  </TouchableOpacity>
                )
              : null}

            {callStart
              ? callStatus === 'video' &&
                realtimeDoctorInfo?.callStatus === 'connected' && (
                  <TouchableOpacity
                    style={{alignItems: 'center'}}
                    onPress={_onFlipButtonPress}>
                    <View
                      style={{
                        backgroundColor: colors.Blue,
                        padding: 15,
                        borderRadius: 100,
                        marginBottom: 5,
                      }}>
                      <MaterialCommunityIcons
                        name="repeat"
                        style={{
                          color: colors.White,
                          fontSize: 20,
                        }}
                      />
                    </View>
                    <TextM style={{fontSize: 14}}>ປ່ຽນກ້ອງ</TextM>
                  </TouchableOpacity>
                )
              : null}

            {callStatus !== '' &&
            realtimeDoctorInfo?.callStatus === 'calling' ? (
              <TouchableOpacity
                onPress={_onEndButtonPress}
                style={{alignItems: 'center'}}>
                <View
                  style={{
                    backgroundColor: colors.Red,
                    padding: 15,
                    borderRadius: 100,
                    marginBottom: 5,
                  }}>
                  <MaterialCommunityIcons
                    name="phone-hangup"
                    style={{
                      color: colors.White,
                      fontSize: 20,
                    }}
                  />
                </View>
                <TextM style={{fontSize: 14}}>ວາງສາຍ</TextM>
              </TouchableOpacity>
            ) : null}

            {realtimeDoctorInfo?.callStatus === 'connected' && callStart ? (
              <TouchableOpacity
                onPress={_onEndButtonPress}
                style={{alignItems: 'center'}}>
                <View
                  style={{
                    backgroundColor: colors.Red,
                    padding: 15,
                    borderRadius: 100,
                    marginBottom: 5,
                  }}>
                  <MaterialCommunityIcons
                    name="phone-hangup"
                    style={{
                      color: colors.White,
                      fontSize: 20,
                    }}
                  />
                </View>
                <TextM style={{fontSize: 14}}>ວາງສາຍ</TextM>
              </TouchableOpacity>
            ) : null}
          </View>
        </View>
      )}
      <TwilioVideo
        ref={twilioVideo}
        onRoomDidConnect={_onRoomDidConnect}
        onRoomDidDisconnect={_onRoomDidDisconnect}
        onRoomDidFailToConnect={_onRoomDidFailToConnect}
        onParticipantAddedVideoTrack={_onParticipantAddedVideoTrack}
        onParticipantRemovedVideoTrack={_onParticipantRemovedVideoTrack}
        onNetworkQualityLevelsChanged={_onNetworkLevelChanged}
        onDominantSpeakerDidChange={_onDominantSpeakerDidChange}
      />

      {/* Check Login User*/}
      <Portal>
        <Dialog
          visible={showAlert}
          onDismiss={hideAlert}
          style={{borderRadius: 10}}>
          <Dialog.Title>
            <TextM fontWeight="bold" style={{fontSize: 18}}>
              ຂໍອະໄພທ່ານຍັງບໍ່ທັນໄດ້ເຂົ້າສູ່ລະບົບ
            </TextM>
          </Dialog.Title>
          <Dialog.Content>
            <TextM>ກະລຸນາເຂົ້າສູ່ລະບົບກ່ອນເພື່ອໂທປຶກສາໝໍໄດ້</TextM>
          </Dialog.Content>
          <Dialog.Actions>
            <ButtonN onPress={() => hideAlert()}>
              <TextM style={{color: colors.Red}}>ກັບຄືນ</TextM>
            </ButtonN>
            <ButtonN
              onPress={() => {
                hideAlert();
                navigation.navigate('Profile');
              }}>
              <TextM>ເຂົ້າສູ່ລະບົບ</TextM>
            </ButtonN>
          </Dialog.Actions>
        </Dialog>
      </Portal>

      {/* Confirm Call */}
      <Portal>
        <Dialog
          visible={showCallAlert}
          onDismiss={hideAlert}
          style={{borderRadius: 10}}>
          <Dialog.Title>
            <TextM fontWeight="bold" style={{fontSize: 18}}>
              ທ່ານຕ້ອງການໂທປຶກສາທ່ານໝໍບໍ ?
            </TextM>
          </Dialog.Title>
          {/* <Dialog.Content>
            <TextM>ກະລຸນາຢືນຢັນການໂທປຶກສາໝໍ</TextM>
          </Dialog.Content> */}
          <Dialog.Actions>
            <ButtonN
              onPress={() => {
                setShowCallAlert(false);
              }}>
              <TextM style={{color: colors.Red}}>ຍົກເລີກ</TextM>
            </ButtonN>
            <ButtonN
              onPress={() => {
                setShowCallAlert(false);
                callType === 'voice' ? onStartVoiceCall() : onStartVideoCall();
              }}>
              <MaterialCommunityIcons
                name="phone-outline"
                style={{color: colors.Blue, fontSize: 16}}
              />{' '}
              <TextM>ໂທປຶກສາໝໍ</TextM>
            </ButtonN>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </Box>
  );
};

export default DoctorDetail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  callContainer: {
    backgroundColor: colors.White,
    flex: 1,
    position: 'absolute',
    bottom: 0,
    top: 0,
    left: 0,
    right: 0,
  },
  welcome: {
    fontSize: 30,
    textAlign: 'center',
    paddingTop: 40,
  },
  input: {
    height: 50,
    borderWidth: 1,
    marginRight: 70,
    marginLeft: 70,
    marginTop: 20,
    marginBottom: 20,
    textAlign: 'center',
    backgroundColor: 'white',
  },
  button: {
    marginTop: 100,
  },
  localVideo: {
    flex: 1,
    width: 100,
    height: 160,
    position: 'absolute',
    right: 10,
    top: 5,
  },
  remoteGrid: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    // marginRight: 50,
  },
  remoteVideo: {
    flex: 1,
    height: windowHeight,
  },
  optionsContainer: {
    paddingVertical: 10,
    backgroundColor: 'rgba(255, 255, 255, 1)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginBottom: 10,
    marginHorizontal: 10,
    borderRadius: 20,
  },
  optionButton: {
    width: 60,
    height: 60,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 100 / 2,
    backgroundColor: 'grey',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
