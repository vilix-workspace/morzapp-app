import React, {useEffect, useState} from 'react';
import {StyleSheet, Keyboard} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {
  Box,
  ScrollView,
  View,
  Text,
  Button,
  useToast,
  HStack,
  VStack,
  Alert,
} from 'native-base';
import {
  IndexPath,
  Layout,
  Select,
  SelectItem,
  Input,
} from '@ui-kitten/components';
import {Dialog, Portal, Button as ButtonN} from 'react-native-paper';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {CommonActions} from '@react-navigation/native';
import WooCommerce from '../../../config/WooCommerce';
// Component
import Loading from '../../shareComponents/Loading';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';
import TextM from '../../shareComponents/TextM';
// Redux
import {useSelector, useDispatch} from 'react-redux';
//Redux action
import {clearCart} from '../../../stores/features/cart/cartSlice';

var radio_props = [
  {label: 'ໂອນຜ່ານບັນຊີທະນາຄານ', value: 'bacs'},
  {label: 'ຈ່າຍເປັນເງິນສົດເກັບປາຍທາງ', value: 'cod'},
  // {label: 'ຈ່າຍຜ່ານກະເປົາເງິນຂອງທ່ານ', value: 'wallet'},
];

const dataCity = [
  {title: 'ເລືອກເມືອງທີ່ທ່ານຢູ່', value: 'No District'},
  {title: 'ຈັນທະບູລີ', value: 'Chanthabuly'},
  {title: 'ສີໂຄດຕະບອງ', value: 'Sikhottabong'},
  {title: 'ໄຊເສດຖາ', value: 'Xaysetha'},
  {title: 'ສີສັດຕະນາກ', value: 'Sisattanak'},
  {title: 'ນາຊາຍທອງ', value: 'Naxaythong'},
  {title: 'ໄຊທານີ', value: 'Xaythany'},
  {title: 'ຫາດຊາຍຟອງ', value: 'Hadxayfong'},
  {title: 'ສັງທອງ', value: 'Sangthong'},
  {title: 'ປາກງື່ມ', value: 'Parknguem'},
];

const Checkout = ({navigation}) => {
  const toast = useToast();
  // Redux
  const reduxStore = useSelector(state => state);
  const {cart, userData} = reduxStore;
  const dispatch = useDispatch();
  // useState
  const [loading, setLoading] = useState(false);
  const [selectedIndex, setSelectedIndex] = React.useState(new IndexPath(0));
  const displayValue = dataCity[selectedIndex.row];
  const [customerName, setCustomerName] = useState(userData?.userInfo?.name);
  const [address, setAddress] = useState('');
  const [secondPhone, setSecondPhone] = useState('');
  const [paymentType, setPaymentType] = useState('bacs');
  const [paymentTypeIndex, setPaymentTypeIndex] = useState(0);
  const [showAlert, setShowAlert] = useState(false);
  const [service, setService] = React.useState('');

  // Funtion
  const showToast = (type, title, desc) => {
    toast.show({
      render: () => {
        return (
          <Alert
            maxWidth="95%"
            alignSelf="center"
            flexDirection="row"
            status={type}
            variant={'left-accent'}>
            <VStack space={1} flexShrink={1} w="100%">
              <HStack
                flexShrink={1}
                alignItems="center"
                justifyContent="space-between">
                <HStack space={2} flexShrink={1} alignItems="center">
                  <Alert.Icon />
                  <TextM>{title}</TextM>
                </HStack>
              </HStack>
              {desc !== undefined && (
                <TextM style={{fontSize: 14, paddingLeft: 25}}>{desc}</TextM>
              )}
            </VStack>
          </Alert>
        );
      },
    });
  };

  // Check Error
  const toastError = error => {
    showToast('warning', 'ກະລຸນາຕື່ມຂໍ້ມູນ' + error + '!');
  };

  // Alert
  const showAlertNow = () => {
    setShowAlert(true);
  };

  const hideAlert = () => {
    setShowAlert(false);
  };

  const numberWithCommas = x => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  const totalPrice = () => {
    let total = 0;
    if (cart?.cartInfo?.length > 0) {
      for (const iterator of cart?.cartInfo) {
        total += parseInt(iterator.price) * iterator.quantity;
      }
      return total;
    } else {
      return total;
    }
  };

  const onCheckDetail = () => {
    if (customerName == '') {
      toastError('ຊື່ລູກຄ້າ');
    } else if (selectedIndex.row == 0) {
      toastError('ເມືອງທີ່ທ່ານຢູ່');
    } else if (address == '') {
      toastError('ທີ່ຢູ່ບ້ານ ແລະ ສະຖານທີ່ໃກ້ຄຽງ');
    } else {
      showAlertNow();
    }
  };

  const onCheckout = async () => {
    setLoading(true);
    const productArray = cart?.cartInfo?.map(({id, quantity}) => ({
      product_id: id,
      quantity: quantity,
    }));

    let scphone = secondPhone !== '' ? ' / ' + secondPhone : '';

    let orderdata = {
      customer_id: userData?.userInfo?.user_id,
      payment_method: paymentType,
      payment_method_title:
        paymentType === 'bacs' ? 'Direct Bank Transfer' : 'Cash on Delivery',
      set_paid: false,
      billing: {
        first_name: userData?.userInfo?.name,
        last_name: '',
        address_1: '',
        address_2: '',
        city: displayValue.value + ' District',
        state: 'Vientiane Capital',
        postcode: '01000',
        country: 'LA',
        email:
          userData?.userInfo?.email === ''
            ? 'noemail@noemail.com'
            : userData?.userInfo?.email,
        phone: userData?.userInfo?.phone + scphone,
      },
      shipping: {
        first_name: customerName,
        last_name: '',
        address_1: address,
        address_2: '',
        city: displayValue.value + ' District',
        state: 'Vientiane Capital',
        postcode: '01000',
        country: 'LA',
      },
      line_items: productArray,
      shipping_lines: [
        {
          method_id: 'free_shipping',
          method_title: 'Free shipping',
          total: '0.00',
        },
      ],
    };

    try {
      await WooCommerce.post('orders', orderdata)
        .then(res => {
          dispatch(clearCart());
          navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes: [
                {
                  name: 'HomeStack',
                },
                {
                  name: 'CurrentTransaction',
                },
              ],
            }),
          );
          setLoading(false);
          showToast('success', 'ການສັ່ງຊື້ສຳເລັດແລ້ວ');
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  // useEffect
  useEffect(() => {
    // console.log('useEffect Checkout cart', cart.cartInfo);
    // console.log('useEffect Checkout userData', userData.userInfo);
  }, []);

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <ScrollView style={{...defaultStyle.fixedLayout}}>
        <TextM>ກະລຸນາເພີ່ມຂໍ້ມູນສຳລັບການຈັດສົ່ງສິນຄ້າ</TextM>
        {/* Customer Name */}
        <Layout style={{marginTop: 5}}>
          <Input
            value={customerName}
            label={
              <HStack>
                <MaterialCommunityIcons
                  name="account"
                  style={{color: colors.DarkBlue, fontSize: 20}}
                />
                <TextM style={{color: colors.DarkBlue}}>ຊື່ລູກຄ້າ</TextM>
                <Text style={{color: colors.Red}}>*</Text>
              </HStack>
            }
            placeholder="ຊື່ ແລະ ນາມສະກຸນ"
            onChangeText={nextValue => setCustomerName(nextValue)}
            textStyle={{
              minHeight: 30,
              fontSize: 18,
              fontFamily: 'NotoSansLao-Regular',
            }}
          />
        </Layout>
        {/* Select District */}
        <Layout style={{marginTop: 5}} level="1">
          <HStack>
            <MaterialCommunityIcons
              name="map-marker"
              style={{color: colors.DarkBlue, fontSize: 20}}
            />
            <TextM style={{color: colors.DarkBlue}}>ເລືອກເມືອງທີ່ທ່ານຢູ່</TextM>
            <Text style={{color: colors.Red, fontSize: 16, fontWeight: 'bold'}}>
              *
            </Text>
          </HStack>
          <Select
            style={{minHeight: 30}}
            value={() => (
              <TextM
                style={{
                  color: selectedIndex.row === 0 ? '#bbbbbb' : '#333333',
                }}>
                {displayValue?.title}
              </TextM>
            )}
            selectedIndex={selectedIndex}
            onSelect={index => setSelectedIndex(index)}
            onFocus={() => Keyboard.dismiss()}>
            {dataCity.map((data, i) => {
              return (
                <SelectItem
                  key={i}
                  title={() => (
                    <TextM style={{color: i === 0 ? '#bbbbbb' : '#333333'}}>
                      {data?.title}
                    </TextM>
                  )}
                />
              );
            })}
          </Select>
        </Layout>
        {/* Address */}
        <Layout style={{marginTop: 10}}>
          <Input
            value={address}
            label={
              <HStack>
                <MaterialCommunityIcons
                  name="home-city"
                  style={{color: colors.DarkBlue, fontSize: 20, marginRight: 5}}
                />
                <TextM style={{color: colors.DarkBlue}}>
                  ຕື່ມທີ່ຢູ່ບ້ານ ແລະ ສະຖານທີ່ໃກ້ຄຽງ
                </TextM>
                <Text style={{color: colors.Red}}>*</Text>
              </HStack>
            }
            placeholder="ຕົວຢ່າງ: ບ້ານທາດຫລວງ ໃກ້ກັບຕະຫລາດທາດຫລວງ..."
            onChangeText={nextValue => setAddress(nextValue)}
            textStyle={{
              minHeight: 30,
              fontSize: 14,
              fontFamily: 'NotoSansLao-Regular',
            }}
          />
        </Layout>
        {/* Second Phone */}
        <Layout style={{marginTop: 5}}>
          <Input
            value={secondPhone}
            label={
              <HStack>
                <MaterialCommunityIcons
                  name="phone"
                  style={{color: colors.DarkBlue, fontSize: 20, marginRight: 5}}
                />
                <TextM style={{color: colors.DarkBlue}}>
                  ເບີໂທສຳຮອງ (ຖ້າມີ)
                </TextM>
              </HStack>
            }
            placeholder="020 - - - - - - - -"
            onChangeText={nextValue => setSecondPhone(nextValue)}
            textStyle={{
              minHeight: 30,
              fontSize: 18,
              fontFamily: 'NotoSansLao-Regular',
            }}
            keyboardType={'numeric'}
          />
        </Layout>
        {/* Sumary Order */}
        <Layout style={{marginTop: 10}}>
          <TextM style={{color: colors.Orange}}>
            <MaterialCommunityIcons
              name="clipboard-text"
              style={{color: colors.Orange, fontSize: 20}}
            />{' '}
            ສະຫລຸບການສັ່ງຊື້
          </TextM>
          <View
            style={{
              borderWidth: 1,
              borderColor: '#ebebeb',
              marginTop: 5,
              padding: 10,
              borderRadius: 10,
              marginBottom: 20,
            }}>
            <HStack space={1} alignItems={'center'}>
              <MaterialCommunityIcons
                name="cash"
                style={{color: colors.DarkBlue, fontSize: 20}}
              />
              <TextM style={{color: colors.DarkBlue}}>ວິທີການຈ່າຍເງິນ</TextM>
            </HStack>
            <View style={{marginTop: 5}}>
              <RadioForm animation={true}>
                {radio_props.map((obj, i) => {
                  var onPress = (value, index) => {
                    setPaymentType(value);
                    setPaymentTypeIndex(index);
                  };
                  return (
                    <RadioButton labelHorizontal={true} key={i}>
                      {/*  You can set RadioButtonLabel before RadioButtonInput */}
                      <RadioButtonInput
                        obj={obj}
                        index={i}
                        isSelected={paymentTypeIndex === i}
                        onPress={onPress}
                        buttonInnerColor={colors.Blue}
                        buttonOuterColor={colors.Blue}
                        buttonSize={12}
                        buttonStyle={{}}
                        buttonWrapStyle={{marginLeft: 15}}
                      />
                      <RadioButtonLabel
                        obj={obj}
                        index={i}
                        onPress={onPress}
                        labelStyle={{
                          color: '#333333',
                          marginLeft: 5,
                          fontFamily: 'NotoSansLao-Regular',
                        }}
                      />
                    </RadioButton>
                  );
                })}
              </RadioForm>
            </View>

            <HStack space={1} alignItems={'center'}>
              <MaterialCommunityIcons
                name="format-list-checkbox"
                style={{color: colors.DarkBlue, fontSize: 20}}
              />
              <TextM style={{color: colors.DarkBlue}}>ລາຍການສິນຄ້າ</TextM>
            </HStack>
            <VStack>
              <View style={{flexDirection: 'row'}}>
                <View style={[styles.insOrderList]}>
                  {cart?.cartInfo?.map((data, i) => {
                    return (
                      <HStack
                        key={i}
                        justifyContent={'space-between'}
                        w={'95%'}>
                        <Box w={'50%'}>
                          <TextM
                            style={{color: colors.LightBlack, fontSize: 14}}
                            numberOfLines={1}>
                            {i + 1}. {data?.name}
                          </TextM>
                        </Box>
                        <Box w={'40%'} alignItems={'flex-end'}>
                          <TextM
                            style={{color: colors.LightBlack, fontSize: 14}}>
                            {data?.quantity} x {numberWithCommas(data?.price)}{' '}
                            ກີບ
                          </TextM>
                        </Box>
                      </HStack>
                    );
                  })}
                </View>
              </View>
            </VStack>

            <HStack mt={1} justifyContent={'space-between'}>
              <Box>
                <TextM>ຄ່າຈັດສົ່ງ</TextM>
              </Box>
              <Box>
                <TextM>ຟຣີ (Free)</TextM>
              </Box>
            </HStack>

            <HStack mt={1} justifyContent={'space-between'}>
              <Box>
                <TextM style={{color: colors.Pink}}>ຍອດລວມທັງໝົດ</TextM>
              </Box>
              <Box>
                <TextM
                  style={{
                    color: colors.Pink,
                    textDecorationLine: 'underline',
                  }}>
                  {numberWithCommas(totalPrice())} ກີບ
                </TextM>
              </Box>
            </HStack>
          </View>
        </Layout>
      </ScrollView>
      {/* Footer */}
      {loading && <Loading />}
      <View
        style={{
          paddingVertical: 5,
          paddingHorizontal: 20,
          alignItems: 'center',
        }}>
        <View style={{justifyContent: 'center', marginTop: 10, width: '100%'}}>
          <Button
            full
            onPress={() => {
              onCheckDetail();
              // onCheckout();
            }}
            style={{
              backgroundColor: colors.Blue,
              borderRadius: 10,
              marginBottom: 15,
            }}>
            <TextM style={{color: colors.White}}>ສັ່ງຊື້ເລີຍ</TextM>
          </Button>
        </View>
      </View>
      {/* Confirm Order */}
      {/* Modal Box */}
      <Portal>
        <Dialog
          visible={showAlert}
          onDismiss={hideAlert}
          style={{borderRadius: 10}}>
          <Dialog.Title>
            <TextM fontWeight="bold" style={{fontSize: 18}}>
              ຢືນຢັນການສັ່ງຊື້ ?{' '}
              {/* <MaterialCommunityIcons
                name="emoticon-sad-outline"
                style={{fontSize: 20, color: colors.Green}}
              /> */}
            </TextM>
          </Dialog.Title>
          <Dialog.Content>
            <TextM>ກະລຸນາກົດຢືນຢັນການສັ່ງຊື້ຂອງທ່ານ</TextM>
          </Dialog.Content>
          <Dialog.Actions>
            <ButtonN onPress={() => hideAlert()}>
              <TextM>ກັບຄືນ</TextM>
            </ButtonN>
            <ButtonN
              onPress={() => {
                onCheckout();
                hideAlert();
              }}>
              <TextM style={{color: colors.Green}}>ຢືນຢັນການສັ່ງຊື້</TextM>
            </ButtonN>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </Box>
  );
};

export default Checkout;

const styles = StyleSheet.create({
  insOrderList: {
    marginLeft: 15,
  },
  insOrderListRight: {
    paddingRight: 10,
  },
});
