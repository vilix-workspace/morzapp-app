import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  FlatList,
  SafeAreaView,
  Dimensions,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
// UI
import {TopNavigation} from '@ui-kitten/components';
import {Box, ScrollView, View, Text, HStack} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// Package
import FastImage from 'react-native-fast-image';
import Swiper from 'react-native-swiper';
// import RNRestart from 'react-native-restart';
import axios from 'axios';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import Config from 'react-native-config';

import WooCommerce from '../../../config/WooCommerce';
// Component
import TopicH from '../../shareComponents/TopicH';
import TextM from '../../shareComponents/TextM';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';
// Redux
import {useSelector, useDispatch} from 'react-redux';
import {space} from 'native-base/lib/typescript/theme/styled-system';

const width = Dimensions.get('window').width;

const Pharmacy = ({navigation}) => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData, cart} = reduxStore;
  const dispatch = useDispatch();
  // useState
  const [banner, setBanner] = useState([]);
  const [productCat, setProductCat] = useState([]);
  const [SKproductCat, setSKProductCat] = useState(true);
  const [SKBanner, setSKBanner] = useState(true);
  const [SKrecommendProducts, setSKRecommendProducts] = useState(false);
  const [SKrandomProducts, setSKRandomProducts] = useState(true);
  const [recommendProducts, setRecommendProducts] = useState([]);
  const [randomProducts, setRandomProducts] = useState([]);
  const [refreshing, setRefreshing] = useState(false);

  const imageWidth = 640; // Replace with the actual width of the image
  const imageHeight = 240; // Replace with the actual height of the image
  const aspectRatio = imageWidth / imageHeight;

  // Function
  const onRefresh = () => {
    setRefreshing(true);
    setSKBanner(true);
    setSKProductCat(true);
    setSKRandomProducts(true);
    setSKRecommendProducts(true);
    try {
      getShopBanner();
      getProductCat();
      setRefreshing(false);
    } catch (error) {
      console.log(error);
    }
  };

  const getShopBanner = async () => {
    try {
      await axios({
        method: 'get',
        url: Config.WP_URL + '/wp/v2/shop_banner',
        headers: {
          Authorization: 'Bearer ' + userData.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(async res => {
          setBanner(res.data);
          setSKBanner(false);
        })
        .catch(error => {
          console.log(error.response);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const getProductCat = async () => {
    try {
      await WooCommerce.get('products/categories', {per_page: 12})
        .then(async res => {
          let parentData = await res.find(obj => obj?.name === 'Customer');

          let dataCat = await res.filter(
            activeCat =>
              activeCat?.count > 0 && activeCat?.parent === parentData?.id,
          );

          const categoryNames = dataCat.map(category => category.name);

          setProductCat(dataCat);
          setSKProductCat(false);

          getRecommendProducts(categoryNames);
          getRandomProducts(categoryNames);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const getRecommendProducts = async categoryNames => {
    try {
      await WooCommerce.get('products', {
        tag: 24,
        per_page: 4,
        status: 'publish',
      })
        .then(async res => {
          const finalData = await res.filter(product =>
            categoryNames.includes(product.categories),
          );

          setRecommendProducts(finalData);
          setSKRecommendProducts(false);
        })
        .catch(error => {
          console.log(error.response);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const getRandomProducts = async categoryNames => {
    try {
      await WooCommerce.get('products', {
        per_page: 18,
        // orderby: 'popularity',
        status: 'publish',
      })
        .then(async res => {
          let finalData = await res.filter(product =>
            categoryNames.includes(product.categories),
          );

          // Shuffle the products array
          const shuffledProducts = finalData.sort(() => 0.5 - Math.random());

          setRandomProducts(shuffledProducts);
          setSKRandomProducts(false);
        })
        .catch(error => {
          console.log(error.response);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const numberWithCommas = x => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  // useEffect
  useEffect(() => {
    // console.log('useEffect Pharmacy', userData);
    getShopBanner();
    getProductCat();
  }, []);

  // Render Cat
  const renderCatItem = ({item}) => (
    <Box
      style={{
        marginTop: 5,
        padding: 2,
        minWidth: 120,
      }}>
      <TouchableOpacity
        key={item.id}
        onPress={() => navigation.navigate('ProductByCat', {catData: item})}>
        <View
          style={{
            marginRight: 10,
            padding: 8,
            borderRadius: 10,
            alignItems: 'center',
            backgroundColor: '#ffffff',
            shadowColor: '#666',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowOpacity: 0.2,
            shadowRadius: 1.41,

            elevation: 2,
          }}>
          <FastImage
            style={{width: 50, height: 50}}
            resizeMode={FastImage.resizeMode.contain}
            source={{uri: item?.image?.src}}
          />
          <TextM
            style={{
              marginTop: 5,
              fontSize: 14,
              textAlign: 'center',
              textAlignVertical: 'center',
              color: colors.DarkBlue,
            }}
            numberOfLines={1}>
            {item.name}
          </TextM>
        </View>
      </TouchableOpacity>
    </Box>
  );

  const renderHeaderLeft = () => (
    <View>
      <FastImage
        style={{width: 120, height: 40}}
        resizeMode={FastImage.resizeMode.contain}
        source={require('../../../assets/img/morzapp-logo-hor-660x180.png')}
      />
    </View>
  );

  const renderRightNotification = () => (
    <HStack space={4}>
      {/* <TouchableOpacity
        onPress={() => {
          navigation.navigate('NotificationList');
          // RNRestart.Restart();
        }}>
        <MaterialCommunityIcons
          name={'bell-ring'}
          style={{color: colors.Orange, fontSize: 28}}
        />
      </TouchableOpacity> */}
      <TouchableOpacity
        style={{width: 34}}
        onPress={() => navigation.navigate('MyCart')}>
        <MaterialCommunityIcons
          name={'cart-outline'}
          style={{color: colors.Gray, fontSize: 28}}
        />
        <Box style={styles.badge}>
          <Text
            style={{
              fontSize: 10,
              fontWeight: 'bold',
              color: colors.White,
            }}>
            {cart?.cartInfo?.length}
          </Text>
        </Box>
      </TouchableOpacity>
    </HStack>
  );

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <SafeAreaView>
        <TopNavigation
          style={{
            paddingHorizontal: 15,
            borderBottomColor: '#ebebeb',
            borderBottomWidth: 1,
          }}
          title={renderHeaderLeft}
          accessoryRight={renderRightNotification}
        />
      </SafeAreaView>
      <ScrollView
        style={{
          ...defaultStyle.fixedLayout,
        }}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <View style={{marginBottom: 5}}>
          <TopicH title="ໝວດໝູ່ສິນຄ້າ" icon="blur-radial" />
          <View>
            {SKproductCat ? (
              <SkeletonPlaceholder>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 10,
                    justifyContent: 'space-around',
                  }}>
                  <View
                    style={{
                      width: 100,
                      height: 92,
                      borderRadius: 10,
                      marginRight: 10,
                    }}
                  />
                  <View
                    style={{
                      width: 100,
                      height: 92,
                      borderRadius: 10,
                      marginRight: 10,
                    }}
                  />
                  <View style={{width: 100, height: 92, borderRadius: 10}} />
                </View>
              </SkeletonPlaceholder>
            ) : (
              <FlatList
                horizontal
                data={productCat}
                renderItem={renderCatItem}
                keyExtractor={item => item.id.toString()}
              />
            )}
          </View>
        </View>
        {/* Top Recommended */}
        {recommendProducts?.length > 0 && (
          <Box mt={1}>
            <TopicH title="ສິນຄ້າແນະນຳ" icon="grain" />
          </Box>
        )}

        {SKrecommendProducts ? (
          <View>
            <SkeletonPlaceholder>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 10,
                  marginBottom: 10,
                  justifyContent: 'space-around',
                }}>
                <View
                  style={{
                    width: 140,
                    height: 140,
                    borderRadius: 10,
                    marginRight: 20,
                  }}
                />
                <View style={{width: 140, height: 140, borderRadius: 10}} />
              </View>
            </SkeletonPlaceholder>
          </View>
        ) : (
          <View style={[styles.columncontainer, {marginTop: 5}]}>
            {recommendProducts?.length > 0 &&
              recommendProducts.map(data => {
                return (
                  <TouchableOpacity
                    key={data.id}
                    onPress={() =>
                      navigation.navigate('ProductDetail', {productData: data})
                    }>
                    <View style={styles.two_columnlayout}>
                      <FastImage
                        style={{
                          width: 80,
                          height: 80,
                          alignSelf: 'center',
                          marginBottom: 4,
                          borderRadius: 5,
                        }}
                        resizeMode={FastImage.resizeMode.contain}
                        source={{
                          uri: data?.images,
                        }}
                      />
                      {data?.tags !== null && (
                        <View
                          style={{
                            backgroundColor: colors.Orange,
                            position: 'absolute',
                            paddingHorizontal: 5,
                            borderRadius: 5,
                            right: 5,
                            top: 5,
                          }}>
                          <TextM
                            style={{color: colors.White, fontSize: 14}}
                            numberOfLines={1}>
                            {data?.tags}
                          </TextM>
                        </View>
                      )}
                      <TextM
                        style={styles.pdname}
                        numberOfLines={1}
                        fontWeight="bold">
                        {data?.name}
                      </TextM>
                      <HStack alignItems={'center'} space={1}>
                        <MaterialCommunityIcons
                          name={'layers'}
                          style={{color: colors.Gray, fontSize: 12}}
                        />
                        <TextM style={styles.pdtag} numberOfLines={1}>
                          {data?.categories}
                        </TextM>
                      </HStack>
                      <TextM style={styles.pdprice} fontWeight="bold">
                        {numberWithCommas(data?.price)} ກີບ
                      </TextM>
                    </View>
                  </TouchableOpacity>
                );
              })}
          </View>
        )}

        {/* Ads Slider */}
        {SKBanner ? (
          <View style={{marginBottom: 10}}>
            <SkeletonPlaceholder>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <View
                  style={{
                    width: '100%',
                    aspectRatio,
                    borderRadius: 10,
                  }}
                />
              </View>
            </SkeletonPlaceholder>
          </View>
        ) : (
          banner?.length > 0 && (
            <View
              style={{
                width: '100%',
                aspectRatio,
                marginBottom: 10,
                marginTop: 2,
              }}>
              <Swiper autoplay={true} loop={true} showsPagination={false}>
                {banner.map((data, index) => {
                  return (
                    <View key={data?.id} style={{alignItems: 'center'}}>
                      <FastImage
                        style={{width: '100%', aspectRatio, borderRadius: 10}}
                        resizeMode={FastImage.resizeMode.contain}
                        source={{uri: data?.img}}
                      />
                    </View>
                  );
                })}
              </Swiper>
            </View>
          )
        )}

        {/* Products Discover */}
        <TopicH title="ສິນຄ້າອື່ນໆ" icon="grain" />
        {SKrandomProducts ? (
          <SkeletonPlaceholder>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 10,
                justifyContent: 'space-around',
              }}>
              <View
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 10,
                }}
              />
              <View
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 10,
                  marginHorizontal: 10,
                }}
              />
              <View style={{width: 100, height: 100, borderRadius: 10}} />
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 10,
                justifyContent: 'space-around',
              }}>
              <View
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 10,
                }}
              />
              <View
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 10,
                  marginHorizontal: 10,
                }}
              />
              <View style={{width: 100, height: 100, borderRadius: 10}} />
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 10,
                justifyContent: 'space-around',
              }}>
              <View
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 10,
                }}
              />
              <View
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 10,
                  marginHorizontal: 10,
                }}
              />
              <View style={{width: 100, height: 100, borderRadius: 10}} />
            </View>
          </SkeletonPlaceholder>
        ) : (
          <View style={[styles.columncontainer, {marginBottom: 30}]}>
            {randomProducts?.length > 0 &&
              randomProducts.map(data => {
                return (
                  <TouchableOpacity
                    key={data.id}
                    onPress={() =>
                      navigation.navigate('ProductDetail', {productData: data})
                    }>
                    <View style={styles.three_columnlayout}>
                      <View>
                        <FastImage
                          style={{
                            width: 80,
                            height: 80,
                            alignSelf: 'center',
                            marginBottom: 2,
                            borderRadius: 5,
                          }}
                          resizeMode="contain"
                          source={{
                            uri: data?.images,
                          }}
                        />
                        {data?.tags !== null && (
                          <View
                            style={{
                              backgroundColor: colors.Orange,
                              position: 'absolute',
                              paddingHorizontal: 5,
                              borderRadius: 5,
                              right: 5,
                              top: 5,
                            }}>
                            <TextM
                              style={{color: colors.White, fontSize: 12}}
                              numberOfLines={1}>
                              {data?.tags}
                            </TextM>
                          </View>
                        )}
                      </View>
                      <View
                        style={{
                          justifyContent: 'center',
                        }}>
                        <TextM
                          style={styles.pdnamerd}
                          numberOfLines={1}
                          fontWeight="bold">
                          {data?.name}
                        </TextM>
                      </View>
                      <HStack alignItems={'center'} space={1}>
                        <MaterialCommunityIcons
                          name={'layers'}
                          style={{color: colors.Gray, fontSize: 12}}
                        />
                        <TextM style={styles.pdtag} numberOfLines={1}>
                          {data?.categories}
                        </TextM>
                      </HStack>
                      <TextM style={styles.pdpricerd} fontWeight="bold">
                        {numberWithCommas(data?.price)} ກີບ
                      </TextM>
                    </View>
                  </TouchableOpacity>
                );
              })}
          </View>
        )}
      </ScrollView>
    </Box>
  );
};

export default Pharmacy;

const styles = StyleSheet.create({
  columncontainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  two_columnlayout: {
    marginHorizontal: 8,
    paddingHorizontal: 12,
    paddingTop: 10,
    paddingBottom: 6,
    borderRadius: 8,
    width: width / 2.5,
    marginBottom: 10,
    backgroundColor: colors.White,
    borderWidth: 1,
    borderColor: '#f2f2f2',
  },
  three_columnlayout: {
    padding: 8,
    width: width / 3.4,
  },
  pdname: {
    fontSize: 14,
    color: colors.DarkBlue,
  },
  pdtag: {
    fontSize: 12,
    color: '#cccccc',
  },
  pdprice: {
    color: colors.Pink,
    fontSize: 14,
  },
  pdnamerd: {
    fontSize: 12,
    color: colors.DarkBlue,
    lineHeight: 15,
    paddingTop: 5,
  },
  pdtagrd: {
    fontSize: 12,
    color: '#cccccc',
  },
  pdpricerd: {
    color: colors.Pink,
    fontSize: 12,
  },
  badge: {
    backgroundColor: colors.Blue,
    position: 'absolute',
    paddingHorizontal: 4,
    borderRadius: 10,
    bottom: 0,
    right: 0,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
});
