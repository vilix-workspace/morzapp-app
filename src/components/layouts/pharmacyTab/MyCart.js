import React, {useEffect, useState} from 'react';
import {StyleSheet, Platform} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {
  Box,
  ScrollView,
  View,
  Button,
  useToast,
  HStack,
  VStack,
  Alert,
} from 'native-base';
import {Button as ButtonN, Dialog, Portal} from 'react-native-paper';
import FastImage from 'react-native-fast-image';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// Redux
import {useSelector, useDispatch} from 'react-redux';
//Redux action
import {
  deleteItem,
  clearCart,
  increaseQuantity,
  decreaseQuantity,
} from '../../../stores/features/cart/cartSlice';

// Custom Style
import {defaultStyle, colors} from '../../shareStyle';
import TextM from '../../shareComponents/TextM';
import Loading from '../../shareComponents/Loading';

const MyCart = ({navigation}) => {
  const toast = useToast();
  // Redux
  const reduxStore = useSelector(state => state);
  const {cart} = reduxStore;
  const dispatch = useDispatch();
  // useState
  const [loading, setLoading] = useState(false);
  const [showAlert, setShowAlert] = useState(false);

  // Function
  const showToast = (type, title, desc) => {
    toast.show({
      render: () => {
        return (
          <Alert
            maxWidth="95%"
            alignSelf="center"
            flexDirection="row"
            status={type}
            variant={'left-accent'}>
            <VStack space={1} flexShrink={1} w="100%">
              <HStack
                flexShrink={1}
                alignItems="center"
                justifyContent="space-between">
                <HStack space={2} flexShrink={1} alignItems="center">
                  <Alert.Icon />
                  <TextM>{title}</TextM>
                </HStack>
              </HStack>
              {desc !== undefined && (
                <TextM style={{fontSize: 14, paddingLeft: 25}}>{desc}</TextM>
              )}
            </VStack>
          </Alert>
        );
      },
    });
  };

  const onClearCart = async () => {
    // setTimeout(() => {
    //   setLoading(false);
    // }, 2000);
    dispatch(clearCart());
    navigation.navigate('Pharmacy');
  };

  // Alert
  const showAlertNow = () => {
    setShowAlert(true);
  };

  const hideAlert = () => {
    setShowAlert(false);
  };

  const numberWithCommas = x => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  const onDeleteItem = index => {
    dispatch(deleteItem(index));
  };

  const totalPrice = () => {
    if (cart?.cartInfo?.length > 0) {
      let total = 0;
      for (const iterator of cart?.cartInfo) {
        total += parseInt(iterator.price) * iterator.quantity;
      }
      // console.log('total', total);
      return total;
    }
  };

  const totalItemPrice = (quantity, price) => {
    let totalItemP = 0;
    totalItemP = quantity * price;
    return totalItemP;
  };

  // useEffect
  useEffect(() => {
    // console.log('useEffect MyCart', cart.cartInfo);
  });

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <ScrollView
        style={{...defaultStyle.fixedLayout}}
        contentContainerStyle={{
          flexGrow: cart?.cartInfo?.length > 0 ? 0 : 1,
          justifyContent: 'center',
        }}>
        {cart?.cartInfo?.length > 0 ? (
          <Box>
            <TouchableOpacity
              style={styles.clearCartStyle}
              onPress={() => {
                showAlertNow();
              }}>
              <TextM style={{fontSize: 14, color: colors.White}}>
                ລົບສິນຄ້າທັງໝົດ{' '}
              </TextM>
              <MaterialCommunityIcons
                name="trash-can"
                style={{color: colors.White, fontSize: 20}}
              />
            </TouchableOpacity>

            {/* Item List */}
            <Box mt={2}>
              {cart?.cartInfo.map((data, index) => {
                return (
                  <HStack
                    key={index}
                    alignItems={'center'}
                    justifyContent={'space-between'}
                    marginBottom={2}>
                    <Box p={1} width={'25%'}>
                      <FastImage
                        source={{uri: data?.images}}
                        style={{borderRadius: 10, width: 70, height: 70}}
                      />
                    </Box>
                    <VStack w={'74%'}>
                      <TextM
                        fontWeight="bold"
                        style={{color: colors.DarkBlue}}
                        numberOfLines={1}>
                        {data?.name}
                      </TextM>
                      {/* <TextM style={{color: colors.Gray, fontSize: 14}}>
                        ຈຳນວນ {data?.quantity} x {numberWithCommas(data?.price)}{' '}
                        ກີບ
                      </TextM> */}
                      <TextM style={{color: colors.Gray, fontSize: 14}}>
                        ລາຄາຕໍ່ອັນ {numberWithCommas(data?.price)} ກີບ
                      </TextM>

                      <HStack
                        alignItems={'center'}
                        justifyContent={'space-between'}>
                        <HStack
                          alignItems={'center'}
                          justifyContent={'center'}
                          space={2}
                          mt={1}
                          style={{
                            borderWidth: 1,
                            borderColor: colors.Light,
                            borderRadius: 15,
                            width: 80,
                            paddingVertical: Platform.OS === 'ios' ? 4 : null,
                          }}>
                          <TouchableOpacity
                            onPress={() => {
                              // minusQty();
                              dispatch(decreaseQuantity(data));
                            }}>
                            <MaterialCommunityIcons
                              name="minus"
                              style={{color: colors.LightBlack, fontSize: 16}}
                            />
                          </TouchableOpacity>
                          <Box>
                            <TextM style={{fontSize: 16}}>
                              {data?.quantity}
                            </TextM>
                          </Box>
                          <TouchableOpacity
                            onPress={() => {
                              // plusQty();
                              dispatch(increaseQuantity(data));
                            }}>
                            <MaterialCommunityIcons
                              name="plus"
                              style={{color: colors.LightBlack, fontSize: 16}}
                            />
                          </TouchableOpacity>
                        </HStack>

                        <Box>
                          <TextM style={{color: colors.Blue}}>
                            ລວມ:{' '}
                            {numberWithCommas(
                              totalItemPrice(data?.quantity, data?.price),
                            )}{' '}
                            ກີບ
                          </TextM>
                        </Box>
                      </HStack>
                    </VStack>
                  </HStack>
                );
              })}
            </Box>
          </Box>
        ) : (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
            }}>
            <Box style={{marginTop: -80}} alignItems={'center'}>
              <TextM>ຂໍອະໄພ! ບໍ່ມີສິນຄ້າໃນກະຕ່າ</TextM>
              <FastImage
                style={{width: 120, height: 120, marginTop: 10}}
                source={require('../../../assets/img/remove-from-cart.png')}
              />
              <Button
                bordered
                style={{alignSelf: 'center', marginTop: 20, borderRadius: 10}}
                onPress={() => {
                  navigation.navigate('Pharmacy');
                }}>
                <TextM style={{color: colors.White}}>ເລືອກຊື້ສິນຄ້າເລີຍ</TextM>
              </Button>
            </Box>
          </View>
        )}
      </ScrollView>
      {/* Clear Cart */}
      <Portal>
        <Dialog
          visible={showAlert}
          onDismiss={hideAlert}
          style={{borderRadius: 10}}>
          <Dialog.Title>
            <TextM fontWeight="bold" style={{fontSize: 18}}>
              ທ່ານຕ້ອງການລົບສິນຄ້າທັງໝົດບໍ ?{' '}
              <MaterialCommunityIcons
                name="emoticon-sad-outline"
                style={{fontSize: 20, color: '#e74c3c'}}
              />
            </TextM>
          </Dialog.Title>
          <Dialog.Content>
            <TextM>ກະລຸນາກົດຢືນຢັນເພື່ອລົບສິນຄ້າທັງໝົດ</TextM>
          </Dialog.Content>
          <Dialog.Actions>
            <ButtonN onPress={() => hideAlert()}>
              <TextM>ກັບຄືນ</TextM>
            </ButtonN>
            <ButtonN
              onPress={() => {
                hideAlert();
                onClearCart();
              }}>
              <TextM style={{color: colors.Red}}>ລົບທັງໝົດ</TextM>
            </ButtonN>
          </Dialog.Actions>
        </Dialog>
      </Portal>
      {loading && <Loading />}
      {/* Bottom Button and Total Price */}
      {cart?.cartInfo?.length > 0 && (
        <View
          style={{
            paddingVertical: 15,
            paddingHorizontal: 20,
            alignItems: 'center',
          }}>
          <View
            style={{
              justifyContent: 'space-between',
              alignItems: 'center',
              flexDirection: 'row',
              width: '100%',
              paddingVertical: 5,
            }}>
            <View>
              <TextM>ລວມລາຄາ:</TextM>
            </View>
            <View>
              <TextM
                style={{
                  fontSize: 20,
                  color: colors.Pink,
                  textDecorationLine: 'underline',
                }}
                fontWeight="bold">
                {numberWithCommas(totalPrice())} ກີບ
              </TextM>
            </View>
          </View>
          <View
            style={{
              justifyContent: 'center',
              width: '100%',
            }}>
            <Button
              onPress={() => {
                // onAddToCart();
                navigation.navigate('Checkout');
              }}
              style={{
                backgroundColor: colors.Blue,
                borderRadius: 10,
              }}>
              <TextM style={{color: colors.White}}>ເພີ່ມສະຖານທີ່ຈັດສົ່ງ</TextM>
            </Button>
          </View>
        </View>
      )}
    </Box>
  );
};

export default MyCart;

const styles = StyleSheet.create({
  clearCartStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: colors.Red,
    alignSelf: 'flex-end',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 10,
  },
});
