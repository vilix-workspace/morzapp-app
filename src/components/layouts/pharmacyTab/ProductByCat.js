import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  FlatList,
  SafeAreaView,
  Dimensions,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
// UI
import {TopNavigation} from '@ui-kitten/components';
import {Box, ScrollView, View, Text, HStack} from 'native-base';
import Config from 'react-native-config';
import FastImage from 'react-native-fast-image';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

import WooCommerce from '../../../config/WooCommerce';
import TextM from '../../shareComponents/TextM';

// Redux
import {useSelector, useDispatch} from 'react-redux';

import {defaultStyle, colors} from '../../shareStyle';

const width = Dimensions.get('window').width;

const ProductByCat = ({navigation, route}) => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData, cart} = reduxStore;
  const dispatch = useDispatch();

  // param data
  const {catData} = route.params;

  // useState
  const [SKProducts, setSKProducts] = useState(true);
  const [products, setProducts] = useState([]);
  const [refreshing, setRefreshing] = useState(false);

  const onRefresh = () => {
    // setRefreshing(true);
    try {
      //   getShopBanner();
    } catch (error) {
      console.log(error);
    }
  };

  const getProductByCat = async () => {
    try {
      await WooCommerce.get('products', {
        category: catData?.id,
        per_page: 45,
      })
        .then(res => {
          console.log('getProductByCat', res);
          setProducts(res);
          setSKProducts(false);
        })
        .catch(error => {
          console.log(error.response);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const numberWithCommas = x => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  const renderHeaderLeft = () => (
    <Box>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <MaterialCommunityIcons
          name="arrow-left-circle"
          style={{color: colors.DarkBlue, marginLeft: 5, fontSize: 38}}
        />
      </TouchableOpacity>
    </Box>
  );

  const renderHeaderTitle = () => (
    <Box w={'60%'} alignItems={'center'}>
      <TextM numberOfLines={1}>ໝວກໝູ່: {catData?.name}</TextM>
    </Box>
  );

  const renderRightNotification = () => (
    <HStack space={4}>
      <TouchableOpacity
        style={{width: 34}}
        onPress={() => navigation.navigate('MyCart')}>
        <MaterialCommunityIcons
          name={'cart-outline'}
          style={{color: colors.Gray, fontSize: 28}}
        />
        <Box style={styles.badge}>
          <Text
            style={{
              fontSize: 10,
              fontWeight: 'bold',
              color: colors.White,
            }}>
            {cart?.cartInfo?.length}
          </Text>
        </Box>
      </TouchableOpacity>
    </HStack>
  );

  // useEffect
  useEffect(() => {
    // console.log('useEffect Pharmacy', userData);
    console.log('check param', catData);
    getProductByCat();
  }, []);

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <SafeAreaView>
        <TopNavigation
          alignment="center"
          style={{
            paddingHorizontal: 15,
            borderBottomColor: '#ebebeb',
            borderBottomWidth: 1,
          }}
          title={renderHeaderTitle}
          accessoryLeft={renderHeaderLeft}
          accessoryRight={renderRightNotification}
        />
      </SafeAreaView>
      <ScrollView
        style={{
          ...defaultStyle.fixedLayout,
        }}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <SafeAreaView>
          <Box>
            {SKProducts ? (
              <SkeletonPlaceholder>
                {[...Array(6)].map((e, i) => (
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginTop: 10,
                      justifyContent: 'space-around',
                    }}>
                    <View
                      style={{
                        width: 100,
                        height: 100,
                        borderRadius: 10,
                      }}
                    />
                    <View
                      style={{
                        width: 100,
                        height: 100,
                        borderRadius: 10,
                      }}
                    />
                    <View style={{width: 100, height: 100, borderRadius: 10}} />
                  </View>
                ))}
              </SkeletonPlaceholder>
            ) : (
              <View style={[styles.columncontainer, {marginBottom: 20}]}>
                {products?.length > 0 &&
                  products.map(data => {
                    return (
                      <TouchableOpacity
                        key={data.id}
                        onPress={() =>
                          navigation.navigate('ProductDetail', {
                            productData: data,
                          })
                        }>
                        <View style={styles.three_columnlayout}>
                          <View>
                            <FastImage
                              style={{
                                width: 80,
                                height: 80,
                                alignSelf: 'center',
                                marginBottom: 2,
                                borderRadius: 5,
                              }}
                              resizeMode="contain"
                              source={{
                                uri: data?.images,
                              }}
                            />
                            {data?.tags !== null && (
                              <View
                                style={{
                                  backgroundColor: colors.Orange,
                                  position: 'absolute',
                                  paddingHorizontal: 5,
                                  borderRadius: 5,
                                  right: 5,
                                  top: 5,
                                }}>
                                <TextM
                                  style={{color: colors.White, fontSize: 12}}
                                  numberOfLines={1}>
                                  {data?.tags}
                                </TextM>
                              </View>
                            )}
                          </View>
                          <View
                            style={{
                              justifyContent: 'center',
                            }}>
                            <TextM
                              style={styles.pdnamerd}
                              numberOfLines={1}
                              fontWeight="bold">
                              {data?.name}
                            </TextM>
                          </View>
                          <HStack space={1} alignItems={'center'}>
                            <MaterialCommunityIcons
                              name={'layers'}
                              style={{color: colors.Gray, fontSize: 12}}
                            />
                            <TextM style={styles.pdtag} numberOfLines={1}>
                              {data?.categories}
                            </TextM>
                          </HStack>
                          <TextM style={styles.pdpricerd} fontWeight="bold">
                            {numberWithCommas(data?.price)} ກີບ
                          </TextM>
                        </View>
                      </TouchableOpacity>
                    );
                  })}
              </View>
            )}
          </Box>
        </SafeAreaView>
      </ScrollView>
    </Box>
  );
};

export default ProductByCat;

const styles = StyleSheet.create({
  columncontainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  two_columnlayout: {
    marginHorizontal: 8,
    paddingHorizontal: 12,
    paddingTop: 10,
    paddingBottom: 6,
    borderRadius: 8,
    width: width / 2.5,
    marginBottom: 10,
    backgroundColor: colors.White,
    borderWidth: 1,
    borderColor: '#f2f2f2',
  },
  three_columnlayout: {
    padding: 8,
    width: width / 3.4,
  },
  pdtag: {
    fontSize: 12,
    color: '#cccccc',
  },
  pdnamerd: {
    fontSize: 12,
    color: colors.DarkBlue,
    lineHeight: 15,
    paddingTop: 5,
  },
  pdpricerd: {
    color: colors.Pink,
    fontSize: 12,
  },
  badge: {
    backgroundColor: colors.Blue,
    position: 'absolute',
    paddingHorizontal: 4,
    borderRadius: 10,
    bottom: 0,
    right: 0,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
});
