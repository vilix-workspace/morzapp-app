import React, {useEffect, useState} from 'react';
import {StyleSheet, useWindowDimensions, SafeAreaView} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {
  Box,
  ScrollView,
  View,
  Text,
  Button,
  Icon,
  useToast,
  HStack,
  VStack,
  Alert,
} from 'native-base';
import {Button as ButtonN, Dialog, Portal} from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FastImage from 'react-native-fast-image';
import RenderHtml, {
  HTMLElementModel,
  HTMLContentModel,
  defaultSystemFonts,
} from 'react-native-render-html';
// Redux
import {useSelector, useDispatch} from 'react-redux';
//Redux action
import {addToCart} from '../../../stores/features/cart/cartSlice';

// Custom Style
import {defaultStyle, colors} from '../../shareStyle';
import TextM from '../../shareComponents/TextM';

const ProductDetail = ({navigation, route}) => {
  const toast = useToast();

  const {width} = useWindowDimensions();
  const systemFonts = [...defaultSystemFonts, 'NotoSansLao-Regular'];
  // Redux
  const reduxStore = useSelector(state => state);
  const {cart, userData} = reduxStore;
  const dispatch = useDispatch();
  // param data
  const {productData} = route.params;
  // useState
  const [productQty, setProductQty] = useState(1);
  const [showAlert, setShowAlert] = useState(false);

  // Function
  const showToast = (type, title, desc) => {
    toast.show({
      render: () => {
        return (
          <Alert
            maxWidth="95%"
            alignSelf="center"
            flexDirection="row"
            status={type}
            variant={'left-accent'}>
            <VStack space={1} flexShrink={1} w="100%">
              <HStack
                flexShrink={1}
                alignItems="center"
                justifyContent="space-between">
                <HStack space={2} flexShrink={1} alignItems="center">
                  <Alert.Icon />
                  <TextM>{title}</TextM>
                </HStack>
              </HStack>
              {desc !== undefined && (
                <TextM style={{fontSize: 14, paddingLeft: 25}}>{desc}</TextM>
              )}
            </VStack>
          </Alert>
        );
      },
    });
  };

  const onAddToCart = () => {
    let cartitem = {
      id: productData?.id,
      name: productData?.name,
      sku: productData?.sku,
      quantity: productQty,
      price: productData?.price,
      images: productData?.images,
    };
    dispatch(addToCart(cartitem));
    showToast('success', 'ເພີ່ມສິນຄ້າໃສ່ກະຕ່າແລ້ວ!');
  };

  const plusQty = () => {
    setProductQty(productQty + 1);
  };
  const minusQty = () => {
    if (productQty !== 1) {
      setProductQty(productQty - 1);
    } else {
    }
  };

  const numberWithCommas = x => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  // Alert
  const showAlertNow = () => {
    setShowAlert(true);
  };

  const hideAlert = () => {
    setShowAlert(false);
  };

  // Check User Login Alert
  const checkUserLogin = () => {
    if (userData?.userInfo === null) {
      showAlertNow();
    } else {
      onAddToCart();
    }
  };

  // useEffect
  useEffect(() => {
    // console.log('useEffect ProductDetail', userData);
    // console.log('cart', cart.cartInfo);
  });

  const customHTMLElementModels = {
    p: HTMLElementModel.fromCustomModel({
      tagName: 'p',
      mixedUAStyles: {
        // backgroundColor: 'blue',
      },
      contentModel: HTMLContentModel.block,
    }),
    body: HTMLElementModel.fromCustomModel({
      tagName: 'body',
      mixedUAStyles: {
        // backgroundColor: 'blue',
        fontFamily: 'NotoSansLao-Regular',
      },
      contentModel: HTMLContentModel.block,
    }),
  };

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <SafeAreaView>
        <HStack
          justifyContent={'space-between'}
          alignItems={'center'}
          px={3}
          py={1}>
          <Box>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <MaterialCommunityIcons
                name="arrow-left-circle"
                style={{color: colors.DarkBlue, marginLeft: 5, fontSize: 38}}
              />
            </TouchableOpacity>
          </Box>
          <Box w={'60%'} alignItems={'center'}>
            <TextM
              fontWeight="bold"
              numberOfLines={1}
              style={{color: colors.DarkBlue}}>
              {productData?.name}
              {/* ຂໍ້ມູນສິນຄ້າ */}
            </TextM>
          </Box>
          <Box>
            <TouchableOpacity
              style={{width: 34}}
              onPress={() => navigation.navigate('MyCart')}>
              <MaterialCommunityIcons
                name={'cart-outline'}
                style={{color: colors.Gray, fontSize: 28}}
              />
              <Box style={styles.badge}>
                <Text
                  style={{
                    fontSize: 10,
                    fontWeight: 'bold',
                    color: colors.White,
                  }}>
                  {cart?.cartInfo?.length}
                </Text>
              </Box>
            </TouchableOpacity>
          </Box>
        </HStack>
      </SafeAreaView>
      <ScrollView style={{...defaultStyle.fixedLayout}}>
        <FastImage
          style={{
            width: 220,
            height: 220,
            borderRadius: 10,
            alignSelf: 'center',
            marginBottom: 5,
          }}
          resizeMode={FastImage.resizeMode.contain}
          source={{
            uri: productData?.images,
          }}
        />
        <View>
          <TextM fontWeight="bold" style={styles.pdname}>
            {productData?.name}
          </TextM>
          {/* Row */}
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TextM style={styles.pdtag}>
              <MaterialCommunityIcons
                name={'layers'}
                style={{color: colors.Gray, fontSize: 16}}
              />{' '}
              {productData?.categories}
            </TextM>

            <View>
              {productData?.tags !== null && (
                <TextM style={{color: colors.Orange}}>
                  <MaterialCommunityIcons
                    name="tag"
                    style={{color: colors.Orange, fontSize: 16}}
                  />{' '}
                  {productData?.tags}
                </TextM>
              )}
            </View>
          </View>
          {/* Price */}
          <TextM style={styles.pdprice}>
            {numberWithCommas(productData?.price)} ກີບ
          </TextM>
        </View>

        <View style={{marginTop: 5, marginBottom: 25}}>
          <HStack alignItems={'center'} space={1}>
            <MaterialCommunityIcons
              name={'format-list-bulleted'}
              style={{color: colors.DarkBlue, fontSize: 16}}
            />
            <TextM
              fontWeight="bold"
              style={{color: colors.DarkBlue, fontSize: 18}}>
              ລາຍລະອຽດຂອງສິນຄ້າ:
            </TextM>
          </HStack>
          {/* <TextM>{productData?.description}</TextM> */}
          <RenderHtml
            contentWidth={width}
            source={{
              html: productData?.description,
            }}
            tagsStyles={defaultStyle.htmlStyle}
            systemFonts={systemFonts}
            customHTMLElementModels={customHTMLElementModels}
          />
        </View>
      </ScrollView>
      <Box
        style={{
          paddingVertical: 12,
          paddingHorizontal: 20,
        }}>
        <HStack
          alignItems={'center'}
          justifyContent={'space-between'}
          space={2}>
          <HStack
            alignItems={'center'}
            space={4}
            style={{
              borderWidth: 1,
              borderColor: colors.Light,
              borderRadius: 10,
              paddingHorizontal: 5,
              paddingVertical: 2,
            }}>
            <TouchableOpacity
              onPress={() => {
                minusQty();
              }}>
              <MaterialCommunityIcons
                name="minus"
                style={{color: colors.LightBlack, fontSize: 30}}
              />
            </TouchableOpacity>
            <Box>
              <TextM style={{fontSize: 22}}>{productQty}</TextM>
            </Box>
            <TouchableOpacity
              onPress={() => {
                plusQty();
              }}>
              <MaterialCommunityIcons
                name="plus"
                style={{color: colors.LightBlack, fontSize: 30}}
              />
            </TouchableOpacity>
          </HStack>

          <Button
            onPress={() => {
              checkUserLogin();
            }}
            style={{
              backgroundColor: colors.Blue,
              borderRadius: 10,
              width: 150,
            }}>
            <TextM style={{color: colors.White}}>ເພິ່ມໃສ່ກະຕ່າ</TextM>
          </Button>
        </HStack>
      </Box>
      {/* Check Login User*/}
      <Portal>
        <Dialog
          visible={showAlert}
          onDismiss={hideAlert}
          style={{borderRadius: 10}}>
          <Dialog.Title>
            <TextM fontWeight="bold" style={{fontSize: 18}}>
              ຂໍອະໄພທ່ານຍັງບໍ່ທັນໄດ້ເຂົ້າສູ່ລະບົບ
            </TextM>
          </Dialog.Title>
          <Dialog.Content>
            <TextM>ກະລຸນາເຂົ້າສູ່ລະບົບກ່ອນເພື່ອສັ່ງສິນຄ້າ</TextM>
          </Dialog.Content>
          <Dialog.Actions>
            <ButtonN onPress={() => hideAlert()}>
              <TextM>ກັບຄືນ</TextM>
            </ButtonN>
            <ButtonN
              onPress={() => {
                hideAlert();
                navigation.navigate('Profile');
              }}>
              <TextM style={{color: colors.Red}}>ເຂົ້າສູ່ລະບົບ</TextM>
            </ButtonN>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </Box>
  );
};

export default ProductDetail;

const styles = StyleSheet.create({
  badge: {
    backgroundColor: colors.Blue,
    position: 'absolute',
    paddingHorizontal: 4,
    borderRadius: 10,
    bottom: 0,
    right: 0,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  pdname: {
    fontSize: 18,
    color: colors.DarkBlue,
  },
  pdtag: {
    fontSize: 14,
    color: colors.Gray,
  },
  pdprice: {
    color: colors.Pink,
    fontSize: 24,
  },
});
