import React, {useEffect, useState} from 'react';
import {StyleSheet, TouchableOpacity, Dimensions} from 'react-native';
import {
  Box,
  ScrollView,
  View,
  Text,
  Icon,
  Button,
  useToast,
  Alert,
  VStack,
  HStack,
} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Button as ButtonN, Dialog, Portal} from 'react-native-paper';
import axios from 'axios';
import {CommonActions} from '@react-navigation/native';
import Config from 'react-native-config';
// Redux
import {useSelector, useDispatch} from 'react-redux';

// Component
import Loading from '../../shareComponents/Loading';
import TextM from '../../shareComponents/TextM';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';

const width = Dimensions.get('window').width;

const topupData = [
  {
    value: '20000',
    name: '20,000 ກີບ',
  },
  {
    value: '50000',
    name: '50,000 ກີບ',
  },
  {
    value: '100000',
    name: '100,000 ກີບ',
  },
  {
    value: '200000',
    name: '200,000 ກີບ',
  },
  {
    value: '500000',
    name: '500,000 ກີບ',
  },
];

const TopupMoney = ({navigation}) => {
  const toast = useToast();
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();
  // useState
  const [loading, setLoading] = useState(false);
  const [activeIndex, setActiveIndex] = useState(null);
  const [activeValue, setActiveValue] = useState(0);
  const [showAlert, setShowAlert] = useState(false);

  // Function

  const showToast = (type, title, desc) => {
    toast.show({
      render: () => {
        return (
          <Alert
            maxWidth="95%"
            alignSelf="center"
            flexDirection="row"
            status={type}
            variant={'left-accent'}>
            <VStack space={1} flexShrink={1} w="100%">
              <HStack
                flexShrink={1}
                alignItems="center"
                justifyContent="space-between">
                <HStack space={2} flexShrink={1} alignItems="center">
                  <Alert.Icon />
                  <TextM>{title}</TextM>
                </HStack>
              </HStack>
              {desc !== undefined && (
                <TextM style={{fontSize: 14, paddingLeft: 25}}>{desc}</TextM>
              )}
            </VStack>
          </Alert>
        );
      },
    });
  };
  // Alert
  const showAlertNow = () => {
    setShowAlert(true);
  };

  const hideAlert = () => {
    setShowAlert(false);
  };

  const numberWithCommas = x => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  const requestTopupMoney = () => {
    hideAlert();
    setLoading(true);
    try {
      axios({
        method: 'post',
        url: Config.WP_URL + '/wp/v2/topup_request',
        data: {
          fields: {
            user_id: userData?.userInfo?.user_id,
            name: userData?.userInfo?.name,
            phone: userData?.userInfo?.phone,
            amount: activeValue,
            payment_referral: '',
            status: 'requested',
          },
          status: 'publish',
        },
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(res => {
          console.log(res);
          setLoading(false);
          navigation.goBack();
          navigation.navigate('TopupHistory');
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  // useEffect
  useEffect(() => {
    console.log('useEffect TopupMoney', userData);
  }, []);

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <ScrollView style={{...defaultStyle.fixedLayout}}>
        <TextM>ເລືອກຈຳນວນເງິນທີ່ທ່ານຕ້ອງການເຕີມ</TextM>

        {/* Money Selection */}
        <View style={{marginVertical: 10}}>
          <TextM style={{alignSelf: 'center', color: colors.Red}}>
            ເລືອກຈຳນວນເງິນ*
          </TextM>
          <View style={styles.columncontainer}>
            {topupData.map((data, i) => {
              let index = i;
              return (
                <TouchableOpacity
                  key={i}
                  onPress={() => {
                    setActiveIndex(index);
                    setActiveValue(data.value);
                  }}>
                  <View
                    style={[
                      styles.two_columnlayout,
                      {
                        backgroundColor:
                          activeIndex == i ? colors.Orange : 'transparent',
                        flexDirection: 'row',
                      },
                    ]}>
                    <MaterialCommunityIcons
                      name={'wallet'}
                      style={{
                        color:
                          activeIndex == i ? colors.White : colors.LightOrange,
                        fontSize: 20,
                      }}
                    />
                    <TextM
                      style={{
                        color: activeIndex == i ? colors.White : colors.Black,
                        paddingLeft: 8,
                      }}>
                      {data?.name}
                    </TextM>
                  </View>
                </TouchableOpacity>
              );
            })}
          </View>
        </View>

        <TextM>
          <TextM style={{textDecorationLine: 'underline', color: colors.Red}}>
            ໝາຍເຫດ:
          </TextM>{' '}
          ຫຼັງຈາກທີ່ທ່ານໄດ້ໂອນເງິນເຂົ້າທາງບັນຊີເຮົາເປັນທີ່ຮຽບຮ້ອຍແລ້ວ
          ກະລຸນາແຈ້ງເອກະສານໃບໂອນເງິນໃຫ້ກັບທາງພວກເຮົາເພື່ອດຳເນີນການເຕີມເງິນເຂົ້າສູ່ບັນຊີຂອງທ່ານຢ່າງສົມບູນ
        </TextM>

        {/* Loading  */}
        {loading && <Loading />}
      </ScrollView>
      <View
        style={{
          justifyContent: 'center',
          marginTop: 10,
          width: '100%',
          paddingVertical: 5,
          paddingHorizontal: 20,
        }}>
        <Button
          full
          onPress={() => {
            activeValue == 0
              ? showToast('warning', 'ກະລຸນາເລືອກຈຳນວນເງິນ!')
              : showAlertNow();
          }}
          style={{
            borderRadius: 5,
            marginBottom: 15,
          }}>
          <TextM style={{color: colors.White}}>ເຕີມເງິນເຂົ້າບັນຊີ</TextM>
        </Button>
      </View>
      {/* Popup */}
      <Portal>
        <Dialog
          visible={showAlert}
          onDismiss={hideAlert}
          style={{borderRadius: 10}}>
          <Dialog.Title>
            <TextM fontWeight="bold" style={{fontSize: 18}}>
              ຢືນຢັນການຕື່ມເງິນເຂົ້າບັນຊີ ?
            </TextM>
          </Dialog.Title>
          <Dialog.Content>
            <TextM>
              ກະລຸນາກົດຢືນຢັນການຕື່ມເງິນເຂົ້າບັນຊີຂອງທ່ານເປັນຈຳນວນເງິນ{' '}
              {numberWithCommas(activeValue)} ກີບ
            </TextM>
          </Dialog.Content>
          <Dialog.Actions>
            <ButtonN onPress={() => hideAlert()}>
              <TextM style={{color: colors.Gray}}>ກັບຄືນ</TextM>
            </ButtonN>
            <ButtonN
              onPress={() => {
                requestTopupMoney();
              }}>
              <TextM style={{color: colors.Green}}>ຢືນຢັນ</TextM>
            </ButtonN>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </Box>
  );
};

export default TopupMoney;

const styles = StyleSheet.create({
  columncontainer: {
    flexDirection: 'row',
    marginTop: 10,
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  two_columnlayout: {
    padding: 10,
    borderColor: '#ececec',
    borderWidth: 1,
    borderRadius: 5,
    width: width / 2.6,
    marginBottom: 10,
    alignItems: 'center',
    paddingVertical: 15,
  },
});
