import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  Dimensions,
  Animated,
  Pressable,
  useWindowDimensions,
} from 'react-native';
import {Box, ScrollView, View, Text} from 'native-base';
import {Layout, Tab, TabView, Icon} from '@ui-kitten/components';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// Component
import Loading from '../../shareComponents/Loading';
import TextM from '../../shareComponents/TextM';

import DoctorLogin from '../../doctorLayout/profileTab/DoctorLogin';
import HospitalLogin from '../../hospitalLayout/accountTab/HospitalLogin';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';

const PartnerLogin = ({navigation}) => {
  const [selectedIndex, setSelectedIndex] = useState(0);

  // const shouldLoadComponent = index => index === selectedIndex;

  const UserMDIcon = props => (
    <Icon name="user-md" {...props} pack="fontawesome5" />
  );

  const HospitalIcon = props => (
    <Icon name="hospital" {...props} pack="fontawesome5" />
  );

  // useEffect
  useEffect(() => {
    // console.log('useEffect PartnerLogin');
  }, []);

  return (
    <Box>
      <TabView
        selectedIndex={selectedIndex}
        // shouldLoadComponent={shouldLoadComponent}
        onSelect={index => {
          setSelectedIndex(index);
        }}>
        <Tab
          title={() => (
            <TextM
              style={{
                color: selectedIndex === 0 ? colors.Pink : '#8F9BB3',
                fontSize: 14,
              }}>
              ສຳລັບທີ່ປຶກສາ
            </TextM>
          )}
          icon={UserMDIcon}
          style={{paddingVertical: 6}}>
          <DoctorLogin navigation={navigation} />
        </Tab>
        <Tab
          title={() => (
            <TextM
              style={{
                color: selectedIndex === 1 ? colors.Pink : '#8F9BB3',
                fontSize: 14,
              }}>
              ສຳລັບໂຮງໝໍ
            </TextM>
          )}
          icon={HospitalIcon}>
          <HospitalLogin navigation={navigation} />
        </Tab>
      </TabView>
    </Box>
  );
};

export default PartnerLogin;

const styles = StyleSheet.create({
  // tabContainer: {
  //   flex: 1,
  // },
});
