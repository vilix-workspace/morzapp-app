import React, {useEffect, useState} from 'react';
import {StyleSheet, TouchableWithoutFeedback} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {
  Box,
  View,
  Text,
  FormControl,
  Input,
  useToast,
  Button,
  ScrollView,
  VStack,
  HStack,
  Alert,
} from 'native-base';
import {Input as InputUI} from '@ui-kitten/components';
import FastImage from 'react-native-fast-image';
import DatePicker from 'react-native-date-picker';
import moment from 'moment';
import 'moment/locale/lo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
// Redux
import {useSelector, useDispatch} from 'react-redux';

// Custom Style
import {defaultStyle, colors} from '../../shareStyle';

// Share Component
import TextM from '../../shareComponents/TextM';

moment.locale('lo');

var radio_props = [
  {label: 'ຊາຍ', value: 'male'},
  {label: 'ຍິງ', value: 'female'},
];

const Registration = ({navigation}) => {
  const toast = useToast();
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();
  // useState
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [dob, setDob] = useState(new Date());
  const [openDate, setOpenDate] = useState(false);
  const [gender, setGender] = useState('male');
  const [genderIndex, setGenderIndex] = useState(0);
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  // Sample Data
  // const [name, setName] = useState('Pay Bounyalith');
  // const [email, setEmail] = useState('');
  // const [dob, setDob] = useState(new Date());
  // const [openDate, setOpenDate] = useState(false);
  // const [gender, setGender] = useState('male');
  // const [genderIndex, setGenderIndex] = useState(0);
  // const [phone, setPhone] = useState('76423996');
  // const [password, setPassword] = useState('123456p');
  // Sample Data
  const [imageProfile, setImageProfile] = useState(null);
  const [secureTextEntry, setSecureTextEntry] = useState(true);

  function validateEmail(email) {
    const re =
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  // Check Error
  const toastError = error => {
    toast.show({
      render: () => {
        return (
          <Alert
            mb={4}
            maxWidth="95%"
            alignSelf="center"
            flexDirection="row"
            status={'warning'}
            variant={'left-accent'}>
            <VStack space={1} flexShrink={1} w="100%">
              <HStack
                flexShrink={1}
                alignItems="center"
                justifyContent="space-between">
                <HStack space={2} flexShrink={1} alignItems="center">
                  <Alert.Icon />
                  <TextM>ກະລຸນາຕື່ມຂໍ້ມູນ {error} ໃຫ້ຖືກຕ້ອງ !!</TextM>
                </HStack>
              </HStack>
            </VStack>
          </Alert>
        );
      },
    });
  };

  const showToast = (type, title, desc) => {
    toast.show({
      render: () => {
        return (
          <Alert
            maxWidth="95%"
            alignSelf="center"
            flexDirection="row"
            status={type}
            variant={'left-accent'}>
            <VStack space={1} flexShrink={1} w="100%">
              <HStack
                flexShrink={1}
                alignItems="center"
                justifyContent="space-between">
                <HStack space={2} flexShrink={1} alignItems="center">
                  <Alert.Icon />
                  <TextM>{title}</TextM>
                </HStack>
              </HStack>
              {desc !== undefined && (
                <TextM style={{fontSize: 14, paddingLeft: 25}}>{desc}</TextM>
              )}
            </VStack>
          </Alert>
        );
      },
    });
  };

  // Set Password Eye
  const toggleSecureEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const renderIcon = () => (
    <TouchableWithoutFeedback onPress={toggleSecureEntry}>
      <MaterialCommunityIcons
        name={secureTextEntry ? 'eye-off' : 'eye'}
        style={{color: '#bdc3c7', fontSize: 20}}
      />
    </TouchableWithoutFeedback>
  );

  const onCheckRegister = () => {
    var todayDate = new Date();

    if (name == '') {
      toastError('ຊື່ຂອງທ່ານ');
      // } else if (!validateEmail(email)) {
      //   toastError('ອີເມວ');
    } else if ((todayDate - dob) / (1000 * 3600 * 24 * 365) > 14 == false) {
      toastError('ວັນເດືອນປີເກີດ');
    } else if (gender == '') {
      toastError('ເພດ');
    } else if (phone == '') {
      toastError('ເບີໂທລະສັບ');
    } else if (password == '' || password.length < 6) {
      showToast('warning', 'ລະຫັດຜ່ານຕ້ອງເກິນ 5 ຕົວ!');
    } else {
      onCheckOTP();
    }
  };

  const onCheckOTP = async () => {
    // let dobtext = moment(dob).format('LLLL');
    let userinfo = {
      name,
      email,
      dob,
      gender,
      phone,
      password,
      imageProfile,
    };

    try {
      firestore()
        .collection('users')
        .where('phone', '==', phone)
        .get()
        .then(querySnapshot => {
          if (querySnapshot.empty) {
            navigation.navigate('OTPVerificationRegis', {userinfo: userinfo});
          } else {
            showToast('error', 'ເບີນີ້ຖືກນຳໃຊ້ແລ້ວ ກະລຸນາໃສ່ເບີໃໝ່!');
          }
        })
        .catch(error => {
          console.error('Error writing document: ', error);
        });
    } catch (error) {
      console.log(error);
    }

    console.log('userinfo', userinfo);
  };

  const getImageProfile = async () => {
    launchImageLibrary(
      {mediaType: 'photo', includeBase64: true, maxHeight: 500, maxWidth: 500},
      async callback => {
        // console.log('callback', callback);
        if (callback.didCancel) {
          console.log('User cancelled image picker');
        } else {
          setImageProfile(callback.assets[0]);
        }
      },
    );
  };

  const requireStar = () => {
    return <Text style={{color: colors.Red, fontWeight: 'bold'}}>*</Text>;
  };

  // useEffect
  useEffect(() => {
    // console.log('useEffect Registration', userData);
  }, []);

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <ScrollView style={{...defaultStyle.fixedLayout}}>
        <View>
          <FastImage
            style={{
              height: 50,
              width: 'auto',
              marginBottom: 10,
            }}
            source={require('../../../assets/img/morzapp-logo-hor-660x180.png')}
            resizeMode={FastImage.resizeMode.contain}
          />
          <TextM>ກະລຸນາຕື່ມຂໍ້ມູນຂອງທ່ານເພື່ອລົງທະບຽນກັບພວກເຮົາຟຣີ</TextM>
        </View>

        <FormControl style={{marginVertical: 5}}>
          <VStack space={2}>
            <Box>
              <FormControl.Label
                style={{
                  alignItems: 'center',
                }}>
                <MaterialCommunityIcons
                  name="badge-account-outline"
                  style={styles.iconColor}
                />
                <TextM style={{paddingTop: 5}}>
                  ຊື່ຂອງທ່ານ{requireStar()}:
                </TextM>
              </FormControl.Label>
              <Input
                placeholder={'John Doe'}
                value={name}
                onChangeText={value => setName(value)}
                placeholderTextColor={'#dedede'}
                style={styles.inputBox}
              />
            </Box>

            <Box>
              <FormControl.Label>
                <MaterialCommunityIcons name="at" style={styles.iconColor} />
                <TextM>ອີເມວ (ຖ້າມີ):</TextM>
              </FormControl.Label>
              <Input
                placeholder={'email@email.com'}
                value={email}
                onChangeText={value => setEmail(value)}
                placeholderTextColor={'#dedede'}
                style={styles.inputBox}
              />
            </Box>

            <Box>
              <FormControl.Label>
                <MaterialCommunityIcons
                  name="calendar-month-outline"
                  style={styles.iconColor}
                />
                <TextM>ວັນເດືອນປີເກີດ{requireStar()}:</TextM>
              </FormControl.Label>
              <TouchableOpacity
                onPress={() => setOpenDate(true)}
                style={{width: '100%', paddingVertical: 10}}>
                <TextM
                  fontWeight="bold"
                  style={{
                    color: colors.Blue,
                    textAlign: 'center',
                  }}>
                  {moment(dob).format('ວັນdddd, DD ເດືອນMMMM ປີ YYYY')}
                </TextM>
              </TouchableOpacity>
              <DatePicker
                modal
                mode={'date'}
                open={openDate}
                date={dob}
                onConfirm={date => {
                  setOpenDate(false);
                  setDob(date);
                }}
                onCancel={() => {
                  setOpenDate(false);
                }}
              />
            </Box>

            <Box>
              <FormControl.Label>
                <MaterialCommunityIcons
                  name="gender-male-female"
                  style={styles.iconColor}
                />
                <TextM>ເພດ{requireStar()}:</TextM>
              </FormControl.Label>
              <View style={{alignItems: 'center'}}>
                <RadioForm formHorizontal={true} animation={true}>
                  {radio_props.map((obj, i) => {
                    var onPress = (value, index) => {
                      setGender(value);
                      setGenderIndex(index);
                    };
                    return (
                      <RadioButton
                        labelHorizontal={true}
                        key={i}
                        style={{marginHorizontal: 30}}>
                        <RadioButtonInput
                          obj={obj}
                          index={i}
                          isSelected={genderIndex === i}
                          onPress={onPress}
                          buttonInnerColor={colors.Blue}
                          buttonOuterColor={colors.Blue}
                          buttonSize={14}
                        />
                        <RadioButtonLabel
                          obj={obj}
                          index={i}
                          onPress={onPress}
                          labelStyle={{
                            color: '#333333',
                            marginLeft: 5,
                            fontFamily: 'NotoSansLao-Regular',
                          }}
                        />
                      </RadioButton>
                    );
                  })}
                </RadioForm>
              </View>
            </Box>

            <Box>
              <FormControl.Label>
                <MaterialCommunityIcons name="phone" style={styles.iconColor} />
                <TextM>ເບີໂທ{requireStar()}:</TextM>
              </FormControl.Label>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    width: '30%',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text>+856 20</Text>
                </View>
                <View style={{width: '70%'}}>
                  <Input
                    placeholder={'ເບີໂທ 8 ຕົວເລກ'}
                    maxLength={8}
                    keyboardType={'numeric'}
                    value={phone}
                    onChangeText={value => setPhone(value)}
                    placeholderTextColor={'#dedede'}
                    style={styles.inputBox}
                  />
                </View>
              </View>
            </Box>

            <Box>
              <FormControl.Label>
                <MaterialCommunityIcons name="lock" style={styles.iconColor} />{' '}
                <TextM>ລະຫັດຜ່ານ{requireStar()}:</TextM>
              </FormControl.Label>
              <InputUI
                placeholder="**********"
                textStyle={{fontSize: 18}}
                value={password}
                onChangeText={nextValue => setPassword(nextValue)}
                secureTextEntry={secureTextEntry}
                accessoryRight={renderIcon}
                height={36}
              />
            </Box>

            <Box>
              <FormControl.Label>
                <MaterialCommunityIcons name="image" style={styles.iconColor} />
                <TextM>ຮູບໂປຣໄຟຣຂອງທ່ານ:</TextM>
              </FormControl.Label>
              <TouchableOpacity
                style={{
                  marginTop: 10,
                  marginBottom: 20,
                  width: 125,
                  borderColor: '#dadada',
                  borderWidth: 1,
                  borderRadius: 10,
                  alignSelf: 'center',
                }}
                onPress={() => {
                  getImageProfile();
                }}>
                <FastImage
                  style={{
                    height: 120,
                    borderRadius: 10,
                    // width: 150,
                  }}
                  source={
                    !imageProfile
                      ? require('../../../assets/img/user.png')
                      : {uri: imageProfile?.uri}
                  }
                  resizeMode={FastImage.resizeMode.cover}
                />
              </TouchableOpacity>
            </Box>
          </VStack>
        </FormControl>
      </ScrollView>
      <Box style={{paddingHorizontal: 20, paddingVertical: 10}}>
        <Button
          style={{width: '100%'}}
          onPress={() => {
            onCheckRegister();
          }}>
          <TextM style={{color: '#ffffff'}}>
            <MaterialCommunityIcons
              name="login"
              style={{color: '#ffffff', fontSize: 16}}
            />{' '}
            ລົງທະບຽນ
          </TextM>
        </Button>
      </Box>
    </Box>
  );
};

export default Registration;

const styles = StyleSheet.create({
  inputNoLeft: {
    marginLeft: 0,
  },
  labelColor: {
    color: colors.Black,
    fontFamily: 'NotoSansLao-Regular',
  },
  iconColor: {
    color: colors.Blue,
    fontSize: 20,
    marginRight: 5,
  },
  inputBox: {
    color: colors.Black,
    fontFamily: 'NotoSansLao-Regular',
    fontSize: 16,
  },
});
