import React, {useEffect, useState} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {
  Box,
  ScrollView,
  View,
  Text,
  Icon,
  Button,
  HStack,
  VStack,
} from 'native-base';
import firestore from '@react-native-firebase/firestore';
import moment from 'moment';
import LottieView from 'lottie-react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
//Redux package
import {useSelector, useDispatch} from 'react-redux';
// Component
import Loading from '../../shareComponents/Loading';
import TextM from '../../shareComponents/TextM';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';

const CallHistory = ({navigation}) => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();
  // useState
  const [callHistory, setCallHistory] = useState([]);
  const [dataLoading, setDataLoading] = useState(false);

  const userInfo = userData?.userInfo;

  // useEffect
  useEffect(() => {
    console.log('useEffect CallHistory', userInfo);
    getCallHistory();
  }, []);

  const getCallHistory = () => {
    try {
      firestore()
        .collection('callHistory')
        .where('user_id', '==', userInfo?.user_id)
        .orderBy('startedAt', 'desc')
        .limit(20)
        .get()
        .then(querySnapshot => {
          // console.log('data', documentSnapshot);
          let allData = [];
          querySnapshot.forEach(doc => {
            allData.push(doc.data());
          });
          setCallHistory(allData);
          setDataLoading(true);
        })
        .catch(error => {
          console.error('Error read ', error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const renderTimeSecond = value => {
    let second = value % 60;
    return (
      Math.floor(value / 60) +
      ':' +
      (second ? (second < 10 ? '0' + second : second) : '00')
    );
  };

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <ScrollView
        style={{...defaultStyle.fixedLayout}}
        contentContainerStyle={{
          flexGrow: callHistory.length > 0 ? 0 : 1,
          justifyContent: 'center',
        }}>
        {dataLoading ? (
          callHistory.length > 0 ? (
            <Box mb={6}>
              <TextM style={{alignSelf: 'flex-end', color: colors.DarkBlue}}>
                ຈຳກັດ 20 ລາຍການລ່າສຸດຍ້ອນຫລັງ
              </TextM>
              {callHistory.map((item, i) => {
                return (
                  <HStack
                    key={i}
                    style={{
                      backgroundColor: colors.LightWhite,
                      borderRadius: 10,
                      marginTop: 8,
                    }}>
                    <VStack
                      space={6}
                      alignItems="center"
                      justifyContent={'center'}
                      width={90}>
                      <FontAwesome5
                        name={item?.callType === 'voice' ? 'phone' : 'video'}
                        style={{color: colors.Blue, fontSize: 28}}
                      />
                      <MaterialCommunityIcons
                        name={
                          item?.durationSec !== undefined ||
                          item?.durationSecbyDoc !== undefined
                            ? 'checkbox-multiple-marked-circle'
                            : 'phone-missed'
                        }
                        style={{
                          color:
                            item?.durationSec !== undefined ||
                            item?.durationSecbyDoc !== undefined
                              ? colors.Green
                              : colors.Red,
                          fontSize: 28,
                        }}
                      />
                    </VStack>
                    <VStack
                      w={'68%'}
                      style={{
                        paddingTop: 10,
                        paddingBottom: 10,
                      }}>
                      <TextM
                        fontWeight="bold"
                        style={{color: colors.DarkBlue, fontSize: 18}}>
                        {item?.doctorName}
                      </TextM>

                      <HStack alignItems={'center'} space={2}>
                        <MaterialCommunityIcons
                          name="badge-account"
                          style={{fontSize: 20, color: colors.DarkBlue}}
                        />
                        <TextM style={{color: colors.DarkLightBlue}}>
                          ທ່ານ {item?.patientName}
                        </TextM>
                      </HStack>

                      <HStack alignItems={'center'} space={2}>
                        <MaterialCommunityIcons
                          name="clock-outline"
                          style={{fontSize: 20, color: colors.DarkBlue}}
                        />
                        <TextM>
                          ເວລາໂທ:{' '}
                          {moment(item?.startedAt?.seconds * 1000).format(
                            'h:mm a',
                          )}
                        </TextM>
                      </HStack>

                      <HStack alignItems={'center'} space={2}>
                        <MaterialCommunityIcons
                          name="calendar-range"
                          style={{fontSize: 20, color: colors.DarkBlue}}
                        />
                        <TextM>
                          ວັນທີ:{' '}
                          {moment(item?.startedAt?.seconds * 1000).format(
                            'DD MMM YYYY',
                          )}
                        </TextM>
                      </HStack>

                      {(item?.durationSec !== undefined ||
                        item?.durationSecbyDoc !== undefined) && (
                        <HStack alignItems={'center'} space={2}>
                          <MaterialCommunityIcons
                            name={'clipboard-text-clock'}
                            style={{fontSize: 20, color: colors.DarkBlue}}
                          />
                          <TextM>
                            ໄລຍະເວລາໃນການໂທ{' '}
                            {item?.durationSec !== undefined
                              ? renderTimeSecond(item?.durationSec)
                              : renderTimeSecond(item?.durationSecbyDoc)}{' '}
                            (ນາທີ:ວິນາທີ)
                          </TextM>
                        </HStack>
                      )}

                      <HStack alignItems={'center'} space={2}>
                        <MaterialCommunityIcons
                          name={'list-status'}
                          style={{fontSize: 20, color: colors.DarkBlue}}
                        />
                        <TextM
                          style={{
                            color:
                              item?.durationSec !== undefined ||
                              item?.durationSecbyDoc !== undefined
                                ? colors.Green
                                : colors.Red,
                          }}>
                          ສະຖານະ{' '}
                          {item?.durationSec !== undefined ||
                          item?.durationSecbyDoc !== undefined
                            ? 'ສຳເລັດແລ້ວ'
                            : 'ບໍ່ຮັບສາຍ'}
                        </TextM>
                      </HStack>
                    </VStack>
                  </HStack>
                );
              })}
            </Box>
          ) : (
            <Box style={{alignItems: 'center'}}>
              <Box style={{height: 180, width: 200, marginTop: -80}}>
                <LottieView
                  source={require('../../../assets/animation/no-result-found.json')}
                  // style={{width: 300}}
                  autoPlay
                  loop
                  resizeMode="cover"
                />
              </Box>
              <TextM>ບໍ່ພົບຂໍ້ມູນຂອງທ່ານ</TextM>
            </Box>
          )
        ) : (
          <Box style={{alignItems: 'center'}}>
            <Box style={{height: 100, width: 200, marginTop: -80}}>
              <LottieView
                source={require('../../../assets/animation/loading-dot.json')}
                autoPlay
                loop
                resizeMode="cover"
              />
            </Box>
            <TextM>ກຳລັງໂຫລດ...</TextM>
          </Box>
        )}
      </ScrollView>
    </Box>
  );
};

export default CallHistory;

const styles = StyleSheet.create({});
