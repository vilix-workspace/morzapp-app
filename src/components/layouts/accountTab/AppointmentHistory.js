import React, {useEffect, useState} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Box, ScrollView, View, HStack} from 'native-base';
import moment from 'moment';
import LottieView from 'lottie-react-native';
import axios from 'axios';
import Config from 'react-native-config';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
// Redux
import {useSelector, useDispatch} from 'react-redux';

// Component
import Loading from '../../shareComponents/Loading';
import TextM from '../../shareComponents/TextM';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';

const AppointmentHistory = ({navigation}) => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();
  // useState
  const [bookHistory, setBookHistory] = useState([]);
  const [dataLoading, setDataLoading] = useState(false);

  const userInfo = userData?.userInfo;

  // useEffect
  useEffect(() => {
    // console.log('useEffect AppointmentHistory', userData);
    getBookHistory();
  }, []);

  const getBookHistory = () => {
    try {
      axios({
        method: 'get',
        url:
          Config.WP_URL +
          '/wp/v2/mzbooking/?filter[meta_query][0][key]=booking_status&filter[meta_query][0][value][0]=completed&filter[meta_query][0][value][1]=cancelled&filter[meta_query][1][key]=contact_phone&filter[meta_query][1][value][0]=' +
          userInfo?.phone +
          '&per_page=20',
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(res => {
          setBookHistory(res.data);
          setDataLoading(true);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <ScrollView
        style={{...defaultStyle.fixedLayout}}
        contentContainerStyle={{
          flexGrow: bookHistory.length > 0 ? 0 : 1,
          justifyContent: 'center',
        }}>
        {dataLoading ? (
          bookHistory.length > 0 ? (
            <View>
              <TextM style={{alignSelf: 'flex-end', color: colors.Blue}}>
                ຈຳກັດຈຳນວນ 20 ລາຍການຍ້ອນຫລັງ
              </TextM>
              {bookHistory.map((item, i) => {
                return (
                  <HStack key={i}>
                    <TouchableOpacity
                      noIndent
                      noBorder
                      thumbnail
                      style={{
                        backgroundColor: colors.LightWhite,
                        borderRadius: 10,
                        marginBottom: 10,
                        marginTop: 8,
                        paddingRight: 20,
                      }}>
                      <Box>
                        <FontAwesome5
                          name="notes-medical"
                          style={{color: colors.Blue}}
                        />
                      </Box>
                      <Box
                        style={{
                          paddingTop: 10,
                          paddingBottom: 10,
                        }}>
                        <TextM
                          fontWeight="bold"
                          style={{color: colors.DarkBlue, fontSize: 18}}>
                          <FontAwesome5
                            name="hospital"
                            style={{
                              color: colors.DarkBlue,
                              fontSize: 20,
                            }}
                          />{' '}
                          {item?.hospital_name}
                        </TextM>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                          }}>
                          <TextM note numberOfLines={1}>
                            ວັນທີ:{' '}
                            {moment(item?.booking_time).format('DD MMM YYYY')}
                          </TextM>
                          <TextM note numberOfLines={1}>
                            ເວລານັດ:{' '}
                            {moment(item?.booking_time).format('h:mm a')}
                          </TextM>
                        </View>
                        <View
                          style={{
                            borderColor: colors.LightBlue,
                            borderWidth: 1,
                            borderStyle: 'dashed',
                            borderRadius: 5,
                            marginVertical: 5,
                            padding: 8,
                          }}>
                          <TextM note>ຂໍ້ມູນຄົນເຈັບ</TextM>
                          <TextM>ຊື່: {item?.patient_name}</TextM>
                          <TextM>ອາຍຸ: {item?.age}</TextM>
                          <TextM>ອາການ: {item?.symptom}</TextM>
                        </View>
                        <TextM
                          style={{
                            color:
                              item?.booking_status === 'cancelled'
                                ? colors.Red
                                : colors.Green,
                          }}>
                          ສະຖານະ:{' '}
                          {item?.booking_status === 'cancelled'
                            ? 'ຍົກເລີກ'
                            : 'ນັດພົບສຳເລັດແລ້ວ'}
                        </TextM>
                      </Box>
                    </TouchableOpacity>
                  </HStack>
                );
              })}
            </View>
          ) : (
            <Box style={{alignItems: 'center'}}>
              <Box style={{height: 180, width: 200, marginTop: -80}}>
                <LottieView
                  source={require('../../../assets/animation/no-result-found.json')}
                  // style={{width: 300}}
                  autoPlay
                  loop
                  resizeMode="cover"
                />
              </Box>
              <TextM>ບໍ່ພົບຂໍ້ມູນຂອງທ່ານ</TextM>
            </Box>
          )
        ) : (
          <Box style={{alignItems: 'center'}}>
            <Box style={{height: 100, width: 200, marginTop: -80}}>
              <LottieView
                source={require('../../../assets/animation/loading-dot.json')}
                autoPlay
                loop
                resizeMode="cover"
              />
            </Box>
            <TextM>ກຳລັງໂຫລດ...</TextM>
          </Box>
        )}
      </ScrollView>
    </Box>
  );
};

export default AppointmentHistory;

const styles = StyleSheet.create({});
