import React, {useEffect, useState} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Box, ScrollView, View} from 'native-base';
import axios from 'axios';
import LottieView from 'lottie-react-native';
import moment from 'moment';
import CryptoJS from 'crypto-js';
import Config from 'react-native-config';
//Redux package
import {useSelector, useDispatch} from 'react-redux';
// Component
import Loading from '../../shareComponents/Loading';
import TextM from '../../shareComponents/TextM';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';
// Server API

const TopupHistory = ({navigation}) => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();
  // useState
  const [allTopupHistory, setAllTopupHistory] = useState([]);
  const [checkLoading, setCheckLoading] = useState(false);

  // Function
  const getTopupHistory = () => {
    setCheckLoading(true);
    try {
      axios({
        method: 'get',
        url:
          Config.WP_URL +
          '/wp/v2/topup_request/?user_id=' +
          userData?.userInfo?.user_id +
          '&per_page=30',
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(res => {
          console.log(res);
          setAllTopupHistory(res.data);
          setCheckLoading(false);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const numberWithCommas = x => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  const renderStatus = status => {
    switch (status) {
      case 'requested':
        return <TextM style={{color: colors.Orange}}>ລໍຖ້າການກວດສອບ</TextM>;
        break;

      case 'cancelled':
        return <TextM style={{color: colors.Red}}>ຍົກເລີກ</TextM>;
        break;

      case 'rejected':
        return <TextM style={{color: colors.Red}}>ຖືກປະຕິເສດ</TextM>;
        break;

      case 'done':
        return <TextM style={{color: colors.Green}}>ສຳເລັດແລ້ວ</TextM>;
        break;

      default:
        break;
    }
  };

  // useEffect
  useEffect(() => {
    console.log('useEffect TopupHistory');
    getTopupHistory();
  }, []);

  return (
    <Box flex={1} backgroundColor={colors.White} justifyContent={'center'}>
      <ScrollView
        style={{...defaultStyle.fixedLayout}}
        contentContainerStyle={{
          flexGrow: allTopupHistory.length > 0 ? 0 : 1,
          justifyContent: 'center',
        }}>
        {!checkLoading ? (
          allTopupHistory.length > 0 ? (
            <View>
              <TextM fontWeight="bold" style={{color: colors.DarkBlue}}>
                ລາຍການທັງໝົດ
              </TextM>
              {allTopupHistory.map((item, i) => {
                return (
                  <View
                    key={i}
                    style={{
                      backgroundColor: colors.LightWhite,
                      padding: 15,
                      borderRadius: 10,
                      marginTop: 10,
                    }}>
                    <TextM>ສະຖານະ: {renderStatus(item?.status)}</TextM>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <TextM note>ID: RF{item?.id}</TextM>
                      <TextM>
                        ຈຳນວນເງິນທີ່ເຕີມ:
                        <TextM style={{color: colors.Blue}}>
                          {' '}
                          {numberWithCommas(item?.amount)} ກີບ
                        </TextM>
                      </TextM>
                    </View>
                    <TextM note style={{textAlign: 'right'}}>
                      ວັນທີ: {moment(item?.date).format('DD MMM YYYY')}
                    </TextM>
                  </View>
                );
              })}
            </View>
          ) : (
            <Box style={{alignItems: 'center'}}>
              <Box style={{height: 180, width: 200, marginTop: -80}}>
                <LottieView
                  source={require('../../../assets/animation/no-result-found.json')}
                  // style={{width: 300}}
                  autoPlay
                  loop
                  resizeMode="cover"
                />
              </Box>
              <TextM>ບໍ່ພົບຂໍ້ມູນຂອງທ່ານ</TextM>
            </Box>
          )
        ) : (
          <Box style={{alignItems: 'center', marginTop: 20}}>
            <Box style={{height: 100, width: 200, marginTop: -80}}>
              <LottieView
                source={require('../../../assets/animation/loading-dot.json')}
                autoPlay
                loop
                resizeMode="cover"
              />
            </Box>
            <TextM>ກຳລັງໂຫລດ...</TextM>
          </Box>
        )}
        {}
      </ScrollView>
    </Box>
  );
};

export default TopupHistory;

const styles = StyleSheet.create({});
