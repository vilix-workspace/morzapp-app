import React, {useEffect, useState} from 'react';
import {Keyboard, StyleSheet, TextInput} from 'react-native';
import {
  Box,
  ScrollView,
  View,
  Button,
  useToast,
  Alert,
  HStack,
  VStack,
} from 'native-base';
import auth from '@react-native-firebase/auth';
import axios from 'axios';
import Config from 'react-native-config';
import RNFetchBlob from 'rn-fetch-blob';
import CryptoJS from 'crypto-js';
import {v4 as uuidv4} from 'uuid';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import firestore from '@react-native-firebase/firestore';

import {defaultStyle, colors} from '../../shareStyle';

// Component
import Loading from '../../shareComponents/Loading';
// Redux
import {useSelector, useDispatch} from 'react-redux';
//Redux action
import {
  addUserData,
  setToken,
} from '../../../stores/features/userData/userDataSlice';
import TextM from '../../shareComponents/TextM';

const OTPVerification = ({navigation, route}) => {
  const toast = useToast();
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();
  // Param Data
  const {userinfo} = route.params;
  // useState
  const [loading, setLoading] = useState(false);
  const [confirm, setConfirm] = useState(null);
  const [code, setCode] = useState('');

  // Handle the button press
  const signInWithPhoneNumber = async phoneNumber => {
    const confirmation = await auth().signInWithPhoneNumber(phoneNumber);
    setConfirm(confirmation);
  };

  const confirmCode = async () => {
    setLoading(true);
    try {
      await confirm.confirm(code);
      await onRegisterPress();
    } catch (error) {
      // console.log('Invalid code.');
      showToast('error', 'ລະຫັດບໍ່ຖຶກຕ້ອງ!');
      setLoading(false);
    }
  };

  // Add Customer Info
  const addCustomerCheck = async () => {
    try {
      await axios({
        method: 'post',
        url: Config.WP_URL + '/wp/v2/customer_check',
        data: {
          title: userinfo.phone,
          fields: {
            phone: userinfo.phone,
          },
          status: 'publish',
        },
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(res => {
          // console.log('addCustomerCheck', res.data);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const addCustomers = async mediadata => {
    // Pass Encrypt
    var passEncrypt = CryptoJS.AES.encrypt(
      userinfo.password,
      Config.PS_AES,
    ).toString();
    let userObject = {
      name: userinfo.name,
      email: userinfo.email,
      dob: userinfo.dob,
      gender: userinfo.gender,
      phone: userinfo.phone,
      password: passEncrypt,
      image_id: mediadata == undefined ? '' : mediadata.id,
      image_url: mediadata == undefined ? '' : mediadata.guid.rendered,
      login_type: 'Phone',
    };
    try {
      await axios({
        method: 'post',
        url: Config.WP_URL + '/wp/v2/customers',
        data: {
          title: userinfo.name,
          fields: userObject,
          status: 'publish',
        },
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(res => {
          createUser(userObject);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const createUser = async userObject => {
    let ramdonEmailID = uuidv4();
    try {
      await axios({
        method: 'post',
        url: Config.WP_URL + '/wp/v2/users',
        data: {
          name: userinfo.name,
          roles: 'customer',
        },
        params: {
          username: userinfo.phone,
          password: userinfo.password,
          email:
            userinfo.email == ''
              ? ramdonEmailID + '@noemail.none'
              : userinfo.email,
        },
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(async res => {
          let userid = {
            user_id: res.data.id,
          };
          let combineuserinfo = Object.assign(userObject, userid);
          dispatch(addUserData(combineuserinfo));
          await onLoginAndgetUserToken();

          try {
            firestore()
              .collection('users')
              .add(combineuserinfo)
              .then(() => {
                console.error('writing document successful');
              })
              .catch(error => {
                console.error('Error writing document: ', error);
              });
          } catch (error) {
            console.log(error);
          }
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const onLoginAndgetUserToken = async () => {
    try {
      await axios({
        method: 'post',
        url: Config.WP_URL + '/api/v1/token',
        data: {
          username: userinfo.phone,
          password: userinfo.password,
        },
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(res => {
          // console.log(res.data);
          let token = res?.data?.jwt_token;
          dispatch(setToken(token));
          navigation.navigate('CompletedRegis');
          setLoading(false);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const uploadImage = async () => {
    if (userinfo.imageProfile !== null) {
      try {
        let url = Config.WP_URL + `/wp/v2/media/`;
        let formData = new FormData();
        //I am sending the whole image object. I also tried to send image.data but all without success
        formData.append('image', userinfo.imageProfile);

        let filename = userinfo.name.toLowerCase();

        await RNFetchBlob.fetch(
          'POST',
          url,
          {
            'Content-Type': 'application/octet',
            'Content-Disposition':
              `application/octet; filename="` + filename + `-profile.jpg`,
            'Cache-Control': 'no-store',
            Authorization: 'Bearer ' + userData?.userToken,
          },
          userinfo.imageProfile?.base64,
        ).then(async data => {
          let mediadata = await JSON.parse(data?.data);
          await addCustomers(mediadata);
        });
      } catch (error) {
        console.log(error);
      }
    } else {
      await addCustomers();
    }
  };

  const onRegisterPress = async () => {
    await addCustomerCheck();
    await uploadImage();
  };

  const showToast = (type, title, desc) => {
    toast.show({
      render: () => {
        return (
          <Alert
            maxWidth="95%"
            alignSelf="center"
            flexDirection="row"
            status={type}
            variant={'left-accent'}>
            <VStack space={1} flexShrink={1} w="100%">
              <HStack
                flexShrink={1}
                alignItems="center"
                justifyContent="space-between">
                <HStack space={2} flexShrink={1} alignItems="center">
                  <Alert.Icon />
                  <TextM>{title}</TextM>
                </HStack>
              </HStack>
              {desc !== undefined && (
                <TextM style={{fontSize: 14, paddingLeft: 25}}>{desc}</TextM>
              )}
            </VStack>
          </Alert>
        );
      },
    });
  };

  // useEffect
  useEffect(() => {
    if (userinfo?.phone?.length == 8) {
      signInWithPhoneNumber('+85620' + userinfo?.phone);
    }
  }, []);

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <ScrollView style={{...defaultStyle.fixedLayout}}>
        <View style={styles.layoutCenter}>
          <MaterialCommunityIcons
            name={'onepassword'}
            style={{color: colors.Blue, fontSize: 80, marginBottom: 20}}
          />
          <TextM
            fontWeight="bold"
            style={{
              fontSize: 26,
            }}>
            ໃສ່ລະຫັດ
          </TextM>
          <TextM
            style={{
              textAlign: 'center',
              marginTop: 10,
            }}>
            ພວກເຮົາໄດ້ສົ່ງລະຫັດ 6 ຕົວເລກໄປຫາທ່ານແລ້ວທາງ SMS ຜ່ານເບີ{' '}
            <TextM style={{color: colors.Green}}>
              +856 20 {userinfo?.phone}
            </TextM>{' '}
            ກະລຸນາຕື່ມລະຫັດໃສ່ດ້ານລຸ່ມ
          </TextM>
          <View style={{marginVertical: 40}}>
            <TextInput
              value={code}
              onChangeText={text => setCode(text)}
              placeholder={'******'}
              placeholderTextColor={colors.Gray}
              style={{
                color: colors.DarkBlue,
                alignSelf: 'center',
                fontSize: 34,
                textAlign: 'center',
              }}
              keyboardType={'numeric'}
              maxLength={6}
            />
          </View>
        </View>
      </ScrollView>
      <Button
        isLoading={confirm !== null ? false : true}
        marginX={5}
        marginY={4}
        disabled={confirm !== null ? false : true}
        style={{
          backgroundColor: confirm !== null ? colors.DarkBlue : colors.Gray,
        }}
        onPress={() => {
          Keyboard.dismiss();
          confirmCode();
        }}>
        <TextM fontWeight="bold" style={{color: colors.White}}>
          ຢືນຢັນລະຫັດ
        </TextM>
      </Button>

      {loading && <Loading />}
    </Box>
  );
};

export default OTPVerification;

const styles = StyleSheet.create({
  layoutCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 30,
    marginTop: 80,
  },
});
