import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Box, ScrollView, View, HStack} from 'native-base';
import FastImage from 'react-native-fast-image';
import LottieView from 'lottie-react-native';
import WooCommerce from '../../../config/WooCommerce';
import moment from 'moment';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
// Redux
import {useSelector, useDispatch} from 'react-redux';

// Component
import Loading from '../../shareComponents/Loading';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';
import TextM from '../../shareComponents/TextM';

const OrderHistory = ({navigation}) => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();
  // useState
  const [loading, setLoading] = useState(false);
  const [orderWooHistory, setOrderWooHistory] = useState([]);
  const [dataLoading, setDataLoading] = useState(false);

  const userInfo = userData.userInfo;

  // useEffect
  useEffect(() => {
    console.log('useEffect OrderHistory', userInfo);
    getOrderHistory();
  }, []);

  const numberWithCommas = x => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  const getOrderHistory = async () => {
    console.log('run getOrderHistory');
    try {
      await WooCommerce.get('orders', {
        customer: userInfo?.user_id,
        status: 'completed,cancelled',
        per_page: 20,
      })
        .then(res => {
          console.log('data', res);
          setOrderWooHistory(res);
          setDataLoading(true);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <ScrollView
        style={{...defaultStyle.fixedLayout}}
        contentContainerStyle={{
          flexGrow: orderWooHistory.length > 0 ? 0 : 1,
          justifyContent: 'center',
        }}>
        {dataLoading ? (
          orderWooHistory.length > 0 ? (
            <View>
              <TextM style={{alignSelf: 'flex-end', color: colors.Blue}}>
                ຈຳກັດຈຳນວນ 20 ລາຍການຍ້ອນຫລັງ
              </TextM>
              {orderWooHistory.map((item, i) => {
                return (
                  <HStack key={i}>
                    <TouchableOpacity
                      noIndent
                      noBorder
                      thumbnail
                      style={{
                        backgroundColor: colors.LightWhite,
                        borderRadius: 10,
                        marginBottom: 10,
                        marginTop: 8,
                        paddingRight: 20,
                      }}>
                      <Box>
                        <FontAwesome5
                          name="list-alt"
                          style={{color: colors.Blue}}
                        />
                      </Box>
                      <Box
                        style={{
                          paddingTop: 10,
                          paddingBottom: 10,
                        }}>
                        <TextM style={{color: colors.DarkBlue, fontSize: 14}}>
                          ລະຫັດອໍເດີ້: {item?.id}
                        </TextM>
                        <View
                          style={{
                            borderColor: colors.LightBlue,
                            borderWidth: 1,
                            borderStyle: 'dashed',
                            borderRadius: 5,
                            marginVertical: 5,
                            padding: 8,
                          }}>
                          <TextM note>ຂໍ້ມູນສິນຄ້າ</TextM>
                          {item?.line_items.map((data, i) => {
                            return (
                              <View key={i}>
                                <TextM style={{fontSize: 14}}>
                                  {i + 1}. {data?.name} ({data?.quantity} x{' '}
                                  {data?.price})
                                </TextM>
                              </View>
                            );
                          })}
                          <TextM
                            style={{
                              color: colors.DarkBlue,
                              fontSize: 14,
                              alignSelf: 'flex-end',
                              marginTop: 5,
                            }}>
                            ລວມ: {numberWithCommas(item?.total)} ກີບ
                          </TextM>
                        </View>
                        {/* Shipping */}
                        <View
                          style={{
                            borderColor: colors.LightBlue,
                            borderWidth: 1,
                            borderStyle: 'dashed',
                            borderRadius: 5,
                            marginVertical: 5,
                            padding: 8,
                          }}>
                          <TextM note>ຂໍ້ມູນການສົ່ງເຄື່ອງ</TextM>
                          <TextM style={{fontSize: 14}}>
                            ຜູ້ສັ່ງເຄື່ອງ: {item?.billing?.first_name}
                          </TextM>
                          <TextM style={{fontSize: 14}}>
                            ເບີໂທຕິດຕໍ່: 020 {item?.billing?.phone}
                          </TextM>
                          <TextM style={{fontSize: 14}}>
                            ສະຖານທີ່: {item?.shipping?.address_1}
                          </TextM>
                          <TextM style={{fontSize: 14}}>
                            ເມືອງ: {item?.shipping?.city}
                          </TextM>
                        </View>
                        <TextM style={{fontSize: 14}}>
                          ສັ່ງຊື້ວັນທີ:{' '}
                          {moment(item?.date_created).format(
                            'DD MMM YYYY ເວລາ: h:mm a',
                          )}
                        </TextM>
                        <TextM style={{fontSize: 14}}>
                          ສະຖານະ:{' '}
                          <TextM
                            style={{
                              color:
                                item?.status === 'completed'
                                  ? colors.Green
                                  : colors.Red,
                            }}>
                            {item?.status === 'completed'
                              ? 'ສຳເລັດແລ້ວ'
                              : 'ຍົກເລີກ'}
                          </TextM>
                        </TextM>
                      </Box>
                    </TouchableOpacity>
                  </HStack>
                );
              })}
            </View>
          ) : (
            <Box style={{alignItems: 'center'}}>
              <Box style={{height: 180, width: 200, marginTop: -80}}>
                <LottieView
                  source={require('../../../assets/animation/no-result-found.json')}
                  // style={{width: 300}}
                  autoPlay
                  loop
                  resizeMode="cover"
                />
              </Box>
              <TextM>ບໍ່ພົບຂໍ້ມູນຂອງທ່ານ</TextM>
            </Box>
          )
        ) : (
          <Box style={{alignItems: 'center'}}>
            <Box style={{height: 100, width: 200, marginTop: -80}}>
              <LottieView
                source={require('../../../assets/animation/loading-dot.json')}
                autoPlay
                loop
                resizeMode="cover"
              />
            </Box>
            <TextM>ກຳລັງໂຫລດ...</TextM>
          </Box>
        )}
      </ScrollView>
    </Box>
  );
};

export default OrderHistory;

const styles = StyleSheet.create({});
