import React, {useEffect, useState} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Box, ScrollView, View} from 'native-base';
import axios from 'axios';
import LottieView from 'lottie-react-native';
import moment from 'moment';
import Config from 'react-native-config';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
//Redux package
import {useSelector, useDispatch} from 'react-redux';
// Component
import Loading from '../../shareComponents/Loading';
import TextM from '../../shareComponents/TextM';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';

const WalletPayment = ({navigation}) => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();
  // useState
  const [allTransaction, setAllTransaction] = useState([]);
  const [checkLoading, setCheckLoading] = useState(false);

  // Function
  const getAllTrans = () => {
    setCheckLoading(true);
    try {
      axios({
        method: 'get',
        url: Config.WP_URL + '/wp/v2/wallet/' + userData?.userInfo?.user_id,
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(res => {
          console.log(res);
          setAllTransaction(res.data);
          setCheckLoading(false);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const numberWithCommas = x => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  // Random ID Number
  const generateUUID = digits => {
    let str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVXZ';
    let uuid = [];
    for (let i = 0; i < digits; i++) {
      uuid.push(str[Math.floor(Math.random() * str.length)]);
    }
    return uuid.join('');
  };

  // useEffect
  useEffect(() => {
    console.log('useEffect WalletPayment');
    getAllTrans();
  }, []);

  return (
    <Box flex={1} backgroundColor={colors.White} justifyContent={'center'}>
      <ScrollView
        style={{...defaultStyle.fixedLayout}}
        contentContainerStyle={{
          flexGrow: allTransaction.length > 0 ? 0 : 1,
          justifyContent: 'center',
        }}>
        {!checkLoading ? (
          allTransaction.length > 0 ? (
            <View>
              <TextM fontWeight="bold" style={{color: colors.DarkBlue}}>
                ລາຍການທັງໝົດ
              </TextM>
              {allTransaction.map((item, i) => {
                return (
                  <View
                    key={i}
                    style={{
                      backgroundColor: colors.LightWhite,
                      padding: 15,
                      borderRadius: 10,
                      marginTop: 10,
                    }}>
                    <TextM>
                      ລະຫັດ:{' '}
                      {item?.type === 'credit'
                        ? 'CR' + item?.transaction_id
                        : 'DE' + item?.transaction_id}
                    </TextM>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <View
                        style={{flexDirection: 'row', alignItems: 'center'}}>
                        <TextM style={{color: colors.LightBlack}}>
                          ປະເພດ:{' '}
                        </TextM>
                        <TextM
                          style={{
                            color:
                              item?.type === 'credit'
                                ? colors.Green
                                : colors.Red,
                          }}>
                          {item?.type === 'credit' ? 'ເຕີມເງິນ' : 'ຈ່າຍເງິນ'}
                        </TextM>
                      </View>
                      <View>
                        <TextM>
                          <TextM style={{color: colors.LightBlack}}>
                            ວັນທີ:
                          </TextM>{' '}
                          {moment(item.date).format('DD/MM/YYYY')}{' '}
                        </TextM>
                      </View>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}>
                      <View style={{width: '20%'}}>
                        <MaterialCommunityIcons
                          name={
                            item?.type === 'credit' ? 'cash-plus' : 'cash-minus'
                          }
                          style={{
                            color:
                              item?.type === 'credit'
                                ? colors.Green
                                : colors.Red,
                            fontSize: 40,
                          }}
                        />
                      </View>
                      <View style={{width: '46%'}}>
                        <TextM style={{fontSize: 14, color: colors.LightBlack}}>
                          ຈຳນວນເງິນ
                        </TextM>
                        <View
                          style={{
                            borderWidth: 1,
                            borderColor:
                              item?.type === 'credit'
                                ? colors.Green
                                : colors.Red,
                            padding: 4,
                            alignItems: 'center',
                            borderRadius: 5,
                          }}>
                          <TextM
                            style={{
                              fontSize: 16,
                            }}>
                            {numberWithCommas(parseInt(item.amount))} ກີບ
                          </TextM>
                        </View>
                      </View>
                      <View
                        style={{
                          width: '30%',
                          alignItems: 'flex-end',
                        }}>
                        <TextM style={{fontSize: 14, color: colors.LightBlack}}>
                          ຍອດເງີນຍັງເຫຼືອ
                        </TextM>
                        <TextM>
                          {numberWithCommas(parseInt(item.balance))} ກີບ
                        </TextM>
                      </View>
                    </View>
                    <View style={{marginTop: 10}}>
                      <TextM>
                        <TextM
                          style={{
                            color: colors.LightBlack,
                          }}>
                          ລາຍລະອຽດ:{' '}
                        </TextM>
                        {item?.details}
                      </TextM>
                    </View>
                  </View>
                );
              })}
            </View>
          ) : (
            <Box style={{alignItems: 'center'}}>
              <Box style={{height: 180, width: 200, marginTop: -80}}>
                <LottieView
                  source={require('../../../assets/animation/no-result-found.json')}
                  // style={{width: 300}}
                  autoPlay
                  loop
                  resizeMode="cover"
                />
              </Box>
              <TextM>ບໍ່ພົບຂໍ້ມູນຂອງທ່ານ</TextM>
            </Box>
          )
        ) : (
          <Box style={{alignItems: 'center', marginTop: 20}}>
            <Box style={{height: 100, width: 200, marginTop: -80}}>
              <LottieView
                source={require('../../../assets/animation/loading-dot.json')}
                autoPlay
                loop
                resizeMode="cover"
              />
            </Box>
            <TextM>ກຳລັງໂຫລດ...</TextM>
          </Box>
        )}
        {}
      </ScrollView>
    </Box>
  );
};

export default WalletPayment;

const styles = StyleSheet.create({});
