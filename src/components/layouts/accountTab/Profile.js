import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native';
import {Box, ScrollView, View, VStack, HStack} from 'native-base';
import {Avatar, Button as ButtonN, Dialog, Portal} from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import VersionNumber from 'react-native-version-number';
import axios from 'axios';
import FastImage from 'react-native-fast-image';
import moment from 'moment';
import Config from 'react-native-config';
import auth from '@react-native-firebase/auth';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
//Redux package
import {useSelector, useDispatch} from 'react-redux';
//Redux action
import {
  logOut,
  setToken,
} from '../../../stores/features/userData/userDataSlice';

// Share Style
import {colors, defaultStyle} from '../../shareStyle';

// Component
import Header from '../../shareComponents/Header';
import TextM from '../../shareComponents/TextM';
import Loading from '../../shareComponents/Loading';
import TopicH from '../../shareComponents/TopicH';

const Profile = ({navigation}) => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();

  // useState
  const [loading, setLoading] = useState(false);
  const [showAlert, setShowAlert] = useState(false);
  const [walletAmount, setWalletAmount] = useState(null);

  // Functions
  const getPubKey = async () => {
    try {
      await axios({
        method: 'get',
        url: Config.WP_URL + '/wp/v2/public_key',
      })
        .then(async res => {
          try {
            await axios({
              method: 'post',
              url: Config.WP_URL + '/api/v1/token',
              data: {
                username: res.data[0].mc,
                password: res.data[0].mcc,
              },
            })
              .then(res => {
                let token = res?.data?.jwt_token;
                dispatch(setToken(token));
                setLoading(false);
              })
              .catch(error => {
                console.log(error);
              });
          } catch (error) {
            console.log(error);
          }
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const onLogOut = async () => {
    setLoading(true);
    auth()
      .signOut()
      .then(async () => {
        console.log('User signed out!');
        dispatch(logOut());
        if (userData?.userInfo?.login_type === 'Google') {
          GoogleSignin.revokeAccess();
        }
        await getPubKey();
      });
  };

  const showAlertNow = () => {
    setShowAlert(true);
  };

  const hideAlert = () => {
    setShowAlert(false);
  };

  const numberWithCommas = x => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  const getCurrentWallet = () => {
    try {
      axios({
        method: 'get',
        url:
          Config.WP_URL +
          '/wp/v2/current_balance/' +
          userData?.userInfo?.user_id,
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(res => {
          // console.log(res);
          setWalletAmount(parseInt(res.data));
          setLoading(false);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  // useEffect
  useEffect(() => {
    console.log(userData);
    getCurrentWallet();
  }, []);

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <SafeAreaView>
        <Header navigation={navigation} />
      </SafeAreaView>
      <ScrollView style={{...defaultStyle.fixedLayout}}>
        <HStack space={3}>
          <Box>
            {userData?.userInfo?.image_url !== '' &&
            userData?.userInfo?.image_url !== undefined ? (
              <FastImage
                style={{
                  width: 90,
                  height: 90,
                  borderRadius: 60,
                }}
                resizeMode={FastImage.resizeMode.cover}
                source={{uri: userData?.userInfo?.image_url}}
              />
            ) : (
              <Avatar.Text
                size={90}
                label={userData?.userInfo?.name.slice(0, 2)}
                color="#ffffff"
                style={{
                  backgroundColor: colors.Blue,
                }}
              />
            )}
          </Box>
          <Box>
            <TextM
              fontWeight="bold"
              style={{fontSize: 22, color: colors.DarkBlue}}>
              {userData?.userInfo?.name}
            </TextM>

            {/* Show Login via Facebook */}
            {userData?.userInfo?.login_type === 'Facebook' ? (
              <HStack>
                <MaterialCommunityIcons
                  name={'facebook'}
                  style={{
                    color: '#3b5998',
                    fontSize: 22,
                  }}
                />
              </HStack>
            ) : null}

            {userData?.userInfo?.dob !== '' &&
              userData?.userInfo?.dob !== undefined && (
                <HStack space={1} mt={1}>
                  <MaterialCommunityIcons name="cake" style={styles.iconpf} />
                  <TextM style={styles.textpf}>
                    {moment(userData?.userInfo?.dob).format('D MMMM YYYY')}
                  </TextM>
                </HStack>
              )}

            {userData?.userInfo?.email !== '' &&
              userData?.userInfo?.email !== undefined && (
                <HStack space={1} mt={1}>
                  <MaterialCommunityIcons name="at" style={styles.iconpf} />
                  <TextM style={styles.textpf}>
                    {userData?.userInfo?.email}
                  </TextM>
                </HStack>
              )}

            {/* Show Login via Google */}
            {userData?.userInfo?.login_type === 'Google' ? (
              <HStack mt={1}>
                <MaterialCommunityIcons
                  name={'google'}
                  style={{
                    color: '#DB4437',
                    fontSize: 22,
                  }}
                />
              </HStack>
            ) : null}

            {/* Show Login via Apple */}
            {userData?.userInfo?.login_type === 'Apple' ? (
              <HStack mt={1}>
                <MaterialCommunityIcons
                  name={'apple'}
                  style={{
                    color: '#222222',
                    fontSize: 22,
                  }}
                />
              </HStack>
            ) : null}

            {userData?.userInfo?.phone !== '' &&
              userData?.userInfo?.phone !== undefined && (
                <HStack space={1} mt={1}>
                  <MaterialCommunityIcons name="phone" style={styles.iconpf} />
                  <TextM style={styles.textpf}>
                    +856 20 {userData?.userInfo?.phone}
                  </TextM>
                </HStack>
              )}

            {/* Show Login via Phone */}
            {userData?.userInfo?.login_type === undefined ? (
              <HStack mt={1}>
                <MaterialCommunityIcons
                  name={'sim'}
                  style={{
                    color: colors.DarkLightBlue,
                    fontSize: 22,
                  }}
                />
              </HStack>
            ) : null}
          </Box>
        </HStack>

        {/* Wallet */}
        <View
          style={{
            marginVertical: 10,
            marginHorizontal: 10,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              borderWidth: 2,
              borderColor: colors.LightBlue,
              borderStyle: 'dashed',
              borderRadius: 10,
              padding: 10,
            }}>
            <View
              style={{
                // width: '70%',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <TextM>ຈຳນວນເງິນໃນບັນຊີຂອງທ່ານ</TextM>
              {walletAmount !== null ? (
                <TextM style={{color: colors.Pink, fontSize: 24}}>
                  {numberWithCommas(walletAmount)} ກີບ
                </TextM>
              ) : (
                <ActivityIndicator
                  size="small"
                  style={{marginVertical: 10}}
                  color={colors.Pink}
                />
              )}
            </View>
            <View
              style={{
                width: '25%',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                style={{
                  borderColor: colors.Blue,
                  borderWidth: 1,
                  borderRadius: 5,
                  paddingHorizontal: 8,
                  paddingVertical: 2,
                }}
                onPress={() => navigation.navigate('TopupMoney')}>
                <TextM>ເຕີມເງິນ</TextM>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  marginTop: 5,
                  paddingHorizontal: 5,
                  paddingVertical: 5,
                }}
                onPress={() => {
                  setLoading(true);
                  getCurrentWallet();
                }}>
                <MaterialCommunityIcons name="reload" style={{fontSize: 20}} />
              </TouchableOpacity>
            </View>
          </View>
        </View>

        {/* List Menu */}
        <VStack pb={5}>
          {/* Head */}
          <Box style={{paddingTop: 8, paddingBottom: 8, borderRadius: 5}}>
            <TopicH
              title={'ປະຫວັດການໃຊ້ງານທັງໝົດ'}
              icon={'clipboard-text-clock'}
            />
          </Box>
          {/* Child */}
          <TouchableOpacity
            style={styles.listSpace}
            onPress={() => {
              navigation.navigate('CallHistory');
            }}>
            <HStack>
              <MaterialCommunityIcons
                name="phone-log"
                style={styles.iconColor}
              />
              <TextM>ປະຫວັດການໂທປຶກສາໝໍ</TextM>
            </HStack>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.listSpace}
            onPress={() => {
              navigation.navigate('AppointmentHistory');
            }}>
            <HStack>
              <FontAwesome5 name="h-square" style={styles.iconColor} />
              <TextM>ປະຫວັດການນັດພົບໝໍ</TextM>
            </HStack>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.listSpace}
            onPress={() => {
              navigation.navigate('OrderHistory');
            }}>
            <HStack>
              <MaterialCommunityIcons name="history" style={styles.iconColor} />
              <TextM>ປະຫວັດການສັ່ງຊື້ສິນຄ້າ</TextM>
            </HStack>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.listSpace}
            onPress={() => {
              navigation.navigate('TopupHistory');
            }}>
            <HStack>
              <MaterialCommunityIcons
                name="wallet-plus"
                style={styles.iconColor}
              />
              <TextM>ປະຫວັດການເຕີມເງິນ</TextM>
            </HStack>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.listSpace}
            onPress={() => {
              navigation.navigate('WalletPayment');
            }}>
            <HStack>
              <MaterialCommunityIcons
                name="cash-multiple"
                style={styles.iconColor}
              />
              <TextM>ປະຫວັດການໃຊ້ຈ່າຍ</TextM>
            </HStack>
          </TouchableOpacity>

          {/* <ListItem
            icon
            style={styles.listSpace}
            onPress={() => {
              navigation.navigate('ContactCenter');
            }}>
            <Box>
              <Icon
                type="MaterialCommunityIcons"
                name="face-agent"
                style={styles.iconColor}
              />
            </Box>
            <Box>
              <TextM>ຕິດຕໍ່ສູນບໍລິການລູກຄ້າ</TextM>
            </Box>
          </ListItem>
          <ListItem
            icon
            style={styles.listSpace}
            onPress={() => {
              navigation.navigate('FAQ');
            }}>
            <Box>
              <Icon
                type="MaterialCommunityIcons"
                name="frequently-asked-questions"
                style={styles.iconColor}
              />
            </Box>
            <Box>
              <TextM>FAQ</TextM>
            </Box>
          </ListItem>
          <ListItem
            icon
            noBorder
            style={styles.listSpace}
            onPress={() => {
              navigation.navigate('ReportUs');
            }}>
            <Box>
              <Icon
                type="MaterialCommunityIcons"
                name="bug"
                style={styles.iconColor}
              />
            </Box>
            <Box>
              <TextM>ລາຍງານບັນຫາໃຫ້ພວກເຮົາຊາບ</TextM>
            </Box>
          </ListItem> */}

          {/* Setting Menu */}
          <Box
            style={{
              paddingTop: 8,
              paddingBottom: 8,
              borderRadius: 5,
            }}>
            <TopicH title={'ການຕັ້ງຄ່າອື່ນໆ'} icon={'cog'} />
          </Box>
          <Box style={[styles.listSpace, {backgroundColor: '#ffffff'}]}>
            <HStack>
              <MaterialCommunityIcons
                name="cellphone-dock"
                style={styles.iconColor}
              />
              <TextM>ແອັບເວີຊັ້ນ {VersionNumber.appVersion}</TextM>
            </HStack>
          </Box>
          <TouchableOpacity
            style={styles.listSpace}
            onPress={() => {
              showAlertNow();
            }}>
            <HStack>
              <MaterialCommunityIcons
                name="logout"
                style={[styles.iconColor, {color: colors.Red}]}
              />
              <TextM style={{color: colors.LightBlack}}>ອອກຈາກລະບົບ</TextM>
            </HStack>
          </TouchableOpacity>
        </VStack>
      </ScrollView>
      {/* Modal Box */}
      <Portal>
        <Dialog
          visible={showAlert}
          onDismiss={hideAlert}
          style={{borderRadius: 10}}>
          <Dialog.Title>
            <TextM fontWeight="bold" style={{fontSize: 18}}>
              ທ່ານຕ້ອງການອອກຈາກລະບົບບໍ່ ?{' '}
              <MaterialCommunityIcons
                name="emoticon-sad-outline"
                style={{fontSize: 20, color: '#e74c3c'}}
              />
            </TextM>
          </Dialog.Title>
          <Dialog.Content>
            <TextM>ກະລຸນາກົດຢືນຢັນເພື່ອອອກຈາກລະບົບ</TextM>
          </Dialog.Content>
          <Dialog.Actions>
            <ButtonN onPress={() => hideAlert()}>
              <TextM>ກັບຄືນ</TextM>
            </ButtonN>
            <ButtonN
              onPress={() => {
                hideAlert();
                onLogOut();
              }}>
              <TextM style={{color: colors.Red}}>ອອກຈາກລະບົບ</TextM>
            </ButtonN>
          </Dialog.Actions>
        </Dialog>
      </Portal>
      {loading && <Loading />}
    </Box>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {},
  listSpace: {
    backgroundColor: '#f6f6f6',
    marginBottom: 5,
    paddingHorizontal: 10,
    paddingVertical: 10,
    borderRadius: 5,
  },
  iconColor: {
    color: colors.Blue,
    marginRight: 8,
    alignSelf: 'center',
    fontSize: 18,
  },
  iconpf: {
    color: colors.DarkLightBlue,
    fontSize: 16,
    alignSelf: 'center',
  },
  textpf: {
    color: colors.LightBlack,
    fontSize: 14,
  },
});
