import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Box, ScrollView, View, Text} from 'native-base';
import FastImage from 'react-native-fast-image';
// Component
import Loading from '../../shareComponents/Loading';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';

const ReportUs = ({navigation}) => {
  // useState
  const [loading, setLoading] = useState(false);

  // useEffect
  useEffect(() => {
    console.log('useEffect ReportUs');
  });

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <ScrollView style={{...defaultStyle.fixedLayout}}>
        <Text>ReportUs</Text>
      </ScrollView>
    </Box>
  );
};

export default ReportUs;

const styles = StyleSheet.create({});
