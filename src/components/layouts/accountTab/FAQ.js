import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Box, ScrollView, View, Text} from 'native-base';
import FastImage from 'react-native-fast-image';
// Component
import Loading from '../../shareComponents/Loading';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';

const FAQ = ({navigation}) => {
  // useState
  const [loading, setLoading] = useState(false);

  // useEffect
  useEffect(() => {
    console.log('useEffect FAQ');
  });

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <ScrollView style={{...defaultStyle.fixedLayout}}>
        <Text>FAQ</Text>
      </ScrollView>
    </Box>
  );
};

export default FAQ;

const styles = StyleSheet.create({});
