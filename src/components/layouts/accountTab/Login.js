import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  TouchableOpacity,
  SafeAreaView,
  Keyboard,
  Dimensions,
  Platform,
} from 'react-native';
import axios from 'axios';
import {Layout, Input} from '@ui-kitten/components';
import {
  Center,
  Button,
  useToast,
  ScrollView,
  Box,
  Alert,
  HStack,
  VStack,
} from 'native-base';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FastImage from 'react-native-fast-image';
import {CommonActions} from '@react-navigation/native';
import Config from 'react-native-config';
import {v4 as uuidv4} from 'uuid';
// Firebase
// import iid from '@react-native-firebase/iid';
import firebase from '@react-native-firebase/app';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
// FBSDK
import {LoginManager, AccessToken, Profile} from 'react-native-fbsdk-next';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import {appleAuth} from '@invertase/react-native-apple-authentication';
//Redux package
import {useSelector, useDispatch} from 'react-redux';
//Redux action
import {
  setSkip,
  setToken,
  addUserData,
} from '../../../stores/features/userData/userDataSlice';
// Color
import {colors, defaultStyle} from '../../shareStyle';
// Component
import Loading from '../../shareComponents/Loading';
import TextM from '../../shareComponents/TextM';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const increment = firebase.firestore.FieldValue.increment(1);

const Login = ({navigation}) => {
  const toast = useToast();
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();

  // useState
  const [loading, setLoading] = useState(false);
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const [secureTextEntry, setSecureTextEntry] = useState(true);
  const [loginBy, setLoginBy] = useState('otp');
  const [appleUser, setAppleUser] = useState({});

  // Function
  const toggleSecureEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const renderIcon = () => (
    <TouchableWithoutFeedback onPress={toggleSecureEntry}>
      <MaterialCommunityIcons
        name={secureTextEntry ? 'eye-off' : 'eye'}
        style={{color: '#bdc3c7', fontSize: 20}}
      />
    </TouchableWithoutFeedback>
  );

  const getPubKey = async () => {
    try {
      await axios({
        method: 'get',
        url: Config.WP_URL + '/wp/v2/public_key',
      })
        .then(async res => {
          try {
            await axios({
              method: 'post',
              url: Config.WP_URL + '/api/v1/token',
              data: {
                username: res.data[0].mc,
                password: res.data[0].mcc,
              },
            })
              .then(res => {
                let token = res?.data?.jwt_token;
                dispatch(setToken(token));
                setLoading(false);
              })
              .catch(error => {
                console.log(error);
              });
          } catch (error) {
            console.log(error);
          }
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const registerGuest = async () => {
    setLoading(true);
    try {
      firestore()
        .collection('anonymous')
        .doc('COUNTLOGIN')
        .update({
          count: increment,
        })
        .then(() => {
          // console.log('Document successfully written!');
          dispatch(setSkip());
          setLoading(false);
        })
        .catch(error => {
          console.error('Error writing document: ', error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  // Apple Login
  const onAppleButtonPress = async () => {
    // Start the sign-in request
    const appleAuthRequestResponse = await appleAuth.performRequest({
      requestedOperation: appleAuth.Operation.LOGIN,
      requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
    });

    setAppleUser({
      name:
        appleAuthRequestResponse?.fullName?.givenName +
        ' ' +
        appleAuthRequestResponse?.fullName?.familyName,
      email: appleAuthRequestResponse?.email,
      login_type: 'Apple',
    });

    // console.log(appleAuthRequestResponse);

    // Ensure Apple returned a user identityToken
    if (!appleAuthRequestResponse.identityToken) {
      throw new Error('Apple Sign-In failed - no identify token returned');
    }

    // Create a Firebase credential from the response
    const {identityToken, nonce} = appleAuthRequestResponse;
    const appleCredential = auth.AppleAuthProvider.credential(
      identityToken,
      nonce,
    );

    // Sign the user in with the credential
    return auth().signInWithCredential(appleCredential);
  };

  const onLoginApple = async () => {
    await onAppleButtonPress().then(async res => {
      setLoading(true);
      // console.log(res);
      try {
        await firestore()
          .collection('appleUsers')
          .where('user_id', '==', res?.user?.uid)
          .get()
          .then(querySnapshot => {
            appleUser['user_id'] = res.user.uid;
            if (querySnapshot.empty) {
              onCreateSocialUser(appleUser, 'AP');
            } else {
              onSocialLogin(appleUser, 'AP');
            }
          })
          .catch(error => {
            console.error('Error writing document: ', error);
            setLoading(false);
          });
      } catch (error) {
        console.log(error);
        setLoading(false);
      }
    });
  };

  // Google Login
  const onGoogleButtonPress = async () => {
    // Check if your device supports Google Play
    await GoogleSignin.hasPlayServices({showPlayServicesUpdateDialog: true});
    // Get the users ID token
    const {idToken} = await GoogleSignin.signIn();

    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);

    // Sign-in the user with the credential
    return auth().signInWithCredential(googleCredential);
  };

  const onLoginGoogle = async () => {
    // alert('run onLoginGoogle');
    await onGoogleButtonPress().then(async () => {
      setLoading(true);
      const currentUser = await GoogleSignin.getCurrentUser();
      let currentProfile = currentUser?.user;
      try {
        await firestore()
          .collection('googleUsers')
          .where('user_id', '==', currentProfile?.id)
          .get()
          .then(querySnapshot => {
            if (querySnapshot.empty) {
              onCreateSocialUser(currentProfile, 'GG');
            } else {
              onSocialLogin(currentProfile, 'GG');
            }
          })
          .catch(error => {
            console.error('Error writing document: ', error);
            setLoading(false);
          });
      } catch (error) {
        console.log(error);
        setLoading(false);
        // alert('google firebase not work');
      }
    });
  };

  // Firebase Facebook Login
  const onFacebookButtonPress = async () => {
    // Attempt login with permissions
    const result = await LoginManager.logInWithPermissions([
      'public_profile',
      'email',
    ]);

    if (result.isCancelled) {
      throw 'User cancelled the login process';
    }

    // Once signed in, get the users AccesToken
    const data = await AccessToken.getCurrentAccessToken();

    if (!data) {
      throw 'Something went wrong obtaining access token';
    }

    // Create a Firebase credential with the AccessToken
    const facebookCredential = auth.FacebookAuthProvider.credential(
      data.accessToken,
    );

    // Sign-in the user with the credential
    return auth().signInWithCredential(facebookCredential);
  };

  const onLoginFacebook = async () => {
    await onFacebookButtonPress().then(() => {
      setLoading(true);
      Profile.getCurrentProfile().then(function (currentProfile) {
        try {
          firestore()
            .collection('facebookUsers')
            .where('user_id', '==', currentProfile?.userID)
            .get()
            .then(querySnapshot => {
              if (querySnapshot.empty) {
                onCreateSocialUser(currentProfile, 'FB');
              } else {
                onSocialLogin(currentProfile, 'FB');
              }
            })
            .catch(error => {
              console.error('Error writing document: ', error);
              setLoading(false);
            });
        } catch (error) {
          console.log(error);
          setLoading(false);
        }
      });
    });
  };

  const onCreateSocialUser = async (currentProfile, type) => {
    let ramdonEmailID = uuidv4();
    try {
      await axios({
        method: 'post',
        url: Config.WP_URL + '/wp/v2/users/register',
        data: {
          // name: currentProfile?.name,
          roles: 'customer',
          username:
            type === 'AP'
              ? currentProfile?.user_id
              : type === 'GG'
              ? currentProfile?.id
              : currentProfile?.userID,
          password:
            type === 'AP'
              ? currentProfile?.user_id + Config.LKEY
              : type === 'GG'
              ? currentProfile?.id + Config.LKEY
              : currentProfile?.userID + Config.LKEY,
          email:
            type === 'AP'
              ? currentProfile?.email
              : type === 'GG'
              ? currentProfile?.email
              : ramdonEmailID + '@noemail.none',
        },
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(async res => {
          let userObjectGG = {
            user_id: currentProfile?.id,
            name: currentProfile?.name,
            image_url: currentProfile?.photo,
            email: currentProfile?.email,
            login_type: 'Google',
          };
          let userObjectFB = {
            user_id: currentProfile?.userID,
            name: currentProfile?.name,
            image_url: currentProfile?.imageURL,
            login_type: 'Facebook',
          };
          let userDB =
            type === 'AP'
              ? 'appleUsers'
              : type === 'GG'
              ? 'googleUsers'
              : 'facebookUsers';
          try {
            await firestore()
              .collection(userDB)
              .add(
                type === 'AP'
                  ? appleUser
                  : type === 'GG'
                  ? userObjectGG
                  : userObjectFB,
              )
              .then(() => {
                // console.error('writing document successful');
              })
              .catch(error => {
                console.error('Error writing document: ', error);
              });
          } catch (error) {
            console.log(error);
          }
          await onSocialLogin(currentProfile, type);
        })
        .catch(error => {
          console.log(error);
          showToast('error', 'ບັນຊີຂອງທ່ານຖືກນຳໃຊ້ແລ້ວ!', 'ກະລຸນາລອງບັນຊີໃໝ່');
          setLoading(false);
          if (type === 'GG') {
            GoogleSignin.revokeAccess();
          }
        });
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  const onSocialLogin = async (currentProfile, type) => {
    setLoading(true);
    try {
      await axios({
        method: 'post',
        url: Config.WP_URL + '/api/v1/token',
        data: {
          username:
            type === 'AP'
              ? currentProfile?.user_id
              : type === 'GG'
              ? currentProfile?.id
              : currentProfile?.userID,
          password:
            type === 'AP'
              ? currentProfile?.user_id + Config.LKEY
              : type === 'GG'
              ? currentProfile?.id + Config.LKEY
              : currentProfile?.userID + Config.LKEY,
        },
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(async res => {
          let token = res?.data?.jwt_token;
          let uid =
            type === 'AP'
              ? currentProfile?.user_id
              : type === 'GG'
              ? currentProfile?.id
              : currentProfile?.userID;
          dispatch(setToken(token));
          await saveSocialUserData(uid, token, type);
        })
        .catch(error => {
          console.log(error.response);
          showToast('warning', 'ເບີໂທ ຫຼື ລະຫັດຜ່ານບໍ່ຖືກຕ້ອງ!');
          setLoading(false);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const saveSocialUserData = async (user_id, token, type) => {
    let userinfo;
    let userDB =
      type === 'AP'
        ? 'appleUsers'
        : type === 'GG'
        ? 'googleUsers'
        : 'facebookUsers';
    try {
      await firestore()
        .collection(userDB)
        .where('user_id', '==', user_id)
        .get()
        .then(querySnapshot => {
          let allData = [];
          querySnapshot.forEach(documentSnapshot => {
            let obj = documentSnapshot.data();
            obj.firebaseID = documentSnapshot.id;
            allData.push(obj);
          });
          userinfo = allData?.[0];
        })
        .catch(error => {
          console.error('Error writing document: ', error);
        });
    } catch (error) {
      console.log(error);
    }

    try {
      await axios({
        method: 'get',
        url: Config.WP_URL + '/wp/v2/users/me',
        headers: {
          Authorization: 'Bearer ' + token,
          'Content-Type': 'application/json',
        },
      })
        .then(res => {
          let userid = {
            user_id: res.data.id,
          };
          let combineuserinfo = Object.assign(userinfo, userid);
          dispatch(addUserData(combineuserinfo));
          dispatch(setSkip());
          showToast('success', 'ການລົງທະບຽນສຳເລັດແລ້ວ!');
          if (type === 'GG') {
            GoogleSignin.revokeAccess();
          }
          setLoading(false);
          navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes: [
                {
                  name: 'HomeStack',
                },
              ],
            }),
          );
        })

        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  // Login via OTP
  const onOTPPress = () => {
    if (phone.length == 8) {
      navigation.navigate('OTPVerification', {phoneValue: phone});
    } else {
      showToast('warning', 'ເບີບໍ່ຄົບ 8 ຕົວເລກ!');
    }
  };

  const onCheckLoginPassword = () => {
    if (phone.length !== 8) {
      showToast('warning', 'ເບີບໍ່ຄົບ 8 ຕົວເລກ!');
    } else if (password == '') {
      showToast('warning', 'ກະລຸນາໃສ່ລະຫັດຜ່ານ!');
    } else {
      onLoginAndgetUserToken();
    }
  };

  const onLoginAndgetUserToken = async () => {
    Keyboard.dismiss();
    setLoading(true);
    try {
      await axios({
        method: 'post',
        url: Config.WP_URL + '/api/v1/token',
        data: {
          username: phone,
          password: password,
        },
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(async res => {
          let token = res?.data?.jwt_token;
          console.log('token', token);
          dispatch(setToken(token));
          await saveUserData(token);
        })
        .catch(error => {
          console.log(error.response);
          showToast('warning', 'ເບີໂທ ຫຼື ລະຫັດຜ່ານບໍ່ຖືກຕ້ອງ!');
          setLoading(false);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const saveUserData = async token => {
    try {
      await axios({
        method: 'get',
        url: Config.WP_URL + '/wp/v2/customers/?phone=' + phone,
        headers: {
          Authorization: 'Bearer ' + token,
          'Content-Type': 'application/json',
        },
      })
        .then(async res => {
          let userinfo = res.data[0];
          try {
            await axios({
              method: 'get',
              url: Config.WP_URL + '/wp/v2/users/me',
              headers: {
                Authorization: 'Bearer ' + token,
                'Content-Type': 'application/json',
              },
            })
              .then(res => {
                let userid = {
                  user_id: res.data.id,
                };
                let combineuserinfo = Object.assign(userinfo, userid);
                dispatch(addUserData(combineuserinfo));
                dispatch(setSkip());
                showToast('success', 'ການລົງທະບຽນສຳເລັດແລ້ວ!');
                setLoading(false);
                navigation.dispatch(
                  CommonActions.reset({
                    index: 1,
                    routes: [
                      {
                        name: 'HomeStack',
                      },
                    ],
                  }),
                );
              })

              .catch(error => {
                console.log(error);
              });
          } catch (error) {
            console.log(error);
          }
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const showToast = (type, title, desc) => {
    toast.show({
      render: () => {
        return (
          <Alert
            maxWidth="95%"
            alignSelf="center"
            flexDirection="row"
            status={type}
            variant={'left-accent'}>
            <VStack space={1} flexShrink={1} w="100%">
              <HStack
                flexShrink={1}
                alignItems="center"
                justifyContent="space-between">
                <HStack space={2} flexShrink={1} alignItems="center">
                  <Alert.Icon />
                  <TextM>{title}</TextM>
                </HStack>
              </HStack>
              {desc !== undefined && (
                <TextM style={{fontSize: 14, paddingLeft: 25}}>{desc}</TextM>
              )}
            </VStack>
          </Alert>
        );
      },
    });
  };

  // useEffect
  useEffect(() => {
    getPubKey();
    return () => {};
  }, []);

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <SafeAreaView>
        <Box style={{display: 'flex'}} px={4}>
          <TouchableOpacity
            onLongPress={() => {
              navigation.navigate('PartnerLogin');
            }}
            style={{
              width: 40,
              height: 40,
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'flex-end',
            }}>
            <FontAwesome5
              name="hospital-user"
              style={{color: '#f1f1f1', fontSize: 24}}
            />
          </TouchableOpacity>
        </Box>
      </SafeAreaView>
      <ScrollView
        contentContainerStyle={{flexGrow: 1, justifyContent: 'center'}}>
        <Center px={6}>
          <FastImage
            style={{width: 140, height: 140, marginVertical: 10}}
            source={require('../../../assets/img/logo-400x400.png')}
          />
          <TextM>ກະລຸນາໃສ່ເບີໂທ 8 ຕົວເລກຂອງທ່ານ</TextM>
          <Layout
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 20,
              marginBottom: 5,
            }}>
            <MaterialCommunityIcons
              name="phone"
              style={{color: colors.Green, fontSize: 20, marginRight: 5}}
            />
            <TextM style={{marginRight: 10, width: 70}}>+856 20</TextM>
            <Input
              style={{
                width: '100%',
                flex: 1,
                backgroundColor: colors.White,
              }}
              placeholder="ເບີໂທ 8 ຕົວເລກ"
              value={phone}
              onChangeText={nextValue =>
                setPhone(nextValue.replace(/[^0-9]/g, ''))
              }
              maxLength={8}
              keyboardType={'numeric'}
              textStyle={{fontSize: 16, fontFamily: 'NotoSansLao-Regular'}}
            />
          </Layout>

          {/* Insert Password */}
          {loginBy === 'password' && (
            <View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginBottom: 5,
                }}>
                <MaterialCommunityIcons
                  name="lock"
                  style={{color: colors.Green, fontSize: 20, marginRight: 5}}
                />
                <TextM style={{marginRight: 10, width: 70}}>ລະຫັດຜ່ານ</TextM>
                <Input
                  style={{
                    width: '100%',
                    flex: 1,
                    backgroundColor: colors.White,
                  }}
                  placeholder="**********"
                  value={password}
                  onChangeText={nextValue => setPassword(nextValue)}
                  secureTextEntry={secureTextEntry}
                  accessoryRight={renderIcon}
                  textStyle={{fontSize: 16}}
                />
              </View>
              {/* <TouchableOpacity style={{alignItems: 'flex-end'}}>
                <TextM style={{color: '#cccccc'}}>ລືມລະຫັດຜ່ານ ?</TextM>
              </TouchableOpacity> */}
            </View>
          )}

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 10,
            }}>
            <TextM style={{marginRight: 10, fontSize: 14, color: colors.Gray}}>
              ເຂົ້າສູ່ລະບົບດ້ວຍ
            </TextM>
            <TouchableOpacity
              style={
                loginBy === 'otp'
                  ? styles.loginViaOTPActive
                  : styles.loginViaOTP
              }
              onPress={() => {
                setLoginBy('otp');
              }}>
              <TextM style={{fontSize: 14}}>ລະຫັດ OTP</TextM>
              {loginBy === 'otp' && (
                <MaterialCommunityIcons
                  name="check"
                  style={{color: colors.Green, fontSize: 16, marginLeft: 5}}
                />
              )}
            </TouchableOpacity>
            <TouchableOpacity
              style={
                loginBy === 'password'
                  ? styles.loginViaPWActive
                  : styles.loginViaPW
              }
              onPress={() => {
                setLoginBy('password');
              }}>
              <TextM style={{fontSize: 14}}>ລະຫັດຜ່ານ</TextM>
              {loginBy === 'password' && (
                <MaterialCommunityIcons
                  name="check"
                  style={{color: colors.Green, fontSize: 16, marginLeft: 5}}
                />
              )}
            </TouchableOpacity>
          </View>

          <Button
            style={{
              marginTop: 20,
              borderRadius: 5,
              width: '100%',
            }}
            onPress={() =>
              loginBy === 'otp' ? onOTPPress() : onCheckLoginPassword()
            }>
            <TextM style={{color: colors.White}}>
              ເຂົ້າສູ່ລະບົບດ້ວຍ{loginBy === 'otp' ? 'ລະຫັດ OTP' : 'ລະຫັດຜ່ານ'}
            </TextM>
          </Button>

          {/* Login with social */}
          <TextM style={{marginVertical: 10, color: colors.DarkBlue}}>
            ລົງທະບຽນດ້ວຍລະບົບອື່ນ
          </TextM>

          <HStack space={8}>
            <TouchableOpacity
              style={[
                styles.sociallayout,
                {
                  backgroundColor: '#3b5998',
                },
              ]}
              onPress={() => onLoginFacebook()}>
              <FontAwesome5
                name="facebook"
                style={{fontSize: 26, color: colors.White}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                onLoginGoogle();
              }}
              style={[
                styles.sociallayout,
                {
                  backgroundColor: '#DB4437',
                },
              ]}>
              <FontAwesome5
                name="google"
                style={{fontSize: 26, color: colors.White}}
              />
            </TouchableOpacity>

            {Platform.OS === 'ios' && (
              <TouchableOpacity
                onPress={() => {
                  onLoginApple();
                }}
                style={[
                  styles.sociallayout,
                  {
                    backgroundColor: '#222222',
                  },
                ]}>
                <FontAwesome5
                  name="apple"
                  style={{fontSize: 26, color: colors.White}}
                />
              </TouchableOpacity>
            )}
          </HStack>

          {/* Sign Up */}
          <Layout
            style={{
              marginVertical: 10,
              flexDirection: 'row',
            }}>
            <TextM style={{color: colors.Gray}}>ຍັງບໍ່ມີບັນຊີ ?</TextM>
            <TouchableOpacity
              style={{marginLeft: 15, flexDirection: 'row'}}
              onPress={() => navigation.navigate('Registration')}>
              <TextM style={{textDecorationLine: 'underline'}}>
                ຫຼືລົງທະບຽນເລີຍໄດ້ທີ່ນີ້
              </TextM>
              <MaterialCommunityIcons
                name="account-supervisor-circle"
                style={{color: colors.Blue, fontSize: 20, marginLeft: 5}}
              />
            </TouchableOpacity>
          </Layout>
        </Center>
      </ScrollView>

      {loading && <Loading />}

      {/* Skip */}
      {!userData?.isSkip && (
        <SafeAreaView>
          <Layout style={{paddingVertical: 15, paddingHorizontal: 20}}>
            <SafeAreaView>
              <TouchableOpacity
                onPress={() => {
                  registerGuest();
                }}>
                <TextM style={{color: colors.Gray}}>ລົງທະບຽນພາຍຫລັງ</TextM>
              </TouchableOpacity>
              {/* <TouchableOpacity
                onPress={() => {
                  auth()
                    .signOut()
                    .then(async () => {
                      console.log('User signed out!');
                    });
                }}>
                <TextM style={{color: colors.Gray}}>Log Out</TextM>
              </TouchableOpacity> */}
            </SafeAreaView>
          </Layout>
        </SafeAreaView>
      )}
    </Box>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  layoutCenter: {
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // paddingHorizontal: 30,
  },
  sociallayout: {
    width: 44,
    height: 44,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  loginViaOTP: {
    flexDirection: 'row',
    paddingVertical: 4,
    paddingHorizontal: 8,
    backgroundColor: '#bdc3c7',
    borderColor: '#bdc3c7',
    borderWidth: 1,
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
  loginViaOTPActive: {
    flexDirection: 'row',
    paddingVertical: 4,
    paddingHorizontal: 8,
    backgroundColor: '#ffffff',
    borderColor: '#bdc3c7',
    borderWidth: 1,
    alignItems: 'center',
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
  loginViaPW: {
    flexDirection: 'row',
    paddingVertical: 4,
    paddingHorizontal: 8,
    backgroundColor: '#bdc3c7',
    borderColor: '#bdc3c7',
    borderWidth: 1,
    borderBottomRightRadius: 5,
    borderTopRightRadius: 5,
  },
  loginViaPWActive: {
    flexDirection: 'row',
    paddingVertical: 4,
    paddingHorizontal: 8,
    backgroundColor: '#ffffff',
    borderColor: '#bdc3c7',
    borderWidth: 1,
    alignItems: 'center',
    borderBottomRightRadius: 5,
    borderTopRightRadius: 5,
  },
});
