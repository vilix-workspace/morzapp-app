import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import {Box, View, Button} from 'native-base';
import {CommonActions} from '@react-navigation/native';
import FastImage from 'react-native-fast-image';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import LottieView from 'lottie-react-native';
// Redux
import {useDispatch} from 'react-redux';
//Redux action
import {setSkip} from '../../../stores/features/userData/userDataSlice';
// Component
import Loading from '../../shareComponents/Loading';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';
import TextM from '../../shareComponents/TextM';

const CompletedRegis = ({navigation}) => {
  const dispatch = useDispatch();
  // useState
  const [loading, setLoading] = useState(false);

  // useEffect
  useEffect(() => {
    // console.log('useEffect CompletedRegis');
  });

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <View style={styles.layoutCenter}>
        <View style={{marginBottom: 20}}>
          <FastImage
            style={{height: 60, width: 220, marginBottom: 10}}
            source={require('../../../assets/img/morzapp-logo-hor-660x180.png')}
            resizeMode={FastImage.resizeMode.contain}
          />
        </View>
        <LottieView
          source={require('../../../assets/animation/success.json')}
          style={{width: 200}}
          autoPlay
          loop
        />
        <TextM style={{fontSize: 22}}>ການລົງທະບຽນສຳເລັດແລ້ວ</TextM>
      </View>
      <Button
        marginX={5}
        marginY={4}
        style={{marginTop: 50}}
        onPress={() => {
          dispatch(setSkip());
          navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes: [
                {
                  name: 'HomeStack',
                },
              ],
            }),
          );
        }}>
        <TextM style={{color: colors.White}}>ເຂົ້າສູ່ແອັບເລີຍ</TextM>
      </Button>
    </Box>
  );
};

export default CompletedRegis;

const styles = StyleSheet.create({
  layoutCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 30,
  },
});
