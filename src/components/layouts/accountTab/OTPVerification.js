import React, {useEffect, useState} from 'react';
import {StyleSheet, TextInput, Keyboard, TouchableOpacity} from 'react-native';
import {
  Box,
  ScrollView,
  View,
  Text,
  useToast,
  Alert,
  VStack,
  HStack,
  Button,
} from 'native-base';
import FastImage from 'react-native-fast-image';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import auth from '@react-native-firebase/auth';
import axios from 'axios';
import CryptoJS from 'crypto-js';
import {CommonActions} from '@react-navigation/native';
import Config from 'react-native-config';
// Component
import Loading from '../../shareComponents/Loading';
import {colors, defaultStyle} from '../../shareStyle';
// Redux
import {useSelector, useDispatch} from 'react-redux';
//Redux action
import {
  addUserData,
  setToken,
  setSkip,
} from '../../../stores/features/userData/userDataSlice';
import TextM from '../../shareComponents/TextM';

const OTPVerification = ({navigation, route}) => {
  const toast = useToast();
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();
  // Param Data
  const {phoneValue} = route.params;
  // useState
  const [loading, setLoading] = useState(false);
  const [confirm, setConfirm] = useState(null);
  const [code, setCode] = useState('');

  // Handle the button press
  const signInWithPhoneNumber = async phoneNumber => {
    const confirmation = await auth().signInWithPhoneNumber(phoneNumber);
    setConfirm(confirmation);
  };

  const confirmCode = async () => {
    setLoading(true);
    try {
      await confirm.confirm(code);
      await saveUserData();
    } catch (error) {
      showToast('error', 'ລະຫັດບໍ່ຖຶກຕ້ອງ! ລອງໃໝ່ອີກຄັ້ງ');
      setLoading(false);
    }
  };

  const saveUserData = async () => {
    try {
      axios({
        method: 'get',
        url: Config.WP_URL + '/wp/v2/customers/?phone=' + phoneValue,
        headers: {
          Authorization: 'Bearer ' + userData.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(async res => {
          if (res?.data?.length == 0) {
            setLoading(false);
            showToast('error', 'ເບີຂອງທ່ານຍັງບໍ່ທັນລົງທະບຽນ!');
          } else {
            await onLoginAndgetUserToken(res.data[0]);
          }
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const onLoginAndgetUserToken = async userInfo => {
    // PassDecrypt
    var bytes = CryptoJS.AES.decrypt(userInfo.password, Config.PS_AES);
    var passDecrypt = bytes.toString(CryptoJS.enc.Utf8);
    try {
      await axios({
        method: 'post',
        url: Config.WP_URL + '/api/v1/token',
        data: {
          username: phoneValue,
          password: passDecrypt,
        },
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(async res => {
          let token = res?.data?.jwt_token;
          dispatch(setToken(token));
          try {
            await axios({
              method: 'get',
              url: Config.WP_URL + '/wp/v2/users/me',
              headers: {
                Authorization: 'Bearer ' + token,
                'Content-Type': 'application/json',
              },
            })
              .then(async res => {
                let userid = {
                  user_id: res.data.id,
                };
                let combineuserinfo = Object.assign(userInfo, userid);
                dispatch(addUserData(combineuserinfo));
                dispatch(setSkip());
                navigation.dispatch(
                  CommonActions.reset({
                    index: 1,
                    routes: [
                      {
                        name: 'HomeStack',
                      },
                    ],
                  }),
                );
                setLoading(false);
                showToast('success', 'ການລົງທະບຽນສຳເລັດແລ້ວ!');
              })
              .catch(error => {
                console.log(error);
              });
          } catch (error) {
            console.log(error);
          }
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const showToast = (type, title, desc) => {
    toast.show({
      render: () => {
        return (
          <Alert
            maxWidth="95%"
            alignSelf="center"
            flexDirection="row"
            status={type}
            variant={'left-accent'}>
            <VStack space={1} flexShrink={1} w="100%">
              <HStack
                flexShrink={1}
                alignItems="center"
                justifyContent="space-between">
                <HStack space={2} flexShrink={1} alignItems="center">
                  <Alert.Icon />
                  <TextM>{title}</TextM>
                </HStack>
              </HStack>
              {desc !== undefined && (
                <TextM style={{fontSize: 14, paddingLeft: 25}}>{desc}</TextM>
              )}
            </VStack>
          </Alert>
        );
      },
    });
  };

  // useEffect
  useEffect(() => {
    if (phoneValue.length == 8) {
      signInWithPhoneNumber('+85620' + phoneValue);
    }
  }, []);

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <ScrollView style={{...defaultStyle.fixedLayout}}>
        <View style={styles.layoutCenter}>
          <MaterialCommunityIcons
            name={'onepassword'}
            style={{color: colors.Blue, fontSize: 80, marginBottom: 20}}
          />

          <TextM
            fontWeight="bold"
            style={{
              fontSize: 26,
            }}>
            ໃສ່ລະຫັດ
          </TextM>
          <Text
            style={{
              textAlign: 'center',
              marginTop: 10,
              fontFamily: 'NotoSansLao-Regular',
            }}>
            ພວກເຮົາໄດ້ສົ່ງລະຫັດ 6 ຕົວເລກໄປຫາທ່ານແລ້ວທາງ SMS ຜ່ານເບີ{' '}
            <Text style={{color: colors.Green}}>+856 20 {phoneValue}</Text>{' '}
            ກະລຸນາຕື່ມລະຫັດໃສ່ດ້ານລຸ່ມ
          </Text>
          <View style={{marginVertical: 40}}>
            <TextInput
              value={code}
              onChangeText={text => setCode(text)}
              placeholder={'******'}
              style={{
                color: colors.DarkBlue,
                alignSelf: 'center',
                fontSize: 34,
                textAlign: 'center',
              }}
              keyboardType={'numeric'}
              maxLength={6}
            />
          </View>
        </View>
      </ScrollView>
      <Button
        isLoading={confirm !== null ? false : true}
        marginX={5}
        marginY={4}
        disabled={confirm !== null ? false : true}
        style={{
          backgroundColor: confirm !== null ? colors.DarkBlue : colors.Gray,
        }}
        onPress={() => {
          Keyboard.dismiss();
          confirmCode();
        }}>
        <TextM fontWeight="bold" style={{color: colors.White}}>
          ຢືນຢັນລະຫັດ
        </TextM>
      </Button>

      {loading && <Loading />}
    </Box>
  );
};

export default OTPVerification;

const styles = StyleSheet.create({
  layoutCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 30,
    marginTop: 80,
  },
});
