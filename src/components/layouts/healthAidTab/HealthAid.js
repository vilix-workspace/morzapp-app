import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Box, ScrollView, Text, VStack} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

// Component
import Header from '../../shareComponents/Header';

// Custom Style
import {defaultStyle} from '../../shareStyle';

const HealthAid = ({navigation}) => {
  // useState
  const [stateName, setStateName] = useState('');

  // useEffect
  useEffect(() => {
    console.log('useEffect HealthAid');
  });

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <Header navigation={navigation} />
      <ScrollView style={{...defaultStyle.fixedLayout}}>
        {/* Health Calculation */}
        <VStack>
          <Box
            itemDivider
            style={{paddingTop: 5, paddingBottom: 5, backgroundColor: null}}>
            <MaterialCommunityIcons
              name="newspaper"
              style={{color: '#00BBF9', marginRight: 10}}
            />
            <Text>Health Calculation</Text>
          </Box>
          <TouchableOpacity>
            <Box style={{paddingTop: 10, paddingBottom: 10}}>
              <Box>
                <Text>Aaron Bennet</Text>
              </Box>
              <Box>
                <MaterialCommunityIcons name="chevron-right" />
              </Box>
            </Box>
          </TouchableOpacity>
          <TouchableOpacity>
            <Box style={{paddingTop: 10, paddingBottom: 10}}>
              <Box>
                <Text>Bennet</Text>
              </Box>
              <Box>
                <MaterialCommunityIcons name="chevron-right" />
              </Box>
            </Box>
          </TouchableOpacity>
        </VStack>
        {/* For women */}
        <VStack style={{marginTop: 20}}>
          <Box
            itemDivider
            style={{paddingTop: 5, paddingBottom: 5, backgroundColor: null}}>
            <MaterialCommunityIcons
              name="newspaper"
              style={{color: '#00BBF9', marginRight: 10}}
            />
            <Text>Health Calculation for Women</Text>
          </Box>
          <TouchableOpacity>
            <Box style={{paddingTop: 10, paddingBottom: 10}}>
              <Box>
                <Text>Aaron Bennet</Text>
              </Box>
              <Box>
                <MaterialCommunityIcons name="chevron-right" />
              </Box>
            </Box>
          </TouchableOpacity>
          <TouchableOpacity>
            <Box style={{paddingTop: 10, paddingBottom: 10}}>
              <Box>
                <Text>Bennet</Text>
              </Box>
              <Box>
                <MaterialCommunityIcons name="chevron-right" />
              </Box>
            </Box>
          </TouchableOpacity>
        </VStack>
      </ScrollView>
    </Box>
  );
};

export default HealthAid;

const styles = StyleSheet.create({
  container: {},
});
