import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  RefreshControl,
  SafeAreaView,
  Dimensions,
  TouchableOpacity,
  Platform,
} from 'react-native';
import {Box, ScrollView, View, HStack, VStack} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// Package
import FastImage from 'react-native-fast-image';
import Swiper from 'react-native-swiper';
import axios from 'axios';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';
import LottieView from 'lottie-react-native';
import Config from 'react-native-config';

//Redux package
import {useSelector, useDispatch} from 'react-redux';

// Component
import Header from '../../shareComponents/Header';
import TextM from '../../shareComponents/TextM';

// Custom Style
import {colors, defaultStyle} from '../../shareStyle';

const width = Dimensions.get('window').width;

const News = ({navigation}) => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();

  // useState
  const [news, setNews] = useState([]);
  const [featureNews, setFeatureNews] = useState([]);
  const [refreshing, setRefreshing] = useState(false);

  const imageWidth = 950; // Replace with the actual width of the image
  const imageHeight = 500; // Replace with the actual height of the image
  const aspectRatio = imageWidth / imageHeight;

  // Functions
  const onRefresh = () => {
    setRefreshing(true);
    try {
      getAllNews();
      setRefreshing(false);
    } catch (error) {
      console.log(error);
    }
  };

  const getAllNews = () => {
    try {
      axios({
        method: 'get',
        url: Config.WP_URL + '/wp/v2/news_tips',
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(res => {
          let ft = res.data.filter(item => item.feature_post == 'feature');
          setFeatureNews(ft);
          let nm = res.data.filter(item => item.feature_post == 'normal');
          setNews(nm);
          console.log(res.data);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  // useEffect
  useEffect(() => {
    getAllNews();
  }, []);

  return (
    <Box flex={1} backgroundColor={colors.White} justifyContent={'center'}>
      <SafeAreaView>
        <Header navigation={navigation} />
      </SafeAreaView>
      <ScrollView
        style={{...defaultStyle.fixedLayout}}
        contentContainerStyle={{
          flexGrow: news.length > 0 ? 0 : 1,
          justifyContent: 'center',
        }}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        {/* Top news */}
        {featureNews?.length > 0 ? (
          <View
            style={{
              marginBottom: 10,
              width: '100%',
              aspectRatio,
            }}>
            <Swiper
              // style={styles.wrapper}
              autoplay={true}
              loop={true}
              showsPagination={false}>
              {featureNews.map(data => {
                return (
                  <View style={styles.slide} key={data?.id}>
                    <TouchableOpacity
                      onPress={() =>
                        navigation.navigate('SingleNews', {news: data})
                      }>
                      <FastImage
                        style={{
                          width: '100%',
                          aspectRatio,
                          borderRadius: 10,
                        }}
                        resizeMode={FastImage.resizeMode.cover}
                        source={{uri: data?.image}}
                      />
                      <LinearGradient
                        colors={[
                          'rgba(0, 0, 0, 0.0)',
                          'rgba(0, 0, 0, 0.6)',
                          'rgba(0, 0, 0, 0.9)',
                        ]}
                        style={{
                          flex: 1,
                          paddingHorizontal: 12,
                          paddingTop: 25,
                          paddingBottom: 10,
                          borderRadius: 10,
                          position: 'absolute',
                          bottom: 0,
                          Box: 0,
                          width: '100%',
                          justifyContent: 'center',
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            marginBottom: 5,
                          }}>
                          <HStack alignItems={'center'} space={1}>
                            <MaterialCommunityIcons
                              name={'calendar'}
                              style={{color: colors.Blue, fontSize: 16}}
                            />
                            <TextM style={{color: colors.Blue, fontSize: 14}}>
                              {moment(data?.date).format('DD/MM/YYYY')}
                            </TextM>
                          </HStack>
                          <View
                            style={{
                              backgroundColor: colors.Orange,
                              paddingHorizontal: 10,
                              borderRadius: 8,
                            }}>
                            <TextM style={{color: colors.White, fontSize: 14}}>
                              {data?.category?.name}
                            </TextM>
                          </View>
                        </View>
                        <TextM
                          fontWeight="bold"
                          numberOfLines={2}
                          style={{
                            color: '#ffffff',
                            lineHeight: 22,
                          }}>
                          {data?.title}
                        </TextM>
                      </LinearGradient>
                    </TouchableOpacity>
                  </View>
                );
              })}
            </Swiper>
          </View>
        ) : (
          <Box style={{alignItems: 'center', marginTop: 20}}>
            <Box style={{height: 100, width: 200, marginTop: -80}}>
              <LottieView
                source={require('../../../assets/animation/loading-dot.json')}
                autoPlay
                loop
                resizeMode="cover"
              />
            </Box>
            <TextM>ກຳລັງໂຫລດ...</TextM>
          </Box>
        )}
        {news?.length > 0 && (
          <Box>
            <HStack
              itemDivider
              style={{
                paddingTop: 5,
                paddingBottom: 5,
                borderRadius: 10,
                marginBottom: 5,
              }}>
              <MaterialCommunityIcons
                name="newspaper"
                style={{
                  color: '#00BBF9',
                  marginRight: 8,
                  alignSelf: 'center',
                  fontSize: 20,
                }}
              />
              <TextM fontWeight="bold">ຂ່າວກ່ຽວກັບສຸຂະພາບ</TextM>
            </HStack>

            {news?.map(data => {
              return (
                <TouchableOpacity
                  style={{marginBottom: 10}}
                  key={data?.id}
                  onPress={() =>
                    navigation.navigate('SingleNews', {news: data})
                  }>
                  <HStack space={2}>
                    <Box>
                      <FastImage
                        square
                        style={{
                          width: 100,
                          height: 100,
                          borderRadius: 5,
                        }}
                        resizeMode={FastImage.resizeMode.cover}
                        source={{uri: data?.image}}
                      />
                    </Box>
                    <VStack width={'68%'}>
                      <View
                        style={{
                          backgroundColor: colors.DarkBlue,
                          paddingHorizontal: 5,
                          borderRadius: 5,
                          alignSelf: 'flex-start',
                          marginBottom: 4,
                        }}>
                        <TextM style={{color: colors.White, fontSize: 12}}>
                          {data?.category?.name}
                        </TextM>
                      </View>
                      <Box>
                        <TextM numberOfLines={2} fontWeight="bold">
                          {data?.title}
                        </TextM>
                      </Box>
                      <HStack alignItems={'center'} space={1}>
                        <MaterialCommunityIcons
                          name={'calendar'}
                          style={{color: colors.Light, fontSize: 16}}
                        />
                        <TextM style={{color: colors.LightBlack, fontSize: 14}}>
                          {moment(data?.date).format('DD/MM/YYYY')}
                        </TextM>
                      </HStack>
                    </VStack>
                    <Box />
                  </HStack>
                </TouchableOpacity>
              );
            })}
          </Box>
        )}
      </ScrollView>
    </Box>
  );
};

export default News;

const styles = StyleSheet.create({
  container: {},
  wrapper: {},
  slide: {
    alignItems: 'center',
  },
});
