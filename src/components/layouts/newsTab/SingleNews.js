import React, {useEffect, useState} from 'react';
import {StyleSheet, TouchableOpacity, useWindowDimensions} from 'react-native';
import {Box, ScrollView, View} from 'native-base';
import FastImage from 'react-native-fast-image';
import RenderHtml, {
  HTMLElementModel,
  HTMLContentModel,
  defaultSystemFonts,
} from 'react-native-render-html';
import moment from 'moment';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// Component
import Loading from '../../shareComponents/Loading';
import TextM from '../../shareComponents/TextM';
// Custom Style
import {defaultStyle, colors} from '../../shareStyle';

const SingleNews = ({navigation, route}) => {
  const {width} = useWindowDimensions();
  const {news} = route.params;
  // useState
  const [state, setState] = useState(null);

  const imageWidth = 950; // Replace with the actual width of the image
  const imageHeight = 500; // Replace with the actual height of the image
  const aspectRatio = imageWidth / imageHeight;

  // useEffect
  useEffect(() => {
    // console.log('useEffect SingleNews', news);
  }, []);

  const MyCustomUlRenderer = props => {
    return (
      <View
        {...props}
        style={{listStyleType: 'emoji', listStyle: '🔔', paddingLeft: 20}}
      />
    );
  };

  const customHTMLElementModels = {
    p: HTMLElementModel.fromCustomModel({
      tagName: 'p',
      mixedUAStyles: {
        fontFamily: 'NotoSansLao-Regular',
      },
      contentModel: HTMLContentModel.block,
    }),
    body: HTMLElementModel.fromCustomModel({
      tagName: 'body',
      mixedUAStyles: {
        fontFamily: 'NotoSansLao-Regular',
        marginTop: 10,
        paddingBottom: 30,
      },
      contentModel: HTMLContentModel.block,
    }),
  };

  const systemFonts = [...defaultSystemFonts, 'NotoSansLao-Regular'];

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <ScrollView style={{...defaultStyle.fixedLayout}}>
        <FastImage
          style={{width: '100%', aspectRatio, borderRadius: 8}}
          resizeMode="cover"
          source={{uri: news?.image}}
        />
        <View
          style={{
            backgroundColor: colors.Orange,
            paddingHorizontal: 10,
            borderRadius: 6,
            alignSelf: 'flex-start',
            marginTop: 10,
          }}>
          <TextM style={{color: colors.White}}>{news?.category?.name}</TextM>
        </View>
        <TextM
          fontWeight="bold"
          style={{
            fontSize: 22,
            marginVertical: 5,
            color: colors.DarkBlue,
          }}>
          {news.title}
        </TextM>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <MaterialCommunityIcons
            name={'calendar'}
            style={{color: colors.Light, fontSize: 16}}
          />
          <TextM style={{color: colors.LightBlack, fontSize: 14}}>
            {' '}
            {moment(news?.date).format('DD/MM/YYYY')}
          </TextM>
        </View>

        <RenderHtml
          contentWidth={width}
          source={{
            html: news?.content_render,
          }}
          tagsStyles={defaultStyle.htmlStyle}
          systemFonts={systemFonts}
          customHTMLElementModels={customHTMLElementModels}
        />
      </ScrollView>
    </Box>
  );
};

export default SingleNews;

const styles = StyleSheet.create({});
