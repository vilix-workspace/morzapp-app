import React, {useEffect, useState} from 'react';
import {StyleSheet, useWindowDimensions} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Box, ScrollView, View} from 'native-base';
import RenderHtml, {
  HTMLElementModel,
  HTMLContentModel,
  defaultSystemFonts,
} from 'react-native-render-html';
import moment from 'moment';
import axios from 'axios';
import Config from 'react-native-config';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
//Redux package
import {useSelector, useDispatch} from 'react-redux';
// Custom Style
import {defaultStyle, colors} from '../shareStyle';
import TextM from '../shareComponents/TextM';

const Policy = ({navigation}) => {
  const {width} = useWindowDimensions();
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();

  // useState
  const [policyData, setPolicyData] = useState(null);

  const getPolicyInfo = () => {
    try {
      axios({
        method: 'get',
        url: Config.WP_URL + '/wp/v2/generalinfo/?type=policy',
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(res => {
          setPolicyData(res.data[0]);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  // useEffect
  useEffect(() => {
    // console.log('useEffect HowtoUse');
    getPolicyInfo();
  }, []);

  const customHTMLElementModels = {
    p: HTMLElementModel.fromCustomModel({
      tagName: 'p',
      mixedUAStyles: {
        // backgroundColor: 'blue',
      },
      contentModel: HTMLContentModel.block,
    }),
    body: HTMLElementModel.fromCustomModel({
      tagName: 'body',
      mixedUAStyles: {
        // backgroundColor: 'blue',
        fontFamily: 'NotoSansLao-Regular',
        // marginTop: 10,
      },
      contentModel: HTMLContentModel.block,
    }),
  };

  const systemFonts = [...defaultSystemFonts, 'NotoSansLao-Regular'];

  return (
    <Box flex={1} backgroundColor={colors.White}>
      <ScrollView style={{...defaultStyle.fixedLayout}}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TextM style={{color: colors.LightBlack, fontSize: 14}}>
            ອັບເດດຂໍ້ມູນວັນທີ:{' '}
          </TextM>
          <MaterialCommunityIcons
            name={'calendar'}
            style={{color: colors.Blue, fontSize: 16}}
          />
          <TextM style={{color: colors.LightBlack, fontSize: 14}}>
            {' '}
            {policyData !== null
              ? moment(policyData?.date).format('DD/MM/YYYY')
              : 'ກຳລັງໂຫລດ...'}
          </TextM>
        </View>
        <RenderHtml
          contentWidth={width}
          source={{
            html: policyData?.information,
          }}
          tagsStyles={defaultStyle.htmlStyle}
          systemFonts={systemFonts}
          customHTMLElementModels={customHTMLElementModels}
        />
      </ScrollView>
    </Box>
  );
};

export default Policy;

const styles = StyleSheet.create({});
