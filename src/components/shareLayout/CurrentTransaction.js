import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import {Box, View, HStack, VStack} from 'native-base';
import {Modal, Tab, TabView, Icon} from '@ui-kitten/components';
import axios from 'axios';
import WooCommerce from '../../config/WooCommerce';
import moment from 'moment';
import LottieView from 'lottie-react-native';
import Config from 'react-native-config';
import StepIndicator from 'react-native-step-indicator';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

//Redux package
import {useSelector, useDispatch} from 'react-redux';
// Component
import Loading from '../shareComponents/Loading';
import TextM from '../shareComponents/TextM';
// Custom Style
import {defaultStyle, colors} from '../shareStyle';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const labelsOrder = ['ລໍຖ້າ', 'ດຳເນີນການ', 'ກວດພິເສດ', 'ສຳເລັດ'];
const labelsBook = ['ສົ່ງຄຳຮ້ອງ', 'ຢືນຢັນ & ລໍຖ້າພົບໝໍ', 'ສຳເລັດ'];
const customStylesOrder = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize: 30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: colors.Green,
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: colors.Green,
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: colors.Green,
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: colors.Green,
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: colors.Green,
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: '#999999',
  labelSize: 13,
  currentStepLabelColor: colors.Green,
  labelFontFamily: 'NotoSansLao-Regular',
};
const customStylesBook = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize: 30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: colors.Orange,
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: colors.Orange,
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: colors.Orange,
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: colors.Orange,
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: colors.Orange,
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: '#999999',
  labelSize: 13,
  currentStepLabelColor: colors.Orange,
  labelFontFamily: 'NotoSansLao-Regular',
};

const CurrentTransaction = ({navigation}) => {
  // Redux
  const reduxStore = useSelector(state => state);
  const {userData} = reduxStore;
  const dispatch = useDispatch();
  // useState
  const [ecommerceOrder, setEcommerceOrder] = useState([]);
  const [appointmentOrder, setAppointmentOrder] = useState([]);
  const [visiblePH, setVisiblePH] = useState(false);
  const [visibleBK, setVisibleBK] = useState(false);
  const [selectedDetail, setSelectedDetail] = useState(null);
  const [dataLoading, setDataLoading] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [currentPosition, setCurrentPosition] = useState(['pending']);

  let userInfo = userData?.userInfo;

  const ShoppingBasketIcon = props => (
    <Icon name="shopping-basket" {...props} pack="fontawesome5" />
  );

  const UserMDIcon = props => (
    <Icon name="user-md" {...props} pack="fontawesome5" />
  );

  // Functions
  const getCurrentOrderEcommerce = async () => {
    try {
      await WooCommerce.get('orders', {
        customer: userInfo?.user_id,
        status: 'pending,processing,on-hold',
      })
        .then(res => {
          if (res?.length > 0) {
            setEcommerceOrder(res);
          }
          setDataLoading(true);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const getCurrentBooking = async () => {
    try {
      await axios({
        method: 'get',
        url:
          Config.WP_URL +
          '/wp/v2/mzbooking/?filter[meta_query][0][key]=booking_status&filter[meta_query][0][value][0]=pending&filter[meta_query][0][value][1]=confirmed&filter[meta_query][1][key]=user_id&filter[meta_query][1][value][0]=' +
          userInfo?.user_id,
        headers: {
          Authorization: 'Bearer ' + userData?.userToken,
          'Content-Type': 'application/json',
        },
      })
        .then(res => {
          if (res?.data?.length > 0) {
            setAppointmentOrder(res.data);
          }
          setDataLoading(true);
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const numberWithCommas = x => {
    return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  const checkOrderStatusIndex = status => {
    const keyCheck = ['pending', 'processing', 'on-hold', 'completed'];

    let i = keyCheck.indexOf(status);

    return i;
  };

  const checkBookStatusIndex = status => {
    const keyCheck = ['pending', 'confirmed', 'completed'];

    let i = keyCheck.indexOf(status);

    return i;
  };

  // useEffect
  useEffect(() => {
    // console.log('useEffect CurrentTransaction', userInfo?.user_id);
    getCurrentOrderEcommerce();
    getCurrentBooking();
    return () => {
      setSelectedDetail(null);
    };
  }, []);

  // Render Layout

  return (
    <Box flex={1} backgroundColor={colors.White}>
      {dataLoading ? (
        ecommerceOrder.length > 0 || appointmentOrder.length > 0 ? (
          <TabView
            selectedIndex={selectedIndex}
            onSelect={index => {
              setSelectedIndex(index);
            }}>
            {ecommerceOrder.length > 0 && (
              <Tab
                title={() => (
                  <TextM
                    style={{
                      color: selectedIndex === 0 ? colors.Pink : '#8F9BB3',
                      fontSize: 14,
                    }}>
                    ການສັ່ງຊື້ສິນຄ້າ
                  </TextM>
                )}
                icon={ShoppingBasketIcon}
                style={{paddingVertical: 6}}>
                <ScrollView style={{...defaultStyle.fixedLayout}}>
                  <View
                    style={{
                      paddingBottom: deviceHeight / 4,
                    }}>
                    {ecommerceOrder.map((item, index) => {
                      return (
                        <View
                          key={index}
                          style={{
                            backgroundColor: colors.LightWhite,
                            padding: 10,
                            marginBottom: 5,
                            borderRadius: 10,
                          }}>
                          <HStack alignItems={'center'} space={1}>
                            <FontAwesome5
                              name="hashtag"
                              style={{fontSize: 16, color: colors.Blue}}
                            />
                            <TextM>ລະຫັດອໍເດິ້:</TextM>
                            <TextM style={{color: colors.Blue}}>
                              MZPH{item?.id}
                            </TextM>
                          </HStack>

                          <HStack alignItems={'center'} space={1}>
                            <FontAwesome5
                              name="calendar-alt"
                              style={{fontSize: 16, color: colors.Blue}}
                            />
                            <TextM>ສັ່ງຊື້ວັນທີ:</TextM>
                            <TextM style={{color: colors.Blue}}>
                              {moment(item?.date_created).format('DD MMM YYYY')}
                            </TextM>
                          </HStack>
                          <HStack space={1} justifyContent={'center'} my={1}>
                            <TextM>ສະຖານະ:</TextM>
                            <TextM style={{color: colors.Orange}}>
                              {item?.status === 'pending'
                                ? 'ກຳລັງລໍຖ້າ'
                                : item?.status === 'processing'
                                ? 'ກຳລັງກະກຽມເຄື່ອງ'
                                : 'ກຳລັງກວດກາ ຕິດຕໍ່ພະນັກງານ'}
                            </TextM>
                          </HStack>
                          <Box>
                            <StepIndicator
                              customStyles={customStylesOrder}
                              currentPosition={checkOrderStatusIndex(
                                item?.status,
                              )}
                              labels={labelsOrder}
                              stepCount={4}
                            />
                          </Box>
                          <TouchableOpacity
                            style={{
                              backgroundColor: colors.Blue,
                              alignItems: 'center',
                              paddingVertical: 4,
                              borderRadius: 6,
                              marginTop: 5,
                            }}
                            onPress={() => {
                              console.log(item);
                              setSelectedDetail(item);
                              setVisiblePH(true);
                            }}>
                            <TextM style={{color: colors.White}}>
                              ເບິ່ງລາຍລະອຽດ
                            </TextM>
                          </TouchableOpacity>
                        </View>
                      );
                    })}
                  </View>
                </ScrollView>
              </Tab>
            )}

            {/* Booking */}
            {appointmentOrder?.length > 0 && (
              <Tab
                title={() => (
                  <TextM
                    style={{
                      color: selectedIndex === 1 ? colors.Pink : '#8F9BB3',
                      fontSize: 14,
                    }}>
                    ການນັດພົບໝໍ
                  </TextM>
                )}
                icon={UserMDIcon}
                style={{paddingVertical: 6}}>
                <ScrollView style={{...defaultStyle.fixedLayout}}>
                  <View
                    style={{
                      paddingBottom: deviceHeight / 4,
                    }}>
                    {appointmentOrder.map((item, index) => {
                      return (
                        <View
                          key={index}
                          style={{
                            backgroundColor: colors.LightWhite,
                            padding: 10,
                            marginBottom: 5,
                            borderRadius: 10,
                          }}>
                          <HStack alignItems={'center'} space={1}>
                            <FontAwesome5
                              name="hashtag"
                              style={{fontSize: 16, color: colors.Orange}}
                            />
                            <TextM>ລະຫັດຈອງ:</TextM>
                            <TextM style={{color: colors.Orange}}>
                              MZAP{item?.id}
                            </TextM>
                          </HStack>

                          <HStack alignItems={'center'} space={1}>
                            <FontAwesome5
                              name="calendar-alt"
                              style={{fontSize: 16, color: colors.Orange}}
                            />
                            <TextM>ນັດພົບວັນທີ:</TextM>
                            <TextM style={{color: colors.Orange}}>
                              {moment(
                                item?.booking_time,
                                'DD-MM-YYYY HH:mm:ss',
                              ).format('DD MMM YYYY')}
                            </TextM>
                          </HStack>
                          <HStack alignItems={'center'} space={1}>
                            <FontAwesome5
                              name="clock"
                              style={{fontSize: 16, color: colors.Orange}}
                            />
                            <TextM>ເວລາ:</TextM>
                            <TextM style={{color: colors.Orange}}>
                              {moment(
                                item?.booking_time,
                                'DD-MM-YYYY HH:mm:ss',
                              ).format('h:mm a')}
                            </TextM>
                          </HStack>
                          <HStack space={1} justifyContent={'center'} my={1}>
                            <TextM>ສະຖານະ:</TextM>
                            <TextM style={{color: colors.Blue}}>
                              {item?.booking_status === 'pending'
                                ? 'ກຳລັງລໍຖ້າ'
                                : item?.booking_status === 'confirmed'
                                ? 'ຢືນຢັນ ແລະ ລໍຖ້າພົບໝໍ'
                                : 'ຕິດຕໍ່ພະນັກງານ'}
                            </TextM>
                          </HStack>

                          <Box>
                            <StepIndicator
                              customStyles={customStylesBook}
                              currentPosition={checkBookStatusIndex(
                                item?.booking_status,
                              )}
                              labels={labelsBook}
                              stepCount={3}
                            />
                          </Box>
                          <TouchableOpacity
                            style={{
                              backgroundColor: colors.Orange,
                              alignItems: 'center',
                              paddingVertical: 4,
                              borderRadius: 6,
                              marginTop: 5,
                            }}
                            onPress={() => {
                              console.log(item);
                              setSelectedDetail(item);
                              setVisibleBK(true);
                            }}>
                            <TextM style={{color: colors.White}}>
                              ເບິ່ງລາຍລະອຽດ
                            </TextM>
                          </TouchableOpacity>
                        </View>
                      );
                    })}
                  </View>
                </ScrollView>
              </Tab>
            )}
          </TabView>
        ) : (
          <Box
            flex={1}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Box
              style={{
                height: 180,
                width: 200,
                marginTop: -80,
              }}>
              <LottieView
                source={require('../../assets/animation/no-result-found.json')}
                autoPlay
                loop
                resizeMode="cover"
              />
            </Box>
            <TextM>ບໍ່ພົບຂໍ້ມູນຂອງທ່ານ</TextM>
          </Box>
        )
      ) : (
        <Box flex={1} style={{alignItems: 'center'}} justifyContent={'center'}>
          <Box
            style={{
              height: 100,
              width: 200,
              marginTop: -80,
            }}>
            <LottieView
              source={require('../../assets/animation/loading-dot.json')}
              autoPlay
              loop
              resizeMode="cover"
            />
          </Box>
          <TextM>ກຳລັງໂຫລດ...</TextM>
        </Box>
      )}

      {/* Modal Order */}
      <Modal
        visible={visiblePH}
        backdropStyle={styles.backdrop}
        onBackdropPress={() => setVisiblePH(false)}>
        <Box
          style={{
            backgroundColor: colors.White,
            padding: 10,
            borderRadius: 10,
            width: deviceWidth - 50,
          }}>
          <VStack space={1} px={2}>
            <HStack space={1} style={{justifyContent: 'space-between'}}>
              <HStack space={1} alignItems={'center'}>
                <FontAwesome5
                  name="hashtag"
                  style={{fontSize: 16, color: colors.Blue, width: 20}}
                />
                <TextM>ເລກອໍເດິ້:</TextM>
              </HStack>
              <View>
                <TextM>MZPH{selectedDetail?.id}</TextM>
              </View>
            </HStack>
            <Box style={{justifyContent: 'space-between'}}>
              <HStack space={1} alignItems={'center'}>
                <FontAwesome5
                  name="cubes"
                  style={{fontSize: 16, color: colors.Blue, width: 20}}
                />
                <TextM>ສິນຄ້າ:</TextM>
              </HStack>
              <View>
                {selectedDetail?.line_items?.map((data, i) => {
                  return (
                    <HStack key={i} alignItems={'center'}>
                      <Box w={'5%'}>
                        <TextM>{i + 1}.</TextM>
                      </Box>
                      <Box w={'35%'} pl={'2px'}>
                        <TextM numberOfLines={1}>{data?.name}</TextM>
                      </Box>
                      <Box w={'25%'}>
                        <TextM style={{fontSize: 12}}>
                          {data?.quantity} x {numberWithCommas(data?.price)}
                        </TextM>
                      </Box>
                      <Box
                        w={'35%'}
                        alignSelf={'flex-end'}
                        alignItems={'flex-end'}>
                        <TextM>
                          {numberWithCommas(data?.quantity * data?.price)}
                        </TextM>
                      </Box>
                    </HStack>
                  );
                })}
              </View>
            </Box>
            <HStack space={1} style={{justifyContent: 'space-between'}}>
              <HStack space={1} alignItems={'center'}>
                <FontAwesome5
                  name="spinner"
                  style={{fontSize: 16, color: colors.Blue, width: 20}}
                />
                <TextM>ສະຖານະ:</TextM>
              </HStack>
              <View>
                <TextM style={{color: colors.Blue}}>
                  {selectedDetail?.status === 'pending'
                    ? 'ກຳລັງລໍຖ້າ'
                    : 'ກຳລັງກະກຽມເຄື່ອງ'}
                </TextM>
              </View>
            </HStack>
            <HStack space={1} style={{justifyContent: 'space-between'}}>
              <HStack space={1} alignItems={'center'}>
                <FontAwesome5
                  name="user-tag"
                  style={{fontSize: 16, color: colors.Blue, width: 20}}
                />
                <TextM>ຜູ້ສັ່ງເຄຶ່ອງ:</TextM>
              </HStack>
              <TextM>{selectedDetail?.billing?.first_name}</TextM>
            </HStack>
            <HStack space={1} style={{justifyContent: 'space-between'}}>
              <HStack space={1} alignItems={'center'}>
                <FontAwesome5
                  name="map-marked-alt"
                  style={{
                    fontSize: 16,
                    color: colors.Blue,
                    width: 20,
                  }}
                />
                <TextM>ສະຖານທີ່:</TextM>
              </HStack>
              <TextM>{selectedDetail?.shipping?.address_1}</TextM>
            </HStack>
            <HStack space={1} style={{justifyContent: 'space-between'}}>
              <HStack space={1} alignItems={'center'}>
                <FontAwesome5
                  name="calendar-alt"
                  style={{fontSize: 16, color: colors.Blue, width: 20}}
                />
                <TextM>ວັນທີ:</TextM>
              </HStack>
              <TextM>
                {moment(selectedDetail?.date_created).format('DD MMM YYYY ')}
              </TextM>
            </HStack>
            <HStack space={1} style={{justifyContent: 'space-between'}}>
              <HStack space={1} alignItems={'center'}>
                <FontAwesome5
                  name="money-check-alt"
                  style={{fontSize: 16, color: colors.Blue, width: 20}}
                />
                <TextM>ລວມທັງໝົດ:</TextM>
              </HStack>

              <TextM fontWeight="bold" style={{color: colors.Blue}}>
                {numberWithCommas(selectedDetail?.total)} ກີບ
              </TextM>
            </HStack>
          </VStack>
          <TouchableOpacity
            style={{
              backgroundColor: colors.Light,
              paddingVertical: 5,
              paddingHorizontal: 10,
              borderRadius: 5,
              marginTop: 10,
              alignItems: 'center',
            }}
            onPress={() => setVisiblePH(false)}>
            <TextM>ປິດອອກ</TextM>
          </TouchableOpacity>
        </Box>
      </Modal>

      {/* Modal Booking */}
      <Modal
        visible={visibleBK}
        backdropStyle={styles.backdrop}
        onBackdropPress={() => setVisibleBK(false)}>
        <View
          style={{
            backgroundColor: colors.White,
            padding: 10,
            borderRadius: 10,
            width: deviceWidth - 50,
          }}>
          <VStack space={1} px={2}>
            <HStack space={1} style={{justifyContent: 'space-between'}}>
              <HStack space={1} alignItems={'center'}>
                <FontAwesome5
                  name="hashtag"
                  style={{fontSize: 16, color: colors.Orange, width: 20}}
                />
                <TextM>ເລກລະຫັດນັດຈອງ:</TextM>
              </HStack>
              <View>
                <TextM>MZAP{selectedDetail?.id}</TextM>
              </View>
            </HStack>
            <HStack space={1} style={{justifyContent: 'space-between'}}>
              <HStack space={1} alignItems={'center'}>
                <FontAwesome5
                  name="map-marked-alt"
                  style={{
                    fontSize: 16,
                    color: colors.Orange,
                    width: 20,
                  }}
                />
                <TextM>ສະຖານທີ່:</TextM>
              </HStack>
              <TextM>{selectedDetail?.hospital_name}</TextM>
            </HStack>
            <HStack space={1} style={{justifyContent: 'space-between'}}>
              <HStack space={1} alignItems={'center'}>
                <FontAwesome5
                  name="user"
                  style={{
                    fontSize: 16,
                    color: colors.Orange,
                    width: 20,
                  }}
                />
                <TextM>ຜູ້ຕິດຕໍ່:</TextM>
              </HStack>
              <TextM>{selectedDetail?.contact_name}</TextM>
            </HStack>
            <HStack space={1} style={{justifyContent: 'space-between'}}>
              <HStack space={1} alignItems={'center'}>
                <FontAwesome5
                  name="user-injured"
                  style={{
                    fontSize: 16,
                    color: colors.Orange,
                    width: 20,
                  }}
                />
                <TextM>ຊື່ຄົນເຈັບ:</TextM>
              </HStack>
              <TextM>{selectedDetail?.patient_name}</TextM>
            </HStack>
            <HStack space={1} style={{justifyContent: 'space-between'}}>
              <HStack space={1} alignItems={'center'}>
                <FontAwesome5
                  name="notes-medical"
                  style={{
                    fontSize: 16,
                    color: colors.Orange,
                    width: 20,
                  }}
                />
                <TextM>ອາການ:</TextM>
              </HStack>
              <TextM>{selectedDetail?.symptom}</TextM>
            </HStack>
            <HStack space={1} style={{justifyContent: 'space-between'}}>
              <HStack space={1} alignItems={'center'}>
                <FontAwesome5
                  name="receipt"
                  style={{
                    fontSize: 16,
                    color: colors.Orange,
                    width: 20,
                  }}
                />
                <TextM>ເຫດຜົນໃນການນັດ:</TextM>
              </HStack>
              <TextM>{selectedDetail?.reason}</TextM>
            </HStack>

            <HStack space={1} style={{justifyContent: 'space-between'}}>
              <HStack space={1} alignItems={'center'}>
                <FontAwesome5
                  name="spinner"
                  style={{fontSize: 16, color: colors.Orange, width: 20}}
                />
                <TextM>ສະຖານະ:</TextM>
              </HStack>
              <View>
                <TextM style={{color: colors.Orange}}>
                  {selectedDetail?.booking_status === 'pending'
                    ? 'ກຳລັງລໍຖ້າ'
                    : 'ຢືນຢັນການນັດພົບ'}
                </TextM>
              </View>
            </HStack>

            <HStack space={1} style={{justifyContent: 'space-between'}}>
              <HStack space={1} alignItems={'center'}>
                <FontAwesome5
                  name="calendar-alt"
                  style={{fontSize: 16, color: colors.Orange, width: 20}}
                />
                <TextM>ວັນທີ:</TextM>
              </HStack>
              <TextM>
                {moment(
                  selectedDetail?.booking_time,
                  'DD-MM-YYYY HH:mm:ss',
                ).format('DD MMM YYYY')}
              </TextM>
            </HStack>
            <HStack space={1} style={{justifyContent: 'space-between'}}>
              <HStack space={1} alignItems={'center'}>
                <FontAwesome5
                  name="clock"
                  style={{fontSize: 16, color: colors.Orange, width: 20}}
                />
                <TextM>ເວລາ:</TextM>
              </HStack>
              <TextM>
                {moment(
                  selectedDetail?.booking_time,
                  'DD-MM-YYYY HH:mm:ss',
                ).format('h:mm a')}
              </TextM>
            </HStack>
          </VStack>
          <TouchableOpacity
            style={{
              backgroundColor: colors.Light,
              paddingVertical: 5,
              paddingHorizontal: 10,
              borderRadius: 5,
              marginTop: 10,
              alignItems: 'center',
            }}
            onPress={() => setVisibleBK(false)}>
            <TextM>ປິດອອກ</TextM>
          </TouchableOpacity>
        </View>
      </Modal>
    </Box>
  );
};

export default CurrentTransaction;

const styles = StyleSheet.create({
  tabContainer: {
    // height: windowHeight,
    // alignItems: 'center',
    // justifyContent: 'center',
    backgroundColor: '#1d1',
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
});
