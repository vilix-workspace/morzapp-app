import {LightBlack, DarkBlue, Black} from './colors';
import {Platform} from 'react-native';

export const fixedLayout = {
  paddingHorizontal: 20,
  paddingTop: 10,
};

export const basicTitle = {
  fontSize: 18,
  paddingTop: 10,
};

export const textParagraph = {
  fontSize: 16,
  color: '#aaaaaa',
  paddingVertical: 6,
};

export const htmlStyle = {
  body: {
    paddingBottom: 20,
  },
  p: {
    color: LightBlack,
    fontSize: Platform.OS === 'android' ? 16 : 18,
    lineHeight: 28,
    marginTop: 0,
    marginBottom: 10,
  },
  li: {
    color: LightBlack,
    fontSize: Platform.OS === 'android' ? 16 : 18,
  },
  ol: {
    lineHeight: 28,
    marginTop: 0,
    marginBottom: 10,
    color: LightBlack,
  },
  ul: {
    lineHeight: 28,
    marginTop: 0,
    marginBottom: 10,
    color: LightBlack,
    listStyleType: 'emoji',
    listStyle: '🔔',
  },
  h1: {
    color: DarkBlue,
    fontWeight: Platform.OS === 'ios' ? null : 'normal',
  },
  h2: {
    marginTop: 0,
    marginBottom: 5,
    color: DarkBlue,
    fontWeight: Platform.OS === 'ios' ? null : 'normal',
  },
  h3: {
    marginTop: 0,
    marginBottom: 5,
    color: DarkBlue,
    fontWeight: Platform.OS === 'ios' ? null : 'normal',
  },
  h4: {
    marginTop: 0,
    marginBottom: 0,
    color: DarkBlue,
    fontWeight: Platform.OS === 'ios' ? null : 'normal',
  },
  h5: {
    marginTop: 0,
    marginBottom: 10,
    color: DarkBlue,
    fontWeight: Platform.OS === 'ios' ? null : 'normal',
  },
};
