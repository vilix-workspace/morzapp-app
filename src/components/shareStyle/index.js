import * as defaultStyle from './defaultStyle';
import * as colors from './colors';

export {defaultStyle, colors};
